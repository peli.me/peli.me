from itsdangerous import URLSafeSerializer, URLSafeTimedSerializer

from app import app

def generate_email_token(email):
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    return serializer.dumps(email, salt='email-confirm')

def confirm_email_token(token, expiration=3600): #1 hour
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    try:
        email  = serializer.loads(token, salt='email-confirm', max_age=expiration)
    except:
        return False
    return email

def generate_status_token(username, state):
    serializer = URLSafeSerializer(app.config['SECRET_KEY'])
    status = {'username': username, 'state': state}
    return serializer.dumps(status, salt='status-confirm')

def confirm_status_token(token):
    serializer = URLSafeSerializer(app.config['SECRET_KEY'])
    try:
        status = serializer.loads(token, salt='status-confirm')
    except:
        return False
    return status

def generate_dcma_token(dcma_id):
    serializer = URLSafeSerializer(app.config['SECRET_KEY'])
    return serializer.dumps(str(dcma_id), salt='dcma')

def confirm_dcma_token(token):
    serializer = URLSafeSerializer(app.config['SECRET_KEY'])
    try:
        dcma_id  = serializer.loads(token, salt='dcma')
    except:
        return False
    return dcma_id
