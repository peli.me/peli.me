/*! jQuery v3.1.1 | (c) jQuery Foundation | jquery.org/license */ ! function(a, b) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function(a) {
        if (!a.document) throw new Error("jQuery requires a window with a document");
        return b(a)
    } : b(a)
}("undefined" != typeof window ? window : this, function(a, b) {
    "use strict";
    var c = [],
        d = a.document,
        e = Object.getPrototypeOf,
        f = c.slice,
        g = c.concat,
        h = c.push,
        i = c.indexOf,
        j = {},
        k = j.toString,
        l = j.hasOwnProperty,
        m = l.toString,
        n = m.call(Object),
        o = {};

    function p(a, b) {
        b = b || d;
        var c = b.createElement("script");
        c.text = a, b.head.appendChild(c).parentNode.removeChild(c)
    }
    var q = "3.1.1",
        r = function(a, b) {
            return new r.fn.init(a, b)
        },
        s = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        t = /^-ms-/,
        u = /-([a-z])/g,
        v = function(a, b) {
            return b.toUpperCase()
        };
    r.fn = r.prototype = {
        jquery: q,
        constructor: r,
        length: 0,
        toArray: function() {
            return f.call(this)
        },
        get: function(a) {
            return null == a ? f.call(this) : a < 0 ? this[a + this.length] : this[a]
        },
        pushStack: function(a) {
            var b = r.merge(this.constructor(), a);
            return b.prevObject = this, b
        },
        each: function(a) {
            return r.each(this, a)
        },
        map: function(a) {
            return this.pushStack(r.map(this, function(b, c) {
                return a.call(b, c, b)
            }))
        },
        slice: function() {
            return this.pushStack(f.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(a) {
            var b = this.length,
                c = +a + (a < 0 ? b : 0);
            return this.pushStack(c >= 0 && c < b ? [this[c]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor()
        },
        push: h,
        sort: c.sort,
        splice: c.splice
    }, r.extend = r.fn.extend = function() {
        var a, b, c, d, e, f, g = arguments[0] || {},
            h = 1,
            i = arguments.length,
            j = !1;
        for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == typeof g || r.isFunction(g) || (g = {}), h === i && (g = this, h--); h < i; h++)
            if (null != (a = arguments[h]))
                for (b in a) c = g[b], d = a[b], g !== d && (j && d && (r.isPlainObject(d) || (e = r.isArray(d))) ? (e ? (e = !1, f = c && r.isArray(c) ? c : []) : f = c && r.isPlainObject(c) ? c : {}, g[b] = r.extend(j, f, d)) : void 0 !== d && (g[b] = d));
        return g
    }, r.extend({
        expando: "jQuery" + (q + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(a) {
            throw new Error(a)
        },
        noop: function() {},
        isFunction: function(a) {
            return "function" === r.type(a)
        },
        isArray: Array.isArray,
        isWindow: function(a) {
            return null != a && a === a.window
        },
        isNumeric: function(a) {
            var b = r.type(a);
            return ("number" === b || "string" === b) && !isNaN(a - parseFloat(a))
        },
        isPlainObject: function(a) {
            var b, c;
            return !(!a || "[object Object]" !== k.call(a)) && (!(b = e(a)) || (c = l.call(b, "constructor") && b.constructor, "function" == typeof c && m.call(c) === n))
        },
        isEmptyObject: function(a) {
            var b;
            for (b in a) return !1;
            return !0
        },
        type: function(a) {
            return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? j[k.call(a)] || "object" : typeof a
        },
        globalEval: function(a) {
            p(a)
        },
        camelCase: function(a) {
            return a.replace(t, "ms-").replace(u, v)
        },
        nodeName: function(a, b) {
            return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase()
        },
        each: function(a, b) {
            var c, d = 0;
            if (w(a)) {
                for (c = a.length; d < c; d++)
                    if (b.call(a[d], d, a[d]) === !1) break
            } else
                for (d in a)
                    if (b.call(a[d], d, a[d]) === !1) break; return a
        },
        trim: function(a) {
            return null == a ? "" : (a + "").replace(s, "")
        },
        makeArray: function(a, b) {
            var c = b || [];
            return null != a && (w(Object(a)) ? r.merge(c, "string" == typeof a ? [a] : a) : h.call(c, a)), c
        },
        inArray: function(a, b, c) {
            return null == b ? -1 : i.call(b, a, c)
        },
        merge: function(a, b) {
            for (var c = +b.length, d = 0, e = a.length; d < c; d++) a[e++] = b[d];
            return a.length = e, a
        },
        grep: function(a, b, c) {
            for (var d, e = [], f = 0, g = a.length, h = !c; f < g; f++) d = !b(a[f], f), d !== h && e.push(a[f]);
            return e
        },
        map: function(a, b, c) {
            var d, e, f = 0,
                h = [];
            if (w(a))
                for (d = a.length; f < d; f++) e = b(a[f], f, c), null != e && h.push(e);
            else
                for (f in a) e = b(a[f], f, c), null != e && h.push(e);
            return g.apply([], h)
        },
        guid: 1,
        proxy: function(a, b) {
            var c, d, e;
            if ("string" == typeof b && (c = a[b], b = a, a = c), r.isFunction(a)) return d = f.call(arguments, 2), e = function() {
                return a.apply(b || this, d.concat(f.call(arguments)))
            }, e.guid = a.guid = a.guid || r.guid++, e
        },
        now: Date.now,
        support: o
    }), "function" == typeof Symbol && (r.fn[Symbol.iterator] = c[Symbol.iterator]), r.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(a, b) {
        j["[object " + b + "]"] = b.toLowerCase()
    });

    function w(a) {
        var b = !!a && "length" in a && a.length,
            c = r.type(a);
        return "function" !== c && !r.isWindow(a) && ("array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a)
    }
    var x = function(a) {
        var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u = "sizzle" + 1 * new Date,
            v = a.document,
            w = 0,
            x = 0,
            y = ha(),
            z = ha(),
            A = ha(),
            B = function(a, b) {
                return a === b && (l = !0), 0
            },
            C = {}.hasOwnProperty,
            D = [],
            E = D.pop,
            F = D.push,
            G = D.push,
            H = D.slice,
            I = function(a, b) {
                for (var c = 0, d = a.length; c < d; c++)
                    if (a[c] === b) return c;
                return -1
            },
            J = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            K = "[\\x20\\t\\r\\n\\f]",
            L = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            M = "\\[" + K + "*(" + L + ")(?:" + K + "*([*^$|!~]?=)" + K + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + L + "))|)" + K + "*\\]",
            N = ":(" + L + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + M + ")*)|.*)\\)|)",
            O = new RegExp(K + "+", "g"),
            P = new RegExp("^" + K + "+|((?:^|[^\\\\])(?:\\\\.)*)" + K + "+$", "g"),
            Q = new RegExp("^" + K + "*," + K + "*"),
            R = new RegExp("^" + K + "*([>+~]|" + K + ")" + K + "*"),
            S = new RegExp("=" + K + "*([^\\]'\"]*?)" + K + "*\\]", "g"),
            T = new RegExp(N),
            U = new RegExp("^" + L + "$"),
            V = {
                ID: new RegExp("^#(" + L + ")"),
                CLASS: new RegExp("^\\.(" + L + ")"),
                TAG: new RegExp("^(" + L + "|[*])"),
                ATTR: new RegExp("^" + M),
                PSEUDO: new RegExp("^" + N),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + K + "*(even|odd|(([+-]|)(\\d*)n|)" + K + "*(?:([+-]|)" + K + "*(\\d+)|))" + K + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + J + ")$", "i"),
                needsContext: new RegExp("^" + K + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + K + "*((?:-\\d)?\\d*)" + K + "*\\)|)(?=[^-]|$)", "i")
            },
            W = /^(?:input|select|textarea|button)$/i,
            X = /^h\d$/i,
            Y = /^[^{]+\{\s*\[native \w/,
            Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            $ = /[+~]/,
            _ = new RegExp("\\\\([\\da-f]{1,6}" + K + "?|(" + K + ")|.)", "ig"),
            aa = function(a, b, c) {
                var d = "0x" + b - 65536;
                return d !== d || c ? b : d < 0 ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320)
            },
            ba = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
            ca = function(a, b) {
                return b ? "\0" === a ? "\ufffd" : a.slice(0, -1) + "\\" + a.charCodeAt(a.length - 1).toString(16) + " " : "\\" + a
            },
            da = function() {
                m()
            },
            ea = ta(function(a) {
                return a.disabled === !0 && ("form" in a || "label" in a)
            }, {
                dir: "parentNode",
                next: "legend"
            });
        try {
            G.apply(D = H.call(v.childNodes), v.childNodes), D[v.childNodes.length].nodeType
        } catch (fa) {
            G = {
                apply: D.length ? function(a, b) {
                    F.apply(a, H.call(b))
                } : function(a, b) {
                    var c = a.length,
                        d = 0;
                    while (a[c++] = b[d++]);
                    a.length = c - 1
                }
            }
        }

        function ga(a, b, d, e) {
            var f, h, j, k, l, o, r, s = b && b.ownerDocument,
                w = b ? b.nodeType : 9;
            if (d = d || [], "string" != typeof a || !a || 1 !== w && 9 !== w && 11 !== w) return d;
            if (!e && ((b ? b.ownerDocument || b : v) !== n && m(b), b = b || n, p)) {
                if (11 !== w && (l = Z.exec(a)))
                    if (f = l[1]) {
                        if (9 === w) {
                            if (!(j = b.getElementById(f))) return d;
                            if (j.id === f) return d.push(j), d
                        } else if (s && (j = s.getElementById(f)) && t(b, j) && j.id === f) return d.push(j), d
                    } else {
                        if (l[2]) return G.apply(d, b.getElementsByTagName(a)), d;
                        if ((f = l[3]) && c.getElementsByClassName && b.getElementsByClassName) return G.apply(d, b.getElementsByClassName(f)), d
                    }
                if (c.qsa && !A[a + " "] && (!q || !q.test(a))) {
                    if (1 !== w) s = b, r = a;
                    else if ("object" !== b.nodeName.toLowerCase()) {
                        (k = b.getAttribute("id")) ? k = k.replace(ba, ca): b.setAttribute("id", k = u), o = g(a), h = o.length;
                        while (h--) o[h] = "#" + k + " " + sa(o[h]);
                        r = o.join(","), s = $.test(a) && qa(b.parentNode) || b
                    }
                    if (r) try {
                        return G.apply(d, s.querySelectorAll(r)), d
                    } catch (x) {} finally {
                        k === u && b.removeAttribute("id")
                    }
                }
            }
            return i(a.replace(P, "$1"), b, d, e)
        }

        function ha() {
            var a = [];

            function b(c, e) {
                return a.push(c + " ") > d.cacheLength && delete b[a.shift()], b[c + " "] = e
            }
            return b
        }

        function ia(a) {
            return a[u] = !0, a
        }

        function ja(a) {
            var b = n.createElement("fieldset");
            try {
                return !!a(b)
            } catch (c) {
                return !1
            } finally {
                b.parentNode && b.parentNode.removeChild(b), b = null
            }
        }

        function ka(a, b) {
            var c = a.split("|"),
                e = c.length;
            while (e--) d.attrHandle[c[e]] = b
        }

        function la(a, b) {
            var c = b && a,
                d = c && 1 === a.nodeType && 1 === b.nodeType && a.sourceIndex - b.sourceIndex;
            if (d) return d;
            if (c)
                while (c = c.nextSibling)
                    if (c === b) return -1;
            return a ? 1 : -1
        }

        function ma(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return "input" === c && b.type === a
            }
        }

        function na(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return ("input" === c || "button" === c) && b.type === a
            }
        }

        function oa(a) {
            return function(b) {
                return "form" in b ? b.parentNode && b.disabled === !1 ? "label" in b ? "label" in b.parentNode ? b.parentNode.disabled === a : b.disabled === a : b.isDisabled === a || b.isDisabled !== !a && ea(b) === a : b.disabled === a : "label" in b && b.disabled === a
            }
        }

        function pa(a) {
            return ia(function(b) {
                return b = +b, ia(function(c, d) {
                    var e, f = a([], c.length, b),
                        g = f.length;
                    while (g--) c[e = f[g]] && (c[e] = !(d[e] = c[e]))
                })
            })
        }

        function qa(a) {
            return a && "undefined" != typeof a.getElementsByTagName && a
        }
        c = ga.support = {}, f = ga.isXML = function(a) {
            var b = a && (a.ownerDocument || a).documentElement;
            return !!b && "HTML" !== b.nodeName
        }, m = ga.setDocument = function(a) {
            var b, e, g = a ? a.ownerDocument || a : v;
            return g !== n && 9 === g.nodeType && g.documentElement ? (n = g, o = n.documentElement, p = !f(n), v !== n && (e = n.defaultView) && e.top !== e && (e.addEventListener ? e.addEventListener("unload", da, !1) : e.attachEvent && e.attachEvent("onunload", da)), c.attributes = ja(function(a) {
                return a.className = "i", !a.getAttribute("className")
            }), c.getElementsByTagName = ja(function(a) {
                return a.appendChild(n.createComment("")), !a.getElementsByTagName("*").length
            }), c.getElementsByClassName = Y.test(n.getElementsByClassName), c.getById = ja(function(a) {
                return o.appendChild(a).id = u, !n.getElementsByName || !n.getElementsByName(u).length
            }), c.getById ? (d.filter.ID = function(a) {
                var b = a.replace(_, aa);
                return function(a) {
                    return a.getAttribute("id") === b
                }
            }, d.find.ID = function(a, b) {
                if ("undefined" != typeof b.getElementById && p) {
                    var c = b.getElementById(a);
                    return c ? [c] : []
                }
            }) : (d.filter.ID = function(a) {
                var b = a.replace(_, aa);
                return function(a) {
                    var c = "undefined" != typeof a.getAttributeNode && a.getAttributeNode("id");
                    return c && c.value === b
                }
            }, d.find.ID = function(a, b) {
                if ("undefined" != typeof b.getElementById && p) {
                    var c, d, e, f = b.getElementById(a);
                    if (f) {
                        if (c = f.getAttributeNode("id"), c && c.value === a) return [f];
                        e = b.getElementsByName(a), d = 0;
                        while (f = e[d++])
                            if (c = f.getAttributeNode("id"), c && c.value === a) return [f]
                    }
                    return []
                }
            }), d.find.TAG = c.getElementsByTagName ? function(a, b) {
                return "undefined" != typeof b.getElementsByTagName ? b.getElementsByTagName(a) : c.qsa ? b.querySelectorAll(a) : void 0
            } : function(a, b) {
                var c, d = [],
                    e = 0,
                    f = b.getElementsByTagName(a);
                if ("*" === a) {
                    while (c = f[e++]) 1 === c.nodeType && d.push(c);
                    return d
                }
                return f
            }, d.find.CLASS = c.getElementsByClassName && function(a, b) {
                if ("undefined" != typeof b.getElementsByClassName && p) return b.getElementsByClassName(a)
            }, r = [], q = [], (c.qsa = Y.test(n.querySelectorAll)) && (ja(function(a) {
                o.appendChild(a).innerHTML = "<a id='" + u + "'></a><select id='" + u + "-\r\\' msallowcapture=''><option selected=''></option></select>", a.querySelectorAll("[msallowcapture^='']").length && q.push("[*^$]=" + K + "*(?:''|\"\")"), a.querySelectorAll("[selected]").length || q.push("\\[" + K + "*(?:value|" + J + ")"), a.querySelectorAll("[id~=" + u + "-]").length || q.push("~="), a.querySelectorAll(":checked").length || q.push(":checked"), a.querySelectorAll("a#" + u + "+*").length || q.push(".#.+[+~]")
            }), ja(function(a) {
                a.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var b = n.createElement("input");
                b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && q.push("name" + K + "*[*^$|!~]?="), 2 !== a.querySelectorAll(":enabled").length && q.push(":enabled", ":disabled"), o.appendChild(a).disabled = !0, 2 !== a.querySelectorAll(":disabled").length && q.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), q.push(",.*:")
            })), (c.matchesSelector = Y.test(s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector)) && ja(function(a) {
                c.disconnectedMatch = s.call(a, "*"), s.call(a, "[s!='']:x"), r.push("!=", N)
            }), q = q.length && new RegExp(q.join("|")), r = r.length && new RegExp(r.join("|")), b = Y.test(o.compareDocumentPosition), t = b || Y.test(o.contains) ? function(a, b) {
                var c = 9 === a.nodeType ? a.documentElement : a,
                    d = b && b.parentNode;
                return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)))
            } : function(a, b) {
                if (b)
                    while (b = b.parentNode)
                        if (b === a) return !0;
                return !1
            }, B = b ? function(a, b) {
                if (a === b) return l = !0, 0;
                var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
                return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & d || !c.sortDetached && b.compareDocumentPosition(a) === d ? a === n || a.ownerDocument === v && t(v, a) ? -1 : b === n || b.ownerDocument === v && t(v, b) ? 1 : k ? I(k, a) - I(k, b) : 0 : 4 & d ? -1 : 1)
            } : function(a, b) {
                if (a === b) return l = !0, 0;
                var c, d = 0,
                    e = a.parentNode,
                    f = b.parentNode,
                    g = [a],
                    h = [b];
                if (!e || !f) return a === n ? -1 : b === n ? 1 : e ? -1 : f ? 1 : k ? I(k, a) - I(k, b) : 0;
                if (e === f) return la(a, b);
                c = a;
                while (c = c.parentNode) g.unshift(c);
                c = b;
                while (c = c.parentNode) h.unshift(c);
                while (g[d] === h[d]) d++;
                return d ? la(g[d], h[d]) : g[d] === v ? -1 : h[d] === v ? 1 : 0
            }, n) : n
        }, ga.matches = function(a, b) {
            return ga(a, null, null, b)
        }, ga.matchesSelector = function(a, b) {
            if ((a.ownerDocument || a) !== n && m(a), b = b.replace(S, "='$1']"), c.matchesSelector && p && !A[b + " "] && (!r || !r.test(b)) && (!q || !q.test(b))) try {
                var d = s.call(a, b);
                if (d || c.disconnectedMatch || a.document && 11 !== a.document.nodeType) return d
            } catch (e) {}
            return ga(b, n, null, [a]).length > 0
        }, ga.contains = function(a, b) {
            return (a.ownerDocument || a) !== n && m(a), t(a, b)
        }, ga.attr = function(a, b) {
            (a.ownerDocument || a) !== n && m(a);
            var e = d.attrHandle[b.toLowerCase()],
                f = e && C.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : void 0;
            return void 0 !== f ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null
        }, ga.escape = function(a) {
            return (a + "").replace(ba, ca)
        }, ga.error = function(a) {
            throw new Error("Syntax error, unrecognized expression: " + a)
        }, ga.uniqueSort = function(a) {
            var b, d = [],
                e = 0,
                f = 0;
            if (l = !c.detectDuplicates, k = !c.sortStable && a.slice(0), a.sort(B), l) {
                while (b = a[f++]) b === a[f] && (e = d.push(f));
                while (e--) a.splice(d[e], 1)
            }
            return k = null, a
        }, e = ga.getText = function(a) {
            var b, c = "",
                d = 0,
                f = a.nodeType;
            if (f) {
                if (1 === f || 9 === f || 11 === f) {
                    if ("string" == typeof a.textContent) return a.textContent;
                    for (a = a.firstChild; a; a = a.nextSibling) c += e(a)
                } else if (3 === f || 4 === f) return a.nodeValue
            } else
                while (b = a[d++]) c += e(b);
            return c
        }, d = ga.selectors = {
            cacheLength: 50,
            createPseudo: ia,
            match: V,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(a) {
                    return a[1] = a[1].replace(_, aa), a[3] = (a[3] || a[4] || a[5] || "").replace(_, aa), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4)
                },
                CHILD: function(a) {
                    return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || ga.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && ga.error(a[0]), a
                },
                PSEUDO: function(a) {
                    var b, c = !a[6] && a[2];
                    return V.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && T.test(c) && (b = g(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3))
                }
            },
            filter: {
                TAG: function(a) {
                    var b = a.replace(_, aa).toLowerCase();
                    return "*" === a ? function() {
                        return !0
                    } : function(a) {
                        return a.nodeName && a.nodeName.toLowerCase() === b
                    }
                },
                CLASS: function(a) {
                    var b = y[a + " "];
                    return b || (b = new RegExp("(^|" + K + ")" + a + "(" + K + "|$)")) && y(a, function(a) {
                        return b.test("string" == typeof a.className && a.className || "undefined" != typeof a.getAttribute && a.getAttribute("class") || "")
                    })
                },
                ATTR: function(a, b, c) {
                    return function(d) {
                        var e = ga.attr(d, a);
                        return null == e ? "!=" === b : !b || (e += "", "=" === b ? e === c : "!=" === b ? e !== c : "^=" === b ? c && 0 === e.indexOf(c) : "*=" === b ? c && e.indexOf(c) > -1 : "$=" === b ? c && e.slice(-c.length) === c : "~=" === b ? (" " + e.replace(O, " ") + " ").indexOf(c) > -1 : "|=" === b && (e === c || e.slice(0, c.length + 1) === c + "-"))
                    }
                },
                CHILD: function(a, b, c, d, e) {
                    var f = "nth" !== a.slice(0, 3),
                        g = "last" !== a.slice(-4),
                        h = "of-type" === b;
                    return 1 === d && 0 === e ? function(a) {
                        return !!a.parentNode
                    } : function(b, c, i) {
                        var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling",
                            q = b.parentNode,
                            r = h && b.nodeName.toLowerCase(),
                            s = !i && !h,
                            t = !1;
                        if (q) {
                            if (f) {
                                while (p) {
                                    m = b;
                                    while (m = m[p])
                                        if (h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) return !1;
                                    o = p = "only" === a && !o && "nextSibling"
                                }
                                return !0
                            }
                            if (o = [g ? q.firstChild : q.lastChild], g && s) {
                                m = q, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n && j[2], m = n && q.childNodes[n];
                                while (m = ++n && m && m[p] || (t = n = 0) || o.pop())
                                    if (1 === m.nodeType && ++t && m === b) {
                                        k[a] = [w, n, t];
                                        break
                                    }
                            } else if (s && (m = b, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n), t === !1)
                                while (m = ++n && m && m[p] || (t = n = 0) || o.pop())
                                    if ((h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) && ++t && (s && (l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), k[a] = [w, t]), m === b)) break;
                            return t -= e, t === d || t % d === 0 && t / d >= 0
                        }
                    }
                },
                PSEUDO: function(a, b) {
                    var c, e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || ga.error("unsupported pseudo: " + a);
                    return e[u] ? e(b) : e.length > 1 ? (c = [a, a, "", b], d.setFilters.hasOwnProperty(a.toLowerCase()) ? ia(function(a, c) {
                        var d, f = e(a, b),
                            g = f.length;
                        while (g--) d = I(a, f[g]), a[d] = !(c[d] = f[g])
                    }) : function(a) {
                        return e(a, 0, c)
                    }) : e
                }
            },
            pseudos: {
                not: ia(function(a) {
                    var b = [],
                        c = [],
                        d = h(a.replace(P, "$1"));
                    return d[u] ? ia(function(a, b, c, e) {
                        var f, g = d(a, null, e, []),
                            h = a.length;
                        while (h--)(f = g[h]) && (a[h] = !(b[h] = f))
                    }) : function(a, e, f) {
                        return b[0] = a, d(b, null, f, c), b[0] = null, !c.pop()
                    }
                }),
                has: ia(function(a) {
                    return function(b) {
                        return ga(a, b).length > 0
                    }
                }),
                contains: ia(function(a) {
                    return a = a.replace(_, aa),
                        function(b) {
                            return (b.textContent || b.innerText || e(b)).indexOf(a) > -1
                        }
                }),
                lang: ia(function(a) {
                    return U.test(a || "") || ga.error("unsupported lang: " + a), a = a.replace(_, aa).toLowerCase(),
                        function(b) {
                            var c;
                            do
                                if (c = p ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + "-");
                            while ((b = b.parentNode) && 1 === b.nodeType);
                            return !1
                        }
                }),
                target: function(b) {
                    var c = a.location && a.location.hash;
                    return c && c.slice(1) === b.id
                },
                root: function(a) {
                    return a === o
                },
                focus: function(a) {
                    return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)
                },
                enabled: oa(!1),
                disabled: oa(!0),
                checked: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && !!a.checked || "option" === b && !!a.selected
                },
                selected: function(a) {
                    return a.parentNode && a.parentNode.selectedIndex, a.selected === !0
                },
                empty: function(a) {
                    for (a = a.firstChild; a; a = a.nextSibling)
                        if (a.nodeType < 6) return !1;
                    return !0
                },
                parent: function(a) {
                    return !d.pseudos.empty(a)
                },
                header: function(a) {
                    return X.test(a.nodeName)
                },
                input: function(a) {
                    return W.test(a.nodeName)
                },
                button: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && "button" === a.type || "button" === b
                },
                text: function(a) {
                    var b;
                    return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase())
                },
                first: pa(function() {
                    return [0]
                }),
                last: pa(function(a, b) {
                    return [b - 1]
                }),
                eq: pa(function(a, b, c) {
                    return [c < 0 ? c + b : c]
                }),
                even: pa(function(a, b) {
                    for (var c = 0; c < b; c += 2) a.push(c);
                    return a
                }),
                odd: pa(function(a, b) {
                    for (var c = 1; c < b; c += 2) a.push(c);
                    return a
                }),
                lt: pa(function(a, b, c) {
                    for (var d = c < 0 ? c + b : c; --d >= 0;) a.push(d);
                    return a
                }),
                gt: pa(function(a, b, c) {
                    for (var d = c < 0 ? c + b : c; ++d < b;) a.push(d);
                    return a
                })
            }
        }, d.pseudos.nth = d.pseudos.eq;
        for (b in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) d.pseudos[b] = ma(b);
        for (b in {
                submit: !0,
                reset: !0
            }) d.pseudos[b] = na(b);

        function ra() {}
        ra.prototype = d.filters = d.pseudos, d.setFilters = new ra, g = ga.tokenize = function(a, b) {
            var c, e, f, g, h, i, j, k = z[a + " "];
            if (k) return b ? 0 : k.slice(0);
            h = a, i = [], j = d.preFilter;
            while (h) {
                c && !(e = Q.exec(h)) || (e && (h = h.slice(e[0].length) || h), i.push(f = [])), c = !1, (e = R.exec(h)) && (c = e.shift(), f.push({
                    value: c,
                    type: e[0].replace(P, " ")
                }), h = h.slice(c.length));
                for (g in d.filter) !(e = V[g].exec(h)) || j[g] && !(e = j[g](e)) || (c = e.shift(), f.push({
                    value: c,
                    type: g,
                    matches: e
                }), h = h.slice(c.length));
                if (!c) break
            }
            return b ? h.length : h ? ga.error(a) : z(a, i).slice(0)
        };

        function sa(a) {
            for (var b = 0, c = a.length, d = ""; b < c; b++) d += a[b].value;
            return d
        }

        function ta(a, b, c) {
            var d = b.dir,
                e = b.next,
                f = e || d,
                g = c && "parentNode" === f,
                h = x++;
            return b.first ? function(b, c, e) {
                while (b = b[d])
                    if (1 === b.nodeType || g) return a(b, c, e);
                return !1
            } : function(b, c, i) {
                var j, k, l, m = [w, h];
                if (i) {
                    while (b = b[d])
                        if ((1 === b.nodeType || g) && a(b, c, i)) return !0
                } else
                    while (b = b[d])
                        if (1 === b.nodeType || g)
                            if (l = b[u] || (b[u] = {}), k = l[b.uniqueID] || (l[b.uniqueID] = {}), e && e === b.nodeName.toLowerCase()) b = b[d] || b;
                            else {
                                if ((j = k[f]) && j[0] === w && j[1] === h) return m[2] = j[2];
                                if (k[f] = m, m[2] = a(b, c, i)) return !0
                            } return !1
            }
        }

        function ua(a) {
            return a.length > 1 ? function(b, c, d) {
                var e = a.length;
                while (e--)
                    if (!a[e](b, c, d)) return !1;
                return !0
            } : a[0]
        }

        function va(a, b, c) {
            for (var d = 0, e = b.length; d < e; d++) ga(a, b[d], c);
            return c
        }

        function wa(a, b, c, d, e) {
            for (var f, g = [], h = 0, i = a.length, j = null != b; h < i; h++)(f = a[h]) && (c && !c(f, d, e) || (g.push(f), j && b.push(h)));
            return g
        }

        function xa(a, b, c, d, e, f) {
            return d && !d[u] && (d = xa(d)), e && !e[u] && (e = xa(e, f)), ia(function(f, g, h, i) {
                var j, k, l, m = [],
                    n = [],
                    o = g.length,
                    p = f || va(b || "*", h.nodeType ? [h] : h, []),
                    q = !a || !f && b ? p : wa(p, m, a, h, i),
                    r = c ? e || (f ? a : o || d) ? [] : g : q;
                if (c && c(q, r, h, i), d) {
                    j = wa(r, n), d(j, [], h, i), k = j.length;
                    while (k--)(l = j[k]) && (r[n[k]] = !(q[n[k]] = l))
                }
                if (f) {
                    if (e || a) {
                        if (e) {
                            j = [], k = r.length;
                            while (k--)(l = r[k]) && j.push(q[k] = l);
                            e(null, r = [], j, i)
                        }
                        k = r.length;
                        while (k--)(l = r[k]) && (j = e ? I(f, l) : m[k]) > -1 && (f[j] = !(g[j] = l))
                    }
                } else r = wa(r === g ? r.splice(o, r.length) : r), e ? e(null, g, r, i) : G.apply(g, r)
            })
        }

        function ya(a) {
            for (var b, c, e, f = a.length, g = d.relative[a[0].type], h = g || d.relative[" "], i = g ? 1 : 0, k = ta(function(a) {
                    return a === b
                }, h, !0), l = ta(function(a) {
                    return I(b, a) > -1
                }, h, !0), m = [function(a, c, d) {
                    var e = !g && (d || c !== j) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d));
                    return b = null, e
                }]; i < f; i++)
                if (c = d.relative[a[i].type]) m = [ta(ua(m), c)];
                else {
                    if (c = d.filter[a[i].type].apply(null, a[i].matches), c[u]) {
                        for (e = ++i; e < f; e++)
                            if (d.relative[a[e].type]) break;
                        return xa(i > 1 && ua(m), i > 1 && sa(a.slice(0, i - 1).concat({
                            value: " " === a[i - 2].type ? "*" : ""
                        })).replace(P, "$1"), c, i < e && ya(a.slice(i, e)), e < f && ya(a = a.slice(e)), e < f && sa(a))
                    }
                    m.push(c)
                }
            return ua(m)
        }

        function za(a, b) {
            var c = b.length > 0,
                e = a.length > 0,
                f = function(f, g, h, i, k) {
                    var l, o, q, r = 0,
                        s = "0",
                        t = f && [],
                        u = [],
                        v = j,
                        x = f || e && d.find.TAG("*", k),
                        y = w += null == v ? 1 : Math.random() || .1,
                        z = x.length;
                    for (k && (j = g === n || g || k); s !== z && null != (l = x[s]); s++) {
                        if (e && l) {
                            o = 0, g || l.ownerDocument === n || (m(l), h = !p);
                            while (q = a[o++])
                                if (q(l, g || n, h)) {
                                    i.push(l);
                                    break
                                }
                            k && (w = y)
                        }
                        c && ((l = !q && l) && r--, f && t.push(l))
                    }
                    if (r += s, c && s !== r) {
                        o = 0;
                        while (q = b[o++]) q(t, u, g, h);
                        if (f) {
                            if (r > 0)
                                while (s--) t[s] || u[s] || (u[s] = E.call(i));
                            u = wa(u)
                        }
                        G.apply(i, u), k && !f && u.length > 0 && r + b.length > 1 && ga.uniqueSort(i)
                    }
                    return k && (w = y, j = v), t
                };
            return c ? ia(f) : f
        }
        return h = ga.compile = function(a, b) {
            var c, d = [],
                e = [],
                f = A[a + " "];
            if (!f) {
                b || (b = g(a)), c = b.length;
                while (c--) f = ya(b[c]), f[u] ? d.push(f) : e.push(f);
                f = A(a, za(e, d)), f.selector = a
            }
            return f
        }, i = ga.select = function(a, b, c, e) {
            var f, i, j, k, l, m = "function" == typeof a && a,
                n = !e && g(a = m.selector || a);
            if (c = c || [], 1 === n.length) {
                if (i = n[0] = n[0].slice(0), i.length > 2 && "ID" === (j = i[0]).type && 9 === b.nodeType && p && d.relative[i[1].type]) {
                    if (b = (d.find.ID(j.matches[0].replace(_, aa), b) || [])[0], !b) return c;
                    m && (b = b.parentNode), a = a.slice(i.shift().value.length)
                }
                f = V.needsContext.test(a) ? 0 : i.length;
                while (f--) {
                    if (j = i[f], d.relative[k = j.type]) break;
                    if ((l = d.find[k]) && (e = l(j.matches[0].replace(_, aa), $.test(i[0].type) && qa(b.parentNode) || b))) {
                        if (i.splice(f, 1), a = e.length && sa(i), !a) return G.apply(c, e), c;
                        break
                    }
                }
            }
            return (m || h(a, n))(e, b, !p, c, !b || $.test(a) && qa(b.parentNode) || b), c
        }, c.sortStable = u.split("").sort(B).join("") === u, c.detectDuplicates = !!l, m(), c.sortDetached = ja(function(a) {
            return 1 & a.compareDocumentPosition(n.createElement("fieldset"))
        }), ja(function(a) {
            return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href")
        }) || ka("type|href|height|width", function(a, b, c) {
            if (!c) return a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2)
        }), c.attributes && ja(function(a) {
            return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value")
        }) || ka("value", function(a, b, c) {
            if (!c && "input" === a.nodeName.toLowerCase()) return a.defaultValue
        }), ja(function(a) {
            return null == a.getAttribute("disabled")
        }) || ka(J, function(a, b, c) {
            var d;
            if (!c) return a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
        }), ga
    }(a);
    r.find = x, r.expr = x.selectors, r.expr[":"] = r.expr.pseudos, r.uniqueSort = r.unique = x.uniqueSort, r.text = x.getText, r.isXMLDoc = x.isXML, r.contains = x.contains, r.escapeSelector = x.escape;
    var y = function(a, b, c) {
            var d = [],
                e = void 0 !== c;
            while ((a = a[b]) && 9 !== a.nodeType)
                if (1 === a.nodeType) {
                    if (e && r(a).is(c)) break;
                    d.push(a)
                }
            return d
        },
        z = function(a, b) {
            for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);
            return c
        },
        A = r.expr.match.needsContext,
        B = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
        C = /^.[^:#\[\.,]*$/;

    function D(a, b, c) {
        return r.isFunction(b) ? r.grep(a, function(a, d) {
            return !!b.call(a, d, a) !== c
        }) : b.nodeType ? r.grep(a, function(a) {
            return a === b !== c
        }) : "string" != typeof b ? r.grep(a, function(a) {
            return i.call(b, a) > -1 !== c
        }) : C.test(b) ? r.filter(b, a, c) : (b = r.filter(b, a), r.grep(a, function(a) {
            return i.call(b, a) > -1 !== c && 1 === a.nodeType
        }))
    }
    r.filter = function(a, b, c) {
        var d = b[0];
        return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? r.find.matchesSelector(d, a) ? [d] : [] : r.find.matches(a, r.grep(b, function(a) {
            return 1 === a.nodeType
        }))
    }, r.fn.extend({
        find: function(a) {
            var b, c, d = this.length,
                e = this;
            if ("string" != typeof a) return this.pushStack(r(a).filter(function() {
                for (b = 0; b < d; b++)
                    if (r.contains(e[b], this)) return !0
            }));
            for (c = this.pushStack([]), b = 0; b < d; b++) r.find(a, e[b], c);
            return d > 1 ? r.uniqueSort(c) : c
        },
        filter: function(a) {
            return this.pushStack(D(this, a || [], !1))
        },
        not: function(a) {
            return this.pushStack(D(this, a || [], !0))
        },
        is: function(a) {
            return !!D(this, "string" == typeof a && A.test(a) ? r(a) : a || [], !1).length
        }
    });
    var E, F = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
        G = r.fn.init = function(a, b, c) {
            var e, f;
            if (!a) return this;
            if (c = c || E, "string" == typeof a) {
                if (e = "<" === a[0] && ">" === a[a.length - 1] && a.length >= 3 ? [null, a, null] : F.exec(a), !e || !e[1] && b) return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a);
                if (e[1]) {
                    if (b = b instanceof r ? b[0] : b, r.merge(this, r.parseHTML(e[1], b && b.nodeType ? b.ownerDocument || b : d, !0)), B.test(e[1]) && r.isPlainObject(b))
                        for (e in b) r.isFunction(this[e]) ? this[e](b[e]) : this.attr(e, b[e]);
                    return this
                }
                return f = d.getElementById(e[2]), f && (this[0] = f, this.length = 1), this
            }
            return a.nodeType ? (this[0] = a, this.length = 1, this) : r.isFunction(a) ? void 0 !== c.ready ? c.ready(a) : a(r) : r.makeArray(a, this)
        };
    G.prototype = r.fn, E = r(d);
    var H = /^(?:parents|prev(?:Until|All))/,
        I = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    r.fn.extend({
        has: function(a) {
            var b = r(a, this),
                c = b.length;
            return this.filter(function() {
                for (var a = 0; a < c; a++)
                    if (r.contains(this, b[a])) return !0
            })
        },
        closest: function(a, b) {
            var c, d = 0,
                e = this.length,
                f = [],
                g = "string" != typeof a && r(a);
            if (!A.test(a))
                for (; d < e; d++)
                    for (c = this[d]; c && c !== b; c = c.parentNode)
                        if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && r.find.matchesSelector(c, a))) {
                            f.push(c);
                            break
                        }
            return this.pushStack(f.length > 1 ? r.uniqueSort(f) : f)
        },
        index: function(a) {
            return a ? "string" == typeof a ? i.call(r(a), this[0]) : i.call(this, a.jquery ? a[0] : a) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(a, b) {
            return this.pushStack(r.uniqueSort(r.merge(this.get(), r(a, b))))
        },
        addBack: function(a) {
            return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
        }
    });

    function J(a, b) {
        while ((a = a[b]) && 1 !== a.nodeType);
        return a
    }
    r.each({
        parent: function(a) {
            var b = a.parentNode;
            return b && 11 !== b.nodeType ? b : null
        },
        parents: function(a) {
            return y(a, "parentNode")
        },
        parentsUntil: function(a, b, c) {
            return y(a, "parentNode", c)
        },
        next: function(a) {
            return J(a, "nextSibling")
        },
        prev: function(a) {
            return J(a, "previousSibling")
        },
        nextAll: function(a) {
            return y(a, "nextSibling")
        },
        prevAll: function(a) {
            return y(a, "previousSibling")
        },
        nextUntil: function(a, b, c) {
            return y(a, "nextSibling", c)
        },
        prevUntil: function(a, b, c) {
            return y(a, "previousSibling", c)
        },
        siblings: function(a) {
            return z((a.parentNode || {}).firstChild, a)
        },
        children: function(a) {
            return z(a.firstChild)
        },
        contents: function(a) {
            return a.contentDocument || r.merge([], a.childNodes)
        }
    }, function(a, b) {
        r.fn[a] = function(c, d) {
            var e = r.map(this, b, c);
            return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = r.filter(d, e)), this.length > 1 && (I[a] || r.uniqueSort(e), H.test(a) && e.reverse()), this.pushStack(e)
        }
    });
    var K = /[^\x20\t\r\n\f]+/g;

    function L(a) {
        var b = {};
        return r.each(a.match(K) || [], function(a, c) {
            b[c] = !0
        }), b
    }
    r.Callbacks = function(a) {
        a = "string" == typeof a ? L(a) : r.extend({}, a);
        var b, c, d, e, f = [],
            g = [],
            h = -1,
            i = function() {
                for (e = a.once, d = b = !0; g.length; h = -1) {
                    c = g.shift();
                    while (++h < f.length) f[h].apply(c[0], c[1]) === !1 && a.stopOnFalse && (h = f.length, c = !1)
                }
                a.memory || (c = !1), b = !1, e && (f = c ? [] : "")
            },
            j = {
                add: function() {
                    return f && (c && !b && (h = f.length - 1, g.push(c)), function d(b) {
                        r.each(b, function(b, c) {
                            r.isFunction(c) ? a.unique && j.has(c) || f.push(c) : c && c.length && "string" !== r.type(c) && d(c)
                        })
                    }(arguments), c && !b && i()), this
                },
                remove: function() {
                    return r.each(arguments, function(a, b) {
                        var c;
                        while ((c = r.inArray(b, f, c)) > -1) f.splice(c, 1), c <= h && h--
                    }), this
                },
                has: function(a) {
                    return a ? r.inArray(a, f) > -1 : f.length > 0
                },
                empty: function() {
                    return f && (f = []), this
                },
                disable: function() {
                    return e = g = [], f = c = "", this
                },
                disabled: function() {
                    return !f
                },
                lock: function() {
                    return e = g = [], c || b || (f = c = ""), this
                },
                locked: function() {
                    return !!e
                },
                fireWith: function(a, c) {
                    return e || (c = c || [], c = [a, c.slice ? c.slice() : c], g.push(c), b || i()), this
                },
                fire: function() {
                    return j.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!d
                }
            };
        return j
    };

    function M(a) {
        return a
    }

    function N(a) {
        throw a
    }

    function O(a, b, c) {
        var d;
        try {
            a && r.isFunction(d = a.promise) ? d.call(a).done(b).fail(c) : a && r.isFunction(d = a.then) ? d.call(a, b, c) : b.call(void 0, a)
        } catch (a) {
            c.call(void 0, a)
        }
    }
    r.extend({
        Deferred: function(b) {
            var c = [
                    ["notify", "progress", r.Callbacks("memory"), r.Callbacks("memory"), 2],
                    ["resolve", "done", r.Callbacks("once memory"), r.Callbacks("once memory"), 0, "resolved"],
                    ["reject", "fail", r.Callbacks("once memory"), r.Callbacks("once memory"), 1, "rejected"]
                ],
                d = "pending",
                e = {
                    state: function() {
                        return d
                    },
                    always: function() {
                        return f.done(arguments).fail(arguments), this
                    },
                    "catch": function(a) {
                        return e.then(null, a)
                    },
                    pipe: function() {
                        var a = arguments;
                        return r.Deferred(function(b) {
                            r.each(c, function(c, d) {
                                var e = r.isFunction(a[d[4]]) && a[d[4]];
                                f[d[1]](function() {
                                    var a = e && e.apply(this, arguments);
                                    a && r.isFunction(a.promise) ? a.promise().progress(b.notify).done(b.resolve).fail(b.reject) : b[d[0] + "With"](this, e ? [a] : arguments)
                                })
                            }), a = null
                        }).promise()
                    },
                    then: function(b, d, e) {
                        var f = 0;

                        function g(b, c, d, e) {
                            return function() {
                                var h = this,
                                    i = arguments,
                                    j = function() {
                                        var a, j;
                                        if (!(b < f)) {
                                            if (a = d.apply(h, i), a === c.promise()) throw new TypeError("Thenable self-resolution");
                                            j = a && ("object" == typeof a || "function" == typeof a) && a.then, r.isFunction(j) ? e ? j.call(a, g(f, c, M, e), g(f, c, N, e)) : (f++, j.call(a, g(f, c, M, e), g(f, c, N, e), g(f, c, M, c.notifyWith))) : (d !== M && (h = void 0, i = [a]), (e || c.resolveWith)(h, i))
                                        }
                                    },
                                    k = e ? j : function() {
                                        try {
                                            j()
                                        } catch (a) {
                                            r.Deferred.exceptionHook && r.Deferred.exceptionHook(a, k.stackTrace), b + 1 >= f && (d !== N && (h = void 0, i = [a]), c.rejectWith(h, i))
                                        }
                                    };
                                b ? k() : (r.Deferred.getStackHook && (k.stackTrace = r.Deferred.getStackHook()), a.setTimeout(k))
                            }
                        }
                        return r.Deferred(function(a) {
                            c[0][3].add(g(0, a, r.isFunction(e) ? e : M, a.notifyWith)), c[1][3].add(g(0, a, r.isFunction(b) ? b : M)), c[2][3].add(g(0, a, r.isFunction(d) ? d : N))
                        }).promise()
                    },
                    promise: function(a) {
                        return null != a ? r.extend(a, e) : e
                    }
                },
                f = {};
            return r.each(c, function(a, b) {
                var g = b[2],
                    h = b[5];
                e[b[1]] = g.add, h && g.add(function() {
                    d = h
                }, c[3 - a][2].disable, c[0][2].lock), g.add(b[3].fire), f[b[0]] = function() {
                    return f[b[0] + "With"](this === f ? void 0 : this, arguments), this
                }, f[b[0] + "With"] = g.fireWith
            }), e.promise(f), b && b.call(f, f), f
        },
        when: function(a) {
            var b = arguments.length,
                c = b,
                d = Array(c),
                e = f.call(arguments),
                g = r.Deferred(),
                h = function(a) {
                    return function(c) {
                        d[a] = this, e[a] = arguments.length > 1 ? f.call(arguments) : c, --b || g.resolveWith(d, e)
                    }
                };
            if (b <= 1 && (O(a, g.done(h(c)).resolve, g.reject), "pending" === g.state() || r.isFunction(e[c] && e[c].then))) return g.then();
            while (c--) O(e[c], h(c), g.reject);
            return g.promise()
        }
    });
    var P = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    r.Deferred.exceptionHook = function(b, c) {
        a.console && a.console.warn && b && P.test(b.name) && a.console.warn("jQuery.Deferred exception: " + b.message, b.stack, c)
    }, r.readyException = function(b) {
        a.setTimeout(function() {
            throw b
        })
    };
    var Q = r.Deferred();
    r.fn.ready = function(a) {
        return Q.then(a)["catch"](function(a) {
            r.readyException(a)
        }), this
    }, r.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function(a) {
            a ? r.readyWait++ : r.ready(!0)
        },
        ready: function(a) {
            (a === !0 ? --r.readyWait : r.isReady) || (r.isReady = !0, a !== !0 && --r.readyWait > 0 || Q.resolveWith(d, [r]))
        }
    }), r.ready.then = Q.then;

    function R() {
        d.removeEventListener("DOMContentLoaded", R),
            a.removeEventListener("load", R), r.ready()
    }
    "complete" === d.readyState || "loading" !== d.readyState && !d.documentElement.doScroll ? a.setTimeout(r.ready) : (d.addEventListener("DOMContentLoaded", R), a.addEventListener("load", R));
    var S = function(a, b, c, d, e, f, g) {
            var h = 0,
                i = a.length,
                j = null == c;
            if ("object" === r.type(c)) {
                e = !0;
                for (h in c) S(a, b, h, c[h], !0, f, g)
            } else if (void 0 !== d && (e = !0, r.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), b = null) : (j = b, b = function(a, b, c) {
                    return j.call(r(a), c)
                })), b))
                for (; h < i; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
            return e ? a : j ? b.call(a) : i ? b(a[0], c) : f
        },
        T = function(a) {
            return 1 === a.nodeType || 9 === a.nodeType || !+a.nodeType
        };

    function U() {
        this.expando = r.expando + U.uid++
    }
    U.uid = 1, U.prototype = {
        cache: function(a) {
            var b = a[this.expando];
            return b || (b = {}, T(a) && (a.nodeType ? a[this.expando] = b : Object.defineProperty(a, this.expando, {
                value: b,
                configurable: !0
            }))), b
        },
        set: function(a, b, c) {
            var d, e = this.cache(a);
            if ("string" == typeof b) e[r.camelCase(b)] = c;
            else
                for (d in b) e[r.camelCase(d)] = b[d];
            return e
        },
        get: function(a, b) {
            return void 0 === b ? this.cache(a) : a[this.expando] && a[this.expando][r.camelCase(b)]
        },
        access: function(a, b, c) {
            return void 0 === b || b && "string" == typeof b && void 0 === c ? this.get(a, b) : (this.set(a, b, c), void 0 !== c ? c : b)
        },
        remove: function(a, b) {
            var c, d = a[this.expando];
            if (void 0 !== d) {
                if (void 0 !== b) {
                    r.isArray(b) ? b = b.map(r.camelCase) : (b = r.camelCase(b), b = b in d ? [b] : b.match(K) || []), c = b.length;
                    while (c--) delete d[b[c]]
                }(void 0 === b || r.isEmptyObject(d)) && (a.nodeType ? a[this.expando] = void 0 : delete a[this.expando])
            }
        },
        hasData: function(a) {
            var b = a[this.expando];
            return void 0 !== b && !r.isEmptyObject(b)
        }
    };
    var V = new U,
        W = new U,
        X = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        Y = /[A-Z]/g;

    function Z(a) {
        return "true" === a || "false" !== a && ("null" === a ? null : a === +a + "" ? +a : X.test(a) ? JSON.parse(a) : a)
    }

    function $(a, b, c) {
        var d;
        if (void 0 === c && 1 === a.nodeType)
            if (d = "data-" + b.replace(Y, "-$&").toLowerCase(), c = a.getAttribute(d), "string" == typeof c) {
                try {
                    c = Z(c)
                } catch (e) {}
                W.set(a, b, c)
            } else c = void 0;
        return c
    }
    r.extend({
        hasData: function(a) {
            return W.hasData(a) || V.hasData(a)
        },
        data: function(a, b, c) {
            return W.access(a, b, c)
        },
        removeData: function(a, b) {
            W.remove(a, b)
        },
        _data: function(a, b, c) {
            return V.access(a, b, c)
        },
        _removeData: function(a, b) {
            V.remove(a, b)
        }
    }), r.fn.extend({
        data: function(a, b) {
            var c, d, e, f = this[0],
                g = f && f.attributes;
            if (void 0 === a) {
                if (this.length && (e = W.get(f), 1 === f.nodeType && !V.get(f, "hasDataAttrs"))) {
                    c = g.length;
                    while (c--) g[c] && (d = g[c].name, 0 === d.indexOf("data-") && (d = r.camelCase(d.slice(5)), $(f, d, e[d])));
                    V.set(f, "hasDataAttrs", !0)
                }
                return e
            }
            return "object" == typeof a ? this.each(function() {
                W.set(this, a)
            }) : S(this, function(b) {
                var c;
                if (f && void 0 === b) {
                    if (c = W.get(f, a), void 0 !== c) return c;
                    if (c = $(f, a), void 0 !== c) return c
                } else this.each(function() {
                    W.set(this, a, b)
                })
            }, null, b, arguments.length > 1, null, !0)
        },
        removeData: function(a) {
            return this.each(function() {
                W.remove(this, a)
            })
        }
    }), r.extend({
        queue: function(a, b, c) {
            var d;
            if (a) return b = (b || "fx") + "queue", d = V.get(a, b), c && (!d || r.isArray(c) ? d = V.access(a, b, r.makeArray(c)) : d.push(c)), d || []
        },
        dequeue: function(a, b) {
            b = b || "fx";
            var c = r.queue(a, b),
                d = c.length,
                e = c.shift(),
                f = r._queueHooks(a, b),
                g = function() {
                    r.dequeue(a, b)
                };
            "inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire()
        },
        _queueHooks: function(a, b) {
            var c = b + "queueHooks";
            return V.get(a, c) || V.access(a, c, {
                empty: r.Callbacks("once memory").add(function() {
                    V.remove(a, [b + "queue", c])
                })
            })
        }
    }), r.fn.extend({
        queue: function(a, b) {
            var c = 2;
            return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? r.queue(this[0], a) : void 0 === b ? this : this.each(function() {
                var c = r.queue(this, a, b);
                r._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && r.dequeue(this, a)
            })
        },
        dequeue: function(a) {
            return this.each(function() {
                r.dequeue(this, a)
            })
        },
        clearQueue: function(a) {
            return this.queue(a || "fx", [])
        },
        promise: function(a, b) {
            var c, d = 1,
                e = r.Deferred(),
                f = this,
                g = this.length,
                h = function() {
                    --d || e.resolveWith(f, [f])
                };
            "string" != typeof a && (b = a, a = void 0), a = a || "fx";
            while (g--) c = V.get(f[g], a + "queueHooks"), c && c.empty && (d++, c.empty.add(h));
            return h(), e.promise(b)
        }
    });
    var _ = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        aa = new RegExp("^(?:([+-])=|)(" + _ + ")([a-z%]*)$", "i"),
        ba = ["Top", "Right", "Bottom", "Left"],
        ca = function(a, b) {
            return a = b || a, "none" === a.style.display || "" === a.style.display && r.contains(a.ownerDocument, a) && "none" === r.css(a, "display")
        },
        da = function(a, b, c, d) {
            var e, f, g = {};
            for (f in b) g[f] = a.style[f], a.style[f] = b[f];
            e = c.apply(a, d || []);
            for (f in b) a.style[f] = g[f];
            return e
        };

    function ea(a, b, c, d) {
        var e, f = 1,
            g = 20,
            h = d ? function() {
                return d.cur()
            } : function() {
                return r.css(a, b, "")
            },
            i = h(),
            j = c && c[3] || (r.cssNumber[b] ? "" : "px"),
            k = (r.cssNumber[b] || "px" !== j && +i) && aa.exec(r.css(a, b));
        if (k && k[3] !== j) {
            j = j || k[3], c = c || [], k = +i || 1;
            do f = f || ".5", k /= f, r.style(a, b, k + j); while (f !== (f = h() / i) && 1 !== f && --g)
        }
        return c && (k = +k || +i || 0, e = c[1] ? k + (c[1] + 1) * c[2] : +c[2], d && (d.unit = j, d.start = k, d.end = e)), e
    }
    var fa = {};

    function ga(a) {
        var b, c = a.ownerDocument,
            d = a.nodeName,
            e = fa[d];
        return e ? e : (b = c.body.appendChild(c.createElement(d)), e = r.css(b, "display"), b.parentNode.removeChild(b), "none" === e && (e = "block"), fa[d] = e, e)
    }

    function ha(a, b) {
        for (var c, d, e = [], f = 0, g = a.length; f < g; f++) d = a[f], d.style && (c = d.style.display, b ? ("none" === c && (e[f] = V.get(d, "display") || null, e[f] || (d.style.display = "")), "" === d.style.display && ca(d) && (e[f] = ga(d))) : "none" !== c && (e[f] = "none", V.set(d, "display", c)));
        for (f = 0; f < g; f++) null != e[f] && (a[f].style.display = e[f]);
        return a
    }
    r.fn.extend({
        show: function() {
            return ha(this, !0)
        },
        hide: function() {
            return ha(this)
        },
        toggle: function(a) {
            return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function() {
                ca(this) ? r(this).show() : r(this).hide()
            })
        }
    });
    var ia = /^(?:checkbox|radio)$/i,
        ja = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
        ka = /^$|\/(?:java|ecma)script/i,
        la = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };
    la.optgroup = la.option, la.tbody = la.tfoot = la.colgroup = la.caption = la.thead, la.th = la.td;

    function ma(a, b) {
        var c;
        return c = "undefined" != typeof a.getElementsByTagName ? a.getElementsByTagName(b || "*") : "undefined" != typeof a.querySelectorAll ? a.querySelectorAll(b || "*") : [], void 0 === b || b && r.nodeName(a, b) ? r.merge([a], c) : c
    }

    function na(a, b) {
        for (var c = 0, d = a.length; c < d; c++) V.set(a[c], "globalEval", !b || V.get(b[c], "globalEval"))
    }
    var oa = /<|&#?\w+;/;

    function pa(a, b, c, d, e) {
        for (var f, g, h, i, j, k, l = b.createDocumentFragment(), m = [], n = 0, o = a.length; n < o; n++)
            if (f = a[n], f || 0 === f)
                if ("object" === r.type(f)) r.merge(m, f.nodeType ? [f] : f);
                else if (oa.test(f)) {
            g = g || l.appendChild(b.createElement("div")), h = (ja.exec(f) || ["", ""])[1].toLowerCase(), i = la[h] || la._default, g.innerHTML = i[1] + r.htmlPrefilter(f) + i[2], k = i[0];
            while (k--) g = g.lastChild;
            r.merge(m, g.childNodes), g = l.firstChild, g.textContent = ""
        } else m.push(b.createTextNode(f));
        l.textContent = "", n = 0;
        while (f = m[n++])
            if (d && r.inArray(f, d) > -1) e && e.push(f);
            else if (j = r.contains(f.ownerDocument, f), g = ma(l.appendChild(f), "script"), j && na(g), c) {
            k = 0;
            while (f = g[k++]) ka.test(f.type || "") && c.push(f)
        }
        return l
    }! function() {
        var a = d.createDocumentFragment(),
            b = a.appendChild(d.createElement("div")),
            c = d.createElement("input");
        c.setAttribute("type", "radio"), c.setAttribute("checked", "checked"), c.setAttribute("name", "t"), b.appendChild(c), o.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked, b.innerHTML = "<textarea>x</textarea>", o.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue
    }();
    var qa = d.documentElement,
        ra = /^key/,
        sa = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        ta = /^([^.]*)(?:\.(.+)|)/;

    function ua() {
        return !0
    }

    function va() {
        return !1
    }

    function wa() {
        try {
            return d.activeElement
        } catch (a) {}
    }

    function xa(a, b, c, d, e, f) {
        var g, h;
        if ("object" == typeof b) {
            "string" != typeof c && (d = d || c, c = void 0);
            for (h in b) xa(a, h, c, d, b[h], f);
            return a
        }
        if (null == d && null == e ? (e = c, d = c = void 0) : null == e && ("string" == typeof c ? (e = d, d = void 0) : (e = d, d = c, c = void 0)), e === !1) e = va;
        else if (!e) return a;
        return 1 === f && (g = e, e = function(a) {
            return r().off(a), g.apply(this, arguments)
        }, e.guid = g.guid || (g.guid = r.guid++)), a.each(function() {
            r.event.add(this, b, e, d, c)
        })
    }
    r.event = {
        global: {},
        add: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, n, o, p, q = V.get(a);
            if (q) {
                c.handler && (f = c, c = f.handler, e = f.selector), e && r.find.matchesSelector(qa, e), c.guid || (c.guid = r.guid++), (i = q.events) || (i = q.events = {}), (g = q.handle) || (g = q.handle = function(b) {
                    return "undefined" != typeof r && r.event.triggered !== b.type ? r.event.dispatch.apply(a, arguments) : void 0
                }), b = (b || "").match(K) || [""], j = b.length;
                while (j--) h = ta.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n && (l = r.event.special[n] || {}, n = (e ? l.delegateType : l.bindType) || n, l = r.event.special[n] || {}, k = r.extend({
                    type: n,
                    origType: p,
                    data: d,
                    handler: c,
                    guid: c.guid,
                    selector: e,
                    needsContext: e && r.expr.match.needsContext.test(e),
                    namespace: o.join(".")
                }, f), (m = i[n]) || (m = i[n] = [], m.delegateCount = 0, l.setup && l.setup.call(a, d, o, g) !== !1 || a.addEventListener && a.addEventListener(n, g)), l.add && (l.add.call(a, k), k.handler.guid || (k.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, k) : m.push(k), r.event.global[n] = !0)
            }
        },
        remove: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, n, o, p, q = V.hasData(a) && V.get(a);
            if (q && (i = q.events)) {
                b = (b || "").match(K) || [""], j = b.length;
                while (j--)
                    if (h = ta.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n) {
                        l = r.event.special[n] || {}, n = (d ? l.delegateType : l.bindType) || n, m = i[n] || [], h = h[2] && new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)"), g = f = m.length;
                        while (f--) k = m[f], !e && p !== k.origType || c && c.guid !== k.guid || h && !h.test(k.namespace) || d && d !== k.selector && ("**" !== d || !k.selector) || (m.splice(f, 1), k.selector && m.delegateCount--, l.remove && l.remove.call(a, k));
                        g && !m.length && (l.teardown && l.teardown.call(a, o, q.handle) !== !1 || r.removeEvent(a, n, q.handle), delete i[n])
                    } else
                        for (n in i) r.event.remove(a, n + b[j], c, d, !0);
                r.isEmptyObject(i) && V.remove(a, "handle events")
            }
        },
        dispatch: function(a) {
            var b = r.event.fix(a),
                c, d, e, f, g, h, i = new Array(arguments.length),
                j = (V.get(this, "events") || {})[b.type] || [],
                k = r.event.special[b.type] || {};
            for (i[0] = b, c = 1; c < arguments.length; c++) i[c] = arguments[c];
            if (b.delegateTarget = this, !k.preDispatch || k.preDispatch.call(this, b) !== !1) {
                h = r.event.handlers.call(this, b, j), c = 0;
                while ((f = h[c++]) && !b.isPropagationStopped()) {
                    b.currentTarget = f.elem, d = 0;
                    while ((g = f.handlers[d++]) && !b.isImmediatePropagationStopped()) b.rnamespace && !b.rnamespace.test(g.namespace) || (b.handleObj = g, b.data = g.data, e = ((r.event.special[g.origType] || {}).handle || g.handler).apply(f.elem, i), void 0 !== e && (b.result = e) === !1 && (b.preventDefault(), b.stopPropagation()))
                }
                return k.postDispatch && k.postDispatch.call(this, b), b.result
            }
        },
        handlers: function(a, b) {
            var c, d, e, f, g, h = [],
                i = b.delegateCount,
                j = a.target;
            if (i && j.nodeType && !("click" === a.type && a.button >= 1))
                for (; j !== this; j = j.parentNode || this)
                    if (1 === j.nodeType && ("click" !== a.type || j.disabled !== !0)) {
                        for (f = [], g = {}, c = 0; c < i; c++) d = b[c], e = d.selector + " ", void 0 === g[e] && (g[e] = d.needsContext ? r(e, this).index(j) > -1 : r.find(e, this, null, [j]).length), g[e] && f.push(d);
                        f.length && h.push({
                            elem: j,
                            handlers: f
                        })
                    }
            return j = this, i < b.length && h.push({
                elem: j,
                handlers: b.slice(i)
            }), h
        },
        addProp: function(a, b) {
            Object.defineProperty(r.Event.prototype, a, {
                enumerable: !0,
                configurable: !0,
                get: r.isFunction(b) ? function() {
                    if (this.originalEvent) return b(this.originalEvent)
                } : function() {
                    if (this.originalEvent) return this.originalEvent[a]
                },
                set: function(b) {
                    Object.defineProperty(this, a, {
                        enumerable: !0,
                        configurable: !0,
                        writable: !0,
                        value: b
                    })
                }
            })
        },
        fix: function(a) {
            return a[r.expando] ? a : new r.Event(a)
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== wa() && this.focus) return this.focus(), !1
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === wa() && this.blur) return this.blur(), !1
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    if ("checkbox" === this.type && this.click && r.nodeName(this, "input")) return this.click(), !1
                },
                _default: function(a) {
                    return r.nodeName(a.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(a) {
                    void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result)
                }
            }
        }
    }, r.removeEvent = function(a, b, c) {
        a.removeEventListener && a.removeEventListener(b, c)
    }, r.Event = function(a, b) {
        return this instanceof r.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? ua : va, this.target = a.target && 3 === a.target.nodeType ? a.target.parentNode : a.target, this.currentTarget = a.currentTarget, this.relatedTarget = a.relatedTarget) : this.type = a, b && r.extend(this, b), this.timeStamp = a && a.timeStamp || r.now(), void(this[r.expando] = !0)) : new r.Event(a, b)
    }, r.Event.prototype = {
        constructor: r.Event,
        isDefaultPrevented: va,
        isPropagationStopped: va,
        isImmediatePropagationStopped: va,
        isSimulated: !1,
        preventDefault: function() {
            var a = this.originalEvent;
            this.isDefaultPrevented = ua, a && !this.isSimulated && a.preventDefault()
        },
        stopPropagation: function() {
            var a = this.originalEvent;
            this.isPropagationStopped = ua, a && !this.isSimulated && a.stopPropagation()
        },
        stopImmediatePropagation: function() {
            var a = this.originalEvent;
            this.isImmediatePropagationStopped = ua, a && !this.isSimulated && a.stopImmediatePropagation(), this.stopPropagation()
        }
    }, r.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        "char": !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function(a) {
            var b = a.button;
            return null == a.which && ra.test(a.type) ? null != a.charCode ? a.charCode : a.keyCode : !a.which && void 0 !== b && sa.test(a.type) ? 1 & b ? 1 : 2 & b ? 3 : 4 & b ? 2 : 0 : a.which
        }
    }, r.event.addProp), r.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(a, b) {
        r.event.special[a] = {
            delegateType: b,
            bindType: b,
            handle: function(a) {
                var c, d = this,
                    e = a.relatedTarget,
                    f = a.handleObj;
                return e && (e === d || r.contains(d, e)) || (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c
            }
        }
    }), r.fn.extend({
        on: function(a, b, c, d) {
            return xa(this, a, b, c, d)
        },
        one: function(a, b, c, d) {
            return xa(this, a, b, c, d, 1)
        },
        off: function(a, b, c) {
            var d, e;
            if (a && a.preventDefault && a.handleObj) return d = a.handleObj, r(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this;
            if ("object" == typeof a) {
                for (e in a) this.off(e, b, a[e]);
                return this
            }
            return b !== !1 && "function" != typeof b || (c = b, b = void 0), c === !1 && (c = va), this.each(function() {
                r.event.remove(this, a, c, b)
            })
        }
    });
    var ya = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        za = /<script|<style|<link/i,
        Aa = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Ba = /^true\/(.*)/,
        Ca = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

    function Da(a, b) {
        return r.nodeName(a, "table") && r.nodeName(11 !== b.nodeType ? b : b.firstChild, "tr") ? a.getElementsByTagName("tbody")[0] || a : a
    }

    function Ea(a) {
        return a.type = (null !== a.getAttribute("type")) + "/" + a.type, a
    }

    function Fa(a) {
        var b = Ba.exec(a.type);
        return b ? a.type = b[1] : a.removeAttribute("type"), a
    }

    function Ga(a, b) {
        var c, d, e, f, g, h, i, j;
        if (1 === b.nodeType) {
            if (V.hasData(a) && (f = V.access(a), g = V.set(b, f), j = f.events)) {
                delete g.handle, g.events = {};
                for (e in j)
                    for (c = 0, d = j[e].length; c < d; c++) r.event.add(b, e, j[e][c])
            }
            W.hasData(a) && (h = W.access(a), i = r.extend({}, h), W.set(b, i))
        }
    }

    function Ha(a, b) {
        var c = b.nodeName.toLowerCase();
        "input" === c && ia.test(a.type) ? b.checked = a.checked : "input" !== c && "textarea" !== c || (b.defaultValue = a.defaultValue)
    }

    function Ia(a, b, c, d) {
        b = g.apply([], b);
        var e, f, h, i, j, k, l = 0,
            m = a.length,
            n = m - 1,
            q = b[0],
            s = r.isFunction(q);
        if (s || m > 1 && "string" == typeof q && !o.checkClone && Aa.test(q)) return a.each(function(e) {
            var f = a.eq(e);
            s && (b[0] = q.call(this, e, f.html())), Ia(f, b, c, d)
        });
        if (m && (e = pa(b, a[0].ownerDocument, !1, a, d), f = e.firstChild, 1 === e.childNodes.length && (e = f), f || d)) {
            for (h = r.map(ma(e, "script"), Ea), i = h.length; l < m; l++) j = e, l !== n && (j = r.clone(j, !0, !0), i && r.merge(h, ma(j, "script"))), c.call(a[l], j, l);
            if (i)
                for (k = h[h.length - 1].ownerDocument, r.map(h, Fa), l = 0; l < i; l++) j = h[l], ka.test(j.type || "") && !V.access(j, "globalEval") && r.contains(k, j) && (j.src ? r._evalUrl && r._evalUrl(j.src) : p(j.textContent.replace(Ca, ""), k))
        }
        return a
    }

    function Ja(a, b, c) {
        for (var d, e = b ? r.filter(b, a) : a, f = 0; null != (d = e[f]); f++) c || 1 !== d.nodeType || r.cleanData(ma(d)), d.parentNode && (c && r.contains(d.ownerDocument, d) && na(ma(d, "script")), d.parentNode.removeChild(d));
        return a
    }
    r.extend({
        htmlPrefilter: function(a) {
            return a.replace(ya, "<$1></$2>")
        },
        clone: function(a, b, c) {
            var d, e, f, g, h = a.cloneNode(!0),
                i = r.contains(a.ownerDocument, a);
            if (!(o.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || r.isXMLDoc(a)))
                for (g = ma(h), f = ma(a), d = 0, e = f.length; d < e; d++) Ha(f[d], g[d]);
            if (b)
                if (c)
                    for (f = f || ma(a), g = g || ma(h), d = 0, e = f.length; d < e; d++) Ga(f[d], g[d]);
                else Ga(a, h);
            return g = ma(h, "script"), g.length > 0 && na(g, !i && ma(a, "script")), h
        },
        cleanData: function(a) {
            for (var b, c, d, e = r.event.special, f = 0; void 0 !== (c = a[f]); f++)
                if (T(c)) {
                    if (b = c[V.expando]) {
                        if (b.events)
                            for (d in b.events) e[d] ? r.event.remove(c, d) : r.removeEvent(c, d, b.handle);
                        c[V.expando] = void 0
                    }
                    c[W.expando] && (c[W.expando] = void 0)
                }
        }
    }), r.fn.extend({
        detach: function(a) {
            return Ja(this, a, !0)
        },
        remove: function(a) {
            return Ja(this, a)
        },
        text: function(a) {
            return S(this, function(a) {
                return void 0 === a ? r.text(this) : this.empty().each(function() {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = a)
                })
            }, null, a, arguments.length)
        },
        append: function() {
            return Ia(this, arguments, function(a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = Da(this, a);
                    b.appendChild(a)
                }
            })
        },
        prepend: function() {
            return Ia(this, arguments, function(a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = Da(this, a);
                    b.insertBefore(a, b.firstChild)
                }
            })
        },
        before: function() {
            return Ia(this, arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this)
            })
        },
        after: function() {
            return Ia(this, arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this.nextSibling)
            })
        },
        empty: function() {
            for (var a, b = 0; null != (a = this[b]); b++) 1 === a.nodeType && (r.cleanData(ma(a, !1)), a.textContent = "");
            return this
        },
        clone: function(a, b) {
            return a = null != a && a, b = null == b ? a : b, this.map(function() {
                return r.clone(this, a, b)
            })
        },
        html: function(a) {
            return S(this, function(a) {
                var b = this[0] || {},
                    c = 0,
                    d = this.length;
                if (void 0 === a && 1 === b.nodeType) return b.innerHTML;
                if ("string" == typeof a && !za.test(a) && !la[(ja.exec(a) || ["", ""])[1].toLowerCase()]) {
                    a = r.htmlPrefilter(a);
                    try {
                        for (; c < d; c++) b = this[c] || {}, 1 === b.nodeType && (r.cleanData(ma(b, !1)), b.innerHTML = a);
                        b = 0
                    } catch (e) {}
                }
                b && this.empty().append(a)
            }, null, a, arguments.length)
        },
        replaceWith: function() {
            var a = [];
            return Ia(this, arguments, function(b) {
                var c = this.parentNode;
                r.inArray(this, a) < 0 && (r.cleanData(ma(this)), c && c.replaceChild(b, this))
            }, a)
        }
    }), r.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(a, b) {
        r.fn[a] = function(a) {
            for (var c, d = [], e = r(a), f = e.length - 1, g = 0; g <= f; g++) c = g === f ? this : this.clone(!0), r(e[g])[b](c), h.apply(d, c.get());
            return this.pushStack(d)
        }
    });
    var Ka = /^margin/,
        La = new RegExp("^(" + _ + ")(?!px)[a-z%]+$", "i"),
        Ma = function(b) {
            var c = b.ownerDocument.defaultView;
            return c && c.opener || (c = a), c.getComputedStyle(b)
        };
    ! function() {
        function b() {
            if (i) {
                i.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", i.innerHTML = "", qa.appendChild(h);
                var b = a.getComputedStyle(i);
                c = "1%" !== b.top, g = "2px" === b.marginLeft, e = "4px" === b.width, i.style.marginRight = "50%", f = "4px" === b.marginRight, qa.removeChild(h), i = null
            }
        }
        var c, e, f, g, h = d.createElement("div"),
            i = d.createElement("div");
        i.style && (i.style.backgroundClip = "content-box", i.cloneNode(!0).style.backgroundClip = "", o.clearCloneStyle = "content-box" === i.style.backgroundClip, h.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", h.appendChild(i), r.extend(o, {
            pixelPosition: function() {
                return b(), c
            },
            boxSizingReliable: function() {
                return b(), e
            },
            pixelMarginRight: function() {
                return b(), f
            },
            reliableMarginLeft: function() {
                return b(), g
            }
        }))
    }();

    function Na(a, b, c) {
        var d, e, f, g, h = a.style;
        return c = c || Ma(a), c && (g = c.getPropertyValue(b) || c[b], "" !== g || r.contains(a.ownerDocument, a) || (g = r.style(a, b)), !o.pixelMarginRight() && La.test(g) && Ka.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f)), void 0 !== g ? g + "" : g
    }

    function Oa(a, b) {
        return {
            get: function() {
                return a() ? void delete this.get : (this.get = b).apply(this, arguments)
            }
        }
    }
    var Pa = /^(none|table(?!-c[ea]).+)/,
        Qa = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        Ra = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        Sa = ["Webkit", "Moz", "ms"],
        Ta = d.createElement("div").style;

    function Ua(a) {
        if (a in Ta) return a;
        var b = a[0].toUpperCase() + a.slice(1),
            c = Sa.length;
        while (c--)
            if (a = Sa[c] + b, a in Ta) return a
    }

    function Va(a, b, c) {
        var d = aa.exec(b);
        return d ? Math.max(0, d[2] - (c || 0)) + (d[3] || "px") : b
    }

    function Wa(a, b, c, d, e) {
        var f, g = 0;
        for (f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0; f < 4; f += 2) "margin" === c && (g += r.css(a, c + ba[f], !0, e)), d ? ("content" === c && (g -= r.css(a, "padding" + ba[f], !0, e)), "margin" !== c && (g -= r.css(a, "border" + ba[f] + "Width", !0, e))) : (g += r.css(a, "padding" + ba[f], !0, e), "padding" !== c && (g += r.css(a, "border" + ba[f] + "Width", !0, e)));
        return g
    }

    function Xa(a, b, c) {
        var d, e = !0,
            f = Ma(a),
            g = "border-box" === r.css(a, "boxSizing", !1, f);
        if (a.getClientRects().length && (d = a.getBoundingClientRect()[b]), d <= 0 || null == d) {
            if (d = Na(a, b, f), (d < 0 || null == d) && (d = a.style[b]), La.test(d)) return d;
            e = g && (o.boxSizingReliable() || d === a.style[b]), d = parseFloat(d) || 0
        }
        return d + Wa(a, b, c || (g ? "border" : "content"), e, f) + "px"
    }
    r.extend({
        cssHooks: {
            opacity: {
                get: function(a, b) {
                    if (b) {
                        var c = Na(a, "opacity");
                        return "" === c ? "1" : c
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": "cssFloat"
        },
        style: function(a, b, c, d) {
            if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
                var e, f, g, h = r.camelCase(b),
                    i = a.style;
                return b = r.cssProps[h] || (r.cssProps[h] = Ua(h) || h), g = r.cssHooks[b] || r.cssHooks[h], void 0 === c ? g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : i[b] : (f = typeof c, "string" === f && (e = aa.exec(c)) && e[1] && (c = ea(a, b, e), f = "number"), null != c && c === c && ("number" === f && (c += e && e[3] || (r.cssNumber[h] ? "" : "px")), o.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (i[b] = "inherit"), g && "set" in g && void 0 === (c = g.set(a, c, d)) || (i[b] = c)), void 0)
            }
        },
        css: function(a, b, c, d) {
            var e, f, g, h = r.camelCase(b);
            return b = r.cssProps[h] || (r.cssProps[h] = Ua(h) || h), g = r.cssHooks[b] || r.cssHooks[h], g && "get" in g && (e = g.get(a, !0, c)), void 0 === e && (e = Na(a, b, d)), "normal" === e && b in Ra && (e = Ra[b]), "" === c || c ? (f = parseFloat(e), c === !0 || isFinite(f) ? f || 0 : e) : e
        }
    }), r.each(["height", "width"], function(a, b) {
        r.cssHooks[b] = {
            get: function(a, c, d) {
                if (c) return !Pa.test(r.css(a, "display")) || a.getClientRects().length && a.getBoundingClientRect().width ? Xa(a, b, d) : da(a, Qa, function() {
                    return Xa(a, b, d)
                })
            },
            set: function(a, c, d) {
                var e, f = d && Ma(a),
                    g = d && Wa(a, b, d, "border-box" === r.css(a, "boxSizing", !1, f), f);
                return g && (e = aa.exec(c)) && "px" !== (e[3] || "px") && (a.style[b] = c, c = r.css(a, b)), Va(a, c, g)
            }
        }
    }), r.cssHooks.marginLeft = Oa(o.reliableMarginLeft, function(a, b) {
        if (b) return (parseFloat(Na(a, "marginLeft")) || a.getBoundingClientRect().left - da(a, {
            marginLeft: 0
        }, function() {
            return a.getBoundingClientRect().left
        })) + "px"
    }), r.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(a, b) {
        r.cssHooks[a + b] = {
            expand: function(c) {
                for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; d < 4; d++) e[a + ba[d] + b] = f[d] || f[d - 2] || f[0];
                return e
            }
        }, Ka.test(a) || (r.cssHooks[a + b].set = Va)
    }), r.fn.extend({
        css: function(a, b) {
            return S(this, function(a, b, c) {
                var d, e, f = {},
                    g = 0;
                if (r.isArray(b)) {
                    for (d = Ma(a), e = b.length; g < e; g++) f[b[g]] = r.css(a, b[g], !1, d);
                    return f
                }
                return void 0 !== c ? r.style(a, b, c) : r.css(a, b)
            }, a, b, arguments.length > 1)
        }
    });

    function Ya(a, b, c, d, e) {
        return new Ya.prototype.init(a, b, c, d, e)
    }
    r.Tween = Ya, Ya.prototype = {
        constructor: Ya,
        init: function(a, b, c, d, e, f) {
            this.elem = a, this.prop = c, this.easing = e || r.easing._default, this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (r.cssNumber[c] ? "" : "px")
        },
        cur: function() {
            var a = Ya.propHooks[this.prop];
            return a && a.get ? a.get(this) : Ya.propHooks._default.get(this)
        },
        run: function(a) {
            var b, c = Ya.propHooks[this.prop];
            return this.options.duration ? this.pos = b = r.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : this.pos = b = a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : Ya.propHooks._default.set(this), this
        }
    }, Ya.prototype.init.prototype = Ya.prototype, Ya.propHooks = {
        _default: {
            get: function(a) {
                var b;
                return 1 !== a.elem.nodeType || null != a.elem[a.prop] && null == a.elem.style[a.prop] ? a.elem[a.prop] : (b = r.css(a.elem, a.prop, ""), b && "auto" !== b ? b : 0)
            },
            set: function(a) {
                r.fx.step[a.prop] ? r.fx.step[a.prop](a) : 1 !== a.elem.nodeType || null == a.elem.style[r.cssProps[a.prop]] && !r.cssHooks[a.prop] ? a.elem[a.prop] = a.now : r.style(a.elem, a.prop, a.now + a.unit)
            }
        }
    }, Ya.propHooks.scrollTop = Ya.propHooks.scrollLeft = {
        set: function(a) {
            a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now)
        }
    }, r.easing = {
        linear: function(a) {
            return a
        },
        swing: function(a) {
            return .5 - Math.cos(a * Math.PI) / 2
        },
        _default: "swing"
    }, r.fx = Ya.prototype.init, r.fx.step = {};
    var Za, $a, _a = /^(?:toggle|show|hide)$/,
        ab = /queueHooks$/;

    function bb() {
        $a && (a.requestAnimationFrame(bb), r.fx.tick())
    }

    function cb() {
        return a.setTimeout(function() {
            Za = void 0
        }), Za = r.now()
    }

    function db(a, b) {
        var c, d = 0,
            e = {
                height: a
            };
        for (b = b ? 1 : 0; d < 4; d += 2 - b) c = ba[d], e["margin" + c] = e["padding" + c] = a;
        return b && (e.opacity = e.width = a), e
    }

    function eb(a, b, c) {
        for (var d, e = (hb.tweeners[b] || []).concat(hb.tweeners["*"]), f = 0, g = e.length; f < g; f++)
            if (d = e[f].call(c, b, a)) return d
    }

    function fb(a, b, c) {
        var d, e, f, g, h, i, j, k, l = "width" in b || "height" in b,
            m = this,
            n = {},
            o = a.style,
            p = a.nodeType && ca(a),
            q = V.get(a, "fxshow");
        c.queue || (g = r._queueHooks(a, "fx"), null == g.unqueued && (g.unqueued = 0, h = g.empty.fire, g.empty.fire = function() {
            g.unqueued || h()
        }), g.unqueued++, m.always(function() {
            m.always(function() {
                g.unqueued--, r.queue(a, "fx").length || g.empty.fire()
            })
        }));
        for (d in b)
            if (e = b[d], _a.test(e)) {
                if (delete b[d], f = f || "toggle" === e, e === (p ? "hide" : "show")) {
                    if ("show" !== e || !q || void 0 === q[d]) continue;
                    p = !0
                }
                n[d] = q && q[d] || r.style(a, d)
            }
        if (i = !r.isEmptyObject(b), i || !r.isEmptyObject(n)) {
            l && 1 === a.nodeType && (c.overflow = [o.overflow, o.overflowX, o.overflowY], j = q && q.display, null == j && (j = V.get(a, "display")), k = r.css(a, "display"), "none" === k && (j ? k = j : (ha([a], !0), j = a.style.display || j, k = r.css(a, "display"), ha([a]))), ("inline" === k || "inline-block" === k && null != j) && "none" === r.css(a, "float") && (i || (m.done(function() {
                o.display = j
            }), null == j && (k = o.display, j = "none" === k ? "" : k)), o.display = "inline-block")), c.overflow && (o.overflow = "hidden", m.always(function() {
                o.overflow = c.overflow[0], o.overflowX = c.overflow[1], o.overflowY = c.overflow[2]
            })), i = !1;
            for (d in n) i || (q ? "hidden" in q && (p = q.hidden) : q = V.access(a, "fxshow", {
                display: j
            }), f && (q.hidden = !p), p && ha([a], !0), m.done(function() {
                p || ha([a]), V.remove(a, "fxshow");
                for (d in n) r.style(a, d, n[d])
            })), i = eb(p ? q[d] : 0, d, m), d in q || (q[d] = i.start, p && (i.end = i.start, i.start = 0))
        }
    }

    function gb(a, b) {
        var c, d, e, f, g;
        for (c in a)
            if (d = r.camelCase(c), e = b[d], f = a[c], r.isArray(f) && (e = f[1], f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), g = r.cssHooks[d], g && "expand" in g) {
                f = g.expand(f), delete a[d];
                for (c in f) c in a || (a[c] = f[c], b[c] = e)
            } else b[d] = e
    }

    function hb(a, b, c) {
        var d, e, f = 0,
            g = hb.prefilters.length,
            h = r.Deferred().always(function() {
                delete i.elem
            }),
            i = function() {
                if (e) return !1;
                for (var b = Za || cb(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; g < i; g++) j.tweens[g].run(f);
                return h.notifyWith(a, [j, f, c]), f < 1 && i ? c : (h.resolveWith(a, [j]), !1)
            },
            j = h.promise({
                elem: a,
                props: r.extend({}, b),
                opts: r.extend(!0, {
                    specialEasing: {},
                    easing: r.easing._default
                }, c),
                originalProperties: b,
                originalOptions: c,
                startTime: Za || cb(),
                duration: c.duration,
                tweens: [],
                createTween: function(b, c) {
                    var d = r.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
                    return j.tweens.push(d), d
                },
                stop: function(b) {
                    var c = 0,
                        d = b ? j.tweens.length : 0;
                    if (e) return this;
                    for (e = !0; c < d; c++) j.tweens[c].run(1);
                    return b ? (h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j, b])) : h.rejectWith(a, [j, b]), this
                }
            }),
            k = j.props;
        for (gb(k, j.opts.specialEasing); f < g; f++)
            if (d = hb.prefilters[f].call(j, a, k, j.opts)) return r.isFunction(d.stop) && (r._queueHooks(j.elem, j.opts.queue).stop = r.proxy(d.stop, d)), d;
        return r.map(k, eb, j), r.isFunction(j.opts.start) && j.opts.start.call(a, j), r.fx.timer(r.extend(i, {
            elem: a,
            anim: j,
            queue: j.opts.queue
        })), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always)
    }
    r.Animation = r.extend(hb, {
            tweeners: {
                "*": [function(a, b) {
                    var c = this.createTween(a, b);
                    return ea(c.elem, a, aa.exec(b), c), c
                }]
            },
            tweener: function(a, b) {
                r.isFunction(a) ? (b = a, a = ["*"]) : a = a.match(K);
                for (var c, d = 0, e = a.length; d < e; d++) c = a[d], hb.tweeners[c] = hb.tweeners[c] || [], hb.tweeners[c].unshift(b)
            },
            prefilters: [fb],
            prefilter: function(a, b) {
                b ? hb.prefilters.unshift(a) : hb.prefilters.push(a)
            }
        }), r.speed = function(a, b, c) {
            var e = a && "object" == typeof a ? r.extend({}, a) : {
                complete: c || !c && b || r.isFunction(a) && a,
                duration: a,
                easing: c && b || b && !r.isFunction(b) && b
            };
            return r.fx.off || d.hidden ? e.duration = 0 : "number" != typeof e.duration && (e.duration in r.fx.speeds ? e.duration = r.fx.speeds[e.duration] : e.duration = r.fx.speeds._default), null != e.queue && e.queue !== !0 || (e.queue = "fx"), e.old = e.complete, e.complete = function() {
                r.isFunction(e.old) && e.old.call(this), e.queue && r.dequeue(this, e.queue)
            }, e
        }, r.fn.extend({
            fadeTo: function(a, b, c, d) {
                return this.filter(ca).css("opacity", 0).show().end().animate({
                    opacity: b
                }, a, c, d)
            },
            animate: function(a, b, c, d) {
                var e = r.isEmptyObject(a),
                    f = r.speed(b, c, d),
                    g = function() {
                        var b = hb(this, r.extend({}, a), f);
                        (e || V.get(this, "finish")) && b.stop(!0)
                    };
                return g.finish = g, e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g)
            },
            stop: function(a, b, c) {
                var d = function(a) {
                    var b = a.stop;
                    delete a.stop, b(c)
                };
                return "string" != typeof a && (c = b, b = a, a = void 0), b && a !== !1 && this.queue(a || "fx", []), this.each(function() {
                    var b = !0,
                        e = null != a && a + "queueHooks",
                        f = r.timers,
                        g = V.get(this);
                    if (e) g[e] && g[e].stop && d(g[e]);
                    else
                        for (e in g) g[e] && g[e].stop && ab.test(e) && d(g[e]);
                    for (e = f.length; e--;) f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), b = !1, f.splice(e, 1));
                    !b && c || r.dequeue(this, a)
                })
            },
            finish: function(a) {
                return a !== !1 && (a = a || "fx"), this.each(function() {
                    var b, c = V.get(this),
                        d = c[a + "queue"],
                        e = c[a + "queueHooks"],
                        f = r.timers,
                        g = d ? d.length : 0;
                    for (c.finish = !0, r.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--;) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));
                    for (b = 0; b < g; b++) d[b] && d[b].finish && d[b].finish.call(this);
                    delete c.finish
                })
            }
        }), r.each(["toggle", "show", "hide"], function(a, b) {
            var c = r.fn[b];
            r.fn[b] = function(a, d, e) {
                return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(db(b, !0), a, d, e)
            }
        }), r.each({
            slideDown: db("show"),
            slideUp: db("hide"),
            slideToggle: db("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function(a, b) {
            r.fn[a] = function(a, c, d) {
                return this.animate(b, a, c, d)
            }
        }), r.timers = [], r.fx.tick = function() {
            var a, b = 0,
                c = r.timers;
            for (Za = r.now(); b < c.length; b++) a = c[b], a() || c[b] !== a || c.splice(b--, 1);
            c.length || r.fx.stop(), Za = void 0
        }, r.fx.timer = function(a) {
            r.timers.push(a), a() ? r.fx.start() : r.timers.pop()
        }, r.fx.interval = 13, r.fx.start = function() {
            $a || ($a = a.requestAnimationFrame ? a.requestAnimationFrame(bb) : a.setInterval(r.fx.tick, r.fx.interval))
        }, r.fx.stop = function() {
            a.cancelAnimationFrame ? a.cancelAnimationFrame($a) : a.clearInterval($a), $a = null
        }, r.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        }, r.fn.delay = function(b, c) {
            return b = r.fx ? r.fx.speeds[b] || b : b, c = c || "fx", this.queue(c, function(c, d) {
                var e = a.setTimeout(c, b);
                d.stop = function() {
                    a.clearTimeout(e)
                }
            })
        },
        function() {
            var a = d.createElement("input"),
                b = d.createElement("select"),
                c = b.appendChild(d.createElement("option"));
            a.type = "checkbox", o.checkOn = "" !== a.value, o.optSelected = c.selected, a = d.createElement("input"), a.value = "t", a.type = "radio", o.radioValue = "t" === a.value
        }();
    var ib, jb = r.expr.attrHandle;
    r.fn.extend({
        attr: function(a, b) {
            return S(this, r.attr, a, b, arguments.length > 1)
        },
        removeAttr: function(a) {
            return this.each(function() {
                r.removeAttr(this, a)
            })
        }
    }), r.extend({
        attr: function(a, b, c) {
            var d, e, f = a.nodeType;
            if (3 !== f && 8 !== f && 2 !== f) return "undefined" == typeof a.getAttribute ? r.prop(a, b, c) : (1 === f && r.isXMLDoc(a) || (e = r.attrHooks[b.toLowerCase()] || (r.expr.match.bool.test(b) ? ib : void 0)),
                void 0 !== c ? null === c ? void r.removeAttr(a, b) : e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : (a.setAttribute(b, c + ""), c) : e && "get" in e && null !== (d = e.get(a, b)) ? d : (d = r.find.attr(a, b), null == d ? void 0 : d))
        },
        attrHooks: {
            type: {
                set: function(a, b) {
                    if (!o.radioValue && "radio" === b && r.nodeName(a, "input")) {
                        var c = a.value;
                        return a.setAttribute("type", b), c && (a.value = c), b
                    }
                }
            }
        },
        removeAttr: function(a, b) {
            var c, d = 0,
                e = b && b.match(K);
            if (e && 1 === a.nodeType)
                while (c = e[d++]) a.removeAttribute(c)
        }
    }), ib = {
        set: function(a, b, c) {
            return b === !1 ? r.removeAttr(a, c) : a.setAttribute(c, c), c
        }
    }, r.each(r.expr.match.bool.source.match(/\w+/g), function(a, b) {
        var c = jb[b] || r.find.attr;
        jb[b] = function(a, b, d) {
            var e, f, g = b.toLowerCase();
            return d || (f = jb[g], jb[g] = e, e = null != c(a, b, d) ? g : null, jb[g] = f), e
        }
    });
    var kb = /^(?:input|select|textarea|button)$/i,
        lb = /^(?:a|area)$/i;
    r.fn.extend({
        prop: function(a, b) {
            return S(this, r.prop, a, b, arguments.length > 1)
        },
        removeProp: function(a) {
            return this.each(function() {
                delete this[r.propFix[a] || a]
            })
        }
    }), r.extend({
        prop: function(a, b, c) {
            var d, e, f = a.nodeType;
            if (3 !== f && 8 !== f && 2 !== f) return 1 === f && r.isXMLDoc(a) || (b = r.propFix[b] || b, e = r.propHooks[b]), void 0 !== c ? e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b]
        },
        propHooks: {
            tabIndex: {
                get: function(a) {
                    var b = r.find.attr(a, "tabindex");
                    return b ? parseInt(b, 10) : kb.test(a.nodeName) || lb.test(a.nodeName) && a.href ? 0 : -1
                }
            }
        },
        propFix: {
            "for": "htmlFor",
            "class": "className"
        }
    }), o.optSelected || (r.propHooks.selected = {
        get: function(a) {
            var b = a.parentNode;
            return b && b.parentNode && b.parentNode.selectedIndex, null
        },
        set: function(a) {
            var b = a.parentNode;
            b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex)
        }
    }), r.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        r.propFix[this.toLowerCase()] = this
    });

    function mb(a) {
        var b = a.match(K) || [];
        return b.join(" ")
    }

    function nb(a) {
        return a.getAttribute && a.getAttribute("class") || ""
    }
    r.fn.extend({
        addClass: function(a) {
            var b, c, d, e, f, g, h, i = 0;
            if (r.isFunction(a)) return this.each(function(b) {
                r(this).addClass(a.call(this, b, nb(this)))
            });
            if ("string" == typeof a && a) {
                b = a.match(K) || [];
                while (c = this[i++])
                    if (e = nb(c), d = 1 === c.nodeType && " " + mb(e) + " ") {
                        g = 0;
                        while (f = b[g++]) d.indexOf(" " + f + " ") < 0 && (d += f + " ");
                        h = mb(d), e !== h && c.setAttribute("class", h)
                    }
            }
            return this
        },
        removeClass: function(a) {
            var b, c, d, e, f, g, h, i = 0;
            if (r.isFunction(a)) return this.each(function(b) {
                r(this).removeClass(a.call(this, b, nb(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ("string" == typeof a && a) {
                b = a.match(K) || [];
                while (c = this[i++])
                    if (e = nb(c), d = 1 === c.nodeType && " " + mb(e) + " ") {
                        g = 0;
                        while (f = b[g++])
                            while (d.indexOf(" " + f + " ") > -1) d = d.replace(" " + f + " ", " ");
                        h = mb(d), e !== h && c.setAttribute("class", h)
                    }
            }
            return this
        },
        toggleClass: function(a, b) {
            var c = typeof a;
            return "boolean" == typeof b && "string" === c ? b ? this.addClass(a) : this.removeClass(a) : r.isFunction(a) ? this.each(function(c) {
                r(this).toggleClass(a.call(this, c, nb(this), b), b)
            }) : this.each(function() {
                var b, d, e, f;
                if ("string" === c) {
                    d = 0, e = r(this), f = a.match(K) || [];
                    while (b = f[d++]) e.hasClass(b) ? e.removeClass(b) : e.addClass(b)
                } else void 0 !== a && "boolean" !== c || (b = nb(this), b && V.set(this, "__className__", b), this.setAttribute && this.setAttribute("class", b || a === !1 ? "" : V.get(this, "__className__") || ""))
            })
        },
        hasClass: function(a) {
            var b, c, d = 0;
            b = " " + a + " ";
            while (c = this[d++])
                if (1 === c.nodeType && (" " + mb(nb(c)) + " ").indexOf(b) > -1) return !0;
            return !1
        }
    });
    var ob = /\r/g;
    r.fn.extend({
        val: function(a) {
            var b, c, d, e = this[0]; {
                if (arguments.length) return d = r.isFunction(a), this.each(function(c) {
                    var e;
                    1 === this.nodeType && (e = d ? a.call(this, c, r(this).val()) : a, null == e ? e = "" : "number" == typeof e ? e += "" : r.isArray(e) && (e = r.map(e, function(a) {
                        return null == a ? "" : a + ""
                    })), b = r.valHooks[this.type] || r.valHooks[this.nodeName.toLowerCase()], b && "set" in b && void 0 !== b.set(this, e, "value") || (this.value = e))
                });
                if (e) return b = r.valHooks[e.type] || r.valHooks[e.nodeName.toLowerCase()], b && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : (c = e.value, "string" == typeof c ? c.replace(ob, "") : null == c ? "" : c)
            }
        }
    }), r.extend({
        valHooks: {
            option: {
                get: function(a) {
                    var b = r.find.attr(a, "value");
                    return null != b ? b : mb(r.text(a))
                }
            },
            select: {
                get: function(a) {
                    var b, c, d, e = a.options,
                        f = a.selectedIndex,
                        g = "select-one" === a.type,
                        h = g ? null : [],
                        i = g ? f + 1 : e.length;
                    for (d = f < 0 ? i : g ? f : 0; d < i; d++)
                        if (c = e[d], (c.selected || d === f) && !c.disabled && (!c.parentNode.disabled || !r.nodeName(c.parentNode, "optgroup"))) {
                            if (b = r(c).val(), g) return b;
                            h.push(b)
                        }
                    return h
                },
                set: function(a, b) {
                    var c, d, e = a.options,
                        f = r.makeArray(b),
                        g = e.length;
                    while (g--) d = e[g], (d.selected = r.inArray(r.valHooks.option.get(d), f) > -1) && (c = !0);
                    return c || (a.selectedIndex = -1), f
                }
            }
        }
    }), r.each(["radio", "checkbox"], function() {
        r.valHooks[this] = {
            set: function(a, b) {
                if (r.isArray(b)) return a.checked = r.inArray(r(a).val(), b) > -1
            }
        }, o.checkOn || (r.valHooks[this].get = function(a) {
            return null === a.getAttribute("value") ? "on" : a.value
        })
    });
    var pb = /^(?:focusinfocus|focusoutblur)$/;
    r.extend(r.event, {
        trigger: function(b, c, e, f) {
            var g, h, i, j, k, m, n, o = [e || d],
                p = l.call(b, "type") ? b.type : b,
                q = l.call(b, "namespace") ? b.namespace.split(".") : [];
            if (h = i = e = e || d, 3 !== e.nodeType && 8 !== e.nodeType && !pb.test(p + r.event.triggered) && (p.indexOf(".") > -1 && (q = p.split("."), p = q.shift(), q.sort()), k = p.indexOf(":") < 0 && "on" + p, b = b[r.expando] ? b : new r.Event(p, "object" == typeof b && b), b.isTrigger = f ? 2 : 3, b.namespace = q.join("."), b.rnamespace = b.namespace ? new RegExp("(^|\\.)" + q.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, b.result = void 0, b.target || (b.target = e), c = null == c ? [b] : r.makeArray(c, [b]), n = r.event.special[p] || {}, f || !n.trigger || n.trigger.apply(e, c) !== !1)) {
                if (!f && !n.noBubble && !r.isWindow(e)) {
                    for (j = n.delegateType || p, pb.test(j + p) || (h = h.parentNode); h; h = h.parentNode) o.push(h), i = h;
                    i === (e.ownerDocument || d) && o.push(i.defaultView || i.parentWindow || a)
                }
                g = 0;
                while ((h = o[g++]) && !b.isPropagationStopped()) b.type = g > 1 ? j : n.bindType || p, m = (V.get(h, "events") || {})[b.type] && V.get(h, "handle"), m && m.apply(h, c), m = k && h[k], m && m.apply && T(h) && (b.result = m.apply(h, c), b.result === !1 && b.preventDefault());
                return b.type = p, f || b.isDefaultPrevented() || n._default && n._default.apply(o.pop(), c) !== !1 || !T(e) || k && r.isFunction(e[p]) && !r.isWindow(e) && (i = e[k], i && (e[k] = null), r.event.triggered = p, e[p](), r.event.triggered = void 0, i && (e[k] = i)), b.result
            }
        },
        simulate: function(a, b, c) {
            var d = r.extend(new r.Event, c, {
                type: a,
                isSimulated: !0
            });
            r.event.trigger(d, null, b)
        }
    }), r.fn.extend({
        trigger: function(a, b) {
            return this.each(function() {
                r.event.trigger(a, b, this)
            })
        },
        triggerHandler: function(a, b) {
            var c = this[0];
            if (c) return r.event.trigger(a, b, c, !0)
        }
    }), r.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(a, b) {
        r.fn[b] = function(a, c) {
            return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b)
        }
    }), r.fn.extend({
        hover: function(a, b) {
            return this.mouseenter(a).mouseleave(b || a)
        }
    }), o.focusin = "onfocusin" in a, o.focusin || r.each({
        focus: "focusin",
        blur: "focusout"
    }, function(a, b) {
        var c = function(a) {
            r.event.simulate(b, a.target, r.event.fix(a))
        };
        r.event.special[b] = {
            setup: function() {
                var d = this.ownerDocument || this,
                    e = V.access(d, b);
                e || d.addEventListener(a, c, !0), V.access(d, b, (e || 0) + 1)
            },
            teardown: function() {
                var d = this.ownerDocument || this,
                    e = V.access(d, b) - 1;
                e ? V.access(d, b, e) : (d.removeEventListener(a, c, !0), V.remove(d, b))
            }
        }
    });
    var qb = a.location,
        rb = r.now(),
        sb = /\?/;
    r.parseXML = function(b) {
        var c;
        if (!b || "string" != typeof b) return null;
        try {
            c = (new a.DOMParser).parseFromString(b, "text/xml")
        } catch (d) {
            c = void 0
        }
        return c && !c.getElementsByTagName("parsererror").length || r.error("Invalid XML: " + b), c
    };
    var tb = /\[\]$/,
        ub = /\r?\n/g,
        vb = /^(?:submit|button|image|reset|file)$/i,
        wb = /^(?:input|select|textarea|keygen)/i;

    function xb(a, b, c, d) {
        var e;
        if (r.isArray(b)) r.each(b, function(b, e) {
            c || tb.test(a) ? d(a, e) : xb(a + "[" + ("object" == typeof e && null != e ? b : "") + "]", e, c, d)
        });
        else if (c || "object" !== r.type(b)) d(a, b);
        else
            for (e in b) xb(a + "[" + e + "]", b[e], c, d)
    }
    r.param = function(a, b) {
        var c, d = [],
            e = function(a, b) {
                var c = r.isFunction(b) ? b() : b;
                d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(null == c ? "" : c)
            };
        if (r.isArray(a) || a.jquery && !r.isPlainObject(a)) r.each(a, function() {
            e(this.name, this.value)
        });
        else
            for (c in a) xb(c, a[c], b, e);
        return d.join("&")
    }, r.fn.extend({
        serialize: function() {
            return r.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var a = r.prop(this, "elements");
                return a ? r.makeArray(a) : this
            }).filter(function() {
                var a = this.type;
                return this.name && !r(this).is(":disabled") && wb.test(this.nodeName) && !vb.test(a) && (this.checked || !ia.test(a))
            }).map(function(a, b) {
                var c = r(this).val();
                return null == c ? null : r.isArray(c) ? r.map(c, function(a) {
                    return {
                        name: b.name,
                        value: a.replace(ub, "\r\n")
                    }
                }) : {
                    name: b.name,
                    value: c.replace(ub, "\r\n")
                }
            }).get()
        }
    });
    var yb = /%20/g,
        zb = /#.*$/,
        Ab = /([?&])_=[^&]*/,
        Bb = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        Cb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Db = /^(?:GET|HEAD)$/,
        Eb = /^\/\//,
        Fb = {},
        Gb = {},
        Hb = "*/".concat("*"),
        Ib = d.createElement("a");
    Ib.href = qb.href;

    function Jb(a) {
        return function(b, c) {
            "string" != typeof b && (c = b, b = "*");
            var d, e = 0,
                f = b.toLowerCase().match(K) || [];
            if (r.isFunction(c))
                while (d = f[e++]) "+" === d[0] ? (d = d.slice(1) || "*", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c)
        }
    }

    function Kb(a, b, c, d) {
        var e = {},
            f = a === Gb;

        function g(h) {
            var i;
            return e[h] = !0, r.each(a[h] || [], function(a, h) {
                var j = h(b, c, d);
                return "string" != typeof j || f || e[j] ? f ? !(i = j) : void 0 : (b.dataTypes.unshift(j), g(j), !1)
            }), i
        }
        return g(b.dataTypes[0]) || !e["*"] && g("*")
    }

    function Lb(a, b) {
        var c, d, e = r.ajaxSettings.flatOptions || {};
        for (c in b) void 0 !== b[c] && ((e[c] ? a : d || (d = {}))[c] = b[c]);
        return d && r.extend(!0, a, d), a
    }

    function Mb(a, b, c) {
        var d, e, f, g, h = a.contents,
            i = a.dataTypes;
        while ("*" === i[0]) i.shift(), void 0 === d && (d = a.mimeType || b.getResponseHeader("Content-Type"));
        if (d)
            for (e in h)
                if (h[e] && h[e].test(d)) {
                    i.unshift(e);
                    break
                }
        if (i[0] in c) f = i[0];
        else {
            for (e in c) {
                if (!i[0] || a.converters[e + " " + i[0]]) {
                    f = e;
                    break
                }
                g || (g = e)
            }
            f = f || g
        }
        if (f) return f !== i[0] && i.unshift(f), c[f]
    }

    function Nb(a, b, c, d) {
        var e, f, g, h, i, j = {},
            k = a.dataTypes.slice();
        if (k[1])
            for (g in a.converters) j[g.toLowerCase()] = a.converters[g];
        f = k.shift();
        while (f)
            if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift())
                if ("*" === f) f = i;
                else if ("*" !== i && i !== f) {
            if (g = j[i + " " + f] || j["* " + f], !g)
                for (e in j)
                    if (h = e.split(" "), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) {
                        g === !0 ? g = j[e] : j[e] !== !0 && (f = h[0], k.unshift(h[1]));
                        break
                    }
            if (g !== !0)
                if (g && a["throws"]) b = g(b);
                else try {
                    b = g(b)
                } catch (l) {
                    return {
                        state: "parsererror",
                        error: g ? l : "No conversion from " + i + " to " + f
                    }
                }
        }
        return {
            state: "success",
            data: b
        }
    }
    r.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: qb.href,
            type: "GET",
            isLocal: Cb.test(qb.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Hb,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": JSON.parse,
                "text xml": r.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(a, b) {
            return b ? Lb(Lb(a, r.ajaxSettings), b) : Lb(r.ajaxSettings, a)
        },
        ajaxPrefilter: Jb(Fb),
        ajaxTransport: Jb(Gb),
        ajax: function(b, c) {
            "object" == typeof b && (c = b, b = void 0), c = c || {};
            var e, f, g, h, i, j, k, l, m, n, o = r.ajaxSetup({}, c),
                p = o.context || o,
                q = o.context && (p.nodeType || p.jquery) ? r(p) : r.event,
                s = r.Deferred(),
                t = r.Callbacks("once memory"),
                u = o.statusCode || {},
                v = {},
                w = {},
                x = "canceled",
                y = {
                    readyState: 0,
                    getResponseHeader: function(a) {
                        var b;
                        if (k) {
                            if (!h) {
                                h = {};
                                while (b = Bb.exec(g)) h[b[1].toLowerCase()] = b[2]
                            }
                            b = h[a.toLowerCase()]
                        }
                        return null == b ? null : b
                    },
                    getAllResponseHeaders: function() {
                        return k ? g : null
                    },
                    setRequestHeader: function(a, b) {
                        return null == k && (a = w[a.toLowerCase()] = w[a.toLowerCase()] || a, v[a] = b), this
                    },
                    overrideMimeType: function(a) {
                        return null == k && (o.mimeType = a), this
                    },
                    statusCode: function(a) {
                        var b;
                        if (a)
                            if (k) y.always(a[y.status]);
                            else
                                for (b in a) u[b] = [u[b], a[b]];
                        return this
                    },
                    abort: function(a) {
                        var b = a || x;
                        return e && e.abort(b), A(0, b), this
                    }
                };
            if (s.promise(y), o.url = ((b || o.url || qb.href) + "").replace(Eb, qb.protocol + "//"), o.type = c.method || c.type || o.method || o.type, o.dataTypes = (o.dataType || "*").toLowerCase().match(K) || [""], null == o.crossDomain) {
                j = d.createElement("a");
                try {
                    j.href = o.url, j.href = j.href, o.crossDomain = Ib.protocol + "//" + Ib.host != j.protocol + "//" + j.host
                } catch (z) {
                    o.crossDomain = !0
                }
            }
            if (o.data && o.processData && "string" != typeof o.data && (o.data = r.param(o.data, o.traditional)), Kb(Fb, o, c, y), k) return y;
            l = r.event && o.global, l && 0 === r.active++ && r.event.trigger("ajaxStart"), o.type = o.type.toUpperCase(), o.hasContent = !Db.test(o.type), f = o.url.replace(zb, ""), o.hasContent ? o.data && o.processData && 0 === (o.contentType || "").indexOf("application/x-www-form-urlencoded") && (o.data = o.data.replace(yb, "+")) : (n = o.url.slice(f.length), o.data && (f += (sb.test(f) ? "&" : "?") + o.data, delete o.data), o.cache === !1 && (f = f.replace(Ab, "$1"), n = (sb.test(f) ? "&" : "?") + "_=" + rb++ + n), o.url = f + n), o.ifModified && (r.lastModified[f] && y.setRequestHeader("If-Modified-Since", r.lastModified[f]), r.etag[f] && y.setRequestHeader("If-None-Match", r.etag[f])), (o.data && o.hasContent && o.contentType !== !1 || c.contentType) && y.setRequestHeader("Content-Type", o.contentType), y.setRequestHeader("Accept", o.dataTypes[0] && o.accepts[o.dataTypes[0]] ? o.accepts[o.dataTypes[0]] + ("*" !== o.dataTypes[0] ? ", " + Hb + "; q=0.01" : "") : o.accepts["*"]);
            for (m in o.headers) y.setRequestHeader(m, o.headers[m]);
            if (o.beforeSend && (o.beforeSend.call(p, y, o) === !1 || k)) return y.abort();
            if (x = "abort", t.add(o.complete), y.done(o.success), y.fail(o.error), e = Kb(Gb, o, c, y)) {
                if (y.readyState = 1, l && q.trigger("ajaxSend", [y, o]), k) return y;
                o.async && o.timeout > 0 && (i = a.setTimeout(function() {
                    y.abort("timeout")
                }, o.timeout));
                try {
                    k = !1, e.send(v, A)
                } catch (z) {
                    if (k) throw z;
                    A(-1, z)
                }
            } else A(-1, "No Transport");

            function A(b, c, d, h) {
                var j, m, n, v, w, x = c;
                k || (k = !0, i && a.clearTimeout(i), e = void 0, g = h || "", y.readyState = b > 0 ? 4 : 0, j = b >= 200 && b < 300 || 304 === b, d && (v = Mb(o, y, d)), v = Nb(o, v, y, j), j ? (o.ifModified && (w = y.getResponseHeader("Last-Modified"), w && (r.lastModified[f] = w), w = y.getResponseHeader("etag"), w && (r.etag[f] = w)), 204 === b || "HEAD" === o.type ? x = "nocontent" : 304 === b ? x = "notmodified" : (x = v.state, m = v.data, n = v.error, j = !n)) : (n = x, !b && x || (x = "error", b < 0 && (b = 0))), y.status = b, y.statusText = (c || x) + "", j ? s.resolveWith(p, [m, x, y]) : s.rejectWith(p, [y, x, n]), y.statusCode(u), u = void 0, l && q.trigger(j ? "ajaxSuccess" : "ajaxError", [y, o, j ? m : n]), t.fireWith(p, [y, x]), l && (q.trigger("ajaxComplete", [y, o]), --r.active || r.event.trigger("ajaxStop")))
            }
            return y
        },
        getJSON: function(a, b, c) {
            return r.get(a, b, c, "json")
        },
        getScript: function(a, b) {
            return r.get(a, void 0, b, "script")
        }
    }), r.each(["get", "post"], function(a, b) {
        r[b] = function(a, c, d, e) {
            return r.isFunction(c) && (e = e || d, d = c, c = void 0), r.ajax(r.extend({
                url: a,
                type: b,
                dataType: e,
                data: c,
                success: d
            }, r.isPlainObject(a) && a))
        }
    }), r._evalUrl = function(a) {
        return r.ajax({
            url: a,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            "throws": !0
        })
    }, r.fn.extend({
        wrapAll: function(a) {
            var b;
            return this[0] && (r.isFunction(a) && (a = a.call(this[0])), b = r(a, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && b.insertBefore(this[0]), b.map(function() {
                var a = this;
                while (a.firstElementChild) a = a.firstElementChild;
                return a
            }).append(this)), this
        },
        wrapInner: function(a) {
            return r.isFunction(a) ? this.each(function(b) {
                r(this).wrapInner(a.call(this, b))
            }) : this.each(function() {
                var b = r(this),
                    c = b.contents();
                c.length ? c.wrapAll(a) : b.append(a)
            })
        },
        wrap: function(a) {
            var b = r.isFunction(a);
            return this.each(function(c) {
                r(this).wrapAll(b ? a.call(this, c) : a)
            })
        },
        unwrap: function(a) {
            return this.parent(a).not("body").each(function() {
                r(this).replaceWith(this.childNodes)
            }), this
        }
    }), r.expr.pseudos.hidden = function(a) {
        return !r.expr.pseudos.visible(a)
    }, r.expr.pseudos.visible = function(a) {
        return !!(a.offsetWidth || a.offsetHeight || a.getClientRects().length)
    }, r.ajaxSettings.xhr = function() {
        try {
            return new a.XMLHttpRequest
        } catch (b) {}
    };
    var Ob = {
            0: 200,
            1223: 204
        },
        Pb = r.ajaxSettings.xhr();
    o.cors = !!Pb && "withCredentials" in Pb, o.ajax = Pb = !!Pb, r.ajaxTransport(function(b) {
        var c, d;
        if (o.cors || Pb && !b.crossDomain) return {
            send: function(e, f) {
                var g, h = b.xhr();
                if (h.open(b.type, b.url, b.async, b.username, b.password), b.xhrFields)
                    for (g in b.xhrFields) h[g] = b.xhrFields[g];
                b.mimeType && h.overrideMimeType && h.overrideMimeType(b.mimeType), b.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest");
                for (g in e) h.setRequestHeader(g, e[g]);
                c = function(a) {
                    return function() {
                        c && (c = d = h.onload = h.onerror = h.onabort = h.onreadystatechange = null, "abort" === a ? h.abort() : "error" === a ? "number" != typeof h.status ? f(0, "error") : f(h.status, h.statusText) : f(Ob[h.status] || h.status, h.statusText, "text" !== (h.responseType || "text") || "string" != typeof h.responseText ? {
                            binary: h.response
                        } : {
                            text: h.responseText
                        }, h.getAllResponseHeaders()))
                    }
                }, h.onload = c(), d = h.onerror = c("error"), void 0 !== h.onabort ? h.onabort = d : h.onreadystatechange = function() {
                    4 === h.readyState && a.setTimeout(function() {
                        c && d()
                    })
                }, c = c("abort");
                try {
                    h.send(b.hasContent && b.data || null)
                } catch (i) {
                    if (c) throw i
                }
            },
            abort: function() {
                c && c()
            }
        }
    }), r.ajaxPrefilter(function(a) {
        a.crossDomain && (a.contents.script = !1)
    }), r.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function(a) {
                return r.globalEval(a), a
            }
        }
    }), r.ajaxPrefilter("script", function(a) {
        void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET")
    }), r.ajaxTransport("script", function(a) {
        if (a.crossDomain) {
            var b, c;
            return {
                send: function(e, f) {
                    b = r("<script>").prop({
                        charset: a.scriptCharset,
                        src: a.url
                    }).on("load error", c = function(a) {
                        b.remove(), c = null, a && f("error" === a.type ? 404 : 200, a.type)
                    }), d.head.appendChild(b[0])
                },
                abort: function() {
                    c && c()
                }
            }
        }
    });
    var Qb = [],
        Rb = /(=)\?(?=&|$)|\?\?/;
    r.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var a = Qb.pop() || r.expando + "_" + rb++;
            return this[a] = !0, a
        }
    }), r.ajaxPrefilter("json jsonp", function(b, c, d) {
        var e, f, g, h = b.jsonp !== !1 && (Rb.test(b.url) ? "url" : "string" == typeof b.data && 0 === (b.contentType || "").indexOf("application/x-www-form-urlencoded") && Rb.test(b.data) && "data");
        if (h || "jsonp" === b.dataTypes[0]) return e = b.jsonpCallback = r.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, h ? b[h] = b[h].replace(Rb, "$1" + e) : b.jsonp !== !1 && (b.url += (sb.test(b.url) ? "&" : "?") + b.jsonp + "=" + e), b.converters["script json"] = function() {
            return g || r.error(e + " was not called"), g[0]
        }, b.dataTypes[0] = "json", f = a[e], a[e] = function() {
            g = arguments
        }, d.always(function() {
            void 0 === f ? r(a).removeProp(e) : a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, Qb.push(e)), g && r.isFunction(f) && f(g[0]), g = f = void 0
        }), "script"
    }), o.createHTMLDocument = function() {
        var a = d.implementation.createHTMLDocument("").body;
        return a.innerHTML = "<form></form><form></form>", 2 === a.childNodes.length
    }(), r.parseHTML = function(a, b, c) {
        if ("string" != typeof a) return [];
        "boolean" == typeof b && (c = b, b = !1);
        var e, f, g;
        return b || (o.createHTMLDocument ? (b = d.implementation.createHTMLDocument(""), e = b.createElement("base"), e.href = d.location.href, b.head.appendChild(e)) : b = d), f = B.exec(a), g = !c && [], f ? [b.createElement(f[1])] : (f = pa([a], b, g), g && g.length && r(g).remove(), r.merge([], f.childNodes))
    }, r.fn.load = function(a, b, c) {
        var d, e, f, g = this,
            h = a.indexOf(" ");
        return h > -1 && (d = mb(a.slice(h)), a = a.slice(0, h)), r.isFunction(b) ? (c = b, b = void 0) : b && "object" == typeof b && (e = "POST"), g.length > 0 && r.ajax({
            url: a,
            type: e || "GET",
            dataType: "html",
            data: b
        }).done(function(a) {
            f = arguments, g.html(d ? r("<div>").append(r.parseHTML(a)).find(d) : a)
        }).always(c && function(a, b) {
            g.each(function() {
                c.apply(this, f || [a.responseText, b, a])
            })
        }), this
    }, r.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(a, b) {
        r.fn[b] = function(a) {
            return this.on(b, a)
        }
    }), r.expr.pseudos.animated = function(a) {
        return r.grep(r.timers, function(b) {
            return a === b.elem
        }).length
    };

    function Sb(a) {
        return r.isWindow(a) ? a : 9 === a.nodeType && a.defaultView
    }
    r.offset = {
        setOffset: function(a, b, c) {
            var d, e, f, g, h, i, j, k = r.css(a, "position"),
                l = r(a),
                m = {};
            "static" === k && (a.style.position = "relative"), h = l.offset(), f = r.css(a, "top"), i = r.css(a, "left"), j = ("absolute" === k || "fixed" === k) && (f + i).indexOf("auto") > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), r.isFunction(b) && (b = b.call(a, c, r.extend({}, h))), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using" in b ? b.using.call(a, m) : l.css(m)
        }
    }, r.fn.extend({
        offset: function(a) {
            if (arguments.length) return void 0 === a ? this : this.each(function(b) {
                r.offset.setOffset(this, a, b)
            });
            var b, c, d, e, f = this[0];
            if (f) return f.getClientRects().length ? (d = f.getBoundingClientRect(), d.width || d.height ? (e = f.ownerDocument, c = Sb(e), b = e.documentElement, {
                top: d.top + c.pageYOffset - b.clientTop,
                left: d.left + c.pageXOffset - b.clientLeft
            }) : d) : {
                top: 0,
                left: 0
            }
        },
        position: function() {
            if (this[0]) {
                var a, b, c = this[0],
                    d = {
                        top: 0,
                        left: 0
                    };
                return "fixed" === r.css(c, "position") ? b = c.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), r.nodeName(a[0], "html") || (d = a.offset()), d = {
                    top: d.top + r.css(a[0], "borderTopWidth", !0),
                    left: d.left + r.css(a[0], "borderLeftWidth", !0)
                }), {
                    top: b.top - d.top - r.css(c, "marginTop", !0),
                    left: b.left - d.left - r.css(c, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                var a = this.offsetParent;
                while (a && "static" === r.css(a, "position")) a = a.offsetParent;
                return a || qa
            })
        }
    }), r.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(a, b) {
        var c = "pageYOffset" === b;
        r.fn[a] = function(d) {
            return S(this, function(a, d, e) {
                var f = Sb(a);
                return void 0 === e ? f ? f[b] : a[d] : void(f ? f.scrollTo(c ? f.pageXOffset : e, c ? e : f.pageYOffset) : a[d] = e)
            }, a, d, arguments.length)
        }
    }), r.each(["top", "left"], function(a, b) {
        r.cssHooks[b] = Oa(o.pixelPosition, function(a, c) {
            if (c) return c = Na(a, b), La.test(c) ? r(a).position()[b] + "px" : c
        })
    }), r.each({
        Height: "height",
        Width: "width"
    }, function(a, b) {
        r.each({
            padding: "inner" + a,
            content: b,
            "": "outer" + a
        }, function(c, d) {
            r.fn[d] = function(e, f) {
                var g = arguments.length && (c || "boolean" != typeof e),
                    h = c || (e === !0 || f === !0 ? "margin" : "border");
                return S(this, function(b, c, e) {
                    var f;
                    return r.isWindow(b) ? 0 === d.indexOf("outer") ? b["inner" + a] : b.document.documentElement["client" + a] : 9 === b.nodeType ? (f = b.documentElement, Math.max(b.body["scroll" + a], f["scroll" + a], b.body["offset" + a], f["offset" + a], f["client" + a])) : void 0 === e ? r.css(b, c, h) : r.style(b, c, e, h)
                }, b, g ? e : void 0, g)
            }
        })
    }), r.fn.extend({
        bind: function(a, b, c) {
            return this.on(a, null, b, c)
        },
        unbind: function(a, b) {
            return this.off(a, null, b)
        },
        delegate: function(a, b, c, d) {
            return this.on(b, a, c, d)
        },
        undelegate: function(a, b, c) {
            return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c)
        }
    }), r.parseJSON = JSON.parse, "function" == typeof define && define.amd && define("jquery", [], function() {
        return r
    });
    var Tb = a.jQuery,
        Ub = a.$;
    return r.noConflict = function(b) {
        return a.$ === r && (a.$ = Ub), b && a.jQuery === r && (a.jQuery = Tb), r
    }, b || (a.jQuery = a.$ = r), r
});
/*!
 * @copyright &copy; Kartik Visweswaran, Krajee.com, 2013 - 2016
 * @version 4.0.1
 *
 * A simple yet powerful JQuery star rating plugin that allows rendering fractional star ratings and supports
 * Right to Left (RTL) input.
 *
 * For more JQuery plugins visit http://plugins.krajee.com
 * For more Yii related demos visit http://demos.krajee.com
 */
! function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof module && module.exports ? module.exports = e(require("jquery")) : e(window.jQuery)
}(function(e) {
    "use strict";
    e.fn.ratingLocales = {};
    var t, a, n, r, i, l, s, o, c, u, h;
    t = ".rating", a = 0, n = 5, r = .5, i = function(t, a) {
        return null === t || void 0 === t || 0 === t.length || a && "" === e.trim(t)
    }, l = function(e, t) {
        return e ? " " + t : ""
    }, s = function(e, t) {
        e.removeClass(t).addClass(t)
    }, o = function(e) {
        var t = ("" + e).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
        return t ? Math.max(0, (t[1] ? t[1].length : 0) - (t[2] ? +t[2] : 0)) : 0
    }, c = function(e, t) {
        return parseFloat(e.toFixed(t))
    }, u = function(e, a, n, r) {
        var i = r ? a : a.split(" ").join(t + " ") + t;
        e.off(i).on(i, n)
    }, h = function(t, a) {
        var n = this;
        n.$element = e(t), n._init(a)
    }, h.prototype = {
        constructor: h,
        _parseAttr: function(e, t) {
            var l, s, o, c, u = this,
                h = u.$element,
                d = h.attr("type");
            if ("range" === d || "number" === d) {
                switch (s = t[e] || h.data(e) || h.attr(e), e) {
                    case "min":
                        o = a;
                        break;
                    case "max":
                        o = n;
                        break;
                    default:
                        o = r
                }
                l = i(s) ? o : s, c = parseFloat(l)
            } else c = parseFloat(t[e]);
            return isNaN(c) ? o : c
        },
        _setDefault: function(e, t) {
            var a = this;
            i(a[e]) && (a[e] = t)
        },
        _listenClick: function(e, t) {
            return e.stopPropagation(), e.preventDefault(), e.handled === !0 ? !1 : (t(e), void(e.handled = !0))
        },
        _starClick: function(e) {
            var t, a = this;
            a._listenClick(e, function(e) {
                return a.inactive ? !1 : (t = a._getTouchPosition(e), a._setStars(t), a.$element.trigger("change").trigger("rating.change", [a.$element.val(), a._getCaption()]), void(a.starClicked = !0))
            })
        },
        _starMouseMove: function(e) {
            var t, a, n = this;
            !n.hoverEnabled || n.inactive || e && e.isDefaultPrevented() || (n.starClicked = !1, t = n._getTouchPosition(e), a = n.calculate(t), n._toggleHover(a), n.$element.trigger("rating.hover", [a.val, a.caption, "stars"]))
        },
        _starMouseLeave: function(e) {
            var t, a = this;
            !a.hoverEnabled || a.inactive || a.starClicked || e && e.isDefaultPrevented() || (t = a.cache, a._toggleHover(t), a.$element.trigger("rating.hoverleave", ["stars"]))
        },
        _clearClick: function(e) {
            var t = this;
            t._listenClick(e, function() {
                t.inactive || (t.clear(), t.clearClicked = !0)
            })
        },
        _clearMouseMove: function(e) {
            var t, a, n, r, i = this;
            !i.hoverEnabled || i.inactive || !i.hoverOnClear || e && e.isDefaultPrevented() || (i.clearClicked = !1, t = '<span class="' + i.clearCaptionClass + '">' + i.clearCaption + "</span>", a = i.clearValue, n = i.getWidthFromValue(a) || 0, r = {
                caption: t,
                width: n,
                val: a
            }, i._toggleHover(r), i.$element.trigger("rating.hover", [a, t, "clear"]))
        },
        _clearMouseLeave: function(e) {
            var t, a = this;
            !a.hoverEnabled || a.inactive || a.clearClicked || !a.hoverOnClear || e && e.isDefaultPrevented() || (t = a.cache, a._toggleHover(t), a.$element.trigger("rating.hoverleave", ["clear"]))
        },
        _resetForm: function(e) {
            var t = this;
            e && e.isDefaultPrevented() || t.inactive || t.reset()
        },
        _setTouch: function(e, t) {
            var a, n, r, l, s, o, c, u = this,
                h = "ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch;
            h && !u.inactive && (a = e.originalEvent, n = i(a.touches) ? a.changedTouches : a.touches, r = u._getTouchPosition(n[0]), t ? (u._setStars(r), u.$element.trigger("change").trigger("rating.change", [u.$element.val(), u._getCaption()]), u.starClicked = !0) : (l = u.calculate(r), s = l.val <= u.clearValue ? u.fetchCaption(u.clearValue) : l.caption, o = u.getWidthFromValue(u.clearValue), c = l.val <= u.clearValue ? o + "%" : l.width, u._setCaption(s), u.$filledStars.css("width", c)))
        },
        _initTouch: function(e) {
            var t = this,
                a = "touchend" === e.type;
            t._setTouch(e, a)
        },
        _initSlider: function(e) {
            var t = this;
            i(t.$element.val()) && t.$element.val(0), t.initialValue = t.$element.val(), t._setDefault("min", t._parseAttr("min", e)), t._setDefault("max", t._parseAttr("max", e)), t._setDefault("step", t._parseAttr("step", e)), (isNaN(t.min) || i(t.min)) && (t.min = a), (isNaN(t.max) || i(t.max)) && (t.max = n), (isNaN(t.step) || i(t.step) || 0 === t.step) && (t.step = r), t.diff = t.max - t.min
        },
        _initHighlight: function(e) {
            var t, a = this,
                n = a._getCaption();
            e || (e = a.$element.val()), t = a.getWidthFromValue(e) + "%", a.$filledStars.width(t), a.cache = {
                caption: n,
                width: t,
                val: e
            }
        },
        _getContainerCss: function() {
            var e = this;
            return "rating-container" + l(e.theme, "theme-" + e.theme) + l(e.rtl, "rating-rtl") + l(e.size, "rating-" + e.size) + l(e.animate, "rating-animate") + l(e.disabled || e.readonly, "rating-disabled") + l(e.containerClass, e.containerClass)
        },
        _checkDisabled: function() {
            var e = this,
                t = e.$element,
                a = e.options;
            e.disabled = void 0 === a.disabled ? t.attr("disabled") || !1 : a.disabled, e.readonly = void 0 === a.readonly ? t.attr("readonly") || !1 : a.readonly, e.inactive = e.disabled || e.readonly, t.attr({
                disabled: e.disabled,
                readonly: e.readonly
            })
        },
        _addContent: function(e, t) {
            var a = this,
                n = a.$container,
                r = "clear" === e;
            return a.rtl ? r ? n.append(t) : n.prepend(t) : r ? n.prepend(t) : n.append(t)
        },
        _generateRating: function() {
            var t, a, n, r = this,
                i = r.$element;
            a = r.$container = e(document.createElement("div")).insertBefore(i), s(a, r._getContainerCss()), r.$rating = t = e(document.createElement("div")).attr("class", "rating").appendTo(a).append(r._getStars("empty")).append(r._getStars("filled")), r.$emptyStars = t.find(".empty-stars"), r.$filledStars = t.find(".filled-stars"), r._renderCaption(), r._renderClear(), r._initHighlight(), a.append(i), r.rtl && (n = Math.max(r.$emptyStars.outerWidth(), r.$filledStars.outerWidth()), r.$emptyStars.width(n))
        },
        _getCaption: function() {
            var e = this;
            return e.$caption && e.$caption.length ? e.$caption.html() : e.defaultCaption
        },
        _setCaption: function(e) {
            var t = this;
            t.$caption && t.$caption.length && t.$caption.html(e)
        },
        _renderCaption: function() {
            var t, a = this,
                n = a.$element.val(),
                r = a.captionElement ? e(a.captionElement) : "";
            if (a.showCaption) {
                if (t = a.fetchCaption(n), r && r.length) return s(r, "caption"), r.html(t), void(a.$caption = r);
                a._addContent("caption", '<div class="caption">' + t + "</div>"), a.$caption = a.$container.find(".caption")
            }
        },
        _renderClear: function() {
            var t, a = this,
                n = a.clearElement ? e(a.clearElement) : "";
            if (a.showClear) {
                if (t = a._getClearClass(), n.length) return s(n, t), n.attr({
                    title: a.clearButtonTitle
                }).html(a.clearButton), void(a.$clear = n);
                a._addContent("clear", '<div class="' + t + '" title="' + a.clearButtonTitle + '">' + a.clearButton + "</div>"), a.$clear = a.$container.find("." + a.clearButtonBaseClass)
            }
        },
        _getClearClass: function() {
            return this.clearButtonBaseClass + " " + (this.inactive ? "" : this.clearButtonActiveClass)
        },
        _getTouchPosition: function(e) {
            var t = i(e.pageX) ? e.originalEvent.touches[0].pageX : e.pageX;
            return t - this.$rating.offset().left
        },
        _toggleHover: function(e) {
            var t, a, n, r = this;
            e && (r.hoverChangeStars && (t = r.getWidthFromValue(r.clearValue), a = e.val <= r.clearValue ? t + "%" : e.width, r.$filledStars.css("width", a)), r.hoverChangeCaption && (n = e.val <= r.clearValue ? r.fetchCaption(r.clearValue) : e.caption, n && r._setCaption(n + "")))
        },
        _init: function(t) {
            var a = this,
                n = a.$element.addClass("hide");
            return a.options = t, e.each(t, function(e, t) {
                a[e] = t
            }), (a.rtl || "rtl" === n.attr("dir")) && (a.rtl = !0, n.attr("dir", "rtl")), a.starClicked = !1, a.clearClicked = !1, a._initSlider(t), a._checkDisabled(), a.displayOnly && (a.inactive = !0, a.showClear = !1, a.showCaption = !1), a._generateRating(), a._listen(), n.removeClass("rating-loading")
        },
        _listen: function() {
            var t = this,
                a = t.$element,
                n = a.closest("form"),
                r = t.$rating,
                i = t.$clear;
            return u(r, "touchstart touchmove touchend", e.proxy(t._initTouch, t)), u(r, "click touchstart", e.proxy(t._starClick, t)), u(r, "mousemove", e.proxy(t._starMouseMove, t)), u(r, "mouseleave", e.proxy(t._starMouseLeave, t)), t.showClear && i.length && (u(i, "click touchstart", e.proxy(t._clearClick, t)), u(i, "mousemove", e.proxy(t._clearMouseMove, t)), u(i, "mouseleave", e.proxy(t._clearMouseLeave, t))), n.length && u(n, "reset", e.proxy(t._resetForm, t)), a
        },
        _getStars: function(e) {
            var t, a = this,
                n = '<span class="' + e + '-stars">';
            for (t = 1; t <= a.stars; t++) n += '<span class="star">' + a[e + "Star"] + "</span>";
            return n + "</span>"
        },
        _setStars: function(e) {
            var t = this,
                a = arguments.length ? t.calculate(e) : t.calculate(),
                n = t.$element;
            return n.val(a.val), t.$filledStars.css("width", a.width), t._setCaption(a.caption), t.cache = a, n
        },
        showStars: function(e) {
            var t = this,
                a = parseFloat(e);
            return t.$element.val(isNaN(a) ? t.clearValue : a), t._setStars()
        },
        calculate: function(e) {
            var t = this,
                a = i(t.$element.val()) ? 0 : t.$element.val(),
                n = arguments.length ? t.getValueFromPosition(e) : a,
                r = t.fetchCaption(n),
                l = t.getWidthFromValue(n);
            return l += "%", {
                caption: r,
                width: l,
                val: n
            }
        },
        getValueFromPosition: function(e) {
            var t, a, n = this,
                r = o(n.step),
                i = n.$rating.width();
            return a = n.diff * e / (i * n.step), a = n.rtl ? Math.floor(a) : Math.ceil(a), t = c(parseFloat(n.min + a * n.step), r), t = Math.max(Math.min(t, n.max), n.min), n.rtl ? n.max - t : t
        },
        getWidthFromValue: function(e) {
            var t, a, n = this,
                r = n.min,
                i = n.max,
                l = n.$emptyStars;
            return !e || r >= e || r === i ? 0 : (a = l.outerWidth(), t = a ? l.width() / a : 1, e >= i ? 100 : (e - r) * t * 100 / (i - r))
        },
        fetchCaption: function(e) {
            var t, a, n, r, l, s = this,
                u = parseFloat(e) || s.clearValue,
                h = s.starCaptions,
                d = s.starCaptionClasses;
            return u && u !== s.clearValue && (u = c(u, o(s.step))), r = "function" == typeof d ? d(u) : d[u], n = "function" == typeof h ? h(u) : h[u], a = i(n) ? s.defaultCaption.replace(/\{rating}/g, u) : n, t = i(r) ? s.clearCaptionClass : r, l = u === s.clearValue ? s.clearCaption : a, '<span class="' + t + '">' + l + "</span>"
        },
        destroy: function() {
            var t = this,
                a = t.$element;
            return i(t.$container) || t.$container.before(a).remove(), e.removeData(a.get(0)), a.off("rating").removeClass("hide")
        },
        create: function(e) {
            var t = this,
                a = e || t.options || {};
            return t.destroy().rating(a)
        },
        clear: function() {
            var e = this,
                t = '<span class="' + e.clearCaptionClass + '">' + e.clearCaption + "</span>";
            return e.inactive || e._setCaption(t), e.showStars(e.clearValue).trigger("change").trigger("rating.clear")
        },
        reset: function() {
            var e = this;
            return e.showStars(e.initialValue).trigger("rating.reset")
        },
        update: function(e) {
            var t = this;
            return arguments.length ? t.showStars(e) : t.$element
        },
        refresh: function(t) {
            var a = this,
                n = a.$element;
            return t ? a.destroy().rating(e.extend(!0, a.options, t)).trigger("rating.refresh") : n
        }
    }, e.fn.rating = function(t) {
        var a = Array.apply(null, arguments),
            n = [];
        switch (a.shift(), this.each(function() {
            var r, l = e(this),
                s = l.data("rating"),
                o = "object" == typeof t && t,
                c = o.language || l.data("language") || "en",
                u = {};
            s || ("en" === c || i(e.fn.ratingLocales[c]) || (u = e.fn.ratingLocales[c]), r = e.extend(!0, {}, e.fn.rating.defaults, e.fn.ratingLocales.en, u, o, l.data()), s = new h(this, r), l.data("rating", s)), "string" == typeof t && n.push(s[t].apply(s, a))
        }), n.length) {
            case 0:
                return this;
            case 1:
                return void 0 === n[0] ? this : n[0];
            default:
                return n
        }
    }, e.fn.rating.defaults = {
        theme: "",
        language: "en",
        stars: 5,
        filledStar: '<i class="glyphicon glyphicon-star"></i>',
        emptyStar: '<i class="glyphicon glyphicon-star-empty"></i>',
        containerClass: "",
        size: "md",
        animate: !0,
        displayOnly: !1,
        rtl: !1,
        showClear: !0,
        showCaption: !0,
        starCaptionClasses: {.5: "label label-danger", 1: "label label-danger", 1.5: "label label-warning", 2: "label label-warning", 2.5: "label label-info", 3: "label label-info", 3.5: "label label-primary", 4: "label label-primary", 4.5: "label label-success", 5: "label label-success"
        },
        clearButton: '<i class="glyphicon glyphicon-minus-sign"></i>',
        clearButtonBaseClass: "clear-rating",
        clearButtonActiveClass: "clear-rating-active",
        clearCaptionClass: "label label-default",
        clearValue: null,
        captionElement: null,
        clearElement: null,
        hoverEnabled: !0,
        hoverChangeCaption: !0,
        hoverChangeStars: !0,
        hoverOnClear: !0
    }, e.fn.ratingLocales.en = {
        defaultCaption: "{rating} Stars",
        starCaptions: {.5: "Half Star", 1: "One Star", 1.5: "One & Half Star", 2: "Two Stars", 2.5: "Two & Half Stars", 3: "Three Stars", 3.5: "Three & Half Stars", 4: "Four Stars", 4.5: "Four & Half Stars", 5: "Five Stars"
        },
        clearButtonTitle: "Clear",
        clearCaption: "Not Rated"
    }, e.fn.rating.Constructor = h, e(document).ready(function() {
        var t = e("input.rating");
        t.length && t.removeClass("rating-loading").addClass("rating-loading").rating()
    })
});
/*!
Waypoints - 4.0.1
Copyright Â© 2011-2016 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blob/master/licenses.txt
*/
! function() {
    "use strict";

    function t(o) {
        if (!o) throw new Error("No options passed to Waypoint constructor");
        if (!o.element) throw new Error("No element option passed to Waypoint constructor");
        if (!o.handler) throw new Error("No handler option passed to Waypoint constructor");
        this.key = "waypoint-" + e, this.options = t.Adapter.extend({}, t.defaults, o), this.element = this.options.element, this.adapter = new t.Adapter(this.element), this.callback = o.handler, this.axis = this.options.horizontal ? "horizontal" : "vertical", this.enabled = this.options.enabled, this.triggerPoint = null, this.group = t.Group.findOrCreate({
            name: this.options.group,
            axis: this.axis
        }), this.context = t.Context.findOrCreateByElement(this.options.context), t.offsetAliases[this.options.offset] && (this.options.offset = t.offsetAliases[this.options.offset]), this.group.add(this), this.context.add(this), i[this.key] = this, e += 1
    }
    var e = 0,
        i = {};
    t.prototype.queueTrigger = function(t) {
        this.group.queueTrigger(this, t)
    }, t.prototype.trigger = function(t) {
        this.enabled && this.callback && this.callback.apply(this, t)
    }, t.prototype.destroy = function() {
        this.context.remove(this), this.group.remove(this), delete i[this.key]
    }, t.prototype.disable = function() {
        return this.enabled = !1, this
    }, t.prototype.enable = function() {
        return this.context.refresh(), this.enabled = !0, this
    }, t.prototype.next = function() {
        return this.group.next(this)
    }, t.prototype.previous = function() {
        return this.group.previous(this)
    }, t.invokeAll = function(t) {
        var e = [];
        for (var o in i) e.push(i[o]);
        for (var n = 0, r = e.length; r > n; n++) e[n][t]()
    }, t.destroyAll = function() {
        t.invokeAll("destroy")
    }, t.disableAll = function() {
        t.invokeAll("disable")
    }, t.enableAll = function() {
        t.Context.refreshAll();
        for (var e in i) i[e].enabled = !0;
        return this
    }, t.refreshAll = function() {
        t.Context.refreshAll()
    }, t.viewportHeight = function() {
        return window.innerHeight || document.documentElement.clientHeight
    }, t.viewportWidth = function() {
        return document.documentElement.clientWidth
    }, t.adapters = [], t.defaults = {
        context: window,
        continuous: !0,
        enabled: !0,
        group: "default",
        horizontal: !1,
        offset: 0
    }, t.offsetAliases = {
        "bottom-in-view": function() {
            return this.context.innerHeight() - this.adapter.outerHeight()
        },
        "right-in-view": function() {
            return this.context.innerWidth() - this.adapter.outerWidth()
        }
    }, window.Waypoint = t
}(),
function() {
    "use strict";

    function t(t) {
        window.setTimeout(t, 1e3 / 60)
    }

    function e(t) {
        this.element = t, this.Adapter = n.Adapter, this.adapter = new this.Adapter(t), this.key = "waypoint-context-" + i, this.didScroll = !1, this.didResize = !1, this.oldScroll = {
            x: this.adapter.scrollLeft(),
            y: this.adapter.scrollTop()
        }, this.waypoints = {
            vertical: {},
            horizontal: {}
        }, t.waypointContextKey = this.key, o[t.waypointContextKey] = this, i += 1, n.windowContext || (n.windowContext = !0, n.windowContext = new e(window)), this.createThrottledScrollHandler(), this.createThrottledResizeHandler()
    }
    var i = 0,
        o = {},
        n = window.Waypoint,
        r = window.onload;
    e.prototype.add = function(t) {
        var e = t.options.horizontal ? "horizontal" : "vertical";
        this.waypoints[e][t.key] = t, this.refresh()
    }, e.prototype.checkEmpty = function() {
        var t = this.Adapter.isEmptyObject(this.waypoints.horizontal),
            e = this.Adapter.isEmptyObject(this.waypoints.vertical),
            i = this.element == this.element.window;
        t && e && !i && (this.adapter.off(".waypoints"), delete o[this.key])
    }, e.prototype.createThrottledResizeHandler = function() {
        function t() {
            e.handleResize(), e.didResize = !1
        }
        var e = this;
        this.adapter.on("resize.waypoints", function() {
            e.didResize || (e.didResize = !0, n.requestAnimationFrame(t))
        })
    }, e.prototype.createThrottledScrollHandler = function() {
        function t() {
            e.handleScroll(), e.didScroll = !1
        }
        var e = this;
        this.adapter.on("scroll.waypoints", function() {
            (!e.didScroll || n.isTouch) && (e.didScroll = !0, n.requestAnimationFrame(t))
        })
    }, e.prototype.handleResize = function() {
        n.Context.refreshAll()
    }, e.prototype.handleScroll = function() {
        var t = {},
            e = {
                horizontal: {
                    newScroll: this.adapter.scrollLeft(),
                    oldScroll: this.oldScroll.x,
                    forward: "right",
                    backward: "left"
                },
                vertical: {
                    newScroll: this.adapter.scrollTop(),
                    oldScroll: this.oldScroll.y,
                    forward: "down",
                    backward: "up"
                }
            };
        for (var i in e) {
            var o = e[i],
                n = o.newScroll > o.oldScroll,
                r = n ? o.forward : o.backward;
            for (var s in this.waypoints[i]) {
                var a = this.waypoints[i][s];
                if (null !== a.triggerPoint) {
                    var l = o.oldScroll < a.triggerPoint,
                        h = o.newScroll >= a.triggerPoint,
                        p = l && h,
                        u = !l && !h;
                    (p || u) && (a.queueTrigger(r), t[a.group.id] = a.group)
                }
            }
        }
        for (var c in t) t[c].flushTriggers();
        this.oldScroll = {
            x: e.horizontal.newScroll,
            y: e.vertical.newScroll
        }
    }, e.prototype.innerHeight = function() {
        return this.element == this.element.window ? n.viewportHeight() : this.adapter.innerHeight()
    }, e.prototype.remove = function(t) {
        delete this.waypoints[t.axis][t.key], this.checkEmpty()
    }, e.prototype.innerWidth = function() {
        return this.element == this.element.window ? n.viewportWidth() : this.adapter.innerWidth()
    }, e.prototype.destroy = function() {
        var t = [];
        for (var e in this.waypoints)
            for (var i in this.waypoints[e]) t.push(this.waypoints[e][i]);
        for (var o = 0, n = t.length; n > o; o++) t[o].destroy()
    }, e.prototype.refresh = function() {
        var t, e = this.element == this.element.window,
            i = e ? void 0 : this.adapter.offset(),
            o = {};
        this.handleScroll(), t = {
            horizontal: {
                contextOffset: e ? 0 : i.left,
                contextScroll: e ? 0 : this.oldScroll.x,
                contextDimension: this.innerWidth(),
                oldScroll: this.oldScroll.x,
                forward: "right",
                backward: "left",
                offsetProp: "left"
            },
            vertical: {
                contextOffset: e ? 0 : i.top,
                contextScroll: e ? 0 : this.oldScroll.y,
                contextDimension: this.innerHeight(),
                oldScroll: this.oldScroll.y,
                forward: "down",
                backward: "up",
                offsetProp: "top"
            }
        };
        for (var r in t) {
            var s = t[r];
            for (var a in this.waypoints[r]) {
                var l, h, p, u, c, d = this.waypoints[r][a],
                    f = d.options.offset,
                    w = d.triggerPoint,
                    y = 0,
                    g = null == w;
                d.element !== d.element.window && (y = d.adapter.offset()[s.offsetProp]), "function" == typeof f ? f = f.apply(d) : "string" == typeof f && (f = parseFloat(f), d.options.offset.indexOf("%") > -1 && (f = Math.ceil(s.contextDimension * f / 100))), l = s.contextScroll - s.contextOffset, d.triggerPoint = Math.floor(y + l - f), h = w < s.oldScroll, p = d.triggerPoint >= s.oldScroll, u = h && p, c = !h && !p, !g && u ? (d.queueTrigger(s.backward), o[d.group.id] = d.group) : !g && c ? (d.queueTrigger(s.forward), o[d.group.id] = d.group) : g && s.oldScroll >= d.triggerPoint && (d.queueTrigger(s.forward), o[d.group.id] = d.group)
            }
        }
        return n.requestAnimationFrame(function() {
            for (var t in o) o[t].flushTriggers()
        }), this
    }, e.findOrCreateByElement = function(t) {
        return e.findByElement(t) || new e(t)
    }, e.refreshAll = function() {
        for (var t in o) o[t].refresh()
    }, e.findByElement = function(t) {
        return o[t.waypointContextKey]
    }, window.onload = function() {
        r && r(), e.refreshAll()
    }, n.requestAnimationFrame = function(e) {
        var i = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || t;
        i.call(window, e)
    }, n.Context = e
}(),
function() {
    "use strict";

    function t(t, e) {
        return t.triggerPoint - e.triggerPoint
    }

    function e(t, e) {
        return e.triggerPoint - t.triggerPoint
    }

    function i(t) {
        this.name = t.name, this.axis = t.axis, this.id = this.name + "-" + this.axis, this.waypoints = [], this.clearTriggerQueues(), o[this.axis][this.name] = this
    }
    var o = {
            vertical: {},
            horizontal: {}
        },
        n = window.Waypoint;
    i.prototype.add = function(t) {
        this.waypoints.push(t)
    }, i.prototype.clearTriggerQueues = function() {
        this.triggerQueues = {
            up: [],
            down: [],
            left: [],
            right: []
        }
    }, i.prototype.flushTriggers = function() {
        for (var i in this.triggerQueues) {
            var o = this.triggerQueues[i],
                n = "up" === i || "left" === i;
            o.sort(n ? e : t);
            for (var r = 0, s = o.length; s > r; r += 1) {
                var a = o[r];
                (a.options.continuous || r === o.length - 1) && a.trigger([i])
            }
        }
        this.clearTriggerQueues()
    }, i.prototype.next = function(e) {
        this.waypoints.sort(t);
        var i = n.Adapter.inArray(e, this.waypoints),
            o = i === this.waypoints.length - 1;
        return o ? null : this.waypoints[i + 1]
    }, i.prototype.previous = function(e) {
        this.waypoints.sort(t);
        var i = n.Adapter.inArray(e, this.waypoints);
        return i ? this.waypoints[i - 1] : null
    }, i.prototype.queueTrigger = function(t, e) {
        this.triggerQueues[e].push(t)
    }, i.prototype.remove = function(t) {
        var e = n.Adapter.inArray(t, this.waypoints);
        e > -1 && this.waypoints.splice(e, 1)
    }, i.prototype.first = function() {
        return this.waypoints[0]
    }, i.prototype.last = function() {
        return this.waypoints[this.waypoints.length - 1]
    }, i.findOrCreate = function(t) {
        return o[t.axis][t.name] || new i(t)
    }, n.Group = i
}(),
function() {
    "use strict";

    function t(t) {
        this.$element = e(t)
    }
    var e = window.jQuery,
        i = window.Waypoint;
    e.each(["innerHeight", "innerWidth", "off", "offset", "on", "outerHeight", "outerWidth", "scrollLeft", "scrollTop"], function(e, i) {
        t.prototype[i] = function() {
            var t = Array.prototype.slice.call(arguments);
            return this.$element[i].apply(this.$element, t)
        }
    }), e.each(["extend", "inArray", "isEmptyObject"], function(i, o) {
        t[o] = e[o]
    }), i.adapters.push({
        name: "jquery",
        Adapter: t
    }), i.Adapter = t
}(),
function() {
    "use strict";

    function t(t) {
        return function() {
            var i = [],
                o = arguments[0];
            return t.isFunction(arguments[0]) && (o = t.extend({}, arguments[1]), o.handler = arguments[0]), this.each(function() {
                var n = t.extend({}, o, {
                    element: this
                });
                "string" == typeof n.context && (n.context = t(this).closest(n.context)[0]), i.push(new e(n))
            }), i
        }
    }
    var e = window.Waypoint;
    window.jQuery && (window.jQuery.fn.waypoint = t(window.jQuery)), window.Zepto && (window.Zepto.fn.waypoint = t(window.Zepto))
}();

/*!
Waypoints Infinite Scroll Shortcut - 4.0.1
Copyright Â© 2011-2016 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blob/master/licenses.txt
*/
! function() {
    "use strict";

    function t(n) {
        this.options = i.extend({}, t.defaults, n), this.container = this.options.element, "auto" !== this.options.container && (this.container = this.options.container), this.$container = i(this.container), this.$more = i(this.options.more), this.$more.length && (this.setupHandler(), this.waypoint = new o(this.options))
    }
    var i = window.jQuery,
        o = window.Waypoint;
    t.prototype.setupHandler = function() {
        this.options.handler = i.proxy(function() {
            this.options.onBeforePageLoad(), this.destroy(), this.$container.addClass(this.options.loadingClass), i.get(i(this.options.more).attr("href"), i.proxy(function(t) {
                var n = i(i.parseHTML(t)),
                    e = n.find(this.options.more),
                    s = n.find(this.options.items);
                s.length || (s = n.filter(this.options.items)), this.$container.append(s), this.$container.removeClass(this.options.loadingClass), e.length || (e = n.filter(this.options.more)), e.length ? (this.$more.replaceWith(e), this.$more = e, this.waypoint = new o(this.options)) : this.$more.remove(), this.options.onAfterPageLoad(s)
            }, this))
        }, this)
    }, t.prototype.destroy = function() {
        this.waypoint && this.waypoint.destroy()
    }, t.defaults = {
        container: "auto",
        items: ".infinite-item",
        more: ".infinite-more-link",
        offset: "bottom-in-view",
        loadingClass: "infinite-loading",
        onBeforePageLoad: i.noop,
        onAfterPageLoad: i.noop
    }, o.Infinite = t
}();

/*!
Waypoints Sticky Element Shortcut - 4.0.1
Copyright Â© 2011-2016 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blob/master/licenses.txt
*/
! function() {
    "use strict";

    function t(s) {
        this.options = e.extend({}, i.defaults, t.defaults, s), this.element = this.options.element, this.$element = e(this.element), this.createWrapper(), this.createWaypoint()
    }
    var e = window.jQuery,
        i = window.Waypoint;
    t.prototype.createWaypoint = function() {
        var t = this.options.handler;
        this.waypoint = new i(e.extend({}, this.options, {
            element: this.wrapper,
            handler: e.proxy(function(e) {
                var i = this.options.direction.indexOf(e) > -1,
                    s = i ? this.$element.outerHeight(!0) : "";
                this.$wrapper.height(s), this.$element.toggleClass(this.options.stuckClass, i), t && t.call(this, e)
            }, this)
        }))
    }, t.prototype.createWrapper = function() {
        this.options.wrapper && this.$element.wrap(this.options.wrapper), this.$wrapper = this.$element.parent(), this.wrapper = this.$wrapper[0]
    }, t.prototype.destroy = function() {
        this.$element.parent()[0] === this.wrapper && (this.waypoint.destroy(), this.$element.removeClass(this.options.stuckClass), this.options.wrapper && this.$element.unwrap())
    }, t.defaults = {
        wrapper: '<div class="sticky-wrapper" />',
        stuckClass: "stuck",
        direction: "down right"
    }, i.Sticky = t
}();
typeof JSON != "object" && (JSON = {}),
    function() {
        "use strict";

        function f(e) {
            return e < 10 ? "0" + e : e
        }

        function quote(e) {
            return escapable.lastIndex = 0, escapable.test(e) ? '"' + e.replace(escapable, function(e) {
                var t = meta[e];
                return typeof t == "string" ? t : "\\u" + ("0000" + e.charCodeAt(0).toString(16)).slice(-4)
            }) + '"' : '"' + e + '"'
        }

        function str(e, t) {
            var n, r, i, s, o = gap,
                u, a = t[e];
            a && typeof a == "object" && typeof a.toJSON == "function" && (a = a.toJSON(e)), typeof rep == "function" && (a = rep.call(t, e, a));
            switch (typeof a) {
                case "string":
                    return quote(a);
                case "number":
                    return isFinite(a) ? String(a) : "null";
                case "boolean":
                case "null":
                    return String(a);
                case "object":
                    if (!a) return "null";
                    gap += indent, u = [];
                    if (Object.prototype.toString.apply(a) === "[object Array]") {
                        s = a.length;
                        for (n = 0; n < s; n += 1) u[n] = str(n, a) || "null";
                        return i = u.length === 0 ? "[]" : gap ? "[\n" + gap + u.join(",\n" + gap) + "\n" + o + "]" : "[" + u.join(",") + "]", gap = o, i
                    }
                    if (rep && typeof rep == "object") {
                        s = rep.length;
                        for (n = 0; n < s; n += 1) typeof rep[n] == "string" && (r = rep[n], i = str(r, a), i && u.push(quote(r) + (gap ? ": " : ":") + i))
                    } else
                        for (r in a) Object.prototype.hasOwnProperty.call(a, r) && (i = str(r, a), i && u.push(quote(r) + (gap ? ": " : ":") + i));
                    return i = u.length === 0 ? "{}" : gap ? "{\n" + gap + u.join(",\n" + gap) + "\n" + o + "}" : "{" + u.join(",") + "}", gap = o, i
            }
        }
        typeof Date.prototype.toJSON != "function" && (Date.prototype.toJSON = function(e) {
            return isFinite(this.valueOf()) ? this.getUTCFullYear() + "-" + f(this.getUTCMonth() + 1) + "-" + f(this.getUTCDate()) + "T" + f(this.getUTCHours()) + ":" + f(this.getUTCMinutes()) + ":" + f(this.getUTCSeconds()) + "Z" : null
        }, String.prototype.toJSON = Number.prototype.toJSON = Boolean.prototype.toJSON = function(e) {
            return this.valueOf()
        });
        var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            gap, indent, meta = {
                "\b": "\\b",
                "	": "\\t",
                "\n": "\\n",
                "\f": "\\f",
                "\r": "\\r",
                '"': '\\"',
                "\\": "\\\\"
            },
            rep;
        typeof JSON.stringify != "function" && (JSON.stringify = function(e, t, n) {
            var r;
            gap = "", indent = "";
            if (typeof n == "number")
                for (r = 0; r < n; r += 1) indent += " ";
            else typeof n == "string" && (indent = n);
            rep = t;
            if (!t || typeof t == "function" || typeof t == "object" && typeof t.length == "number") return str("", {
                "": e
            });
            throw new Error("JSON.stringify")
        }), typeof JSON.parse != "function" && (JSON.parse = function(text, reviver) {
            function walk(e, t) {
                var n, r, i = e[t];
                if (i && typeof i == "object")
                    for (n in i) Object.prototype.hasOwnProperty.call(i, n) && (r = walk(i, n), r !== undefined ? i[n] = r : delete i[n]);
                return reviver.call(e, t, i)
            }
            var j;
            text = String(text), cx.lastIndex = 0, cx.test(text) && (text = text.replace(cx, function(e) {
                return "\\u" + ("0000" + e.charCodeAt(0).toString(16)).slice(-4)
            }));
            if (/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, ""))) return j = eval("(" + text + ")"), typeof reviver == "function" ? walk({
                "": j
            }, "") : j;
            throw new SyntaxError("JSON.parse")
        })
    }(),
    function(e, t) {
        "use strict";
        var n = e.History = e.History || {},
            r = e.jQuery;
        if (typeof n.Adapter != "undefined") throw new Error("History.js Adapter has already been loaded...");
        n.Adapter = {
            bind: function(e, t, n) {
                r(e).bind(t, n)
            },
            trigger: function(e, t, n) {
                r(e).trigger(t, n)
            },
            extractEventData: function(e, n, r) {
                var i = n && n.originalEvent && n.originalEvent[e] || r && r[e] || t;
                return i
            },
            onDomLoad: function(e) {
                r(e)
            }
        }, typeof n.init != "undefined" && n.init()
    }(window),
    function(e, t) {
        "use strict";
        var n = e.document,
            r = e.setTimeout || r,
            i = e.clearTimeout || i,
            s = e.setInterval || s,
            o = e.History = e.History || {};
        if (typeof o.initHtml4 != "undefined") throw new Error("History.js HTML4 Support has already been loaded...");
        o.initHtml4 = function() {
            if (typeof o.initHtml4.initialized != "undefined") return !1;
            o.initHtml4.initialized = !0, o.enabled = !0, o.savedHashes = [], o.isLastHash = function(e) {
                var t = o.getHashByIndex(),
                    n;
                return n = e === t, n
            }, o.isHashEqual = function(e, t) {
                return e = encodeURIComponent(e).replace(/%25/g, "%"), t = encodeURIComponent(t).replace(/%25/g, "%"), e === t
            }, o.saveHash = function(e) {
                return o.isLastHash(e) ? !1 : (o.savedHashes.push(e), !0)
            }, o.getHashByIndex = function(e) {
                var t = null;
                return typeof e == "undefined" ? t = o.savedHashes[o.savedHashes.length - 1] : e < 0 ? t = o.savedHashes[o.savedHashes.length + e] : t = o.savedHashes[e], t
            }, o.discardedHashes = {}, o.discardedStates = {}, o.discardState = function(e, t, n) {
                var r = o.getHashByState(e),
                    i;
                return i = {
                    discardedState: e,
                    backState: n,
                    forwardState: t
                }, o.discardedStates[r] = i, !0
            }, o.discardHash = function(e, t, n) {
                var r = {
                    discardedHash: e,
                    backState: n,
                    forwardState: t
                };
                return o.discardedHashes[e] = r, !0
            }, o.discardedState = function(e) {
                var t = o.getHashByState(e),
                    n;
                return n = o.discardedStates[t] || !1, n
            }, o.discardedHash = function(e) {
                var t = o.discardedHashes[e] || !1;
                return t
            }, o.recycleState = function(e) {
                var t = o.getHashByState(e);
                return o.discardedState(e) && delete o.discardedStates[t], !0
            }, o.emulated.hashChange && (o.hashChangeInit = function() {
                o.checkerFunction = null;
                var t = "",
                    r, i, u, a, f = Boolean(o.getHash());
                return o.isInternetExplorer() ? (r = "historyjs-iframe", i = n.createElement("iframe"), i.setAttribute("id", r), i.setAttribute("src", "#"), i.style.display = "none", n.body.appendChild(i), i.contentWindow.document.open(), i.contentWindow.document.close(), u = "", a = !1, o.checkerFunction = function() {
                    if (a) return !1;
                    a = !0;
                    var n = o.getHash(),
                        r = o.getHash(i.contentWindow.document);
                    return n !== t ? (t = n, r !== n && (u = r = n, i.contentWindow.document.open(), i.contentWindow.document.close(), i.contentWindow.document.location.hash = o.escapeHash(n)), o.Adapter.trigger(e, "hashchange")) : r !== u && (u = r, f && r === "" ? o.back() : o.setHash(r, !1)), a = !1, !0
                }) : o.checkerFunction = function() {
                    var n = o.getHash() || "";
                    return n !== t && (t = n, o.Adapter.trigger(e, "hashchange")), !0
                }, o.intervalList.push(s(o.checkerFunction, o.options.hashChangeInterval)), !0
            }, o.Adapter.onDomLoad(o.hashChangeInit)), o.emulated.pushState && (o.onHashChange = function(t) {
                var n = t && t.newURL || o.getLocationHref(),
                    r = o.getHashByUrl(n),
                    i = null,
                    s = null,
                    u = null,
                    a;
                return o.isLastHash(r) ? (o.busy(!1), !1) : (o.doubleCheckComplete(), o.saveHash(r), r && o.isTraditionalAnchor(r) ? (o.Adapter.trigger(e, "anchorchange"), o.busy(!1), !1) : (i = o.extractState(o.getFullUrl(r || o.getLocationHref()), !0), o.isLastSavedState(i) ? (o.busy(!1), !1) : (s = o.getHashByState(i), a = o.discardedState(i), a ? (o.getHashByIndex(-2) === o.getHashByState(a.forwardState) ? o.back(!1) : o.forward(!1), !1) : (o.pushState(i.data, i.title, encodeURI(i.url), !1), !0))))
            }, o.Adapter.bind(e, "hashchange", o.onHashChange), o.pushState = function(t, n, r, i) {
                r = encodeURI(r).replace(/%25/g, "%");
                if (o.getHashByUrl(r)) throw new Error("History.js does not support states with fragment-identifiers (hashes/anchors).");
                if (i !== !1 && o.busy()) return o.pushQueue({
                    scope: o,
                    callback: o.pushState,
                    args: arguments,
                    queue: i
                }), !1;
                o.busy(!0);
                var s = o.createStateObject(t, n, r),
                    u = o.getHashByState(s),
                    a = o.getState(!1),
                    f = o.getHashByState(a),
                    l = o.getHash(),
                    c = o.expectedStateId == s.id;
                return o.storeState(s), o.expectedStateId = s.id, o.recycleState(s), o.setTitle(s), u === f ? (o.busy(!1), !1) : (o.saveState(s), c || o.Adapter.trigger(e, "statechange"), !o.isHashEqual(u, l) && !o.isHashEqual(u, o.getShortUrl(o.getLocationHref())) && o.setHash(u, !1), o.busy(!1), !0)
            }, o.replaceState = function(t, n, r, i) {
                r = encodeURI(r).replace(/%25/g, "%");
                if (o.getHashByUrl(r)) throw new Error("History.js does not support states with fragment-identifiers (hashes/anchors).");
                if (i !== !1 && o.busy()) return o.pushQueue({
                    scope: o,
                    callback: o.replaceState,
                    args: arguments,
                    queue: i
                }), !1;
                o.busy(!0);
                var s = o.createStateObject(t, n, r),
                    u = o.getHashByState(s),
                    a = o.getState(!1),
                    f = o.getHashByState(a),
                    l = o.getStateByIndex(-2);
                return o.discardState(a, s, l), u === f ? (o.storeState(s), o.expectedStateId = s.id, o.recycleState(s), o.setTitle(s), o.saveState(s), o.Adapter.trigger(e, "statechange"), o.busy(!1)) : o.pushState(s.data, s.title, s.url, !1), !0
            }), o.emulated.pushState && o.getHash() && !o.emulated.hashChange && o.Adapter.onDomLoad(function() {
                o.Adapter.trigger(e, "hashchange")
            })
        }, typeof o.init != "undefined" && o.init()
    }(window),
    function(e, t) {
        "use strict";
        var n = e.console || t,
            r = e.document,
            i = e.navigator,
            s = !1,
            o = e.setTimeout,
            u = e.clearTimeout,
            a = e.setInterval,
            f = e.clearInterval,
            l = e.JSON,
            c = e.alert,
            h = e.History = e.History || {},
            p = e.history;
        try {
            s = e.sessionStorage, s.setItem("TEST", "1"), s.removeItem("TEST")
        } catch (d) {
            s = !1
        }
        l.stringify = l.stringify || l.encode, l.parse = l.parse || l.decode;
        if (typeof h.init != "undefined") throw new Error("History.js Core has already been loaded...");
        h.init = function(e) {
            return typeof h.Adapter == "undefined" ? !1 : (typeof h.initCore != "undefined" && h.initCore(), typeof h.initHtml4 != "undefined" && h.initHtml4(), !0)
        }, h.initCore = function(d) {
            if (typeof h.initCore.initialized != "undefined") return !1;
            h.initCore.initialized = !0, h.options = h.options || {}, h.options.hashChangeInterval = h.options.hashChangeInterval || 100, h.options.safariPollInterval = h.options.safariPollInterval || 500, h.options.doubleCheckInterval = h.options.doubleCheckInterval || 500, h.options.disableSuid = h.options.disableSuid || !1, h.options.storeInterval = h.options.storeInterval || 1e3, h.options.busyDelay = h.options.busyDelay || 250, h.options.debug = h.options.debug || !1, h.options.initialTitle = h.options.initialTitle || r.title, h.options.html4Mode = h.options.html4Mode || !1, h.options.delayInit = h.options.delayInit || !1, h.intervalList = [], h.clearAllIntervals = function() {
                var e, t = h.intervalList;
                if (typeof t != "undefined" && t !== null) {
                    for (e = 0; e < t.length; e++) f(t[e]);
                    h.intervalList = null
                }
            }, h.debug = function() {
                (h.options.debug || !1) && h.log.apply(h, arguments)
            }, h.log = function() {
                var e = typeof n != "undefined" && typeof n.log != "undefined" && typeof n.log.apply != "undefined",
                    t = r.getElementById("log"),
                    i, s, o, u, a;
                e ? (u = Array.prototype.slice.call(arguments), i = u.shift(), typeof n.debug != "undefined" ? n.debug.apply(n, [i, u]) : n.log.apply(n, [i, u])) : i = "\n" + arguments[0] + "\n";
                for (s = 1, o = arguments.length; s < o; ++s) {
                    a = arguments[s];
                    if (typeof a == "object" && typeof l != "undefined") try {
                        a = l.stringify(a)
                    } catch (f) {}
                    i += "\n" + a + "\n"
                }
                return t ? (t.value += i + "\n-----\n", t.scrollTop = t.scrollHeight - t.clientHeight) : e || c(i), !0
            }, h.getInternetExplorerMajorVersion = function() {
                var e = h.getInternetExplorerMajorVersion.cached = typeof h.getInternetExplorerMajorVersion.cached != "undefined" ? h.getInternetExplorerMajorVersion.cached : function() {
                    var e = 3,
                        t = r.createElement("div"),
                        n = t.getElementsByTagName("i");
                    while ((t.innerHTML = "<!--[if gt IE " + ++e + "]><i></i><![endif]-->") && n[0]);
                    return e > 4 ? e : !1
                }();
                return e
            }, h.isInternetExplorer = function() {
                var e = h.isInternetExplorer.cached = typeof h.isInternetExplorer.cached != "undefined" ? h.isInternetExplorer.cached : Boolean(h.getInternetExplorerMajorVersion());
                return e
            }, h.options.html4Mode ? h.emulated = {
                pushState: !0,
                hashChange: !0
            } : h.emulated = {
                pushState: !Boolean(e.history && e.history.pushState && e.history.replaceState && !/ Mobile\/([1-7][a-z]|(8([abcde]|f(1[0-8]))))/i.test(i.userAgent) && !/AppleWebKit\/5([0-2]|3[0-2])/i.test(i.userAgent)),
                hashChange: Boolean(!("onhashchange" in e || "onhashchange" in r) || h.isInternetExplorer() && h.getInternetExplorerMajorVersion() < 8)
            }, h.enabled = !h.emulated.pushState, h.bugs = {
                setHash: Boolean(!h.emulated.pushState && i.vendor === "Apple Computer, Inc." && /AppleWebKit\/5([0-2]|3[0-3])/.test(i.userAgent)),
                safariPoll: Boolean(!h.emulated.pushState && i.vendor === "Apple Computer, Inc." && /AppleWebKit\/5([0-2]|3[0-3])/.test(i.userAgent)),
                ieDoubleCheck: Boolean(h.isInternetExplorer() && h.getInternetExplorerMajorVersion() < 8),
                hashEscape: Boolean(h.isInternetExplorer() && h.getInternetExplorerMajorVersion() < 7)
            }, h.isEmptyObject = function(e) {
                for (var t in e)
                    if (e.hasOwnProperty(t)) return !1;
                return !0
            }, h.cloneObject = function(e) {
                var t, n;
                return e ? (t = l.stringify(e), n = l.parse(t)) : n = {}, n
            }, h.getRootUrl = function() {
                var e = r.location.protocol + "//" + (r.location.hostname || r.location.host);
                if (r.location.port || !1) e += ":" + r.location.port;
                return e += "/", e
            }, h.getBaseHref = function() {
                var e = r.getElementsByTagName("base"),
                    t = null,
                    n = "";
                return e.length === 1 && (t = e[0], n = t.href.replace(/[^\/]+$/, "")), n = n.replace(/\/+$/, ""), n && (n += "/"), n
            }, h.getBaseUrl = function() {
                var e = h.getBaseHref() || h.getBasePageUrl() || h.getRootUrl();
                return e
            }, h.getPageUrl = function() {
                var e = h.getState(!1, !1),
                    t = (e || {}).url || h.getLocationHref(),
                    n;
                return n = t.replace(/\/+$/, "").replace(/[^\/]+$/, function(e, t, n) {
                    return /\./.test(e) ? e : e + "/"
                }), n
            }, h.getBasePageUrl = function() {
                var e = h.getLocationHref().replace(/[#\?].*/, "").replace(/[^\/]+$/, function(e, t, n) {
                    return /[^\/]$/.test(e) ? "" : e
                }).replace(/\/+$/, "") + "/";
                return e
            }, h.getFullUrl = function(e, t) {
                var n = e,
                    r = e.substring(0, 1);
                return t = typeof t == "undefined" ? !0 : t, /[a-z]+\:\/\//.test(e) || (r === "/" ? n = h.getRootUrl() + e.replace(/^\/+/, "") : r === "#" ? n = h.getPageUrl().replace(/#.*/, "") + e : r === "?" ? n = h.getPageUrl().replace(/[\?#].*/, "") + e : t ? n = h.getBaseUrl() + e.replace(/^(\.\/)+/, "") : n = h.getBasePageUrl() + e.replace(/^(\.\/)+/, "")), n.replace(/\#$/, "")
            }, h.getShortUrl = function(e) {
                var t = e,
                    n = h.getBaseUrl(),
                    r = h.getRootUrl();
                return h.emulated.pushState && (t = t.replace(n, "")), t = t.replace(r, "/"), h.isTraditionalAnchor(t) && (t = "./" + t), t = t.replace(/^(\.\/)+/g, "./").replace(/\#$/, ""), t
            }, h.getLocationHref = function(e) {
                return e = e || r, e.URL === e.location.href ? e.location.href : e.location.href === decodeURIComponent(e.URL) ? e.URL : e.location.hash && decodeURIComponent(e.location.href.replace(/^[^#]+/, "")) === e.location.hash ? e.location.href : e.URL.indexOf("#") == -1 && e.location.href.indexOf("#") != -1 ? e.location.href : e.URL || e.location.href
            }, h.store = {}, h.idToState = h.idToState || {}, h.stateToId = h.stateToId || {}, h.urlToId = h.urlToId || {}, h.storedStates = h.storedStates || [], h.savedStates = h.savedStates || [], h.normalizeStore = function() {
                h.store.idToState = h.store.idToState || {}, h.store.urlToId = h.store.urlToId || {}, h.store.stateToId = h.store.stateToId || {}
            }, h.getState = function(e, t) {
                typeof e == "undefined" && (e = !0), typeof t == "undefined" && (t = !0);
                var n = h.getLastSavedState();
                return !n && t && (n = h.createStateObject()), e && (n = h.cloneObject(n), n.url = n.cleanUrl || n.url), n
            }, h.getIdByState = function(e) {
                var t = h.extractId(e.url),
                    n;
                if (!t) {
                    n = h.getStateString(e);
                    if (typeof h.stateToId[n] != "undefined") t = h.stateToId[n];
                    else if (typeof h.store.stateToId[n] != "undefined") t = h.store.stateToId[n];
                    else {
                        for (;;) {
                            t = (new Date).getTime() + String(Math.random()).replace(/\D/g, "");
                            if (typeof h.idToState[t] == "undefined" && typeof h.store.idToState[t] == "undefined") break
                        }
                        h.stateToId[n] = t, h.idToState[t] = e
                    }
                }
                return t
            }, h.normalizeState = function(e) {
                var t, n;
                if (!e || typeof e != "object") e = {};
                if (typeof e.normalized != "undefined") return e;
                if (!e.data || typeof e.data != "object") e.data = {};
                return t = {}, t.normalized = !0, t.title = e.title || "", t.url = h.getFullUrl(e.url ? e.url : h.getLocationHref()), t.hash = h.getShortUrl(t.url), t.data = h.cloneObject(e.data), t.id = h.getIdByState(t), t.cleanUrl = t.url.replace(/\??\&_suid.*/, ""), t.url = t.cleanUrl, n = !h.isEmptyObject(t.data), (t.title || n) && h.options.disableSuid !== !0 && (t.hash = h.getShortUrl(t.url).replace(/\??\&_suid.*/, ""), /\?/.test(t.hash) || (t.hash += "?"), t.hash += "&_suid=" + t.id), t.hashedUrl = h.getFullUrl(t.hash), (h.emulated.pushState || h.bugs.safariPoll) && h.hasUrlDuplicate(t) && (t.url = t.hashedUrl), t
            }, h.createStateObject = function(e, t, n) {
                var r = {
                    data: e,
                    title: t,
                    url: n
                };
                return r = h.normalizeState(r), r
            }, h.getStateById = function(e) {
                e = String(e);
                var n = h.idToState[e] || h.store.idToState[e] || t;
                return n
            }, h.getStateString = function(e) {
                var t, n, r;
                return t = h.normalizeState(e), n = {
                    data: t.data,
                    title: e.title,
                    url: e.url
                }, r = l.stringify(n), r
            }, h.getStateId = function(e) {
                var t, n;
                return t = h.normalizeState(e), n = t.id, n
            }, h.getHashByState = function(e) {
                var t, n;
                return t = h.normalizeState(e), n = t.hash, n
            }, h.extractId = function(e) {
                var t, n, r, i;
                return e.indexOf("#") != -1 ? i = e.split("#")[0] : i = e, n = /(.*)\&_suid=([0-9]+)$/.exec(i), r = n ? n[1] || e : e, t = n ? String(n[2] || "") : "", t || !1
            }, h.isTraditionalAnchor = function(e) {
                var t = !/[\/\?\.]/.test(e);
                return t
            }, h.extractState = function(e, t) {
                var n = null,
                    r, i;
                return t = t || !1, r = h.extractId(e), r && (n = h.getStateById(r)), n || (i = h.getFullUrl(e), r = h.getIdByUrl(i) || !1, r && (n = h.getStateById(r)), !n && t && !h.isTraditionalAnchor(e) && (n = h.createStateObject(null, null, i))), n
            }, h.getIdByUrl = function(e) {
                var n = h.urlToId[e] || h.store.urlToId[e] || t;
                return n
            }, h.getLastSavedState = function() {
                return h.savedStates[h.savedStates.length - 1] || t
            }, h.getLastStoredState = function() {
                return h.storedStates[h.storedStates.length - 1] || t
            }, h.hasUrlDuplicate = function(e) {
                var t = !1,
                    n;
                return n = h.extractState(e.url), t = n && n.id !== e.id, t
            }, h.storeState = function(e) {
                return h.urlToId[e.url] = e.id, h.storedStates.push(h.cloneObject(e)), e
            }, h.isLastSavedState = function(e) {
                var t = !1,
                    n, r, i;
                return h.savedStates.length && (n = e.id, r = h.getLastSavedState(), i = r.id, t = n === i), t
            }, h.saveState = function(e) {
                return h.isLastSavedState(e) ? !1 : (h.savedStates.push(h.cloneObject(e)), !0)
            }, h.getStateByIndex = function(e) {
                var t = null;
                return typeof e == "undefined" ? t = h.savedStates[h.savedStates.length - 1] : e < 0 ? t = h.savedStates[h.savedStates.length + e] : t = h.savedStates[e], t
            }, h.getCurrentIndex = function() {
                var e = null;
                return h.savedStates.length < 1 ? e = 0 : e = h.savedStates.length - 1, e
            }, h.getHash = function(e) {
                var t = h.getLocationHref(e),
                    n;
                return n = h.getHashByUrl(t), n
            }, h.unescapeHash = function(e) {
                var t = h.normalizeHash(e);
                return t = decodeURIComponent(t), t
            }, h.normalizeHash = function(e) {
                var t = e.replace(/[^#]*#/, "").replace(/#.*/, "");
                return t
            }, h.setHash = function(e, t) {
                var n, i;
                return t !== !1 && h.busy() ? (h.pushQueue({
                    scope: h,
                    callback: h.setHash,
                    args: arguments,
                    queue: t
                }), !1) : (h.busy(!0), n = h.extractState(e, !0), n && !h.emulated.pushState ? h.pushState(n.data, n.title, n.url, !1) : h.getHash() !== e && (h.bugs.setHash ? (i = h.getPageUrl(), h.pushState(null, null, i + "#" + e, !1)) : r.location.hash = e), h)
            }, h.escapeHash = function(t) {
                var n = h.normalizeHash(t);
                return n = e.encodeURIComponent(n), h.bugs.hashEscape || (n = n.replace(/\%21/g, "!").replace(/\%26/g, "&").replace(/\%3D/g, "=").replace(/\%3F/g, "?")), n
            }, h.getHashByUrl = function(e) {
                var t = String(e).replace(/([^#]*)#?([^#]*)#?(.*)/, "$2");
                return t = h.unescapeHash(t), t
            }, h.setTitle = function(e) {
                var t = e.title,
                    n;
                t || (n = h.getStateByIndex(0), n && n.url === e.url && (t = n.title || h.options.initialTitle));
                try {
                    r.getElementsByTagName("title")[0].innerHTML = t.replace("<", "&lt;").replace(">", "&gt;").replace(" & ", " &amp; ")
                } catch (i) {}
                return r.title = t, h
            }, h.queues = [], h.busy = function(e) {
                typeof e != "undefined" ? h.busy.flag = e : typeof h.busy.flag == "undefined" && (h.busy.flag = !1);
                if (!h.busy.flag) {
                    u(h.busy.timeout);
                    var t = function() {
                        var e, n, r;
                        if (h.busy.flag) return;
                        for (e = h.queues.length - 1; e >= 0; --e) {
                            n = h.queues[e];
                            if (n.length === 0) continue;
                            r = n.shift(), h.fireQueueItem(r), h.busy.timeout = o(t, h.options.busyDelay)
                        }
                    };
                    h.busy.timeout = o(t, h.options.busyDelay)
                }
                return h.busy.flag
            }, h.busy.flag = !1, h.fireQueueItem = function(e) {
                return e.callback.apply(e.scope || h, e.args || [])
            }, h.pushQueue = function(e) {
                return h.queues[e.queue || 0] = h.queues[e.queue || 0] || [], h.queues[e.queue || 0].push(e), h
            }, h.queue = function(e, t) {
                return typeof e == "function" && (e = {
                    callback: e
                }), typeof t != "undefined" && (e.queue = t), h.busy() ? h.pushQueue(e) : h.fireQueueItem(e), h
            }, h.clearQueue = function() {
                return h.busy.flag = !1, h.queues = [], h
            }, h.stateChanged = !1, h.doubleChecker = !1, h.doubleCheckComplete = function() {
                return h.stateChanged = !0, h.doubleCheckClear(), h
            }, h.doubleCheckClear = function() {
                return h.doubleChecker && (u(h.doubleChecker), h.doubleChecker = !1), h
            }, h.doubleCheck = function(e) {
                return h.stateChanged = !1, h.doubleCheckClear(), h.bugs.ieDoubleCheck && (h.doubleChecker = o(function() {
                    return h.doubleCheckClear(), h.stateChanged || e(), !0
                }, h.options.doubleCheckInterval)), h
            }, h.safariStatePoll = function() {
                var t = h.extractState(h.getLocationHref()),
                    n;
                if (!h.isLastSavedState(t)) return n = t, n || (n = h.createStateObject()), h.Adapter.trigger(e, "popstate"), h;
                return
            }, h.back = function(e) {
                return e !== !1 && h.busy() ? (h.pushQueue({
                    scope: h,
                    callback: h.back,
                    args: arguments,
                    queue: e
                }), !1) : (h.busy(!0), h.doubleCheck(function() {
                    h.back(!1)
                }), p.go(-1), !0)
            }, h.forward = function(e) {
                return e !== !1 && h.busy() ? (h.pushQueue({
                    scope: h,
                    callback: h.forward,
                    args: arguments,
                    queue: e
                }), !1) : (h.busy(!0), h.doubleCheck(function() {
                    h.forward(!1)
                }), p.go(1), !0)
            }, h.go = function(e, t) {
                var n;
                if (e > 0)
                    for (n = 1; n <= e; ++n) h.forward(t);
                else {
                    if (!(e < 0)) throw new Error("History.go: History.go requires a positive or negative integer passed.");
                    for (n = -1; n >= e; --n) h.back(t)
                }
                return h
            };
            if (h.emulated.pushState) {
                var v = function() {};
                h.pushState = h.pushState || v, h.replaceState = h.replaceState || v
            } else h.onPopState = function(t, n) {
                var r = !1,
                    i = !1,
                    s, o;
                return h.doubleCheckComplete(), s = h.getHash(), s ? (o = h.extractState(s || h.getLocationHref(), !0), o ? h.replaceState(o.data, o.title, o.url, !1) : (h.Adapter.trigger(e, "anchorchange"), h.busy(!1)), h.expectedStateId = !1, !1) : (r = h.Adapter.extractEventData("state", t, n) || !1, r ? i = h.getStateById(r) : h.expectedStateId ? i = h.getStateById(h.expectedStateId) : i = h.extractState(h.getLocationHref()), i || (i = h.createStateObject(null, null, h.getLocationHref())), h.expectedStateId = !1, h.isLastSavedState(i) ? (h.busy(!1), !1) : (h.storeState(i), h.saveState(i), h.setTitle(i), h.Adapter.trigger(e, "statechange"), h.busy(!1), !0))
            }, h.Adapter.bind(e, "popstate", h.onPopState), h.pushState = function(t, n, r, i) {
                if (h.getHashByUrl(r) && h.emulated.pushState) throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");
                if (i !== !1 && h.busy()) return h.pushQueue({
                    scope: h,
                    callback: h.pushState,
                    args: arguments,
                    queue: i
                }), !1;
                h.busy(!0);
                var s = h.createStateObject(t, n, r);
                return h.isLastSavedState(s) ? h.busy(!1) : (h.storeState(s), h.expectedStateId = s.id, p.pushState(s.id, s.title, s.url), h.Adapter.trigger(e, "popstate")), !0
            }, h.replaceState = function(t, n, r, i) {
                if (h.getHashByUrl(r) && h.emulated.pushState) throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");
                if (i !== !1 && h.busy()) return h.pushQueue({
                    scope: h,
                    callback: h.replaceState,
                    args: arguments,
                    queue: i
                }), !1;
                h.busy(!0);
                var s = h.createStateObject(t, n, r);
                return h.isLastSavedState(s) ? h.busy(!1) : (h.storeState(s), h.expectedStateId = s.id, p.replaceState(s.id, s.title, s.url), h.Adapter.trigger(e, "popstate")), !0
            };
            if (s) {
                try {
                    h.store = l.parse(s.getItem("History.store")) || {}
                } catch (m) {
                    h.store = {}
                }
                h.normalizeStore()
            } else h.store = {}, h.normalizeStore();
            h.Adapter.bind(e, "unload", h.clearAllIntervals), h.saveState(h.storeState(h.extractState(h.getLocationHref(), !0))), s && (h.onUnload = function() {
                var e, t, n;
                try {
                    e = l.parse(s.getItem("History.store")) || {}
                } catch (r) {
                    e = {}
                }
                e.idToState = e.idToState || {}, e.urlToId = e.urlToId || {}, e.stateToId = e.stateToId || {};
                for (t in h.idToState) {
                    if (!h.idToState.hasOwnProperty(t)) continue;
                    e.idToState[t] = h.idToState[t]
                }
                for (t in h.urlToId) {
                    if (!h.urlToId.hasOwnProperty(t)) continue;
                    e.urlToId[t] = h.urlToId[t]
                }
                for (t in h.stateToId) {
                    if (!h.stateToId.hasOwnProperty(t)) continue;
                    e.stateToId[t] = h.stateToId[t]
                }
                h.store = e, h.normalizeStore(), n = l.stringify(e);
                try {
                    s.setItem("History.store", n)
                } catch (i) {
                    if (i.code !== DOMException.QUOTA_EXCEEDED_ERR) throw i;
                    s.length && (s.removeItem("History.store"), s.setItem("History.store", n))
                }
            }, h.intervalList.push(a(h.onUnload, h.options.storeInterval)), h.Adapter.bind(e, "beforeunload", h.onUnload), h.Adapter.bind(e, "unload", h.onUnload));
            if (!h.emulated.pushState) {
                h.bugs.safariPoll && h.intervalList.push(a(h.safariStatePoll, h.options.safariPollInterval));
                if (i.vendor === "Apple Computer, Inc." || (i.appCodeName || "") === "Mozilla") h.Adapter.bind(e, "hashchange", function() {
                    h.Adapter.trigger(e, "popstate")
                }), h.getHash() && h.Adapter.onDomLoad(function() {
                    h.Adapter.trigger(e, "hashchange")
                })
            }
        }, (!h.options || !h.options.delayInit) && h.init()
    }(window);
/**
 * Copyright (c) 2007-2015 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
 * Licensed under MIT
 * @author Ariel Flesler
 * @version 2.1.1
 */
;
(function(f) {
    "use strict";
    "function" === typeof define && define.amd ? define(["jquery"], f) : "undefined" !== typeof module && module.exports ? module.exports = f(require("jquery")) : f(jQuery)
})(function($) {
    "use strict";

    function n(a) {
        return !a.nodeName || -1 !== $.inArray(a.nodeName.toLowerCase(), ["iframe", "#document", "html", "body"])
    }

    function h(a) {
        return $.isFunction(a) || $.isPlainObject(a) ? a : {
            top: a,
            left: a
        }
    }
    var p = $.scrollTo = function(a, d, b) {
        return $(window).scrollTo(a, d, b)
    };
    p.defaults = {
        axis: "xy",
        duration: 0,
        limit: !0
    };
    $.fn.scrollTo = function(a, d, b) {
        "object" === typeof d && (b = d, d = 0);
        "function" === typeof b && (b = {
            onAfter: b
        });
        "max" === a && (a = 9E9);
        b = $.extend({}, p.defaults, b);
        d = d || b.duration;
        var u = b.queue && 1 < b.axis.length;
        u && (d /= 2);
        b.offset = h(b.offset);
        b.over = h(b.over);
        return this.each(function() {
            function k(a) {
                var k = $.extend({}, b, {
                    queue: !0,
                    duration: d,
                    complete: a && function() {
                        a.call(q, e, b)
                    }
                });
                r.animate(f, k)
            }
            if (null !== a) {
                var l = n(this),
                    q = l ? this.contentWindow || window : this,
                    r = $(q),
                    e = a,
                    f = {},
                    t;
                switch (typeof e) {
                    case "number":
                    case "string":
                        if (/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(e)) {
                            e = h(e);
                            break
                        }
                        e = l ? $(e) : $(e, q);
                        if (!e.length) return;
                    case "object":
                        if (e.is || e.style) t = (e = $(e)).offset()
                }
                var v = $.isFunction(b.offset) && b.offset(q, e) || b.offset;
                $.each(b.axis.split(""), function(a, c) {
                    var d = "x" === c ? "Left" : "Top",
                        m = d.toLowerCase(),
                        g = "scroll" + d,
                        h = r[g](),
                        n = p.max(q, c);
                    t ? (f[g] = t[m] + (l ? 0 : h - r.offset()[m]), b.margin && (f[g] -= parseInt(e.css("margin" + d), 10) || 0, f[g] -= parseInt(e.css("border" + d + "Width"), 10) || 0), f[g] += v[m] || 0, b.over[m] && (f[g] += e["x" === c ? "width" : "height"]() * b.over[m])) : (d = e[m], f[g] = d.slice && "%" === d.slice(-1) ? parseFloat(d) / 100 * n : d);
                    b.limit && /^\d+$/.test(f[g]) && (f[g] = 0 >= f[g] ? 0 : Math.min(f[g], n));
                    !a && 1 < b.axis.length && (h === f[g] ? f = {} : u && (k(b.onAfterFirst), f = {}))
                });
                k(b.onAfter)
            }
        })
    };
    p.max = function(a, d) {
        var b = "x" === d ? "Width" : "Height",
            h = "scroll" + b;
        if (!n(a)) return a[h] - $(a)[b.toLowerCase()]();
        var b = "client" + b,
            k = a.ownerDocument || a.document,
            l = k.documentElement,
            k = k.body;
        return Math.max(l[h], k[h]) - Math.min(l[b], k[b])
    };
    $.Tween.propHooks.scrollLeft = $.Tween.propHooks.scrollTop = {
        get: function(a) {
            return $(a.elem)[a.prop]()
        },
        set: function(a) {
            var d = this.get(a);
            if (a.options.interrupt && a._last && a._last !== d) return $(a.elem).stop();
            var b = Math.round(a.now);
            d !== b && ($(a.elem)[a.prop](b), a._last = this.get(a))
        }
    };
    return p
});
! function(t) {
    "use strict";
    var e, i, n, s, o = "dotdotdot",
        r = "3.2.2";
    t[o] && t[o].version > r || (t[o] = function(t, e) {
        this.$dot = t, this.api = ["getInstance", "truncate", "restore", "destroy", "watch", "unwatch"], this.opts = e;
        var i = this.$dot.data(o);
        return i && i.destroy(), this.init(), this.truncate(), this.opts.watch && this.watch(), this
    }, t[o].version = r, t[o].uniqueId = 0, t[o].defaults = {
        ellipsis: "â¦ ",
        callback: function(t) {},
        truncate: "word",
        tolerance: 0,
        keep: null,
        watch: "window",
        height: null
    }, t[o].prototype = {
        init: function() {
            this.watchTimeout = null, this.watchInterval = null, this.uniqueId = t[o].uniqueId++, this.originalStyle = this.$dot.attr("style") || "", this.originalContent = this._getOriginalContent(), "break-word" !== this.$dot.css("word-wrap") && this.$dot.css("word-wrap", "break-word"), "nowrap" === this.$dot.css("white-space") && this.$dot.css("white-space", "normal"), null === this.opts.height && (this.opts.height = this._getMaxHeight()), "string" == typeof this.opts.ellipsis && (this.opts.ellipsis = document.createTextNode(this.opts.ellipsis))
        },
        getInstance: function() {
            return this
        },
        truncate: function() {
            this.$inner = this.$dot.wrapInner("<div />").children().css({
                display: "block",
                height: "auto",
                width: "auto",
                border: "none",
                padding: 0,
                margin: 0
            }), this.$inner.empty().append(this.originalContent.clone(!0)), this.maxHeight = this._getMaxHeight();
            var t = !1;
            return this._fits() || (t = !0, this._truncateToNode(this.$inner[0])), this.$dot[t ? "addClass" : "removeClass"](e.truncated), this.$inner.replaceWith(this.$inner.contents()), this.$inner = null, this.opts.callback.call(this.$dot[0], t), t
        },
        restore: function() {
            this.unwatch(), this.$dot.empty().append(this.originalContent).attr("style", this.originalStyle).removeClass(e.truncated)
        },
        destroy: function() {
            this.restore(), this.$dot.data(o, null)
        },
        watch: function() {
            var t = this;
            this.unwatch();
            var e = {};
            "window" == this.opts.watch ? s.on(n.resize + t.uniqueId, function(i) {
                t.watchTimeout && clearTimeout(t.watchTimeout), t.watchTimeout = setTimeout(function() {
                    e = t._watchSizes(e, s, "width", "height")
                }, 100)
            }) : this.watchInterval = setInterval(function() {
                e = t._watchSizes(e, t.$dot, "innerWidth", "innerHeight")
            }, 500)
        },
        unwatch: function() {
            s.off(n.resize + this.uniqueId), this.watchInterval && clearInterval(this.watchInterval), this.watchTimeout && clearTimeout(this.watchTimeout)
        },
        _api: function() {
            var e = this,
                i = {};
            return t.each(this.api, function(t) {
                var n = this;
                i[n] = function() {
                    var t = e[n].apply(e, arguments);
                    return void 0 === t ? i : t
                }
            }), i
        },
        _truncateToNode: function(i) {
            var n = [],
                s = [];
            if (t(i).contents().each(function() {
                    var i = t(this);
                    if (!i.hasClass(e.keep)) {
                        var o = document.createComment("");
                        i.replaceWith(o), s.push(this), n.push(o)
                    }
                }), s.length) {
                for (var o = 0; o < s.length; o++) {
                    t(n[o]).replaceWith(s[o]), t(s[o]).append(this.opts.ellipsis);
                    var r = this._fits();
                    if (t(this.opts.ellipsis, s[o]).remove(), !r) {
                        if ("node" == this.opts.truncate && o > 1) return void t(s[o - 2]).remove();
                        break
                    }
                }
                for (var h = o; h < n.length; h++) t(n[h]).remove();
                var a = s[Math.max(0, Math.min(o, s.length - 1))];
                if (1 == a.nodeType) {
                    var d = t("<" + a.nodeName + " />");
                    d.append(this.opts.ellipsis), t(a).replaceWith(d), this._fits() ? d.replaceWith(a) : (d.remove(), a = s[Math.max(0, o - 1)])
                }
                1 == a.nodeType ? this._truncateToNode(a) : this._truncateToWord(a)
            }
        },
        _truncateToWord: function(t) {
            for (var e = t, i = this, n = this.__getTextContent(e), s = -1 !== n.indexOf(" ") ? " " : "ã", o = n.split(s), r = "", h = o.length; h >= 0; h--)
                if (r = o.slice(0, h).join(s), i.__setTextContent(e, i._addEllipsis(r)), i._fits()) {
                    "letter" == i.opts.truncate && (i.__setTextContent(e, o.slice(0, h + 1).join(s)), i._truncateToLetter(e));
                    break
                }
        },
        _truncateToLetter: function(t) {
            for (var e = this, i = this.__getTextContent(t).split(""), n = "", s = i.length; s >= 0 && (!(n = i.slice(0, s).join("")).length || (e.__setTextContent(t, e._addEllipsis(n)), !e._fits())); s--);
        },
        _fits: function() {
            return this.$inner.innerHeight() <= this.maxHeight + this.opts.tolerance
        },
        _addEllipsis: function(e) {
            for (var i = [" ", "ã", ",", ";", ".", "!", "?"]; t.inArray(e.slice(-1), i) > -1;) e = e.slice(0, -1);
            return e += this.__getTextContent(this.opts.ellipsis)
        },
        _getOriginalContent: function() {
            var i = this;
            return this.$dot.find("script, style").addClass(e.keep), this.opts.keep && this.$dot.find(this.opts.keep).addClass(e.keep), this.$dot.find("*").not("." + e.keep).add(this.$dot).contents().each(function() {
                var e = this,
                    n = t(this);
                if (3 == e.nodeType) {
                    if ("" == t.trim(i.__getTextContent(e))) {
                        if (n.parent().is("table, thead, tbody, tfoot, tr, dl, ul, ol, video")) return void n.remove();
                        if (n.prev().is("div, p, table, td, td, dt, dd, li")) return void n.remove();
                        if (n.next().is("div, p, table, td, td, dt, dd, li")) return void n.remove();
                        if (!n.prev().length) return void n.remove();
                        if (!n.next().length) return void n.remove()
                    }
                } else 8 == e.nodeType && n.remove()
            }), this.$dot.contents()
        },
        _getMaxHeight: function() {
            if ("number" == typeof this.opts.height) return this.opts.height;
            for (var t = ["maxHeight", "height"], e = 0, i = 0; i < t.length; i++)
                if ("px" == (e = window.getComputedStyle(this.$dot[0])[t[i]]).slice(-2)) {
                    e = parseFloat(e);
                    break
                }
            t = [];
            switch (this.$dot.css("boxSizing")) {
                case "border-box":
                    t.push("borderTopWidth"), t.push("borderBottomWidth");
                case "padding-box":
                    t.push("paddingTop"), t.push("paddingBottom")
            }
            for (i = 0; i < t.length; i++) {
                var n = window.getComputedStyle(this.$dot[0])[t[i]];
                "px" == n.slice(-2) && (e -= parseFloat(n))
            }
            return Math.max(e, 0)
        },
        _watchSizes: function(t, e, i, n) {
            if (this.$dot.is(":visible")) {
                var s = {
                    width: e[i](),
                    height: e[n]()
                };
                return t.width == s.width && t.height == s.height || this.truncate(), s
            }
            return t
        },
        __getTextContent: function(t) {
            for (var e = ["nodeValue", "textContent", "innerText"], i = 0; i < e.length; i++)
                if ("string" == typeof t[e[i]]) return t[e[i]];
            return ""
        },
        __setTextContent: function(t, e) {
            for (var i = ["nodeValue", "textContent", "innerText"], n = 0; n < i.length; n++) t[i[n]] = e
        }
    }, t.fn[o] = function(r) {
        return function() {
            s = t(window);
            e = {};
            i = {};
            n = {};
            t.each([e, i, n], function(t, e) {
                e.add = function(t) {
                    t = t.split(" ");
                    for (var i = 0, n = t.length; i < n; i++) e[t[i]] = e.ddd(t[i])
                }
            });
            e.ddd = function(t) {
                return "ddd-" + t
            };
            e.add("truncated keep");
            i.ddd = function(t) {
                return "ddd-" + t
            };
            n.ddd = function(t) {
                return t + ".ddd"
            };
            n.add("resize");
            (function() {})
        }(), r = t.extend(!0, {}, t[o].defaults, r), this.each(function() {
            t(this).data(o, new t[o](t(this), r)._api())
        })
    })
}(jQuery);
/**!
 * easy-pie-chart
 * Lightweight plugin to render simple, animated and retina optimized pie charts
 *
 * @license
 * @author Robert Fleischmann <rendro87@gmail.com> (http://robert-fleischmann.de)
 * @version 2.1.7
 **/
! function(a, b) {
    "function" == typeof define && define.amd ? define(["jquery"], function(a) {
        return b(a)
    }) : "object" == typeof exports ? module.exports = b(require("jquery")) : b(jQuery)
}(this, function(a) {
    var b = function(a, b) {
            var c, d = document.createElement("canvas");
            a.appendChild(d), "object" == typeof G_vmlCanvasManager && G_vmlCanvasManager.initElement(d);
            var e = d.getContext("2d");
            d.width = d.height = b.size;
            var f = 1;
            window.devicePixelRatio > 1 && (f = window.devicePixelRatio, d.style.width = d.style.height = [b.size, "px"].join(""), d.width = d.height = b.size * f, e.scale(f, f)), e.translate(b.size / 2, b.size / 2), e.rotate((-0.5 + b.rotate / 180) * Math.PI);
            var g = (b.size - b.lineWidth) / 2;
            b.scaleColor && b.scaleLength && (g -= b.scaleLength + 2), Date.now = Date.now || function() {
                return +new Date
            };
            var h = function(a, b, c) {
                    c = Math.min(Math.max(-1, c || 0), 1);
                    var d = 0 >= c ? !0 : !1;
                    e.beginPath(), e.arc(0, 0, g, 0, 2 * Math.PI * c, d), e.strokeStyle = a, e.lineWidth = b, e.stroke()
                },
                i = function() {
                    var a, c;
                    e.lineWidth = 1, e.fillStyle = b.scaleColor, e.save();
                    for (var d = 24; d > 0; --d) d % 6 === 0 ? (c = b.scaleLength, a = 0) : (c = .6 * b.scaleLength, a = b.scaleLength - c), e.fillRect(-b.size / 2 + a, 0, c, 1), e.rotate(Math.PI / 12);
                    e.restore()
                },
                j = function() {
                    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(a) {
                        window.setTimeout(a, 1e3 / 60)
                    }
                }(),
                k = function() {
                    b.scaleColor && i(), b.trackColor && h(b.trackColor, b.trackWidth || b.lineWidth, 1)
                };
            this.getCanvas = function() {
                return d
            }, this.getCtx = function() {
                return e
            }, this.clear = function() {
                e.clearRect(b.size / -2, b.size / -2, b.size, b.size)
            }, this.draw = function(a) {
                b.scaleColor || b.trackColor ? e.getImageData && e.putImageData ? c ? e.putImageData(c, 0, 0) : (k(), c = e.getImageData(0, 0, b.size * f, b.size * f)) : (this.clear(), k()) : this.clear(), e.lineCap = b.lineCap;
                var d;
                d = "function" == typeof b.barColor ? b.barColor(a) : b.barColor, h(d, b.lineWidth, a / 100)
            }.bind(this), this.animate = function(a, c) {
                var d = Date.now();
                b.onStart(a, c);
                var e = function() {
                    var f = Math.min(Date.now() - d, b.animate.duration),
                        g = b.easing(this, f, a, c - a, b.animate.duration);
                    this.draw(g), b.onStep(a, c, g), f >= b.animate.duration ? b.onStop(a, c) : j(e)
                }.bind(this);
                j(e)
            }.bind(this)
        },
        c = function(a, c) {
            var d = {
                barColor: "#ef1e25",
                trackColor: "#f9f9f9",
                scaleColor: "#dfe0e0",
                scaleLength: 5,
                lineCap: "round",
                lineWidth: 3,
                trackWidth: void 0,
                size: 110,
                rotate: 0,
                animate: {
                    duration: 1e3,
                    enabled: !0
                },
                easing: function(a, b, c, d, e) {
                    return b /= e / 2, 1 > b ? d / 2 * b * b + c : -d / 2 * (--b * (b - 2) - 1) + c
                },
                onStart: function(a, b) {},
                onStep: function(a, b, c) {},
                onStop: function(a, b) {}
            };
            if ("undefined" != typeof b) d.renderer = b;
            else {
                if ("undefined" == typeof SVGRenderer) throw new Error("Please load either the SVG- or the CanvasRenderer");
                d.renderer = SVGRenderer
            }
            var e = {},
                f = 0,
                g = function() {
                    this.el = a, this.options = e;
                    for (var b in d) d.hasOwnProperty(b) && (e[b] = c && "undefined" != typeof c[b] ? c[b] : d[b], "function" == typeof e[b] && (e[b] = e[b].bind(this)));
                    "string" == typeof e.easing && "undefined" != typeof jQuery && jQuery.isFunction(jQuery.easing[e.easing]) ? e.easing = jQuery.easing[e.easing] : e.easing = d.easing, "number" == typeof e.animate && (e.animate = {
                        duration: e.animate,
                        enabled: !0
                    }), "boolean" != typeof e.animate || e.animate || (e.animate = {
                        duration: 1e3,
                        enabled: e.animate
                    }), this.renderer = new e.renderer(a, e), this.renderer.draw(f), a.dataset && a.dataset.percent ? this.update(parseFloat(a.dataset.percent)) : a.getAttribute && a.getAttribute("data-percent") && this.update(parseFloat(a.getAttribute("data-percent")))
                }.bind(this);
            this.update = function(a) {
                return a = parseFloat(a), e.animate.enabled ? this.renderer.animate(f, a) : this.renderer.draw(a), f = a, this
            }.bind(this), this.disableAnimation = function() {
                return e.animate.enabled = !1, this
            }, this.enableAnimation = function() {
                return e.animate.enabled = !0, this
            }, g()
        };
    a.fn.easyPieChart = function(b) {
        return this.each(function() {
            var d;
            a.data(this, "easyPieChart") || (d = a.extend({}, b, a(this).data()), a.data(this, "easyPieChart", new c(this, d)))
        })
    }
});
/**
 * jQuery serializeObject
 * @copyright 2014, macek <paulmacek@gmail.com>
 * @link https://github.com/macek/jquery-serialize-object
 * @license BSD
 * @version 2.5.0
 */
! function(e, i) {
    if ("function" == typeof define && define.amd) define(["exports", "jquery"], function(e, r) {
        return i(e, r)
    });
    else if ("undefined" != typeof exports) {
        var r = require("jquery");
        i(exports, r)
    } else i(e, e.jQuery || e.Zepto || e.ender || e.$)
}(this, function(e, i) {
    function r(e, r) {
        function n(e, i, r) {
            return e[i] = r, e
        }

        function a(e, i) {
            for (var r, a = e.match(t.key); void 0 !== (r = a.pop());)
                if (t.push.test(r)) {
                    var u = s(e.replace(/\[\]$/, ""));
                    i = n([], u, i)
                } else t.fixed.test(r) ? i = n([], r, i) : t.named.test(r) && (i = n({}, r, i));
            return i
        }

        function s(e) {
            return void 0 === h[e] && (h[e] = 0), h[e]++
        }

        function u(e) {
            switch (i('[name="' + e.name + '"]', r).attr("type")) {
                case "checkbox":
                    return "on" === e.value ? !0 : e.value;
                default:
                    return e.value
            }
        }

        function f(i) {
            if (!t.validate.test(i.name)) return this;
            var r = a(i.name, u(i));
            return l = e.extend(!0, l, r), this
        }

        function d(i) {
            if (!e.isArray(i)) throw new Error("formSerializer.addPairs expects an Array");
            for (var r = 0, t = i.length; t > r; r++) this.addPair(i[r]);
            return this
        }

        function o() {
            return l
        }

        function c() {
            return JSON.stringify(o())
        }
        var l = {},
            h = {};
        this.addPair = f, this.addPairs = d, this.serialize = o, this.serializeJSON = c
    }
    var t = {
        validate: /^[a-z_][a-z0-9_]*(?:\[(?:\d*|[a-z0-9_]+)\])*$/i,
        key: /[a-z0-9_]+|(?=\[\])/gi,
        push: /^$/,
        fixed: /^\d+$/,
        named: /^[a-z0-9_]+$/i
    };
    return r.patterns = t, r.serializeObject = function() {
        return new r(i, this).addPairs(this.serializeArray()).serialize()
    }, r.serializeJSON = function() {
        return new r(i, this).addPairs(this.serializeArray()).serializeJSON()
    }, "undefined" != typeof i.fn && (i.fn.serializeObject = r.serializeObject, i.fn.serializeJSON = r.serializeJSON), e.FormSerializer = r, r
});
! function(a) {
    function b(b) {
        if ("string" == typeof b.data && (b.data = {
                keys: b.data
            }), b.data && b.data.keys && "string" == typeof b.data.keys) {
            var c = b.handler,
                d = b.data.keys.toLowerCase().split(" ");
            b.handler = function(b) {
                if (this === b.target || !(a.hotkeys.options.filterInputAcceptingElements && a.hotkeys.textInputTypes.test(b.target.nodeName) || a.hotkeys.options.filterContentEditable && a(b.target).attr("contenteditable") || a.hotkeys.options.filterTextInputs && a.inArray(b.target.type, a.hotkeys.textAcceptingInputTypes) > -1)) {
                    var e = "keypress" !== b.type && a.hotkeys.specialKeys[b.which],
                        f = String.fromCharCode(b.which).toLowerCase(),
                        g = "",
                        h = {};
                    a.each(["alt", "ctrl", "shift"], function(a, c) {
                        b[c + "Key"] && e !== c && (g += c + "+")
                    }), b.metaKey && !b.ctrlKey && "meta" !== e && (g += "meta+"), b.metaKey && "meta" !== e && g.indexOf("alt+ctrl+shift+") > -1 && (g = g.replace("alt+ctrl+shift+", "hyper+")), e ? h[g + e] = !0 : (h[g + f] = !0, h[g + a.hotkeys.shiftNums[f]] = !0, "shift+" === g && (h[a.hotkeys.shiftNums[f]] = !0));
                    for (var i = 0, j = d.length; i < j; i++)
                        if (h[d[i]]) return c.apply(this, arguments)
                }
            }
        }
    }
    a.hotkeys = {
        version: "0.2.0",
        specialKeys: {
            8: "backspace",
            9: "tab",
            10: "return",
            13: "return",
            16: "shift",
            17: "ctrl",
            18: "alt",
            19: "pause",
            20: "capslock",
            27: "esc",
            32: "space",
            33: "pageup",
            34: "pagedown",
            35: "end",
            36: "home",
            37: "left",
            38: "up",
            39: "right",
            40: "down",
            45: "insert",
            46: "del",
            59: ";",
            61: "=",
            96: "0",
            97: "1",
            98: "2",
            99: "3",
            100: "4",
            101: "5",
            102: "6",
            103: "7",
            104: "8",
            105: "9",
            106: "*",
            107: "+",
            109: "-",
            110: ".",
            111: "/",
            112: "f1",
            113: "f2",
            114: "f3",
            115: "f4",
            116: "f5",
            117: "f6",
            118: "f7",
            119: "f8",
            120: "f9",
            121: "f10",
            122: "f11",
            123: "f12",
            144: "numlock",
            145: "scroll",
            173: "-",
            186: ";",
            187: "=",
            188: ",",
            189: "-",
            190: ".",
            191: "/",
            192: "`",
            219: "[",
            220: "\\",
            221: "]",
            222: "'"
        },
        shiftNums: {
            "`": "~",
            1: "!",
            2: "@",
            3: "#",
            4: "$",
            5: "%",
            6: "^",
            7: "&",
            8: "*",
            9: "(",
            0: ")",
            "-": "_",
            "=": "+",
            ";": ": ",
            "'": '"',
            ",": "<",
            ".": ">",
            "/": "?",
            "\\": "|"
        },
        textAcceptingInputTypes: ["text", "password", "number", "email", "url", "range", "date", "month", "week", "time", "datetime", "datetime-local", "search", "color", "tel"],
        textInputTypes: /textarea|input|select/i,
        options: {
            filterInputAcceptingElements: !0,
            filterTextInputs: !0,
            filterContentEditable: !0
        }
    }, a.each(["keydown", "keyup", "keypress"], function() {
        a.event.special[this] = {
            add: b
        }
    })
}(jQuery || this.jQuery || window.jQuery);
/** 
 * Kendo UI v2018.1.221 (http://www.telerik.com/kendo-ui)                                                                                                                                               
 * Copyright 2018 Telerik AD. All rights reserved.                                                                                                                                                      
 *                                                                                                                                                                                                      
 * Kendo UI commercial licenses may be obtained at                                                                                                                                                      
 * http://www.telerik.com/purchase/license-agreement/kendo-ui-complete                                                                                                                                  
 * If you do not own a commercial license, this file shall be governed by the trial license terms.                                                                                                      

*/
! function(e, define) {
    define("kendo.core.min", ["jquery"], e)
}(function() {
    return function(e, t, n) {
        function r() {}

        function o(e, t) {
            if (t) return "'" + e.split("'").join("\\'").split('\\"').join('\\\\\\"').replace(/\n/g, "\\n").replace(/\r/g, "\\r").replace(/\t/g, "\\t") + "'";
            var n = e.charAt(0),
                r = e.substring(1);
            return "=" === n ? "+(" + r + ")+" : ":" === n ? "+$kendoHtmlEncode(" + r + ")+" : ";" + e + ";$kendoOutput+="
        }

        function i(e, t, n) {
            return e += "", t = t || 2, n = t - e.length, n ? L[t].substring(0, n) + e : e
        }

        function a(e) {
            var t = e.css(ye.support.transitions.css + "box-shadow") || e.css("box-shadow"),
                n = t ? t.match(ze) || [0, 0, 0, 0, 0] : [0, 0, 0, 0, 0],
                r = Te.max(+n[3], +(n[4] || 0));
            return {
                left: -n[1] + r,
                right: +n[1] + r,
                bottom: +n[2] + r
            }
        }

        function s(t, n) {
            var r, o, i, a, s, u, l, c = ke.browser,
                d = ye._outerWidth,
                f = ye._outerHeight;
            return t.parent().hasClass("k-animation-container") ? (u = t.parent(".k-animation-container"), l = u[0].style, u.is(":hidden") && u.css({
                display: "",
                position: ""
            }), r = Oe.test(l.width) || Oe.test(l.height), r || u.css({
                width: n ? d(t) + 1 : d(t),
                height: f(t),
                boxSizing: "content-box",
                mozBoxSizing: "content-box",
                webkitBoxSizing: "content-box"
            })) : (o = t[0].style.width, i = t[0].style.height, a = Oe.test(o), s = Oe.test(i), r = a || s, !a && (!n || n && o) && (o = n ? d(t) + 1 : d(t)), !s && (!n || n && i) && (i = f(t)), t.wrap(e("<div/>").addClass("k-animation-container").css({
                width: o,
                height: i
            })), r && t.css({
                width: "100%",
                height: "100%",
                boxSizing: "border-box",
                mozBoxSizing: "border-box",
                webkitBoxSizing: "border-box"
            })), c.msie && Te.floor(c.version) <= 7 && (t.css({
                zoom: 1
            }), t.children(".k-menu").width(t.width())), t.parent()
        }

        function u(e) {
            var t = 1,
                n = arguments.length;
            for (t = 1; t < n; t++) l(e, arguments[t]);
            return e
        }

        function l(e, t) {
            var n, r, o, i, a, s = ye.data.ObservableArray,
                u = ye.data.LazyObservableArray,
                c = ye.data.DataSource,
                d = ye.data.HierarchicalDataSource;
            for (n in t) r = t[n], o = typeof r, i = o === Ae && null !== r ? r.constructor : null, i && i !== Array && i !== s && i !== u && i !== c && i !== d && i !== RegExp ? r instanceof Date ? e[n] = new Date(r.getTime()) : _(r.clone) ? e[n] = r.clone() : (a = e[n], e[n] = typeof a === Ae ? a || {} : {}, l(e[n], r)) : o !== Fe && (e[n] = r);
            return e
        }

        function c(e, t, r) {
            for (var o in t)
                if (t.hasOwnProperty(o) && t[o].test(e)) return o;
            return r !== n ? r : e
        }

        function d(e) {
            return e.replace(/([a-z][A-Z])/g, function(e) {
                return e.charAt(0) + "-" + e.charAt(1).toLowerCase()
            })
        }

        function f(e) {
            return e.replace(/\-(\w)/g, function(e, t) {
                return t.toUpperCase()
            })
        }

        function p(t, n) {
            var r, o = {};
            return document.defaultView && document.defaultView.getComputedStyle ? (r = document.defaultView.getComputedStyle(t, ""), n && e.each(n, function(e, t) {
                o[t] = r.getPropertyValue(t)
            })) : (r = t.currentStyle, n && e.each(n, function(e, t) {
                o[t] = r[f(t)]
            })), ye.size(o) || (o = r), o
        }

        function m(e) {
            if (e && e.className && "string" == typeof e.className && e.className.indexOf("k-auto-scrollable") > -1) return !0;
            var t = p(e, ["overflow"]).overflow;
            return "auto" == t || "scroll" == t
        }

        function h(t, r) {
            var o, i = ke.browser.webkit,
                a = ke.browser.mozilla,
                s = t instanceof e ? t[0] : t;
            if (t) return o = ke.isRtl(t), r === n ? o && i ? s.scrollWidth - s.clientWidth - s.scrollLeft : Math.abs(s.scrollLeft) : (s.scrollLeft = o && i ? s.scrollWidth - s.clientWidth - r : o && a ? -r : r, n)
        }

        function g(e) {
            var t, n = 0;
            for (t in e) e.hasOwnProperty(t) && "toJSON" != t && n++;
            return n
        }

        function y(e, n, r) {
            var o, i, a;
            return n || (n = "offset"), o = e[n](), i = {
                top: o.top,
                right: o.right,
                bottom: o.bottom,
                left: o.left
            }, ke.browser.msie && (ke.pointers || ke.msPointers) && !r && (a = ke.isRtl(e) ? 1 : -1, i.top -= t.pageYOffset - document.documentElement.scrollTop, i.left -= t.pageXOffset + a * document.documentElement.scrollLeft), i
        }

        function v(e) {
            var t = {};
            return be("string" == typeof e ? e.split(" ") : e, function(e) {
                t[e] = this
            }), t
        }

        function b(e) {
            return new ye.effects.Element(e)
        }

        function w(e, t, n, r) {
            return typeof e === He && (_(t) && (r = t, t = 400, n = !1), _(n) && (r = n, n = !1), typeof t === Pe && (n = t, t = 400), e = {
                effects: e,
                duration: t,
                reverse: n,
                complete: r
            }), ve({
                effects: {},
                duration: 400,
                reverse: !1,
                init: Se,
                teardown: Se,
                hide: !1
            }, e, {
                completeCallback: e.complete,
                complete: Se
            })
        }

        function M(t, n, r, o, i) {
            for (var a, s = 0, u = t.length; s < u; s++) a = e(t[s]), a.queue(function() {
                B.promise(a, w(n, r, o, i))
            });
            return t
        }

        function S(e, t, n, r) {
            return t && (t = t.split(" "), be(t, function(t, n) {
                e.toggleClass(n, r)
            })), e
        }

        function T(e) {
            return ("" + e).replace(Y, "&amp;").replace(q, "&lt;").replace(G, "&gt;").replace(J, "&quot;").replace(V, "&#39;")
        }

        function x(e, t) {
            var r;
            return 0 === t.indexOf("data") && (t = t.substring(4), t = t.charAt(0).toLowerCase() + t.substring(1)), t = t.replace(oe, "-$1"), r = e.getAttribute("data-" + ye.ns + t), null === r ? r = n : "null" === r ? r = null : "true" === r ? r = !0 : "false" === r ? r = !1 : Ce.test(r) && "mask" != t ? r = parseFloat(r) : ne.test(r) && !re.test(r) && (r = Function("return (" + r + ")")()), r
        }

        function k(t, r) {
            var o, i, a = {};
            for (o in r) i = x(t, o), i !== n && (te.test(o) && (i = "string" == typeof i ? ye.template(e("#" + i).html()) : t.getAttribute(o)), a[o] = i);
            return a
        }

        function O(t, n) {
            return e.contains(t, n) ? -1 : 1
        }

        function D() {
            var t = e(this);
            return e.inArray(t.attr("data-" + ye.ns + "role"), ["slider", "rangeslider"]) > -1 || t.is(":visible")
        }

        function z(e, t) {
            var n = e.nodeName.toLowerCase();
            return (/input|select|textarea|button|object/.test(n) ? !e.disabled : "a" === n ? e.href || t : t) && C(e)
        }

        function C(t) {
            return e.expr.filters.visible(t) && !e(t).parents().addBack().filter(function() {
                return "hidden" === e.css(this, "visibility")
            }).length
        }

        function E(e, t) {
            return new E.fn.init(e, t)
        }
        var H, _, A, N, P, F, R, U, I, W, $, L, j, B, Y, q, J, V, G, K, Q, Z, X, ee, te, ne, re, oe, ie, ae, se, ue, le, ce, de, fe, pe, me, he, ge, ye = t.kendo = t.kendo || {
                cultures: {}
            },
            ve = e.extend,
            be = e.each,
            we = e.isArray,
            Me = e.proxy,
            Se = e.noop,
            Te = Math,
            xe = t.JSON || {},
            ke = {},
            Oe = /%/,
            De = /\{(\d+)(:[^\}]+)?\}/g,
            ze = /(\d+(?:\.?)\d*)px\s*(\d+(?:\.?)\d*)px\s*(\d+(?:\.?)\d*)px\s*(\d+)?/i,
            Ce = /^(\+|-?)\d+(\.?)\d*$/,
            Ee = "function",
            He = "string",
            _e = "number",
            Ae = "object",
            Ne = "null",
            Pe = "boolean",
            Fe = "undefined",
            Re = {},
            Ue = {},
            Ie = [].slice;
        ye.version = "2018.1.221".replace(/^\s+|\s+$/g, ""), r.extend = function(e) {
                var t, n, r = function() {},
                    o = this,
                    i = e && e.init ? e.init : function() {
                        o.apply(this, arguments)
                    };
                r.prototype = o.prototype, n = i.fn = i.prototype = new r;
                for (t in e) n[t] = null != e[t] && e[t].constructor === Object ? ve(!0, {}, r.prototype[t], e[t]) : e[t];
                return n.constructor = i, i.extend = o.extend, i
            }, r.prototype._initOptions = function(e) {
                this.options = u({}, this.options, e)
            }, _ = ye.isFunction = function(e) {
                return "function" == typeof e
            }, A = function() {
                this._defaultPrevented = !0
            }, N = function() {
                return this._defaultPrevented === !0
            }, P = r.extend({
                init: function() {
                    this._events = {}
                },
                bind: function(e, t, r) {
                    var o, i, a, s, u, l = this,
                        c = typeof e === He ? [e] : e,
                        d = typeof t === Ee;
                    if (t === n) {
                        for (o in e) l.bind(o, e[o]);
                        return l
                    }
                    for (o = 0, i = c.length; o < i; o++) e = c[o], s = d ? t : t[e], s && (r && (a = s, s = function() {
                        l.unbind(e, s), a.apply(l, arguments)
                    }, s.original = a), u = l._events[e] = l._events[e] || [], u.push(s));
                    return l
                },
                one: function(e, t) {
                    return this.bind(e, t, !0)
                },
                first: function(e, t) {
                    var n, r, o, i, a = this,
                        s = typeof e === He ? [e] : e,
                        u = typeof t === Ee;
                    for (n = 0, r = s.length; n < r; n++) e = s[n], o = u ? t : t[e], o && (i = a._events[e] = a._events[e] || [], i.unshift(o));
                    return a
                },
                trigger: function(e, t) {
                    var n, r, o = this,
                        i = o._events[e];
                    if (i) {
                        for (t = t || {}, t.sender = o, t._defaultPrevented = !1, t.preventDefault = A, t.isDefaultPrevented = N, i = i.slice(), n = 0, r = i.length; n < r; n++) i[n].call(o, t);
                        return t._defaultPrevented === !0
                    }
                    return !1
                },
                unbind: function(e, t) {
                    var r, o = this,
                        i = o._events[e];
                    if (e === n) o._events = {};
                    else if (i)
                        if (t)
                            for (r = i.length - 1; r >= 0; r--) i[r] !== t && i[r].original !== t || i.splice(r, 1);
                        else o._events[e] = [];
                    return o
                }
            }), F = /^\w+/, R = /\$\{([^}]*)\}/g, U = /\\\}/g, I = /__CURLY__/g, W = /\\#/g, $ = /__SHARP__/g, L = ["", "0", "00", "000", "0000"], H = {
                paramName: "data",
                useWithBlock: !0,
                render: function(e, t) {
                    var n, r, o = "";
                    for (n = 0, r = t.length; n < r; n++) o += e(t[n]);
                    return o
                },
                compile: function(e, t) {
                    var n, r, i, a = ve({}, this, t),
                        s = a.paramName,
                        u = s.match(F)[0],
                        l = a.useWithBlock,
                        c = "var $kendoOutput, $kendoHtmlEncode = kendo.htmlEncode;";
                    if (_(e)) return e;
                    for (c += l ? "with(" + s + "){" : "", c += "$kendoOutput=", r = e.replace(U, "__CURLY__").replace(R, "#=$kendoHtmlEncode($1)#").replace(I, "}").replace(W, "__SHARP__").split("#"), i = 0; i < r.length; i++) c += o(r[i], i % 2 === 0);
                    c += l ? ";}" : ";", c += "return $kendoOutput;", c = c.replace($, "#");
                    try {
                        return n = Function(u, c), n._slotCount = Math.floor(r.length / 2), n
                    } catch (d) {
                        throw Error(ye.format("Invalid template:'{0}' Generated code:'{1}'", e, c))
                    }
                }
            },
            function() {
                function e(e) {
                    return a.lastIndex = 0, a.test(e) ? '"' + e.replace(a, function(e) {
                        var t = s[e];
                        return typeof t === He ? t : "\\u" + ("0000" + e.charCodeAt(0).toString(16)).slice(-4)
                    }) + '"' : '"' + e + '"'
                }

                function t(i, a) {
                    var s, l, c, d, f, p, m = n,
                        h = a[i];
                    if (h && typeof h === Ae && typeof h.toJSON === Ee && (h = h.toJSON(i)), typeof o === Ee && (h = o.call(a, i, h)), p = typeof h, p === He) return e(h);
                    if (p === _e) return isFinite(h) ? h + "" : Ne;
                    if (p === Pe || p === Ne) return h + "";
                    if (p === Ae) {
                        if (!h) return Ne;
                        if (n += r, f = [], "[object Array]" === u.apply(h)) {
                            for (d = h.length, s = 0; s < d; s++) f[s] = t(s, h) || Ne;
                            return c = 0 === f.length ? "[]" : n ? "[\n" + n + f.join(",\n" + n) + "\n" + m + "]" : "[" + f.join(",") + "]", n = m, c
                        }
                        if (o && typeof o === Ae)
                            for (d = o.length, s = 0; s < d; s++) typeof o[s] === He && (l = o[s], c = t(l, h), c && f.push(e(l) + (n ? ": " : ":") + c));
                        else
                            for (l in h) Object.hasOwnProperty.call(h, l) && (c = t(l, h), c && f.push(e(l) + (n ? ": " : ":") + c));
                        return c = 0 === f.length ? "{}" : n ? "{\n" + n + f.join(",\n" + n) + "\n" + m + "}" : "{" + f.join(",") + "}", n = m, c
                    }
                }
                var n, r, o, a = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
                    s = {
                        "\b": "\\b",
                        "\t": "\\t",
                        "\n": "\\n",
                        "\f": "\\f",
                        "\r": "\\r",
                        '"': '\\"',
                        "\\": "\\\\"
                    },
                    u = {}.toString;
                typeof Date.prototype.toJSON !== Ee && (Date.prototype.toJSON = function() {
                    var e = this;
                    return isFinite(e.valueOf()) ? i(e.getUTCFullYear(), 4) + "-" + i(e.getUTCMonth() + 1) + "-" + i(e.getUTCDate()) + "T" + i(e.getUTCHours()) + ":" + i(e.getUTCMinutes()) + ":" + i(e.getUTCSeconds()) + "Z" : null
                }, String.prototype.toJSON = Number.prototype.toJSON = Boolean.prototype.toJSON = function() {
                    return this.valueOf()
                }), typeof xe.stringify !== Ee && (xe.stringify = function(e, i, a) {
                    var s;
                    if (n = "", r = "", typeof a === _e)
                        for (s = 0; s < a; s += 1) r += " ";
                    else typeof a === He && (r = a);
                    if (o = i, i && typeof i !== Ee && (typeof i !== Ae || typeof i.length !== _e)) throw Error("JSON.stringify");
                    return t("", {
                        "": e
                    })
                })
            }(),
            function() {
                function t(e) {
                    if (e) {
                        if (e.numberFormat) return e;
                        if (typeof e === He) {
                            var t = ye.cultures;
                            return t[e] || t[e.split("-")[0]] || null
                        }
                        return null
                    }
                    return null
                }

                function r(e) {
                    return e && (e = t(e)), e || ye.cultures.current
                }

                function o(e, t, o) {
                    o = r(o);
                    var a = o.calendars.standard,
                        s = a.days,
                        u = a.months;
                    return t = a.patterns[t] || t, t.replace(c, function(t) {
                        var r, o, l;
                        return "d" === t ? o = e.getDate() : "dd" === t ? o = i(e.getDate()) : "ddd" === t ? o = s.namesAbbr[e.getDay()] : "dddd" === t ? o = s.names[e.getDay()] : "M" === t ? o = e.getMonth() + 1 : "MM" === t ? o = i(e.getMonth() + 1) : "MMM" === t ? o = u.namesAbbr[e.getMonth()] : "MMMM" === t ? o = u.names[e.getMonth()] : "yy" === t ? o = i(e.getFullYear() % 100) : "yyyy" === t ? o = i(e.getFullYear(), 4) : "h" === t ? o = e.getHours() % 12 || 12 : "hh" === t ? o = i(e.getHours() % 12 || 12) : "H" === t ? o = e.getHours() : "HH" === t ? o = i(e.getHours()) : "m" === t ? o = e.getMinutes() : "mm" === t ? o = i(e.getMinutes()) : "s" === t ? o = e.getSeconds() : "ss" === t ? o = i(e.getSeconds()) : "f" === t ? o = Te.floor(e.getMilliseconds() / 100) : "ff" === t ? (o = e.getMilliseconds(), o > 99 && (o = Te.floor(o / 10)), o = i(o)) : "fff" === t ? o = i(e.getMilliseconds(), 3) : "tt" === t ? o = e.getHours() < 12 ? a.AM[0] : a.PM[0] : "zzz" === t ? (r = e.getTimezoneOffset(), l = r < 0, o = ("" + Te.abs(r / 60)).split(".")[0], r = Te.abs(r) - 60 * o, o = (l ? "+" : "-") + i(o), o += ":" + i(r)) : "zz" !== t && "z" !== t || (o = e.getTimezoneOffset() / 60, l = o < 0, o = ("" + Te.abs(o)).split(".")[0], o = (l ? "+" : "-") + ("zz" === t ? i(o) : o)), o !== n ? o : t.slice(1, t.length - 1)
                    })
                }

                function a(e, t, o) {
                    o = r(o);
                    var i, a, l, c, w, M, S, T, x, k, O, D, z, C, E, H, _, A, N, P, F, R, U, I = o.numberFormat,
                        W = I[h],
                        $ = I.decimals,
                        L = I.pattern[0],
                        j = [],
                        B = e < 0,
                        Y = m,
                        q = m,
                        J = -1;
                    if (e === n) return m;
                    if (!isFinite(e)) return e;
                    if (!t) return o.name.length ? e.toLocaleString() : "" + e;
                    if (w = d.exec(t)) {
                        if (t = w[1].toLowerCase(), a = "c" === t, l = "p" === t, (a || l) && (I = a ? I.currency : I.percent, W = I[h], $ = I.decimals, i = I.symbol, L = I.pattern[B ? 0 : 1]), c = w[2], c && ($ = +c), "e" === t) return c ? e.toExponential($) : e.toExponential();
                        if (l && (e *= 100), e = u(e, $), B = e < 0, e = e.split(h), M = e[0], S = e[1], B && (M = M.substring(1)), q = s(M, 0, M.length, I), S && (q += W + S), "n" === t && !B) return q;
                        for (e = m, k = 0, O = L.length; k < O; k++) D = L.charAt(k), e += "n" === D ? q : "$" === D || "%" === D ? i : D;
                        return e
                    }
                    if (B && (e = -e), (t.indexOf("'") > -1 || t.indexOf('"') > -1 || t.indexOf("\\") > -1) && (t = t.replace(f, function(e) {
                            var t = e.charAt(0).replace("\\", ""),
                                n = e.slice(1).replace(t, "");
                            return j.push(n), b
                        })), t = t.split(";"), B && t[1]) t = t[1], C = !0;
                    else if (0 === e) {
                        if (t = t[2] || t[0], t.indexOf(y) == -1 && t.indexOf(v) == -1) return t
                    } else t = t[0];
                    if (P = t.indexOf("%"), F = t.indexOf("$"), l = P != -1, a = F != -1, l && (e *= 100), a && "\\" === t[F - 1] && (t = t.split("\\").join(""), a = !1), (a || l) && (I = a ? I.currency : I.percent, W = I[h], $ = I.decimals, i = I.symbol), z = t.indexOf(g) > -1, z && (t = t.replace(p, m)), E = t.indexOf(h), O = t.length, E != -1 ? (S = ("" + e).split("e"), S = S[1] ? u(e, Math.abs(S[1])) : S[0], S = S.split(h)[1] || m, _ = t.lastIndexOf(v) - E, H = t.lastIndexOf(y) - E, A = _ > -1, N = H > -1, k = S.length, A || N || (t = t.substring(0, E) + t.substring(E + 1), O = t.length, E = -1, k = 0), A && _ > H ? k = _ : H > _ && (N && k > H ? k = H : A && k < _ && (k = _)), k > -1 && (e = u(e, k))) : e = u(e), H = t.indexOf(y), R = _ = t.indexOf(v), J = H == -1 && _ != -1 ? _ : H != -1 && _ == -1 ? H : H > _ ? _ : H, H = t.lastIndexOf(y), _ = t.lastIndexOf(v), U = H == -1 && _ != -1 ? _ : H != -1 && _ == -1 ? H : H > _ ? H : _, J == O && (U = J), J != -1) {
                        for (q = ("" + e).split(h), M = q[0], S = q[1] || m, T = M.length, x = S.length, B && e * -1 >= 0 && (B = !1), e = t.substring(0, J), B && !C && (e += "-"), k = J; k < O; k++) {
                            if (D = t.charAt(k), E == -1) {
                                if (U - k < T) {
                                    e += M;
                                    break
                                }
                            } else if (_ != -1 && _ < k && (Y = m), E - k <= T && E - k > -1 && (e += M, k = E), E === k) {
                                e += (S ? W : m) + S, k += U - E + 1;
                                continue
                            }
                            D === v ? (e += D, Y = D) : D === y && (e += Y)
                        }
                        if (z && (e = s(e, J + (B && !C ? 1 : 0), Math.max(U, T + J), I)), U >= J && (e += t.substring(U + 1)), a || l) {
                            for (q = m, k = 0, O = e.length; k < O; k++) D = e.charAt(k), q += "$" === D || "%" === D ? i : D;
                            e = q
                        }
                        if (O = j.length)
                            for (k = 0; k < O; k++) e = e.replace(b, j[k])
                    }
                    return e
                }
                var s, u, l, c = /dddd|ddd|dd|d|MMMM|MMM|MM|M|yyyy|yy|HH|H|hh|h|mm|m|fff|ff|f|tt|ss|s|zzz|zz|z|"[^"]*"|'[^']*'/g,
                    d = /^(n|c|p|e)(\d*)$/i,
                    f = /(\\.)|(['][^']*[']?)|(["][^"]*["]?)/g,
                    p = /\,/g,
                    m = "",
                    h = ".",
                    g = ",",
                    y = "#",
                    v = "0",
                    b = "??",
                    w = "en-US",
                    M = {}.toString;
                ye.cultures["en-US"] = {
                    name: w,
                    numberFormat: {
                        pattern: ["-n"],
                        decimals: 2,
                        ",": ",",
                        ".": ".",
                        groupSize: [3],
                        percent: {
                            pattern: ["-n %", "n %"],
                            decimals: 2,
                            ",": ",",
                            ".": ".",
                            groupSize: [3],
                            symbol: "%"
                        },
                        currency: {
                            name: "US Dollar",
                            abbr: "USD",
                            pattern: ["($n)", "$n"],
                            decimals: 2,
                            ",": ",",
                            ".": ".",
                            groupSize: [3],
                            symbol: "$"
                        }
                    },
                    calendars: {
                        standard: {
                            days: {
                                names: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                namesAbbr: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                                namesShort: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"]
                            },
                            months: {
                                names: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                                namesAbbr: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
                            },
                            AM: ["AM", "am", "AM"],
                            PM: ["PM", "pm", "PM"],
                            patterns: {
                                d: "M/d/yyyy",
                                D: "dddd, MMMM dd, yyyy",
                                F: "dddd, MMMM dd, yyyy h:mm:ss tt",
                                g: "M/d/yyyy h:mm tt",
                                G: "M/d/yyyy h:mm:ss tt",
                                m: "MMMM dd",
                                M: "MMMM dd",
                                s: "yyyy'-'MM'-'ddTHH':'mm':'ss",
                                t: "h:mm tt",
                                T: "h:mm:ss tt",
                                u: "yyyy'-'MM'-'dd HH':'mm':'ss'Z'",
                                y: "MMMM, yyyy",
                                Y: "MMMM, yyyy"
                            },
                            "/": "/",
                            ":": ":",
                            firstDay: 0,
                            twoDigitYearMax: 2029
                        }
                    }
                }, ye.culture = function(e) {
                    var r, o = ye.cultures;
                    return e === n ? o.current : (r = t(e) || o[w], r.calendar = r.calendars.standard, o.current = r, n)
                }, ye.findCulture = t, ye.getCulture = r, ye.culture(w), s = function(e, t, r, o) {
                    var i, a, s, u, l, c, d = e.indexOf(o[h]),
                        f = o.groupSize.slice(),
                        p = f.shift();
                    if (r = d !== -1 ? d : r + 1, i = e.substring(t, r), a = i.length, a >= p) {
                        for (s = a, u = []; s > -1;)
                            if (l = i.substring(s - p, s), l && u.push(l), s -= p, c = f.shift(), p = c !== n ? c : p, 0 === p) {
                                s > 0 && u.push(i.substring(0, s));
                                break
                            }
                        i = u.reverse().join(o[g]), e = e.substring(0, t) + i + e.substring(r)
                    }
                    return e
                }, u = function(e, t) {
                    return t = t || 0, e = ("" + e).split("e"), e = Math.round(+(e[0] + "e" + (e[1] ? +e[1] + t : t))), e = ("" + e).split("e"), e = +(e[0] + "e" + (e[1] ? +e[1] - t : -t)), e.toFixed(Math.min(t, 20))
                }, l = function(e, t, r) {
                    if (t) {
                        if ("[object Date]" === M.call(e)) return o(e, t, r);
                        if (typeof e === _e) return a(e, t, r)
                    }
                    return e !== n ? e : ""
                }, ye.format = function(e) {
                    var t = arguments;
                    return e.replace(De, function(e, n, r) {
                        var o = t[parseInt(n, 10) + 1];
                        return l(o, r ? r.substring(1) : "")
                    })
                }, ye._extractFormat = function(e) {
                    return "{0:" === e.slice(0, 3) && (e = e.slice(3, e.length - 1)), e
                }, ye._activeElement = function() {
                    try {
                        return document.activeElement
                    } catch (e) {
                        return document.documentElement.activeElement
                    }
                }, ye._round = u, ye._outerWidth = function(t, n) {
                    return e(t).outerWidth(n || !1) || 0
                }, ye._outerHeight = function(t, n) {
                    return e(t).outerHeight(n || !1) || 0
                }, ye.toString = l
            }(),
            function() {
                function t(e, t, n) {
                    return !(e >= t && e <= n)
                }

                function r(e) {
                    return e.charAt(0)
                }

                function o(t) {
                    return e.map(t, r)
                }

                function i(e, t) {
                    t || 23 !== e.getHours() || e.setHours(e.getHours() + 2)
                }

                function a(e) {
                    for (var t = 0, n = e.length, r = []; t < n; t++) r[t] = (e[t] + "").toLowerCase();
                    return r
                }

                function s(e) {
                    var t, n = {};
                    for (t in e) n[t] = a(e[t]);
                    return n
                }

                function u(e, r, a, u) {
                    if (!e) return null;
                    var l, c, d, f, p, g, y, v, b, M, S, T, x, k = function(e) {
                            for (var t = 0; r[R] === e;) t++, R++;
                            return t > 0 && (R -= 1), t
                        },
                        O = function(t) {
                            var n = w[t] || RegExp("^\\d{1," + t + "}"),
                                r = e.substr(U, t).match(n);
                            return r ? (r = r[0], U += r.length, parseInt(r, 10)) : null
                        },
                        D = function(t, n) {
                            for (var r, o, i, a = 0, s = t.length, u = 0, l = 0; a < s; a++) r = t[a], o = r.length, i = e.substr(U, o), n && (i = i.toLowerCase()), i == r && o > u && (u = o, l = a);
                            return u ? (U += u, l + 1) : null
                        },
                        z = function() {
                            var t = !1;
                            return e.charAt(U) === r[R] && (U++, t = !0), t
                        },
                        C = a.calendars.standard,
                        E = null,
                        H = null,
                        _ = null,
                        A = null,
                        N = null,
                        P = null,
                        F = null,
                        R = 0,
                        U = 0,
                        I = !1,
                        W = new Date,
                        $ = C.twoDigitYearMax || 2029,
                        L = W.getFullYear();
                    for (r || (r = "d"), f = C.patterns[r], f && (r = f), r = r.split(""), d = r.length; R < d; R++)
                        if (l = r[R], I) "'" === l ? I = !1 : z();
                        else if ("d" === l) {
                        if (c = k("d"), C._lowerDays || (C._lowerDays = s(C.days)), null !== _ && c > 2) continue;
                        if (_ = c < 3 ? O(2) : D(C._lowerDays[3 == c ? "namesAbbr" : "names"], !0), null === _ || t(_, 1, 31)) return null
                    } else if ("M" === l) {
                        if (c = k("M"), C._lowerMonths || (C._lowerMonths = s(C.months)), H = c < 3 ? O(2) : D(C._lowerMonths[3 == c ? "namesAbbr" : "names"], !0), null === H || t(H, 1, 12)) return null;
                        H -= 1
                    } else if ("y" === l) {
                        if (c = k("y"), E = O(c), null === E) return null;
                        2 == c && ("string" == typeof $ && ($ = L + parseInt($, 10)), E = L - L % 100 + E, E > $ && (E -= 100))
                    } else if ("h" === l) {
                        if (k("h"), A = O(2), 12 == A && (A = 0), null === A || t(A, 0, 11)) return null
                    } else if ("H" === l) {
                        if (k("H"), A = O(2), null === A || t(A, 0, 23)) return null
                    } else if ("m" === l) {
                        if (k("m"), N = O(2), null === N || t(N, 0, 59)) return null
                    } else if ("s" === l) {
                        if (k("s"), P = O(2), null === P || t(P, 0, 59)) return null
                    } else if ("f" === l) {
                        if (c = k("f"), x = e.substr(U, c).match(w[3]), F = O(c), null !== F && (F = parseFloat("0." + x[0], 10), F = ye._round(F, 3), F *= 1e3), null === F || t(F, 0, 999)) return null
                    } else if ("t" === l) {
                        if (c = k("t"), v = C.AM, b = C.PM, 1 === c && (v = o(v), b = o(b)), p = D(b), !p && !D(v)) return null
                    } else if ("z" === l) {
                        if (g = !0, c = k("z"), "Z" === e.substr(U, 1)) {
                            z();
                            continue
                        }
                        if (y = e.substr(U, 6).match(c > 2 ? h : m), !y) return null;
                        if (y = y[0].split(":"), M = y[0], S = y[1], !S && M.length > 3 && (U = M.length - 2, S = M.substring(U), M = M.substring(0, U)), M = parseInt(M, 10), t(M, -12, 13)) return null;
                        if (c > 2 && (S = parseInt(S, 10), isNaN(S) || t(S, 0, 59))) return null
                    } else if ("'" === l) I = !0, z();
                    else if (!z()) return null;
                    return u && !/^\s*$/.test(e.substr(U)) ? null : (T = null !== A || null !== N || P || null, null === E && null === H && null === _ && T ? (E = L, H = W.getMonth(), _ = W.getDate()) : (null === E && (E = L), null === _ && (_ = 1)), p && A < 12 && (A += 12), g ? (M && (A += -M), S && (N += -S), e = new Date(Date.UTC(E, H, _, A, N, P, F))) : (e = new Date(E, H, _, A, N, P, F), i(e, A)), E < 100 && e.setFullYear(E), e.getDate() !== _ && g === n ? null : e)
                }

                function l(e) {
                    var t = "-" === e.substr(0, 1) ? -1 : 1;
                    return e = e.substring(1), e = 60 * parseInt(e.substr(0, 2), 10) + parseInt(e.substring(2), 10), t * e
                }

                function c(e) {
                    var t, n, r, o = Te.max(v.length, b.length),
                        i = e.calendar.patterns,
                        a = [];
                    for (r = 0; r < o; r++) {
                        for (t = v[r], n = 0; n < t.length; n++) a.push(i[t[n]]);
                        a = a.concat(b[r])
                    }
                    return a
                }

                function d(e, t, n, r) {
                    var o, i, a, s;
                    if ("[object Date]" === M.call(e)) return e;
                    if (o = 0, i = null, e && 0 === e.indexOf("/D") && (i = g.exec(e))) return i = i[1], s = y.exec(i.substring(1)), i = new Date(parseInt(i, 10)), s && (s = l(s[0]), i = ye.timezone.apply(i, 0), i = ye.timezone.convert(i, 0, -1 * s)), i;
                    for (n = ye.getCulture(n), t || (t = c(n)), t = we(t) ? t : [t], a = t.length; o < a; o++)
                        if (i = u(e, t[o], n, r)) return i;
                    return i
                }
                var f = /\u00A0/g,
                    p = /[eE][\-+]?[0-9]+/,
                    m = /[+|\-]\d{1,2}/,
                    h = /[+|\-]\d{1,2}:?\d{2}/,
                    g = /^\/Date\((.*?)\)\/$/,
                    y = /[+-]\d*/,
                    v = [
                        [],
                        ["G", "g", "F"],
                        ["D", "d", "y", "m", "T", "t"]
                    ],
                    b = [
                        ["yyyy-MM-ddTHH:mm:ss.fffffffzzz", "yyyy-MM-ddTHH:mm:ss.fffffff", "yyyy-MM-ddTHH:mm:ss.fffzzz", "yyyy-MM-ddTHH:mm:ss.fff", "ddd MMM dd yyyy HH:mm:ss", "yyyy-MM-ddTHH:mm:sszzz", "yyyy-MM-ddTHH:mmzzz", "yyyy-MM-ddTHH:mmzz", "yyyy-MM-ddTHH:mm:ss", "yyyy-MM-dd HH:mm:ss", "yyyy/MM/dd HH:mm:ss"],
                        ["yyyy-MM-ddTHH:mm", "yyyy-MM-dd HH:mm", "yyyy/MM/dd HH:mm"],
                        ["yyyy/MM/dd", "yyyy-MM-dd", "HH:mm:ss", "HH:mm"]
                    ],
                    w = {
                        2: /^\d{1,2}/,
                        3: /^\d{1,3}/,
                        4: /^\d{4}/
                    },
                    M = {}.toString;
                ye.parseDate = function(e, t, n) {
                    return d(e, t, n, !1)
                }, ye.parseExactDate = function(e, t, n) {
                    return d(e, t, n, !0)
                }, ye.parseInt = function(e, t) {
                    var n = ye.parseFloat(e, t);
                    return n && (n = 0 | n), n
                }, ye.parseFloat = function(e, t, n) {
                    if (!e && 0 !== e) return null;
                    if (typeof e === _e) return e;
                    e = "" + e, t = ye.getCulture(t);
                    var r, o, i = t.numberFormat,
                        a = i.percent,
                        s = i.currency,
                        u = s.symbol,
                        l = a.symbol,
                        c = e.indexOf("-");
                    return p.test(e) ? (e = parseFloat(e.replace(i["."], ".")), isNaN(e) && (e = null), e) : c > 0 ? null : (c = c > -1, e.indexOf(u) > -1 || n && n.toLowerCase().indexOf("c") > -1 ? (i = s, r = i.pattern[0].replace("$", u).split("n"), e.indexOf(r[0]) > -1 && e.indexOf(r[1]) > -1 && (e = e.replace(r[0], "").replace(r[1], ""), c = !0)) : e.indexOf(l) > -1 && (o = !0, i = a, u = l), e = e.replace("-", "").replace(u, "").replace(f, " ").split(i[","].replace(f, " ")).join("").replace(i["."], "."), e = parseFloat(e), isNaN(e) ? e = null : c && (e *= -1), e && o && (e /= 100), e)
                }
            }(),
            function() {
                var r, o, i, a, s, u, l, d, f;
                ke._scrollbar = n, ke.scrollbar = function(e) {
                    if (isNaN(ke._scrollbar) || e) {
                        var t, n = document.createElement("div");
                        return n.style.cssText = "overflow:scroll;overflow-x:hidden;zoom:1;clear:both;display:block", n.innerHTML = "&nbsp;", document.body.appendChild(n), ke._scrollbar = t = n.offsetWidth - n.scrollWidth, document.body.removeChild(n), t
                    }
                    return ke._scrollbar
                }, ke.isRtl = function(t) {
                    return e(t).closest(".k-rtl").length > 0
                }, r = document.createElement("table");
                try {
                    r.innerHTML = "<tr><td></td></tr>", ke.tbodyInnerHtml = !0
                } catch (p) {
                    ke.tbodyInnerHtml = !1
                }
                ke.touch = "ontouchstart" in t, o = document.documentElement.style, i = ke.transitions = !1, a = ke.transforms = !1, s = "HTMLElement" in t ? HTMLElement.prototype : [], ke.hasHW3D = "WebKitCSSMatrix" in t && "m11" in new t.WebKitCSSMatrix || "MozPerspective" in o || "msPerspective" in o, ke.cssFlexbox = "flexWrap" in o || "WebkitFlexWrap" in o || "msFlexWrap" in o, be(["Moz", "webkit", "O", "ms"], function() {
                    var e, t = "" + this,
                        n = typeof r.style[t + "Transition"] === He;
                    if (n || typeof r.style[t + "Transform"] === He) return e = t.toLowerCase(), a = {
                        css: "ms" != e ? "-" + e + "-" : "",
                        prefix: t,
                        event: "o" === e || "webkit" === e ? e : ""
                    }, n && (i = a, i.event = i.event ? i.event + "TransitionEnd" : "transitionend"), !1
                }), r = null, ke.transforms = a, ke.transitions = i, ke.devicePixelRatio = t.devicePixelRatio === n ? 1 : t.devicePixelRatio;
                try {
                    ke.screenWidth = t.outerWidth || t.screen ? t.screen.availWidth : t.innerWidth, ke.screenHeight = t.outerHeight || t.screen ? t.screen.availHeight : t.innerHeight
                } catch (p) {
                    ke.screenWidth = t.screen.availWidth, ke.screenHeight = t.screen.availHeight
                }
                ke.detectOS = function(e) {
                        var n, r, o = !1,
                            i = [],
                            a = !/mobile safari/i.test(e),
                            s = {
                                wp: /(Windows Phone(?: OS)?)\s(\d+)\.(\d+(\.\d+)?)/,
                                fire: /(Silk)\/(\d+)\.(\d+(\.\d+)?)/,
                                android: /(Android|Android.*(?:Opera|Firefox).*?\/)\s*(\d+)\.(\d+(\.\d+)?)/,
                                iphone: /(iPhone|iPod).*OS\s+(\d+)[\._]([\d\._]+)/,
                                ipad: /(iPad).*OS\s+(\d+)[\._]([\d_]+)/,
                                meego: /(MeeGo).+NokiaBrowser\/(\d+)\.([\d\._]+)/,
                                webos: /(webOS)\/(\d+)\.(\d+(\.\d+)?)/,
                                blackberry: /(BlackBerry|BB10).*?Version\/(\d+)\.(\d+(\.\d+)?)/,
                                playbook: /(PlayBook).*?Tablet\s*OS\s*(\d+)\.(\d+(\.\d+)?)/,
                                windows: /(MSIE)\s+(\d+)\.(\d+(\.\d+)?)/,
                                tizen: /(tizen).*?Version\/(\d+)\.(\d+(\.\d+)?)/i,
                                sailfish: /(sailfish).*rv:(\d+)\.(\d+(\.\d+)?).*firefox/i,
                                ffos: /(Mobile).*rv:(\d+)\.(\d+(\.\d+)?).*Firefox/
                            },
                            u = {
                                ios: /^i(phone|pad|pod)$/i,
                                android: /^android|fire$/i,
                                blackberry: /^blackberry|playbook/i,
                                windows: /windows/,
                                wp: /wp/,
                                flat: /sailfish|ffos|tizen/i,
                                meego: /meego/
                            },
                            l = {
                                tablet: /playbook|ipad|fire/i
                            },
                            d = {
                                omini: /Opera\sMini/i,
                                omobile: /Opera\sMobi/i,
                                firefox: /Firefox|Fennec/i,
                                mobilesafari: /version\/.*safari/i,
                                ie: /MSIE|Windows\sPhone/i,
                                chrome: /chrome|crios/i,
                                webkit: /webkit/i
                            };
                        for (r in s)
                            if (s.hasOwnProperty(r) && (i = e.match(s[r]))) {
                                if ("windows" == r && "plugins" in navigator) return !1;
                                o = {}, o.device = r, o.tablet = c(r, l, !1), o.browser = c(e, d, "default"), o.name = c(r, u), o[o.name] = !0, o.majorVersion = i[2], o.minorVersion = i[3].replace("_", "."), n = o.minorVersion.replace(".", "").substr(0, 2), o.flatVersion = o.majorVersion + n + Array(3 - (n.length < 3 ? n.length : 2)).join("0"), o.cordova = typeof t.PhoneGap !== Fe || typeof t.cordova !== Fe, o.appMode = t.navigator.standalone || /file|local|wmapp/.test(t.location.protocol) || o.cordova, o.android && (ke.devicePixelRatio < 1.5 && o.flatVersion < 400 || a) && (ke.screenWidth > 800 || ke.screenHeight > 800) && (o.tablet = r);
                                break
                            }
                        return o
                    }, u = ke.mobileOS = ke.detectOS(navigator.userAgent), ke.wpDevicePixelRatio = u.wp ? screen.width / 320 : 0, ke.hasNativeScrolling = !1, (u.ios || u.android && u.majorVersion > 2 || u.wp) && (ke.hasNativeScrolling = u), ke.delayedClick = function() {
                        if (ke.touch) {
                            if (u.ios) return !0;
                            if (u.android) return !ke.browser.chrome || !(ke.browser.version < 32) && !(e("meta[name=viewport]").attr("content") || "").match(/user-scalable=no/i)
                        }
                        return !1
                    }, ke.mouseAndTouchPresent = ke.touch && !(ke.mobileOS.ios || ke.mobileOS.android), ke.detectBrowser = function(e) {
                        var t, n = !1,
                            r = [],
                            o = {
                                edge: /(edge)[ \/]([\w.]+)/i,
                                webkit: /(chrome)[ \/]([\w.]+)/i,
                                safari: /(webkit)[ \/]([\w.]+)/i,
                                opera: /(opera)(?:.*version|)[ \/]([\w.]+)/i,
                                msie: /(msie\s|trident.*? rv:)([\w.]+)/i,
                                mozilla: /(mozilla)(?:.*? rv:([\w.]+)|)/i
                            };
                        for (t in o)
                            if (o.hasOwnProperty(t) && (r = e.match(o[t]))) {
                                n = {}, n[t] = !0, n[r[1].toLowerCase().split(" ")[0].split("/")[0]] = !0, n.version = parseInt(document.documentMode || r[2], 10);
                                break
                            }
                        return n
                    }, ke.browser = ke.detectBrowser(navigator.userAgent), ke.detectClipboardAccess = function() {
                        var e = {
                            copy: !!document.queryCommandSupported && document.queryCommandSupported("copy"),
                            cut: !!document.queryCommandSupported && document.queryCommandSupported("cut"),
                            paste: !!document.queryCommandSupported && document.queryCommandSupported("paste")
                        };
                        return ke.browser.chrome && (e.paste = !1, ke.browser.version >= 43 && (e.copy = !0, e.cut = !0)), e
                    }, ke.clipboard = ke.detectClipboardAccess(), ke.zoomLevel = function() {
                        var e, n, r;
                        try {
                            return e = ke.browser, n = 0, r = document.documentElement, e.msie && 11 == e.version && r.scrollHeight > r.clientHeight && !ke.touch && (n = ke.scrollbar()), ke.touch ? r.clientWidth / t.innerWidth : e.msie && e.version >= 10 ? ((top || t).document.documentElement.offsetWidth + n) / (top || t).innerWidth : 1
                        } catch (o) {
                            return 1
                        }
                    }, ke.cssBorderSpacing = n !== o.borderSpacing && !(ke.browser.msie && ke.browser.version < 8),
                    function(t) {
                        var n = "",
                            r = e(document.documentElement),
                            o = parseInt(t.version, 10);
                        t.msie ? n = "ie" : t.mozilla ? n = "ff" : t.safari ? n = "safari" : t.webkit ? n = "webkit" : t.opera ? n = "opera" : t.edge && (n = "edge"), n && (n = "k-" + n + " k-" + n + o), ke.mobileOS && (n += " k-mobile"), ke.cssFlexbox || (n += " k-no-flexbox"), r.addClass(n)
                    }(ke.browser), ke.eventCapture = document.documentElement.addEventListener, l = document.createElement("input"), ke.placeholder = "placeholder" in l, ke.propertyChangeEvent = "onpropertychange" in l, ke.input = function() {
                        for (var e, t = ["number", "date", "time", "month", "week", "datetime", "datetime-local"], n = t.length, r = "test", o = {}, i = 0; i < n; i++) e = t[i], l.setAttribute("type", e), l.value = r, o[e.replace("-", "")] = "text" !== l.type && l.value !== r;
                        return o
                    }(), l.style.cssText = "float:left;", ke.cssFloat = !!l.style.cssFloat, l = null, ke.stableSort = function() {
                        var e, t = 513,
                            n = [{
                                index: 0,
                                field: "b"
                            }];
                        for (e = 1; e < t; e++) n.push({
                            index: e,
                            field: "a"
                        });
                        return n.sort(function(e, t) {
                            return e.field > t.field ? 1 : e.field < t.field ? -1 : 0
                        }), 1 === n[0].index
                    }(), ke.matchesSelector = s.webkitMatchesSelector || s.mozMatchesSelector || s.msMatchesSelector || s.oMatchesSelector || s.matchesSelector || s.matches || function(t) {
                        for (var n = document.querySelectorAll ? (this.parentNode || document).querySelectorAll(t) || [] : e(t), r = n.length; r--;)
                            if (n[r] == this) return !0;
                        return !1
                    }, ke.pushState = t.history && t.history.pushState, d = document.documentMode, ke.hashChange = "onhashchange" in t && !(ke.browser.msie && (!d || d <= 8)), ke.customElements = "registerElement" in t.document, f = ke.browser.chrome, ke.msPointers = !f && t.MSPointerEvent, ke.pointers = !f && t.PointerEvent, ke.kineticScrollNeeded = u && (ke.touch || ke.msPointers || ke.pointers)
            }(), j = {
                left: {
                    reverse: "right"
                },
                right: {
                    reverse: "left"
                },
                down: {
                    reverse: "up"
                },
                up: {
                    reverse: "down"
                },
                top: {
                    reverse: "bottom"
                },
                bottom: {
                    reverse: "top"
                },
                "in": {
                    reverse: "out"
                },
                out: {
                    reverse: "in"
                }
            }, B = {}, e.extend(B, {
                enabled: !0,
                Element: function(t) {
                    this.element = e(t)
                },
                promise: function(e, t) {
                    e.is(":visible") || e.css({
                        display: e.data("olddisplay") || "block"
                    }).css("display"), t.hide && e.data("olddisplay", e.css("display")).hide(), t.init && t.init(), t.completeCallback && t.completeCallback(e), e.dequeue()
                },
                disable: function() {
                    this.enabled = !1, this.promise = this.promiseShim
                },
                enable: function() {
                    this.enabled = !0, this.promise = this.animatedPromise
                }
            }), B.promiseShim = B.promise, "kendoAnimate" in e.fn || ve(e.fn, {
                kendoStop: function(e, t) {
                    return this.stop(e, t)
                },
                kendoAnimate: function(e, t, n, r) {
                    return M(this, e, t, n, r)
                },
                kendoAddClass: function(e, t) {
                    return ye.toggleClass(this, e, t, !0)
                },
                kendoRemoveClass: function(e, t) {
                    return ye.toggleClass(this, e, t, !1)
                },
                kendoToggleClass: function(e, t, n) {
                    return ye.toggleClass(this, e, t, n)
                }
            }), Y = /&/g, q = /</g, J = /"/g, V = /'/g, G = />/g, K = function(e) {
                return e.target
            }, ke.touch && (K = function(e) {
                var t = "originalEvent" in e ? e.originalEvent.changedTouches : "changedTouches" in e ? e.changedTouches : null;
                return t ? document.elementFromPoint(t[0].clientX, t[0].clientY) : e.target
            }, be(["swipe", "swipeLeft", "swipeRight", "swipeUp", "swipeDown", "doubleTap", "tap"], function(t, n) {
                e.fn[n] = function(e) {
                    return this.bind(n, e)
                }
            })), ke.touch ? ke.mobileOS ? (ke.mousedown = "touchstart", ke.mouseup = "touchend", ke.mousemove = "touchmove", ke.mousecancel = "touchcancel", ke.click = "touchend", ke.resize = "orientationchange") : (ke.mousedown = "mousedown touchstart", ke.mouseup = "mouseup touchend", ke.mousemove = "mousemove touchmove", ke.mousecancel = "mouseleave touchcancel", ke.click = "click", ke.resize = "resize") : ke.pointers ? (ke.mousemove = "pointermove", ke.mousedown = "pointerdown", ke.mouseup = "pointerup", ke.mousecancel = "pointercancel", ke.click = "pointerup", ke.resize = "orientationchange resize") : ke.msPointers ? (ke.mousemove = "MSPointerMove", ke.mousedown = "MSPointerDown", ke.mouseup = "MSPointerUp", ke.mousecancel = "MSPointerCancel", ke.click = "MSPointerUp", ke.resize = "orientationchange resize") : (ke.mousemove = "mousemove", ke.mousedown = "mousedown", ke.mouseup = "mouseup", ke.mousecancel = "mouseleave", ke.click = "click", ke.resize = "resize"), Q = function(e, t) {
                var n, r, o, i, a = t || "d",
                    s = 1;
                for (r = 0, o = e.length; r < o; r++) i = e[r], "" !== i && (n = i.indexOf("["), 0 !== n && (n == -1 ? i = "." + i : (s++, i = "." + i.substring(0, n) + " || {})" + i.substring(n))), s++, a += i + (r < o - 1 ? " || {})" : ")"));
                return Array(s).join("(") + a
            }, Z = /^([a-z]+:)?\/\//i, ve(ye, {
                widgets: [],
                _widgetRegisteredCallbacks: [],
                ui: ye.ui || {},
                fx: ye.fx || b,
                effects: ye.effects || B,
                mobile: ye.mobile || {},
                data: ye.data || {},
                dataviz: ye.dataviz || {},
                drawing: ye.drawing || {},
                spreadsheet: {
                    messages: {}
                },
                keys: {
                    INSERT: 45,
                    DELETE: 46,
                    BACKSPACE: 8,
                    TAB: 9,
                    ENTER: 13,
                    ESC: 27,
                    LEFT: 37,
                    UP: 38,
                    RIGHT: 39,
                    DOWN: 40,
                    END: 35,
                    HOME: 36,
                    SPACEBAR: 32,
                    PAGEUP: 33,
                    PAGEDOWN: 34,
                    F2: 113,
                    F10: 121,
                    F12: 123,
                    NUMPAD_PLUS: 107,
                    NUMPAD_MINUS: 109,
                    NUMPAD_DOT: 110
                },
                support: ye.support || ke,
                animate: ye.animate || M,
                ns: "",
                attr: function(e) {
                    return "data-" + ye.ns + e
                },
                getShadows: a,
                wrap: s,
                deepExtend: u,
                getComputedStyles: p,
                webComponents: ye.webComponents || [],
                isScrollable: m,
                scrollLeft: h,
                size: g,
                toCamelCase: f,
                toHyphens: d,
                getOffset: ye.getOffset || y,
                parseEffects: ye.parseEffects || v,
                toggleClass: ye.toggleClass || S,
                directions: ye.directions || j,
                Observable: P,
                Class: r,
                Template: H,
                template: Me(H.compile, H),
                render: Me(H.render, H),
                stringify: Me(xe.stringify, xe),
                eventTarget: K,
                htmlEncode: T,
                isLocalUrl: function(e) {
                    return e && !Z.test(e)
                },
                expr: function(e, t, n) {
                    return e = e || "", typeof t == He && (n = t, t = !1), n = n || "d", e && "[" !== e.charAt(0) && (e = "." + e), t ? (e = e.replace(/"([^.]*)\.([^"]*)"/g, '"$1_$DOT$_$2"'), e = e.replace(/'([^.]*)\.([^']*)'/g, "'$1_$DOT$_$2'"), e = Q(e.split("."), n), e = e.replace(/_\$DOT\$_/g, ".")) : e = n + e, e
                },
                getter: function(e, t) {
                    var n = e + t;
                    return Re[n] = Re[n] || Function("d", "return " + ye.expr(e, t))
                },
                setter: function(e) {
                    return Ue[e] = Ue[e] || Function("d,value", ye.expr(e) + "=value")
                },
                accessor: function(e) {
                    return {
                        get: ye.getter(e),
                        set: ye.setter(e)
                    }
                },
                guid: function() {
                    var e, t, n = "";
                    for (e = 0; e < 32; e++) t = 16 * Te.random() | 0, 8 != e && 12 != e && 16 != e && 20 != e || (n += "-"), n += (12 == e ? 4 : 16 == e ? 3 & t | 8 : t).toString(16);
                    return n
                },
                roleSelector: function(e) {
                    return e.replace(/(\S+)/g, "[" + ye.attr("role") + "=$1],").slice(0, -1)
                },
                directiveSelector: function(e) {
                    var t, n = e.split(" ");
                    if (n)
                        for (t = 0; t < n.length; t++) "view" != n[t] && (n[t] = n[t].replace(/(\w*)(view|bar|strip|over)$/, "$1-$2"));
                    return n.join(" ").replace(/(\S+)/g, "kendo-mobile-$1,").slice(0, -1)
                },
                triggeredByInput: function(e) {
                    return /^(label|input|textarea|select)$/i.test(e.target.tagName)
                },
                onWidgetRegistered: function(e) {
                    for (var t = 0, n = ye.widgets.length; t < n; t++) e(ye.widgets[t]);
                    ye._widgetRegisteredCallbacks.push(e);
                },
                logToConsole: function(e, r) {
                    var o = t.console;
                    !ye.suppressLog && n !== o && o.log && o[r || "log"](e)
                }
            }), X = P.extend({
                init: function(e, t) {
                    var n, r = this;
                    r.element = ye.jQuery(e).handler(r), r.angular("init", t), P.fn.init.call(r), n = t ? t.dataSource : null, n && (t = ve({}, t, {
                        dataSource: {}
                    })), t = r.options = ve(!0, {}, r.options, t), n && (t.dataSource = n), r.element.attr(ye.attr("role")) || r.element.attr(ye.attr("role"), (t.name || "").toLowerCase()), r.element.data("kendo" + t.prefix + t.name, r), r.bind(r.events, t)
                },
                events: [],
                options: {
                    prefix: ""
                },
                _hasBindingTarget: function() {
                    return !!this.element[0].kendoBindingTarget
                },
                _tabindex: function(e) {
                    e = e || this.wrapper;
                    var t = this.element,
                        n = "tabindex",
                        r = e.attr(n) || t.attr(n);
                    t.removeAttr(n), e.attr(n, isNaN(r) ? 0 : r)
                },
                setOptions: function(t) {
                    this._setEvents(t), e.extend(this.options, t)
                },
                _setEvents: function(e) {
                    for (var t, n = this, r = 0, o = n.events.length; r < o; r++) t = n.events[r], n.options[t] && e[t] && n.unbind(t, n.options[t]);
                    n.bind(n.events, e)
                },
                resize: function(e) {
                    var t = this.getSize(),
                        n = this._size;
                    (e || (t.width > 0 || t.height > 0) && (!n || t.width !== n.width || t.height !== n.height)) && (this._size = t, this._resize(t, e), this.trigger("resize", t))
                },
                getSize: function() {
                    return ye.dimensions(this.element)
                },
                size: function(e) {
                    return e ? (this.setSize(e), n) : this.getSize()
                },
                setSize: e.noop,
                _resize: e.noop,
                destroy: function() {
                    var e = this;
                    e.element.removeData("kendo" + e.options.prefix + e.options.name), e.element.removeData("handler"), e.unbind()
                },
                _destroy: function() {
                    this.destroy()
                },
                angular: function() {},
                _muteAngularRebind: function(e) {
                    this._muteRebind = !0, e.call(this), this._muteRebind = !1
                }
            }), ee = X.extend({
                dataItems: function() {
                    return this.dataSource.flatView()
                },
                _angularItems: function(t) {
                    var n = this;
                    n.angular(t, function() {
                        return {
                            elements: n.items(),
                            data: e.map(n.dataItems(), function(e) {
                                return {
                                    dataItem: e
                                }
                            })
                        }
                    })
                }
            }), ye.dimensions = function(e, t) {
                var n = e[0];
                return t && e.css(t), {
                    width: n.offsetWidth,
                    height: n.offsetHeight
                }
            }, ye.notify = Se, te = /template$/i, ne = /^\s*(?:\{(?:.|\r\n|\n)*\}|\[(?:.|\r\n|\n)*\])\s*$/, re = /^\{(\d+)(:[^\}]+)?\}|^\[[A-Za-z_]+\]$/, oe = /([A-Z])/g, ye.initWidget = function(r, o, i) {
                var a, s, u, l, c, d, f, p, m, h, g, y, v;
                if (i ? i.roles && (i = i.roles) : i = ye.ui.roles, r = r.nodeType ? r : r[0], d = r.getAttribute("data-" + ye.ns + "role")) {
                    m = d.indexOf(".") === -1, u = m ? i[d] : ye.getter(d)(t), g = e(r).data(), y = u ? "kendo" + u.fn.options.prefix + u.fn.options.name : "", h = m ? RegExp("^kendo.*" + d + "$", "i") : RegExp("^" + y + "$", "i");
                    for (v in g)
                        if (v.match(h)) {
                            if (v !== y) return g[v];
                            a = g[v]
                        }
                    if (u) {
                        for (p = x(r, "dataSource"), o = e.extend({}, k(r, u.fn.options), o), p && (o.dataSource = typeof p === He ? ye.getter(p)(t) : p), l = 0, c = u.fn.events.length; l < c; l++) s = u.fn.events[l], f = x(r, s), f !== n && (o[s] = ye.getter(f)(t));
                        return a ? e.isEmptyObject(o) || a.setOptions(o) : a = new u(r, o), a
                    }
                }
            }, ye.rolesFromNamespaces = function(e) {
                var t, n, r = [];
                for (e[0] || (e = [ye.ui, ye.dataviz.ui]), t = 0, n = e.length; t < n; t++) r[t] = e[t].roles;
                return ve.apply(null, [{}].concat(r.reverse()))
            }, ye.init = function(t) {
                var n = ye.rolesFromNamespaces(Ie.call(arguments, 1));
                e(t).find("[data-" + ye.ns + "role]").addBack().each(function() {
                    ye.initWidget(this, {}, n)
                })
            }, ye.destroy = function(t) {
                e(t).find("[data-" + ye.ns + "role]").addBack().each(function() {
                    var t, n = e(this).data();
                    for (t in n) 0 === t.indexOf("kendo") && typeof n[t].destroy === Ee && n[t].destroy()
                })
            }, ye.resize = function(t, n) {
                var r, o = e(t).find("[data-" + ye.ns + "role]").addBack().filter(D);
                o.length && (r = e.makeArray(o), r.sort(O), e.each(r, function() {
                    var t = ye.widgetInstance(e(this));
                    t && t.resize(n)
                }))
            }, ye.parseOptions = k, ve(ye.ui, {
                Widget: X,
                DataBoundWidget: ee,
                roles: {},
                progress: function(t, n, r) {
                    var o, i, a, s, u, l = t.find(".k-loading-mask"),
                        c = ye.support,
                        d = c.browser;
                    r = e.extend({}, {
                        width: "100%",
                        height: "100%",
                        top: t.scrollTop(),
                        opacity: !1
                    }, r), u = r.opacity ? "k-loading-mask k-opaque" : "k-loading-mask", n ? l.length || (o = c.isRtl(t), i = o ? "right" : "left", s = t.scrollLeft(), a = d.webkit && o ? t[0].scrollWidth - t.width() - 2 * s : 0, l = e(ye.format("<div class='{0}'><span class='k-loading-text'>{1}</span><div class='k-loading-image'/><div class='k-loading-color'/></div>", u, ye.ui.progress.messages.loading)).width(r.width).height(r.height).css("top", r.top).css(i, Math.abs(s) + a).prependTo(t)) : l && l.remove()
                },
                plugin: function(t, r, o) {
                    var i, a, s, u, l = t.fn.options.name;
                    for (r = r || ye.ui, o = o || "", r[l] = t, r.roles[l.toLowerCase()] = t, i = "getKendo" + o + l, l = "kendo" + o + l, a = {
                            name: l,
                            widget: t,
                            prefix: o || ""
                        }, ye.widgets.push(a), s = 0, u = ye._widgetRegisteredCallbacks.length; s < u; s++) ye._widgetRegisteredCallbacks[s](a);
                    e.fn[l] = function(r) {
                        var o, i = this;
                        return typeof r === He ? (o = Ie.call(arguments, 1), this.each(function() {
                            var t, a, s = e.data(this, l);
                            if (!s) throw Error(ye.format("Cannot call method '{0}' of {1} before it is initialized", r, l));
                            if (t = s[r], typeof t !== Ee) throw Error(ye.format("Cannot find method '{0}' of {1}", r, l));
                            if (a = t.apply(s, o), a !== n) return i = a, !1
                        })) : this.each(function() {
                            return new t(this, r)
                        }), i
                    }, e.fn[l].widget = t, e.fn[i] = function() {
                        return this.data(l)
                    }
                }
            }), ye.ui.progress.messages = {
                loading: "Loading..."
            }, ie = {
                bind: function() {
                    return this
                },
                nullObject: !0,
                options: {}
            }, ae = X.extend({
                init: function(e, t) {
                    X.fn.init.call(this, e, t), this.element.autoApplyNS(), this.wrapper = this.element, this.element.addClass("km-widget")
                },
                destroy: function() {
                    X.fn.destroy.call(this), this.element.kendoDestroy()
                },
                options: {
                    prefix: "Mobile"
                },
                events: [],
                view: function() {
                    var e = this.element.closest(ye.roleSelector("view splitview modalview drawer"));
                    return ye.widgetInstance(e, ye.mobile.ui) || ie
                },
                viewHasNativeScrolling: function() {
                    var e = this.view();
                    return e && e.options.useNativeScrolling
                },
                container: function() {
                    var e = this.element.closest(ye.roleSelector("view layout modalview drawer splitview"));
                    return ye.widgetInstance(e.eq(0), ye.mobile.ui) || ie
                }
            }), ve(ye.mobile, {
                init: function(e) {
                    ye.init(e, ye.mobile.ui, ye.ui, ye.dataviz.ui)
                },
                appLevelNativeScrolling: function() {
                    return ye.mobile.application && ye.mobile.application.options && ye.mobile.application.options.useNativeScrolling
                },
                roles: {},
                ui: {
                    Widget: ae,
                    DataBoundWidget: ee.extend(ae.prototype),
                    roles: {},
                    plugin: function(e) {
                        ye.ui.plugin(e, ye.mobile.ui, "Mobile")
                    }
                }
            }), u(ye.dataviz, {
                init: function(e) {
                    ye.init(e, ye.dataviz.ui)
                },
                ui: {
                    roles: {},
                    themes: {},
                    views: [],
                    plugin: function(e) {
                        ye.ui.plugin(e, ye.dataviz.ui)
                    }
                },
                roles: {}
            }), ye.touchScroller = function(t, n) {
                return n || (n = {}), n.useNative = !0, e(t).map(function(t, r) {
                    return r = e(r), !(!ke.kineticScrollNeeded || !ye.mobile.ui.Scroller || r.data("kendoMobileScroller")) && (r.kendoMobileScroller(n), r.data("kendoMobileScroller"))
                })[0]
            }, ye.preventDefault = function(e) {
                e.preventDefault()
            }, ye.widgetInstance = function(e, n) {
                var r, o, i, a, s = e.data(ye.ns + "role"),
                    u = [];
                if (s) {
                    if ("content" === s && (s = "scroller"), n)
                        if (n[0])
                            for (r = 0, o = n.length; r < o; r++) u.push(n[r].roles[s]);
                        else u.push(n.roles[s]);
                    else u = [ye.ui.roles[s], ye.dataviz.ui.roles[s], ye.mobile.ui.roles[s]];
                    for (s.indexOf(".") >= 0 && (u = [ye.getter(s)(t)]), r = 0, o = u.length; r < o; r++)
                        if (i = u[r], i && (a = e.data("kendo" + i.fn.options.prefix + i.fn.options.name))) return a
                }
            }, ye.onResize = function(n) {
                var r = n;
                return ke.mobileOS.android && (r = function() {
                    setTimeout(n, 600)
                }), e(t).on(ke.resize, r), r
            }, ye.unbindResize = function(n) {
                e(t).off(ke.resize, n)
            }, ye.attrValue = function(e, t) {
                return e.data(ye.ns + t)
            }, ye.days = {
                Sunday: 0,
                Monday: 1,
                Tuesday: 2,
                Wednesday: 3,
                Thursday: 4,
                Friday: 5,
                Saturday: 6
            }, e.extend(e.expr[":"], {
                kendoFocusable: function(t) {
                    var n = e.attr(t, "tabindex");
                    return z(t, !isNaN(n) && n > -1)
                }
            }), se = ["mousedown", "mousemove", "mouseenter", "mouseleave", "mouseover", "mouseout", "mouseup", "click"], ue = "label, input, [data-rel=external]", le = {
                setupMouseMute: function() {
                    var t, n = 0,
                        r = se.length,
                        o = document.documentElement;
                    if (!le.mouseTrap && ke.eventCapture)
                        for (le.mouseTrap = !0, le.bustClick = !1, le.captureMouse = !1, t = function(t) {
                                le.captureMouse && ("click" === t.type ? le.bustClick && !e(t.target).is(ue) && (t.preventDefault(), t.stopPropagation()) : t.stopPropagation())
                            }; n < r; n++) o.addEventListener(se[n], t, !0)
                },
                muteMouse: function(e) {
                    le.captureMouse = !0, e.data.bustClick && (le.bustClick = !0), clearTimeout(le.mouseTrapTimeoutID)
                },
                unMuteMouse: function() {
                    clearTimeout(le.mouseTrapTimeoutID), le.mouseTrapTimeoutID = setTimeout(function() {
                        le.captureMouse = !1, le.bustClick = !1
                    }, 400)
                }
            }, ce = {
                down: "touchstart mousedown",
                move: "mousemove touchmove",
                up: "mouseup touchend touchcancel",
                cancel: "mouseleave touchcancel"
            }, ke.touch && (ke.mobileOS.ios || ke.mobileOS.android) ? ce = {
                down: "touchstart",
                move: "touchmove",
                up: "touchend touchcancel",
                cancel: "touchcancel"
            } : ke.pointers ? ce = {
                down: "pointerdown",
                move: "pointermove",
                up: "pointerup",
                cancel: "pointercancel pointerleave"
            } : ke.msPointers && (ce = {
                down: "MSPointerDown",
                move: "MSPointerMove",
                up: "MSPointerUp",
                cancel: "MSPointerCancel MSPointerLeave"
            }), !ke.msPointers || "onmspointerenter" in t || e.each({
                MSPointerEnter: "MSPointerOver",
                MSPointerLeave: "MSPointerOut"
            }, function(t, n) {
                e.event.special[t] = {
                    delegateType: n,
                    bindType: n,
                    handle: function(t) {
                        var r, o = this,
                            i = t.relatedTarget,
                            a = t.handleObj;
                        return i && (i === o || e.contains(o, i)) || (t.type = a.origType, r = a.handler.apply(this, arguments), t.type = n), r
                    }
                }
            }), de = function(e) {
                return ce[e] || e
            }, fe = /([^ ]+)/g, ye.applyEventMap = function(e, t) {
                return e = e.replace(fe, de), t && (e = e.replace(fe, "$1." + t)), e
            }, pe = e.fn.on, ve(!0, E, e), E.fn = E.prototype = new e, E.fn.constructor = E, E.fn.init = function(t, n) {
                return n && n instanceof e && !(n instanceof E) && (n = E(n)), e.fn.init.call(this, t, n, me)
            }, E.fn.init.prototype = E.fn, me = E(document), ve(E.fn, {
                handler: function(e) {
                    return this.data("handler", e), this
                },
                autoApplyNS: function(e) {
                    return this.data("kendoNS", e || ye.guid()), this
                },
                on: function() {
                    var e, t, n, r, o, i, a = this,
                        s = a.data("kendoNS");
                    return 1 === arguments.length ? pe.call(a, arguments[0]) : (e = a, t = Ie.call(arguments), typeof t[t.length - 1] === Fe && t.pop(), n = t[t.length - 1], r = ye.applyEventMap(t[0], s), ke.mouseAndTouchPresent && r.search(/mouse|click/) > -1 && this[0] !== document.documentElement && (le.setupMouseMute(), o = 2 === t.length ? null : t[1], i = r.indexOf("click") > -1 && r.indexOf("touchend") > -1, pe.call(this, {
                        touchstart: le.muteMouse,
                        touchend: le.unMuteMouse
                    }, o, {
                        bustClick: i
                    })), typeof n === He && (e = a.data("handler"), n = e[n], t[t.length - 1] = function(t) {
                        n.call(e, t)
                    }), t[0] = r, pe.apply(a, t), a)
                },
                kendoDestroy: function(e) {
                    return e = e || this.data("kendoNS"), e && this.off("." + e), this
                }
            }), ye.jQuery = E, ye.eventMap = ce, ye.timezone = function() {
                function e(e, t) {
                    var n, r, o, i = t[3],
                        a = t[4],
                        s = t[5],
                        u = t[8];
                    return u || (t[8] = u = {}), u[e] ? u[e] : (isNaN(a) ? 0 === a.indexOf("last") ? (n = new Date(Date.UTC(e, c[i] + 1, 1, s[0] - 24, s[1], s[2], 0)), r = d[a.substr(4, 3)], o = n.getUTCDay(), n.setUTCDate(n.getUTCDate() + r - o - (r > o ? 7 : 0))) : a.indexOf(">=") >= 0 && (n = new Date(Date.UTC(e, c[i], a.substr(5), s[0], s[1], s[2], 0)), r = d[a.substr(0, 3)], o = n.getUTCDay(), n.setUTCDate(n.getUTCDate() + r - o + (r < o ? 7 : 0))) : n = new Date(Date.UTC(e, c[i], a, s[0], s[1], s[2], 0)), u[e] = n)
                }

                function t(t, n, r) {
                    var o, i, a, s;
                    return (n = n[r]) ? (a = new Date(t).getUTCFullYear(), n = jQuery.grep(n, function(e) {
                        var t = e[0],
                            n = e[1];
                        return t <= a && (n >= a || t == a && "only" == n || "max" == n)
                    }), n.push(t), n.sort(function(t, n) {
                        return "number" != typeof t && (t = +e(a, t)), "number" != typeof n && (n = +e(a, n)), t - n
                    }), s = n[jQuery.inArray(t, n) - 1] || n[n.length - 1], isNaN(s) ? s : null) : (o = r.split(":"), i = 0, o.length > 1 && (i = 60 * o[0] + +o[1]), [-1e6, "max", "-", "Jan", 1, [0, 0, 0], i, "-"])
                }

                function n(e, t, n) {
                    var r, o, i, a = t[n];
                    if ("string" == typeof a && (a = t[a]), !a) throw Error('Timezone "' + n + '" is either incorrect, or kendo.timezones.min.js is not included.');
                    for (r = a.length - 1; r >= 0 && (o = a[r][3], !(o && e > o)); r--);
                    if (i = a[r + 1], !i) throw Error('Timezone "' + n + '" not found on ' + e + ".");
                    return i
                }

                function r(e, r, o, i) {
                    typeof e != _e && (e = Date.UTC(e.getFullYear(), e.getMonth(), e.getDate(), e.getHours(), e.getMinutes(), e.getSeconds(), e.getMilliseconds()));
                    var a = n(e, r, i);
                    return {
                        zone: a,
                        rule: t(e, o, a[1])
                    }
                }

                function o(e, t) {
                    var n, o, i;
                    return "Etc/UTC" == t || "Etc/GMT" == t ? 0 : (n = r(e, this.zones, this.rules, t), o = n.zone, i = n.rule, ye.parseFloat(i ? o[0] - i[6] : o[0]))
                }

                function i(e, t) {
                    var n = r(e, this.zones, this.rules, t),
                        o = n.zone,
                        i = n.rule,
                        a = o[2];
                    return a.indexOf("/") >= 0 ? a.split("/")[i && +i[6] ? 1 : 0] : a.indexOf("%s") >= 0 ? a.replace("%s", i && "-" != i[7] ? i[7] : "") : a
                }

                function a(e, t, n) {
                    var r, o, i, a = n;
                    return typeof t == He && (t = this.offset(e, t)), typeof n == He && (n = this.offset(e, n)), o = e.getTimezoneOffset(), e = new Date(e.getTime() + 6e4 * (t - n)), i = e.getTimezoneOffset(), typeof a == He && (a = this.offset(e, a)), r = i - o + (n - a), new Date(e.getTime() + 6e4 * r)
                }

                function s(e, t) {
                    return this.convert(e, e.getTimezoneOffset(), t)
                }

                function u(e, t) {
                    return this.convert(e, t, e.getTimezoneOffset())
                }

                function l(e) {
                    return this.apply(new Date(e), "Etc/UTC")
                }
                var c = {
                        Jan: 0,
                        Feb: 1,
                        Mar: 2,
                        Apr: 3,
                        May: 4,
                        Jun: 5,
                        Jul: 6,
                        Aug: 7,
                        Sep: 8,
                        Oct: 9,
                        Nov: 10,
                        Dec: 11
                    },
                    d = {
                        Sun: 0,
                        Mon: 1,
                        Tue: 2,
                        Wed: 3,
                        Thu: 4,
                        Fri: 5,
                        Sat: 6
                    };
                return {
                    zones: {},
                    rules: {},
                    offset: o,
                    convert: a,
                    apply: s,
                    remove: u,
                    abbr: i,
                    toLocalDate: l
                }
            }(), ye.date = function() {
                function e(e, t) {
                    return 0 === t && 23 === e.getHours() && (e.setHours(e.getHours() + 2), !0)
                }

                function t(t, n, r) {
                    var o = t.getHours();
                    r = r || 1, n = (n - t.getDay() + 7 * r) % 7, t.setDate(t.getDate() + n), e(t, o)
                }

                function r(e, n, r) {
                    return e = new Date(e), t(e, n, r), e
                }

                function o(e) {
                    return new Date(e.getFullYear(), e.getMonth(), 1)
                }

                function i(e) {
                    var t = new Date(e.getFullYear(), e.getMonth() + 1, 0),
                        n = o(e),
                        r = Math.abs(t.getTimezoneOffset() - n.getTimezoneOffset());
                    return r && t.setHours(n.getHours() + r / 60), t
                }

                function a(e, t) {
                    return 1 !== t ? m(r(e, t, -1), 4) : m(e, 4 - (e.getDay() || 7))
                }

                function s(e, t) {
                    var n = new Date(e.getFullYear(), 0, 1, (-6)),
                        r = a(e, t),
                        o = r.getTime() - n.getTime(),
                        i = Math.floor(o / M);
                    return 1 + Math.floor(i / 7)
                }

                function u(e, t) {
                    var r, o, i;
                    return t === n && (t = ye.culture().calendar.firstDay), r = m(e, -7), o = m(e, 7), i = s(e, t), 0 === i ? s(r, t) + 1 : 53 === i && s(o, t) > 1 ? 1 : i
                }

                function l(t) {
                    return t = new Date(t.getFullYear(), t.getMonth(), t.getDate(), 0, 0, 0), e(t, 0), t
                }

                function c(e) {
                    return Date.UTC(e.getFullYear(), e.getMonth(), e.getDate(), e.getHours(), e.getMinutes(), e.getSeconds(), e.getMilliseconds())
                }

                function d(e) {
                    return b(e).getTime() - l(b(e))
                }

                function f(e, t, n) {
                    var r, o = d(t),
                        i = d(n);
                    return !e || o == i || (t >= n && (n += M), r = d(e), o > r && (r += M), i < o && (i += M), r >= o && r <= i)
                }

                function p(e, t, n) {
                    var r, o = t.getTime(),
                        i = n.getTime();
                    return o >= i && (i += M), r = e.getTime(), r >= o && r <= i
                }

                function m(t, n) {
                    var r = t.getHours();
                    return t = new Date(t), h(t, n * M), e(t, r), t
                }

                function h(e, t, n) {
                    var r, o = e.getTimezoneOffset();
                    e.setTime(e.getTime() + t), n || (r = e.getTimezoneOffset() - o, e.setTime(e.getTime() + r * w))
                }

                function g(t, n) {
                    return t = new Date(ye.date.getDate(t).getTime() + ye.date.getMilliseconds(n)), e(t, n.getHours()), t
                }

                function y() {
                    return l(new Date)
                }

                function v(e) {
                    return l(e).getTime() == y().getTime()
                }

                function b(e) {
                    var t = new Date(1980, 1, 1, 0, 0, 0);
                    return e && t.setHours(e.getHours(), e.getMinutes(), e.getSeconds(), e.getMilliseconds()), t
                }
                var w = 6e4,
                    M = 864e5;
                return {
                    adjustDST: e,
                    dayOfWeek: r,
                    setDayOfWeek: t,
                    getDate: l,
                    isInDateRange: p,
                    isInTimeRange: f,
                    isToday: v,
                    nextDay: function(e) {
                        return m(e, 1)
                    },
                    previousDay: function(e) {
                        return m(e, -1)
                    },
                    toUtcTime: c,
                    MS_PER_DAY: M,
                    MS_PER_HOUR: 60 * w,
                    MS_PER_MINUTE: w,
                    setTime: h,
                    setHours: g,
                    addDays: m,
                    today: y,
                    toInvariantTime: b,
                    firstDayOfMonth: o,
                    lastDayOfMonth: i,
                    weekInYear: u,
                    getMilliseconds: d
                }
            }(), ye.stripWhitespace = function(e) {
                var t, n, r;
                if (document.createNodeIterator)
                    for (t = document.createNodeIterator(e, NodeFilter.SHOW_TEXT, function(t) {
                            return t.parentNode == e ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_REJECT
                        }, !1); t.nextNode();) t.referenceNode && !t.referenceNode.textContent.trim() && t.referenceNode.parentNode.removeChild(t.referenceNode);
                else
                    for (n = 0; n < e.childNodes.length; n++) r = e.childNodes[n], 3 != r.nodeType || /\S/.test(r.nodeValue) || (e.removeChild(r), n--), 1 == r.nodeType && ye.stripWhitespace(r)
            }, he = t.requestAnimationFrame || t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || t.oRequestAnimationFrame || t.msRequestAnimationFrame || function(e) {
                setTimeout(e, 1e3 / 60)
            }, ye.animationFrame = function(e) {
                he.call(t, e)
            }, ge = [], ye.queueAnimation = function(e) {
                ge[ge.length] = e, 1 === ge.length && ye.runNextAnimation()
            }, ye.runNextAnimation = function() {
                ye.animationFrame(function() {
                    ge[0] && (ge.shift()(), ge[0] && ye.runNextAnimation())
                })
            }, ye.parseQueryStringParams = function(e) {
                for (var t = e.split("?")[1] || "", n = {}, r = t.split(/&|=/), o = r.length, i = 0; i < o; i += 2) "" !== r[i] && (n[decodeURIComponent(r[i])] = decodeURIComponent(r[i + 1]));
                return n
            }, ye.elementUnderCursor = function(e) {
                if (n !== e.x.client) return document.elementFromPoint(e.x.client, e.y.client)
            }, ye.wheelDeltaY = function(e) {
                var t, r = e.originalEvent,
                    o = r.wheelDeltaY;
                return r.wheelDelta ? (o === n || o) && (t = r.wheelDelta) : r.detail && r.axis === r.VERTICAL_AXIS && (t = 10 * -r.detail), t
            }, ye.throttle = function(e, t) {
                var r, o, i = 0;
                return !t || t <= 0 ? e : (o = function() {
                    function o() {
                        e.apply(a, u), i = +new Date
                    }
                    var a = this,
                        s = +new Date - i,
                        u = arguments;
                    return i ? (r && clearTimeout(r), s > t ? o() : r = setTimeout(o, t - s), n) : o()
                }, o.cancel = function() {
                    clearTimeout(r)
                }, o)
            }, ye.caret = function(t, r, o) {
                var i, a, s, u, l, c = r !== n;
                if (o === n && (o = r), t[0] && (t = t[0]), !c || !t.disabled) {
                    try {
                        t.selectionStart !== n ? c ? (t.focus(), a = ke.mobileOS, a.wp || a.android ? setTimeout(function() {
                            t.setSelectionRange(r, o)
                        }, 0) : t.setSelectionRange(r, o)) : r = [t.selectionStart, t.selectionEnd] : document.selection && (e(t).is(":visible") && t.focus(), i = t.createTextRange(), c ? (i.collapse(!0), i.moveStart("character", r), i.moveEnd("character", o - r), i.select()) : (s = i.duplicate(), i.moveToBookmark(document.selection.createRange().getBookmark()), s.setEndPoint("EndToStart", i), u = s.text.length, l = u + i.text.length, r = [u, l]))
                    } catch (d) {
                        r = []
                    }
                    return r
                }
            }, ye.compileMobileDirective = function(e, n) {
                var r = t.angular;
                return e.attr("data-" + ye.ns + "role", e[0].tagName.toLowerCase().replace("kendo-mobile-", "").replace("-", "")), r.element(e).injector().invoke(["$compile", function(t) {
                    t(e)(n), /^\$(digest|apply)$/.test(n.$$phase) || n.$digest()
                }]), ye.widgetInstance(e, ye.mobile.ui)
            }, ye.antiForgeryTokens = function() {
                var t = {},
                    r = e("meta[name=csrf-token],meta[name=_csrf]").attr("content"),
                    o = e("meta[name=csrf-param],meta[name=_csrf_header]").attr("content");
                return e("input[name^='__RequestVerificationToken']").each(function() {
                    t[this.name] = this.value
                }), o !== n && r !== n && (t[o] = r), t
            }, ye.cycleForm = function(e) {
                function t(e) {
                    var t = ye.widgetInstance(e);
                    t && t.focus ? t.focus() : e.focus()
                }
                var n = e.find("input, .k-widget").first(),
                    r = e.find("button, .k-button").last();
                r.on("keydown", function(e) {
                    e.keyCode != ye.keys.TAB || e.shiftKey || (e.preventDefault(), t(n))
                }), n.on("keydown", function(e) {
                    e.keyCode == ye.keys.TAB && e.shiftKey && (e.preventDefault(), t(r))
                })
            }, ye.focusElement = function(n) {
                var r = [],
                    o = n.parentsUntil("body").filter(function(e, t) {
                        var n = ye.getComputedStyles(t, ["overflow"]);
                        return "visible" !== n.overflow
                    }).add(t);
                o.each(function(t, n) {
                    r[t] = e(n).scrollTop()
                });
                try {
                    n[0].setActive()
                } catch (i) {
                    n[0].focus()
                }
                o.each(function(t, n) {
                    e(n).scrollTop(r[t])
                })
            },
            function() {
                function n(t, n, r, o) {
                    var i, a, s = e("<form>").attr({
                            action: r,
                            method: "POST",
                            target: o
                        }),
                        u = ye.antiForgeryTokens();
                    u.fileName = n, i = t.split(";base64,"), u.contentType = i[0].replace("data:", ""), u.base64 = i[1];
                    for (a in u) u.hasOwnProperty(a) && e("<input>").attr({
                        value: u[a],
                        name: a,
                        type: "hidden"
                    }).appendTo(s);
                    s.appendTo("body").submit().remove()
                }

                function r(e, t) {
                    var n, r, o, i, a, s = e;
                    if ("string" == typeof e) {
                        for (n = e.split(";base64,"), r = n[0], o = atob(n[1]), i = new Uint8Array(o.length), a = 0; a < o.length; a++) i[a] = o.charCodeAt(a);
                        s = new Blob([i.buffer], {
                            type: r
                        })
                    }
                    navigator.msSaveBlob(s, t)
                }

                function o(e, n) {
                    t.Blob && e instanceof Blob && (e = URL.createObjectURL(e)), i.download = n, i.href = e;
                    var r = document.createEvent("MouseEvents");
                    r.initMouseEvent("click", !0, !1, t, 0, 0, 0, 0, 0, !1, !1, !1, !1, 0, null), i.dispatchEvent(r), setTimeout(function() {
                        URL.revokeObjectURL(e)
                    })
                }
                var i = document.createElement("a"),
                    a = "download" in i && !ye.support.browser.edge;
                ye.saveAs = function(e) {
                    var t = n;
                    e.forceProxy || (a ? t = o : navigator.msSaveBlob && (t = r)), t(e.dataURI, e.fileName, e.proxyURL, e.proxyTarget)
                }
            }(), ye.proxyModelSetters = function(e) {
                var t = {};
                return Object.keys(e || {}).forEach(function(n) {
                    Object.defineProperty(t, n, {
                        get: function() {
                            return e[n]
                        },
                        set: function(t) {
                            e[n] = t, e.dirty = !0
                        }
                    })
                }), t
            }
    }(jQuery, window), window.kendo
}, "function" == typeof define && define.amd ? define : function(e, t, n) {
    (n || t)()
});
//# sourceMappingURL=kendo.core.min.js.map;
/** 
 * Kendo UI v2018.1.221 (http://www.telerik.com/kendo-ui)                                                                                                                                               
 * Copyright 2018 Telerik AD. All rights reserved.                                                                                                                                                      
 *                                                                                                                                                                                                      
 * Kendo UI commercial licenses may be obtained at                                                                                                                                                      
 * http://www.telerik.com/purchase/license-agreement/kendo-ui-complete                                                                                                                                  
 * If you do not own a commercial license, this file shall be governed by the trial license terms.                                                                                                      

*/
! function(e, define) {
    define("kendo.data.min", ["kendo.core.min", "kendo.data.odata.min", "kendo.data.xml.min"], e)
}(function() {
    return function(e, t) {
        function r(e, t, r, i) {
            return function(n) {
                var a, s = {};
                for (a in n) s[a] = n[a];
                s.field = i ? r + "." + n.field : r, t == De && e._notifyChange && e._notifyChange(s), e.trigger(t, s)
            }
        }

        function i(t, r) {
            if (t === r) return !0;
            var n, a = e.type(t),
                s = e.type(r);
            if (a !== s) return !1;
            if ("date" === a) return t.getTime() === r.getTime();
            if ("object" !== a && "array" !== a) return !1;
            for (n in t)
                if (!i(t[n], r[n])) return !1;
            return !0
        }

        function n(e, t) {
            var r, i;
            for (i in e) {
                if (r = e[i], de(r) && r.field && r.field === t) return r;
                if (r === t) return r
            }
            return null
        }

        function a(e) {
            this.data = e || []
        }

        function s(e, r) {
            if (e) {
                var i = typeof e === we ? {
                        field: e,
                        dir: r
                    } : e,
                    n = ce(i) ? i : i !== t ? [i] : [];
                return ge(n, function(e) {
                    return !!e.dir
                })
            }
        }

        function o(e) {
            var t, r, i, n, a = e.filters;
            if (a)
                for (t = 0, r = a.length; t < r; t++) i = a[t], n = i.operator, n && typeof n === we && (i.operator = X[n.toLowerCase()] || n), o(i)
        }

        function u(e) {
            if (e && !fe(e)) return !ce(e) && e.filters || (e = {
                logic: "and",
                filters: ce(e) ? e : [e]
            }), o(e), e
        }

        function l(e, t) {
            return !e.logic && !t.logic && (e.field === t.field && e.value === t.value && e.operator === t.operator)
        }

        function h(e) {
            return e = e || {}, fe(e) ? {
                logic: "and",
                filters: []
            } : u(e)
        }

        function d(e, t) {
            return t.logic || e.field > t.field ? 1 : e.field < t.field ? -1 : 0
        }

        function f(e, t) {
            var r, i, n, a, s;
            if (e = h(e), t = h(t), e.logic !== t.logic) return !1;
            if (n = (e.filters || []).slice(), a = (t.filters || []).slice(), n.length !== a.length) return !1;
            for (n = n.sort(d), a = a.sort(d), s = 0; s < n.length; s++)
                if (r = n[s], i = a[s], r.logic && i.logic) {
                    if (!f(r, i)) return !1
                } else if (!l(r, i)) return !1;
            return !0
        }

        function c(e) {
            return ce(e) ? e : [e]
        }

        function g(e, r) {
            var i = typeof e === we ? {
                    field: e,
                    dir: r
                } : e,
                n = ce(i) ? i : i !== t ? [i] : [];
            return L(n, function(e) {
                return {
                    field: e.field,
                    dir: e.dir || "asc",
                    aggregates: e.aggregates
                }
            })
        }

        function p(e, t) {
            return e && e.getTime && t && t.getTime ? e.getTime() === t.getTime() : e === t
        }

        function _(e, t, r, i, n, a) {
            var s, o, u, l, h;
            for (t = t || [], l = t.length, s = 0; s < l; s++) o = t[s], u = o.aggregate, h = o.field, e[h] = e[h] || {}, a[h] = a[h] || {}, a[h][u] = a[h][u] || {}, e[h][u] = Y[u.toLowerCase()](e[h][u], r, me.accessor(h), i, n, a[h][u])
        }

        function v(e) {
            return "number" == typeof e && !isNaN(e)
        }

        function m(e) {
            return e && e.getTime
        }

        function y(e) {
            var t, r = e.length,
                i = Array(r);
            for (t = 0; t < r; t++) i[t] = e[t].toJSON();
            return i
        }

        function S(e, t, r, i, n) {
            var a, s, o, u, l, h = {};
            for (u = 0, l = e.length; u < l; u++) {
                a = e[u];
                for (s in t) o = n[s], o && o !== s && (h[o] || (h[o] = me.setter(o)), h[o](a, t[s](a)), delete a[s])
            }
        }

        function b(e, t, r, i, n) {
            var a, s, o, u, l;
            for (u = 0, l = e.length; u < l; u++) {
                a = e[u];
                for (s in t) a[s] = r._parse(s, t[s](a)), o = n[s], o && o !== s && delete a[o]
            }
        }

        function w(e, t, r, i, n) {
            var a, s, o, u;
            for (s = 0, u = e.length; s < u; s++) a = e[s], o = i[a.field], o && o != a.field && (a.field = o), a.value = r._parse(a.field, a.value), a.hasSubgroups ? w(a.items, t, r, i, n) : b(a.items, t, r, i, n)
        }

        function k(e, t, r, i, n, a) {
            return function(s) {
                return s = e(s), s && !fe(i) && ("[object Array]" === We.call(s) || s instanceof Ke || (s = [s]), r(s, i, new t, n, a)), s || []
            }
        }

        function x(e, t, r, i) {
            for (var n, a, s, o = 0; t.length && i && (n = t[o], a = n.items, s = a.length, e && e.field === n.field && e.value === n.value ? (e.hasSubgroups && e.items.length ? x(e.items[e.items.length - 1], n.items, r, i) : (a = a.slice(r, r + i), e.items = e.items.concat(a)), t.splice(o--, 1)) : n.hasSubgroups && a.length ? (x(n, a, r, i), n.items.length || t.splice(o--, 1)) : (a = a.slice(r, r + i), n.items = a, n.items.length || t.splice(o--, 1)), 0 === a.length ? r -= s : (r = 0, i -= a.length), !(++o >= t.length)););
            o < t.length && t.splice(o, t.length - o)
        }

        function q(e) {
            var t, r, i, n, a, s = [];
            for (t = 0, r = e.length; t < r; t++)
                if (a = e.at(t), a.hasSubgroups) s = s.concat(q(a.items));
                else
                    for (i = a.items, n = 0; n < i.length; n++) s.push(i.at(n));
            return s
        }

        function F(e, t) {
            var r, i, n;
            if (t)
                for (r = 0, i = e.length; r < i; r++) n = e.at(r), n.hasSubgroups ? F(n.items, t) : n.items = new H(n.items, t)
        }

        function R(e, t) {
            for (var r = 0, i = e.length; r < i; r++)
                if (e[r].hasSubgroups) {
                    if (R(e[r].items, t)) return !0
                } else if (t(e[r].items, e[r])) return !0
        }

        function D(e, t, r, i) {
            for (var n = 0; n < e.length && e[n].data !== t && !z(e[n].data, r, i); n++);
        }

        function z(e, t, r) {
            for (var i = 0, n = e.length; i < n; i++) {
                if (e[i] && e[i].hasSubgroups) return z(e[i].items, t, r);
                if (e[i] === t || e[i] === r) return e[i] = r, !0
            }
        }

        function O(e, r, i, n, a) {
            var s, o, u, l;
            for (s = 0, o = e.length; s < o; s++)
                if (u = e[s], u && !(u instanceof n))
                    if (u.hasSubgroups === t || a) {
                        for (l = 0; l < r.length; l++)
                            if (r[l] === u) {
                                e[s] = r.at(l), D(i, r, u, e[s]);
                                break
                            }
                    } else O(u.items, r, i, n, a)
        }

        function P(e, t) {
            var r, i, n = e.length;
            for (i = 0; i < n; i++)
                if (r = e[i], r.uid && r.uid == t.uid) return e.splice(i, 1), r
        }

        function C(e, t) {
            return t ? A(e, function(e) {
                return e.uid && e.uid == t.uid || e[t.idField] === t.id && t.id !== t._defaultId
            }) : -1
        }

        function T(e, t) {
            return t ? A(e, function(e) {
                return e.uid == t.uid
            }) : -1
        }

        function A(e, t) {
            var r, i;
            for (r = 0, i = e.length; r < i; r++)
                if (t(e[r])) return r;
            return -1
        }

        function I(e, t) {
            var r, i;
            return e && !fe(e) ? (r = e[t], i = de(r) ? r.from || r.field || t : e[t] || t, ye(i) ? t : i) : t
        }

        function M(e, t) {
            var r, i, n, a = {};
            for (n in e) "filters" !== n && (a[n] = e[n]);
            if (e.filters)
                for (a.filters = [], r = 0, i = e.filters.length; r < i; r++) a.filters[r] = M(e.filters[r], t);
            else a.field = I(t.fields, a.field);
            return a
        }

        function N(e, t) {
            var r, i, n, a, s, o = [];
            for (r = 0, i = e.length; r < i; r++) {
                n = {}, a = e[r];
                for (s in a) n[s] = a[s];
                n.field = I(t.fields, n.field), n.aggregates && ce(n.aggregates) && (n.aggregates = N(n.aggregates, t)), o.push(n)
            }
            return o
        }

        function j(t, r) {
            var i, n, a, s, o, u, l, h, d, f;
            for (t = e(t)[0], i = t.options, n = r[0], a = r[1], s = [], o = 0, u = i.length; o < u; o++) d = {}, h = i[o], l = h.parentNode, l === t && (l = null), h.disabled || l && l.disabled || (l && (d.optgroup = l.label), d[n.field] = h.text, f = h.attributes.value, f = f && f.specified ? h.value : h.text, d[a.field] = f, s.push(d));
            return s
        }

        function G(t, r) {
            var i, n, a, s, o, u, l, h = e(t)[0].tBodies[0],
                d = h ? h.rows : [],
                f = r.length,
                c = [];
            for (i = 0, n = d.length; i < n; i++) {
                for (o = {}, l = !0, s = d[i].cells, a = 0; a < f; a++) u = s[a], "th" !== u.nodeName.toLowerCase() && (l = !1, o[r[a].field] = u.innerHTML);
                l || c.push(o)
            }
            return c
        }

        function B(e) {
            return function() {
                var t = this._data,
                    r = ie.fn[e].apply(this, Je.call(arguments));
                return this._data != t && this._attachBubbleHandlers(), r
            }
        }

        function E(t, r) {
            function i(e, t) {
                return e.filter(t).add(e.find(t))
            }
            var n, a, s, o, u, l, h, d, f = e(t).children(),
                c = [],
                g = r[0].field,
                p = r[1] && r[1].field,
                _ = r[2] && r[2].field,
                v = r[3] && r[3].field;
            for (n = 0, a = f.length; n < a; n++) s = {
                _loaded: !0
            }, o = f.eq(n), l = o[0].firstChild, d = o.children(), t = d.filter("ul"), d = d.filter(":not(ul)"), u = o.attr("data-id"), u && (s.id = u), l && (s[g] = 3 == l.nodeType ? l.nodeValue : d.text()), p && (s[p] = i(d, "a").attr("href")), v && (s[v] = i(d, "img").attr("src")), _ && (h = i(d, ".k-sprite").prop("className"), s[_] = h && e.trim(h.replace("k-sprite", ""))), t.length && (s.items = E(t.eq(0), r)), "true" == o.attr("data-hasChildren") && (s.hasChildren = !0), c.push(s);
            return c
        }
        var L, H, U, J, V, W, Q, $, K, X, Y, Z, ee, te, re, ie, ne, ae, se, oe, ue, le = e.extend,
            he = e.proxy,
            de = e.isPlainObject,
            fe = e.isEmptyObject,
            ce = e.isArray,
            ge = e.grep,
            pe = e.ajax,
            _e = e.each,
            ve = e.noop,
            me = window.kendo,
            ye = me.isFunction,
            Se = me.Observable,
            be = me.Class,
            we = "string",
            ke = "function",
            xe = "create",
            qe = "read",
            Fe = "update",
            Re = "destroy",
            De = "change",
            ze = "sync",
            Oe = "get",
            Pe = "error",
            Ce = "requestStart",
            Te = "progress",
            Ae = "requestEnd",
            Ie = [xe, qe, Fe, Re],
            Me = function(e) {
                return e
            },
            Ne = me.getter,
            je = me.stringify,
            Ge = Math,
            Be = [].push,
            Ee = [].join,
            Le = [].pop,
            He = [].splice,
            Ue = [].shift,
            Je = [].slice,
            Ve = [].unshift,
            We = {}.toString,
            Qe = me.support.stableSort,
            $e = /^\/Date\((.*?)\)\/$/,
            Ke = Se.extend({
                init: function(e, t) {
                    var r = this;
                    r.type = t || U, Se.fn.init.call(r), r.length = e.length, r.wrapAll(e, r)
                },
                at: function(e) {
                    return this[e]
                },
                toJSON: function() {
                    var e, t, r = this.length,
                        i = Array(r);
                    for (e = 0; e < r; e++) t = this[e], t instanceof U && (t = t.toJSON()), i[e] = t;
                    return i
                },
                parent: ve,
                wrapAll: function(e, t) {
                    var r, i, n = this,
                        a = function() {
                            return n
                        };
                    for (t = t || [], r = 0, i = e.length; r < i; r++) t[r] = n.wrap(e[r], a);
                    return t
                },
                wrap: function(e, t) {
                    var r, i = this;
                    return null !== e && "[object Object]" === We.call(e) && (r = e instanceof i.type || e instanceof W, r || (e = e instanceof U ? e.toJSON() : e, e = new i.type(e)), e.parent = t, e.bind(De, function(e) {
                        i.trigger(De, {
                            field: e.field,
                            node: e.node,
                            index: e.index,
                            items: e.items || [this],
                            action: e.node ? e.action || "itemloaded" : "itemchange"
                        })
                    })), e
                },
                push: function() {
                    var e, t = this.length,
                        r = this.wrapAll(arguments);
                    return e = Be.apply(this, r), this.trigger(De, {
                        action: "add",
                        index: t,
                        items: r
                    }), e
                },
                slice: Je,
                sort: [].sort,
                join: Ee,
                pop: function() {
                    var e = this.length,
                        t = Le.apply(this);
                    return e && this.trigger(De, {
                        action: "remove",
                        index: e - 1,
                        items: [t]
                    }), t
                },
                splice: function(e, t, r) {
                    var i, n, a, s = this.wrapAll(Je.call(arguments, 2));
                    if (i = He.apply(this, [e, t].concat(s)), i.length)
                        for (this.trigger(De, {
                                action: "remove",
                                index: e,
                                items: i
                            }), n = 0, a = i.length; n < a; n++) i[n] && i[n].children && i[n].unbind(De);
                    return r && this.trigger(De, {
                        action: "add",
                        index: e,
                        items: s
                    }), i
                },
                shift: function() {
                    var e = this.length,
                        t = Ue.apply(this);
                    return e && this.trigger(De, {
                        action: "remove",
                        index: 0,
                        items: [t]
                    }), t
                },
                unshift: function() {
                    var e, t = this.wrapAll(arguments);
                    return e = Ve.apply(this, t), this.trigger(De, {
                        action: "add",
                        index: 0,
                        items: t
                    }), e
                },
                indexOf: function(e) {
                    var t, r, i = this;
                    for (t = 0, r = i.length; t < r; t++)
                        if (i[t] === e) return t;
                    return -1
                },
                forEach: function(e, t) {
                    for (var r = 0, i = this.length, n = t || window; r < i; r++) e.call(n, this[r], r, this)
                },
                map: function(e, t) {
                    for (var r = 0, i = [], n = this.length, a = t || window; r < n; r++) i[r] = e.call(a, this[r], r, this);
                    return i
                },
                reduce: function(e) {
                    var t, r = 0,
                        i = this.length;
                    for (2 == arguments.length ? t = arguments[1] : r < i && (t = this[r++]); r < i; r++) t = e(t, this[r], r, this);
                    return t
                },
                reduceRight: function(e) {
                    var t, r = this.length - 1;
                    for (2 == arguments.length ? t = arguments[1] : r > 0 && (t = this[r--]); r >= 0; r--) t = e(t, this[r], r, this);
                    return t
                },
                filter: function(e, t) {
                    for (var r, i = 0, n = [], a = this.length, s = t || window; i < a; i++) r = this[i], e.call(s, r, i, this) && (n[n.length] = r);
                    return n
                },
                find: function(e, t) {
                    for (var r, i = 0, n = this.length, a = t || window; i < n; i++)
                        if (r = this[i], e.call(a, r, i, this)) return r
                },
                every: function(e, t) {
                    for (var r, i = 0, n = this.length, a = t || window; i < n; i++)
                        if (r = this[i], !e.call(a, r, i, this)) return !1;
                    return !0
                },
                some: function(e, t) {
                    for (var r, i = 0, n = this.length, a = t || window; i < n; i++)
                        if (r = this[i], e.call(a, r, i, this)) return !0;
                    return !1
                },
                remove: function(e) {
                    var t = this.indexOf(e);
                    t !== -1 && this.splice(t, 1)
                },
                empty: function() {
                    this.splice(0, this.length)
                }
            });
        "undefined" != typeof Symbol && Symbol.iterator && !Ke.prototype[Symbol.iterator] && (Ke.prototype[Symbol.iterator] = [][Symbol.iterator]), H = Ke.extend({
            init: function(e, t) {
                Se.fn.init.call(this), this.type = t || U;
                for (var r = 0; r < e.length; r++) this[r] = e[r];
                this.length = r, this._parent = he(function() {
                    return this
                }, this)
            },
            at: function(e) {
                var t = this[e];
                return t instanceof this.type ? t.parent = this._parent : t = this[e] = this.wrap(t, this._parent), t
            }
        }), U = Se.extend({
            init: function(e) {
                var t, r, i = this,
                    n = function() {
                        return i
                    };
                Se.fn.init.call(this), this._handlers = {};
                for (r in e) t = e[r], "object" == typeof t && t && !t.getTime && "_" != r.charAt(0) && (t = i.wrap(t, r, n)), i[r] = t;
                i.uid = me.guid()
            },
            shouldSerialize: function(e) {
                return this.hasOwnProperty(e) && "_handlers" !== e && "_events" !== e && typeof this[e] !== ke && "uid" !== e
            },
            forEach: function(e) {
                for (var t in this) this.shouldSerialize(t) && e(this[t], t)
            },
            toJSON: function() {
                var e, t, r = {};
                for (t in this) this.shouldSerialize(t) && (e = this[t], (e instanceof U || e instanceof Ke) && (e = e.toJSON()), r[t] = e);
                return r
            },
            get: function(e) {
                var t, r = this;
                return r.trigger(Oe, {
                    field: e
                }), t = "this" === e ? r : me.getter(e, !0)(r)
            },
            _set: function(e, t) {
                var r, i, n, a = this,
                    s = e.indexOf(".") >= 0;
                if (s)
                    for (r = e.split("."), i = ""; r.length > 1;) {
                        if (i += r.shift(), n = me.getter(i, !0)(a), n instanceof U) return n.set(r.join("."), t), s;
                        i += "."
                    }
                return me.setter(e)(a, t), s
            },
            set: function(e, t) {
                var r = this,
                    i = !1,
                    n = e.indexOf(".") >= 0,
                    a = me.getter(e, !0)(r);
                return a !== t && (a instanceof Se && this._handlers[e] && (this._handlers[e].get && a.unbind(Oe, this._handlers[e].get), a.unbind(De, this._handlers[e].change)), i = r.trigger("set", {
                    field: e,
                    value: t
                }), i || (n || (t = r.wrap(t, e, function() {
                    return r
                })), (!r._set(e, t) || e.indexOf("(") >= 0 || e.indexOf("[") >= 0) && r.trigger(De, {
                    field: e
                }))), i
            },
            parent: ve,
            wrap: function(e, t, i) {
                var n, a, s, o, u = this,
                    l = We.call(e);
                return null == e || "[object Object]" !== l && "[object Array]" !== l || (s = e instanceof Ke, o = e instanceof ie, "[object Object]" !== l || o || s ? ("[object Array]" === l || s || o) && (s || o || (e = new Ke(e)), a = r(u, De, t, !1), e.bind(De, a), u._handlers[t] = {
                    change: a
                }) : (e instanceof U || (e = new U(e)), n = r(u, Oe, t, !0), e.bind(Oe, n), a = r(u, De, t, !0), e.bind(De, a), u._handlers[t] = {
                    get: n,
                    change: a
                }), e.parent = i), e
            }
        }), J = {
            number: function(e) {
                return typeof e === we && "null" === e.toLowerCase() ? null : me.parseFloat(e)
            },
            date: function(e) {
                return typeof e === we && "null" === e.toLowerCase() ? null : me.parseDate(e)
            },
            "boolean": function(e) {
                return typeof e === we ? "null" === e.toLowerCase() ? null : "true" === e.toLowerCase() : null != e ? !!e : e
            },
            string: function(e) {
                return typeof e === we && "null" === e.toLowerCase() ? null : null != e ? e + "" : e
            },
            "default": function(e) {
                return e
            }
        }, V = {
            string: "",
            number: 0,
            date: new Date,
            "boolean": !1,
            "default": ""
        }, W = U.extend({
            init: function(r) {
                var i, n, a = this;
                if ((!r || e.isEmptyObject(r)) && (r = e.extend({}, a.defaults, r), a._initializers))
                    for (i = 0; i < a._initializers.length; i++) n = a._initializers[i], r[n] = a.defaults[n]();
                U.fn.init.call(a, r), a.dirty = !1, a.dirtyFields = {}, a.idField && (a.id = a.get(a.idField), a.id === t && (a.id = a._defaultId))
            },
            shouldSerialize: function(e) {
                return U.fn.shouldSerialize.call(this, e) && "uid" !== e && !("id" !== this.idField && "id" === e) && "dirty" !== e && "dirtyFields" !== e && "_accessors" !== e
            },
            _parse: function(e, t) {
                var r, i = this,
                    a = e,
                    s = i.fields || {};
                return e = s[e], e || (e = n(s, a)), e && (r = e.parse, !r && e.type && (r = J[e.type.toLowerCase()])), r ? r(t) : t
            },
            _notifyChange: function(e) {
                var t = e.action;
                "add" != t && "remove" != t || (this.dirty = !0, this.dirtyFields[e.field] = !0)
            },
            editable: function(e) {
                return e = (this.fields || {})[e], !e || e.editable !== !1
            },
            set: function(e, t, r) {
                var n = this,
                    a = n.dirty;
                n.editable(e) && (t = n._parse(e, t), i(t, n.get(e)) ? n.trigger("equalSet", {
                    field: e,
                    value: t
                }) : (n.dirty = !0, n.dirtyFields[e] = !0, U.fn.set.call(n, e, t, r) && !a && (n.dirty = a, n.dirty || (n.dirtyFields[e] = !1))))
            },
            accept: function(e) {
                var t, r, i = this,
                    n = function() {
                        return i
                    };
                for (t in e) r = e[t], "_" != t.charAt(0) && (r = i.wrap(e[t], t, n)), i._set(t, r);
                i.idField && (i.id = i.get(i.idField)), i.dirty = !1, i.dirtyFields = {}
            },
            isNew: function() {
                return this.id === this._defaultId
            }
        }), W.define = function(e, r) {
            r === t && (r = e, e = W);
            var i, n, a, s, o, u, l, h, d = le({
                    defaults: {}
                }, r),
                f = {},
                c = d.id,
                g = [];
            if (c && (d.idField = c), d.id && delete d.id, c && (d.defaults[c] = d._defaultId = ""), "[object Array]" === We.call(d.fields)) {
                for (u = 0, l = d.fields.length; u < l; u++) a = d.fields[u], typeof a === we ? f[a] = {} : a.field && (f[a.field] = a);
                d.fields = f
            }
            for (n in d.fields) a = d.fields[n], s = a.type || "default", o = null, h = n, n = typeof a.field === we ? a.field : n, a.nullable || (o = d.defaults[h !== n ? h : n] = a.defaultValue !== t ? a.defaultValue : V[s.toLowerCase()], "function" == typeof o && g.push(n)), r.id === n && (d._defaultId = o), d.defaults[h !== n ? h : n] = o, a.parse = a.parse || J[s];
            return g.length > 0 && (d._initializers = g), i = e.extend(d), i.define = function(e) {
                return W.define(i, e)
            }, d.fields && (i.fields = d.fields, i.idField = d.idField), i
        }, Q = {
            selector: function(e) {
                return ye(e) ? e : Ne(e)
            },
            compare: function(e) {
                var t = this.selector(e);
                return function(e, r) {
                    return e = t(e), r = t(r), null == e && null == r ? 0 : null == e ? -1 : null == r ? 1 : e.localeCompare ? e.localeCompare(r) : e > r ? 1 : e < r ? -1 : 0
                }
            },
            create: function(e) {
                var t = e.compare || this.compare(e.field);
                return "desc" == e.dir ? function(e, r) {
                    return t(r, e, !0)
                } : t
            },
            combine: function(e) {
                return function(t, r) {
                    var i, n, a = e[0](t, r);
                    for (i = 1, n = e.length; i < n; i++) a = a || e[i](t, r);
                    return a
                }
            }
        }, $ = le({}, Q, {
            asc: function(e) {
                var t = this.selector(e);
                return function(e, r) {
                    var i = t(e),
                        n = t(r);
                    return i && i.getTime && n && n.getTime && (i = i.getTime(), n = n.getTime()), i === n ? e.__position - r.__position : null == i ? -1 : null == n ? 1 : i.localeCompare ? i.localeCompare(n) : i > n ? 1 : -1
                }
            },
            desc: function(e) {
                var t = this.selector(e);
                return function(e, r) {
                    var i = t(e),
                        n = t(r);
                    return i && i.getTime && n && n.getTime && (i = i.getTime(), n = n.getTime()), i === n ? e.__position - r.__position : null == i ? 1 : null == n ? -1 : n.localeCompare ? n.localeCompare(i) : i < n ? 1 : -1
                }
            },
            create: function(e) {
                return this[e.dir](e.field)
            }
        }), L = function(e, t) {
            var r, i = e.length,
                n = Array(i);
            for (r = 0; r < i; r++) n[r] = t(e[r], r, e);
            return n
        }, K = function() {
            function e(e) {
                return "string" == typeof e && (e = e.replace(/[\r\n]+/g, "")), JSON.stringify(e)
            }

            function t(t) {
                return function(r, i, n) {
                    return i += "", n && (r = "(" + r + " || '').toLowerCase()", i = i.toLowerCase()), t(r, e(i), n)
                }
            }

            function r(t, r, i, n) {
                if (null != i) {
                    if (typeof i === we) {
                        var a = $e.exec(i);
                        a ? i = new Date((+a[1])) : n ? (i = e(i.toLowerCase()), r = "((" + r + " || '')+'').toLowerCase()") : i = e(i)
                    }
                    i.getTime && (r = "(" + r + "&&" + r + ".getTime?" + r + ".getTime():" + r + ")", i = i.getTime())
                }
                return r + " " + t + " " + i
            }

            function i(e) {
                var t, r, i, n;
                for (t = "/^", r = !1, i = 0; i < e.length; ++i) {
                    if (n = e.charAt(i), r) t += "\\" + n;
                    else {
                        if ("~" == n) {
                            r = !0;
                            continue
                        }
                        t += "*" == n ? ".*" : "?" == n ? "." : ".+^$()[]{}|\\/\n\r\u2028\u2029Â ".indexOf(n) >= 0 ? "\\" + n : n
                    }
                    r = !1
                }
                return t + "$/"
            }
            return {
                quote: function(t) {
                    return t && t.getTime ? "new Date(" + t.getTime() + ")" : e(t)
                },
                eq: function(e, t, i) {
                    return r("==", e, t, i)
                },
                neq: function(e, t, i) {
                    return r("!=", e, t, i)
                },
                gt: function(e, t, i) {
                    return r(">", e, t, i)
                },
                gte: function(e, t, i) {
                    return r(">=", e, t, i)
                },
                lt: function(e, t, i) {
                    return r("<", e, t, i)
                },
                lte: function(e, t, i) {
                    return r("<=", e, t, i)
                },
                startswith: t(function(e, t) {
                    return e + ".lastIndexOf(" + t + ", 0) == 0"
                }),
                doesnotstartwith: t(function(e, t) {
                    return e + ".lastIndexOf(" + t + ", 0) == -1"
                }),
                endswith: t(function(e, t) {
                    var r = t ? t.length - 2 : 0;
                    return e + ".indexOf(" + t + ", " + e + ".length - " + r + ") >= 0"
                }),
                doesnotendwith: t(function(e, t) {
                    var r = t ? t.length - 2 : 0;
                    return e + ".indexOf(" + t + ", " + e + ".length - " + r + ") < 0"
                }),
                contains: t(function(e, t) {
                    return e + ".indexOf(" + t + ") >= 0"
                }),
                doesnotcontain: t(function(e, t) {
                    return e + ".indexOf(" + t + ") == -1"
                }),
                matches: t(function(e, t) {
                    return t = t.substring(1, t.length - 1), i(t) + ".test(" + e + ")"
                }),
                doesnotmatch: t(function(e, t) {
                    return t = t.substring(1, t.length - 1), "!" + i(t) + ".test(" + e + ")"
                }),
                isempty: function(e) {
                    return e + " === ''"
                },
                isnotempty: function(e) {
                    return e + " !== ''"
                },
                isnull: function(e) {
                    return "(" + e + " == null)"
                },
                isnotnull: function(e) {
                    return "(" + e + " != null)"
                },
                isnullorempty: function(e) {
                    return "(" + e + " === null) || (" + e + " === '')"
                },
                isnotnullorempty: function(e) {
                    return "(" + e + " !== null) && (" + e + " !== '')"
                }
            }
        }(), a.filterExpr = function(e) {
            var r, i, n, s, o, u, l = [],
                h = {
                    and: " && ",
                    or: " || "
                },
                d = [],
                f = [],
                c = e.filters;
            for (r = 0, i = c.length; r < i; r++) n = c[r], o = n.field, u = n.operator, n.filters ? (s = a.filterExpr(n), n = s.expression.replace(/__o\[(\d+)\]/g, function(e, t) {
                return t = +t, "__o[" + (f.length + t) + "]"
            }).replace(/__f\[(\d+)\]/g, function(e, t) {
                return t = +t, "__f[" + (d.length + t) + "]"
            }), f.push.apply(f, s.operators), d.push.apply(d, s.fields)) : (typeof o === ke ? (s = "__f[" + d.length + "](d)", d.push(o)) : s = me.expr(o), typeof u === ke ? (n = "__o[" + f.length + "](" + s + ", " + K.quote(n.value) + ")", f.push(u)) : n = K[(u || "eq").toLowerCase()](s, n.value, n.ignoreCase === t || n.ignoreCase)), l.push(n);
            return {
                expression: "(" + l.join(h[e.logic]) + ")",
                fields: d,
                operators: f
            }
        }, X = {
            "==": "eq",
            equals: "eq",
            isequalto: "eq",
            equalto: "eq",
            equal: "eq",
            "!=": "neq",
            ne: "neq",
            notequals: "neq",
            isnotequalto: "neq",
            notequalto: "neq",
            notequal: "neq",
            "<": "lt",
            islessthan: "lt",
            lessthan: "lt",
            less: "lt",
            "<=": "lte",
            le: "lte",
            islessthanorequalto: "lte",
            lessthanequal: "lte",
            ">": "gt",
            isgreaterthan: "gt",
            greaterthan: "gt",
            greater: "gt",
            ">=": "gte",
            isgreaterthanorequalto: "gte",
            greaterthanequal: "gte",
            ge: "gte",
            notsubstringof: "doesnotcontain",
            isnull: "isnull",
            isempty: "isempty",
            isnotempty: "isnotempty"
        }, a.normalizeFilter = u, a.compareFilters = f, a.prototype = {
            toArray: function() {
                return this.data
            },
            range: function(e, t) {
                return new a(this.data.slice(e, e + t))
            },
            skip: function(e) {
                return new a(this.data.slice(e))
            },
            take: function(e) {
                return new a(this.data.slice(0, e))
            },
            select: function(e) {
                return new a(L(this.data, e))
            },
            order: function(e, t, r) {
                var i = {
                    dir: t
                };
                return e && (e.compare ? i.compare = e.compare : i.field = e), new a(r ? this.data.sort(Q.create(i)) : this.data.slice(0).sort(Q.create(i)))
            },
            orderBy: function(e, t) {
                return this.order(e, "asc", t)
            },
            orderByDescending: function(e, t) {
                return this.order(e, "desc", t)
            },
            sort: function(e, t, r, i) {
                var n, a, o = s(e, t),
                    u = [];
                if (r = r || Q, o.length) {
                    for (n = 0, a = o.length; n < a; n++) u.push(r.create(o[n]));
                    return this.orderBy({
                        compare: r.combine(u)
                    }, i)
                }
                return this
            },
            filter: function(e) {
                var t, r, i, n, s, o, l, h, d = this.data,
                    f = [];
                if (e = u(e), !e || 0 === e.filters.length) return this;
                for (n = a.filterExpr(e), o = n.fields, l = n.operators, s = h = Function("d, __f, __o", "return " + n.expression), (o.length || l.length) && (h = function(e) {
                        return s(e, o, l)
                    }), t = 0, i = d.length; t < i; t++) r = d[t], h(r) && f.push(r);
                return new a(f)
            },
            group: function(e, t) {
                e = g(e || []), t = t || this.data;
                var r, i = this,
                    n = new a(i.data);
                return e.length > 0 && (r = e[0], n = n.groupBy(r).select(function(i) {
                    var n = new a(t).filter([{
                        field: i.field,
                        operator: "eq",
                        value: i.value,
                        ignoreCase: !1
                    }]);
                    return {
                        field: i.field,
                        value: i.value,
                        items: e.length > 1 ? new a(i.items).group(e.slice(1), n.toArray()).toArray() : i.items,
                        hasSubgroups: e.length > 1,
                        aggregates: n.aggregate(r.aggregates)
                    }
                })), n
            },
            groupBy: function(e) {
                if (fe(e) || !this.data.length) return new a([]);
                var t, r, i, n, s = e.field,
                    o = this._sortForGrouping(s, e.dir || "asc"),
                    u = me.accessor(s),
                    l = u.get(o[0], s),
                    h = {
                        field: s,
                        value: l,
                        items: []
                    },
                    d = [h];
                for (i = 0, n = o.length; i < n; i++) t = o[i], r = u.get(t, s), p(l, r) || (l = r, h = {
                    field: s,
                    value: l,
                    items: []
                }, d.push(h)), h.items.push(t);
                return new a(d)
            },
            _sortForGrouping: function(e, t) {
                var r, i, n = this.data;
                if (!Qe) {
                    for (r = 0, i = n.length; r < i; r++) n[r].__position = r;
                    for (n = new a(n).sort(e, t, $).toArray(), r = 0, i = n.length; r < i; r++) delete n[r].__position;
                    return n
                }
                return this.sort(e, t).toArray()
            },
            aggregate: function(e) {
                var t, r, i = {},
                    n = {};
                if (e && e.length)
                    for (t = 0, r = this.data.length; t < r; t++) _(i, e, this.data[t], t, r, n);
                return i
            }
        }, Y = {
            sum: function(e, t, r) {
                var i = r.get(t);
                return v(e) ? v(i) && (e += i) : e = i, e
            },
            count: function(e) {
                return (e || 0) + 1
            },
            average: function(e, r, i, n, a, s) {
                var o = i.get(r);
                return s.count === t && (s.count = 0), v(e) ? v(o) && (e += o) : e = o, v(o) && s.count++, n == a - 1 && v(e) && (e /= s.count), e
            },
            max: function(e, t, r) {
                var i = r.get(t);
                return v(e) || m(e) || (e = i), e < i && (v(i) || m(i)) && (e = i), e
            },
            min: function(e, t, r) {
                var i = r.get(t);
                return v(e) || m(e) || (e = i), e > i && (v(i) || m(i)) && (e = i), e
            }
        }, a.process = function(e, r, i) {
            r = r || {};
            var n, o = new a(e),
                u = r.group,
                l = g(u || []).concat(s(r.sort || [])),
                h = r.filterCallback,
                d = r.filter,
                f = r.skip,
                c = r.take;
            return d && (o = o.filter(d), h && (o = h(o)), n = o.toArray().length), l && (o = i ? o.sort(l, t, t, i) : o.sort(l), u && (e = o.toArray())), f !== t && c !== t && (o = o.range(f, c)), u && (o = o.group(u, e)), {
                total: n,
                data: o.toArray()
            }
        }, Z = be.extend({
            init: function(e) {
                this.data = e.data
            },
            read: function(e) {
                e.success(this.data)
            },
            update: function(e) {
                e.success(e.data)
            },
            create: function(e) {
                e.success(e.data)
            },
            destroy: function(e) {
                e.success(e.data)
            }
        }), ee = be.extend({
            init: function(e) {
                var t, r = this;
                e = r.options = le({}, r.options, e), _e(Ie, function(t, r) {
                    typeof e[r] === we && (e[r] = {
                        url: e[r]
                    })
                }), r.cache = e.cache ? te.create(e.cache) : {
                    find: ve,
                    add: ve
                }, t = e.parameterMap, r.submit = e.submit, ye(e.push) && (r.push = e.push), r.push || (r.push = Me), r.parameterMap = ye(t) ? t : function(e) {
                    var r = {};
                    return _e(e, function(e, i) {
                        e in t && (e = t[e], de(e) && (i = e.value(i), e = e.key)), r[e] = i
                    }), r
                }
            },
            options: {
                parameterMap: Me
            },
            create: function(e) {
                return pe(this.setup(e, xe))
            },
            read: function(r) {
                var i, n, a, s = this,
                    o = s.cache;
                r = s.setup(r, qe), i = r.success || ve, n = r.error || ve, a = o.find(r.data), a !== t ? i(a) : (r.success = function(e) {
                    o.add(r.data, e), i(e)
                }, e.ajax(r))
            },
            update: function(e) {
                return pe(this.setup(e, Fe))
            },
            destroy: function(e) {
                return pe(this.setup(e, Re))
            },
            setup: function(e, t) {
                e = e || {};
                var r, i = this,
                    n = i.options[t],
                    a = ye(n.data) ? n.data(e.data) : n.data;
                return e = le(!0, {}, n, e), r = le(!0, {}, a, e.data), e.data = i.parameterMap(r, t), ye(e.url) && (e.url = e.url(r)), e
            }
        }), te = be.extend({
            init: function() {
                this._store = {}
            },
            add: function(e, r) {
                e !== t && (this._store[je(e)] = r)
            },
            find: function(e) {
                return this._store[je(e)]
            },
            clear: function() {
                this._store = {}
            },
            remove: function(e) {
                delete this._store[je(e)]
            }
        }), te.create = function(e) {
            var t = {
                inmemory: function() {
                    return new te
                }
            };
            return de(e) && ye(e.find) ? e : e === !0 ? new te : t[e]()
        }, re = be.extend({
            init: function(e) {
                var t, r, i, n, a, s, o, u, l, h, d, f, c, g = this;
                e = e || {};
                for (t in e) r = e[t], g[t] = typeof r === we ? Ne(r) : r;
                n = e.modelBase || W, de(g.model) && (g.model = i = n.define(g.model)), a = he(g.data, g), g._dataAccessFunction = a, g.model && (s = he(g.groups, g), o = he(g.serialize, g), u = {}, l = {}, h = {}, d = {}, f = !1, i = g.model, i.fields && (_e(i.fields, function(e, t) {
                    var r;
                    c = e, de(t) && t.field ? c = t.field : typeof t === we && (c = t), de(t) && t.from && (r = t.from), f = f || r && r !== e || c !== e, l[e] = Ne(r || c), h[e] = Ne(e), u[r || c] = e, d[e] = r || c
                }), !e.serialize && f && (g.serialize = k(o, i, S, h, u, d))), g._dataAccessFunction = a, g.data = k(a, i, b, l, u, d), g.groups = k(s, i, w, l, u, d))
            },
            errors: function(e) {
                return e ? e.errors : null
            },
            parse: Me,
            data: Me,
            total: function(e) {
                return e.length
            },
            groups: Me,
            aggregates: function() {
                return {}
            },
            serialize: function(e) {
                return e
            }
        }), ie = Se.extend({
            init: function(e) {
                var r, i, n, a = this;
                e && (i = e.data), e = a.options = le({}, a.options, e), a._map = {}, a._prefetch = {}, a._data = [], a._pristineData = [], a._ranges = [], a._view = [], a._pristineTotal = 0, a._destroyed = [], a._pageSize = e.pageSize, a._page = e.page || (e.pageSize ? 1 : t), a._sort = s(e.sort), a._filter = u(e.filter), a._group = g(e.group), a._aggregate = e.aggregate, a._total = e.total, a._shouldDetachObservableParents = !0, Se.fn.init.call(a), a.transport = ne.create(e, i, a), ye(a.transport.push) && a.transport.push({
                    pushCreate: he(a._pushCreate, a),
                    pushUpdate: he(a._pushUpdate, a),
                    pushDestroy: he(a._pushDestroy, a)
                }), null != e.offlineStorage && ("string" == typeof e.offlineStorage ? (n = e.offlineStorage, a._storage = {
                    getItem: function() {
                        return JSON.parse(localStorage.getItem(n))
                    },
                    setItem: function(e) {
                        localStorage.setItem(n, je(a.reader.serialize(e)))
                    }
                }) : a._storage = e.offlineStorage), a.reader = new me.data.readers[e.schema.type || "json"](e.schema), r = a.reader.model || {}, a._detachObservableParents(), a._data = a._observe(a._data), a._online = !0, a.bind(["push", Pe, De, Ce, ze, Ae, Te], e)
            },
            options: {
                data: null,
                schema: {
                    modelBase: W
                },
                offlineStorage: null,
                serverSorting: !1,
                serverPaging: !1,
                serverFiltering: !1,
                serverGrouping: !1,
                serverAggregates: !1,
                batch: !1,
                inPlaceSort: !1
            },
            clone: function() {
                return this
            },
            online: function(r) {
                return r !== t ? this._online != r && (this._online = r, r) ? this.sync() : e.Deferred().resolve().promise() : this._online
            },
            offlineData: function(e) {
                return null == this.options.offlineStorage ? null : e !== t ? this._storage.setItem(e) : this._storage.getItem() || []
            },
            _isServerGrouped: function() {
                var e = this.group() || [];
                return this.options.serverGrouping && e.length
            },
            _pushCreate: function(e) {
                this._push(e, "pushCreate")
            },
            _pushUpdate: function(e) {
                this._push(e, "pushUpdate")
            },
            _pushDestroy: function(e) {
                this._push(e, "pushDestroy")
            },
            _push: function(e, t) {
                var r = this._readData(e);
                r || (r = e), this[t](r)
            },
            _flatData: function(e, t) {
                if (e) {
                    if (this._isServerGrouped()) return q(e);
                    if (!t)
                        for (var r = 0; r < e.length; r++) e.at(r)
                }
                return e
            },
            parent: ve,
            get: function(e) {
                var t, r, i = this._flatData(this._data, this.options.useRanges);
                for (t = 0, r = i.length; t < r; t++)
                    if (i[t].id == e) return i[t]
            },
            getByUid: function(e) {
                return this._getByUid(e, this._data)
            },
            _getByUid: function(e, t) {
                var r, i, n = this._flatData(t, this.options.useRanges);
                if (n)
                    for (r = 0, i = n.length; r < i; r++)
                        if (n[r].uid == e) return n[r]
            },
            indexOf: function(e) {
                return T(this._data, e)
            },
            at: function(e) {
                return this._data.at(e)
            },
            data: function(e) {
                var r, i = this;
                if (e === t) {
                    if (i._data)
                        for (r = 0; r < i._data.length; r++) i._data.at(r);
                    return i._data
                }
                i._detachObservableParents(), i._data = this._observe(e), i._pristineData = e.slice(0), i._storeData(), i._ranges = [], i.trigger("reset"), i._addRange(i._data), i._total = i._data.length, i._pristineTotal = i._total, i._process(i._data)
            },
            view: function(e) {
                return e === t ? this._view : (this._view = this._observeView(e), t)
            },
            _observeView: function(e) {
                var t, r = this;
                return O(e, r._data, r._ranges, r.reader.model || U, r._isServerGrouped()), t = new H(e, r.reader.model), t.parent = function() {
                    return r.parent()
                }, t
            },
            flatView: function() {
                var e = this.group() || [];
                return e.length ? q(this._view) : this._view
            },
            add: function(e) {
                return this.insert(this._data.length, e)
            },
            _createNewModel: function(e) {
                return this.reader.model ? new this.reader.model(e) : e instanceof U ? e : new U(e)
            },
            insert: function(e, t) {
                return t || (t = e, e = 0), t instanceof W || (t = this._createNewModel(t)), this._isServerGrouped() ? this._data.splice(e, 0, this._wrapInEmptyGroup(t)) : this._data.splice(e, 0, t), this._insertModelInRange(e, t), t
            },
            pushInsert: function(e, t) {
                var r, i, n, a, s, o;
                t || (t = e, e = 0), ce(t) || (t = [t]), r = [], i = this.options.autoSync, this.options.autoSync = !1;
                try {
                    for (n = 0; n < t.length; n++) a = t[n], s = this.insert(e, a), r.push(s), o = s.toJSON(), this._isServerGrouped() && (o = this._wrapInEmptyGroup(o)), this._pristineData.push(o), e++
                } finally {
                    this.options.autoSync = i
                }
                r.length && this.trigger("push", {
                    type: "create",
                    items: r
                })
            },
            pushCreate: function(e) {
                this.pushInsert(this._data.length, e)
            },
            pushUpdate: function(e) {
                var t, r, i, n, a;
                for (ce(e) || (e = [e]), t = [], r = 0; r < e.length; r++) i = e[r], n = this._createNewModel(i), a = this.get(n.id), a ? (t.push(a), a.accept(i), a.trigger(De), this._updatePristineForModel(a, i)) : this.pushCreate(i);
                t.length && this.trigger("push", {
                    type: "update",
                    items: t
                })
            },
            pushDestroy: function(e) {
                var t = this._removeItems(e);
                t.length && this.trigger("push", {
                    type: "destroy",
                    items: t
                })
            },
            _removeItems: function(e) {
                var t, r, i, n, a, s;
                ce(e) || (e = [e]), t = [], r = this.options.autoSync, this.options.autoSync = !1;
                try {
                    for (i = 0; i < e.length; i++) n = e[i], a = this._createNewModel(n), s = !1, this._eachItem(this._data, function(e) {
                        var r, i;
                        for (r = 0; r < e.length; r++)
                            if (i = e.at(r), i.id === a.id) {
                                t.push(i), e.splice(r, 1), s = !0;
                                break
                            }
                    }), s && (this._removePristineForModel(a), this._destroyed.pop())
                } finally {
                    this.options.autoSync = r
                }
                return t
            },
            remove: function(e) {
                var t, r = this,
                    i = r._isServerGrouped();
                return this._eachItem(r._data, function(n) {
                    if (t = P(n, e), t && i) return t.isNew && t.isNew() || r._destroyed.push(t), !0
                }), this._removeModelFromRanges(e), e
            },
            destroyed: function() {
                return this._destroyed
            },
            created: function() {
                var e, t, r = [],
                    i = this._flatData(this._data, this.options.useRanges);
                for (e = 0, t = i.length; e < t; e++) i[e].isNew && i[e].isNew() && r.push(i[e]);
                return r
            },
            updated: function() {
                var e, t, r = [],
                    i = this._flatData(this._data, this.options.useRanges);
                for (e = 0, t = i.length; e < t; e++) i[e].isNew && !i[e].isNew() && i[e].dirty && r.push(i[e]);
                return r
            },
            sync: function() {
                var t, r = this,
                    i = [],
                    n = [],
                    a = r._destroyed,
                    s = e.Deferred().resolve().promise();
                if (r.online()) {
                    if (!r.reader.model) return s;
                    i = r.created(), n = r.updated(), t = [], r.options.batch && r.transport.submit ? t = r._sendSubmit(i, n, a) : (t.push.apply(t, r._send("create", i)), t.push.apply(t, r._send("update", n)), t.push.apply(t, r._send("destroy", a))), s = e.when.apply(null, t).then(function() {
                        var e, t;
                        for (e = 0, t = arguments.length; e < t; e++) arguments[e] && r._accept(arguments[e]);
                        r._storeData(!0), r._change({
                            action: "sync"
                        }), r.trigger(ze)
                    })
                } else r._storeData(!0), r._change({
                    action: "sync"
                });
                return s
            },
            cancelChanges: function(e) {
                var t = this;
                e instanceof me.data.Model ? t._cancelModel(e) : (t._destroyed = [], t._detachObservableParents(), t._data = t._observe(t._pristineData), t.options.serverPaging && (t._total = t._pristineTotal), t._ranges = [], t._addRange(t._data, 0), t._change(), t._markOfflineUpdatesAsDirty())
            },
            _markOfflineUpdatesAsDirty: function() {
                var e = this;
                null != e.options.offlineStorage && e._eachItem(e._data, function(e) {
                    var t, r;
                    for (t = 0; t < e.length; t++) r = e.at(t), "update" != r.__state__ && "create" != r.__state__ || (r.dirty = !0)
                })
            },
            hasChanges: function() {
                var e, t, r = this._flatData(this._data, this.options.useRanges);
                if (this._destroyed.length) return !0;
                for (e = 0, t = r.length; e < t; e++)
                    if (r[e].isNew && r[e].isNew() || r[e].dirty) return !0;
                return !1
            },
            _accept: function(t) {
                var r, i = this,
                    n = t.models,
                    a = t.response,
                    s = 0,
                    o = i._isServerGrouped(),
                    u = i._pristineData,
                    l = t.type;
                if (i.trigger(Ae, {
                        response: a,
                        type: l
                    }), a && !fe(a)) {
                    if (a = i.reader.parse(a), i._handleCustomErrors(a)) return;
                    a = i.reader.data(a), ce(a) || (a = [a])
                } else a = e.map(n, function(e) {
                    return e.toJSON()
                });
                for ("destroy" === l && (i._destroyed = []), s = 0, r = n.length; s < r; s++) "destroy" !== l ? (n[s].accept(a[s]), "create" === l ? u.push(o ? i._wrapInEmptyGroup(n[s]) : a[s]) : "update" === l && i._updatePristineForModel(n[s], a[s])) : i._removePristineForModel(n[s])
            },
            _updatePristineForModel: function(e, t) {
                this._executeOnPristineForModel(e, function(e, r) {
                    me.deepExtend(r[e], t)
                })
            },
            _executeOnPristineForModel: function(e, t) {
                this._eachPristineItem(function(r) {
                    var i = C(r, e);
                    if (i > -1) return t(i, r), !0
                })
            },
            _removePristineForModel: function(e) {
                this._executeOnPristineForModel(e, function(e, t) {
                    t.splice(e, 1)
                })
            },
            _readData: function(e) {
                var t = this._isServerGrouped() ? this.reader.groups : this.reader.data;
                return t.call(this.reader, e)
            },
            _eachPristineItem: function(e) {
                this._eachItem(this._pristineData, e)
            },
            _eachItem: function(e, t) {
                e && e.length && (this._isServerGrouped() ? R(e, t) : t(e))
            },
            _pristineForModel: function(e) {
                var t, r, i = function(i) {
                    if (r = C(i, e), r > -1) return t = i[r], !0
                };
                return this._eachPristineItem(i), t
            },
            _cancelModel: function(e) {
                var t = this,
                    r = this._pristineForModel(e);
                this._eachItem(this._data, function(i) {
                    var n = T(i, e);
                    n >= 0 && (!r || e.isNew() && !r.__state__ ? (i.splice(n, 1), t._removeModelFromRanges(e)) : (i[n].accept(r), "update" == r.__state__ && (i[n].dirty = !0)))
                })
            },
            _submit: function(t, r) {
                var i = this;
                i.trigger(Ce, {
                    type: "submit"
                }), i.trigger(Te), i.transport.submit(le({
                    success: function(r, i) {
                        var n = e.grep(t, function(e) {
                            return e.type == i
                        })[0];
                        n && n.resolve({
                            response: r,
                            models: n.models,
                            type: i
                        })
                    },
                    error: function(e, r, n) {
                        for (var a = 0; a < t.length; a++) t[a].reject(e);
                        i.error(e, r, n)
                    }
                }, r))
            },
            _sendSubmit: function(t, r, i) {
                var n = this,
                    a = [];
                return n.options.batch && (t.length && a.push(e.Deferred(function(e) {
                    e.type = "create", e.models = t
                })), r.length && a.push(e.Deferred(function(e) {
                    e.type = "update", e.models = r
                })), i.length && a.push(e.Deferred(function(e) {
                    e.type = "destroy", e.models = i
                })), n._submit(a, {
                    data: {
                        created: n.reader.serialize(y(t)),
                        updated: n.reader.serialize(y(r)),
                        destroyed: n.reader.serialize(y(i))
                    }
                })), a
            },
            _promise: function(t, r, i) {
                var n = this;
                return e.Deferred(function(e) {
                    n.trigger(Ce, {
                        type: i
                    }), n.trigger(Te), n.transport[i].call(n.transport, le({
                        success: function(t) {
                            e.resolve({
                                response: t,
                                models: r,
                                type: i
                            })
                        },
                        error: function(t, r, i) {
                            e.reject(t), n.error(t, r, i)
                        }
                    }, t))
                }).promise()
            },
            _send: function(e, t) {
                var r, i, n = this,
                    a = [],
                    s = n.reader.serialize(y(t));
                if (n.options.batch) t.length && a.push(n._promise({
                    data: {
                        models: s
                    }
                }, t, e));
                else
                    for (r = 0, i = t.length; r < i; r++) a.push(n._promise({
                        data: s[r]
                    }, [t[r]], e));
                return a
            },
            read: function(t) {
                var r = this,
                    i = r._params(t),
                    n = e.Deferred();
                return r._queueRequest(i, function() {
                    var e = r.trigger(Ce, {
                        type: "read"
                    });
                    e ? (r._dequeueRequest(), n.resolve(e)) : (r.trigger(Te), r._ranges = [], r.trigger("reset"), r.online() ? r.transport.read({
                        data: i,
                        success: function(e) {
                            r._ranges = [], r.success(e, i), n.resolve()
                        },
                        error: function() {
                            var e = Je.call(arguments);
                            r.error.apply(r, e), n.reject.apply(n, e)
                        }
                    }) : null != r.options.offlineStorage && (r.success(r.offlineData(), i), n.resolve()))
                }), n.promise()
            },
            _readAggregates: function(e) {
                return this.reader.aggregates(e)
            },
            success: function(e) {
                var r, i, n, a, s, o, u, l, h, d, f = this,
                    c = f.options;
                if (f.trigger(Ae, {
                        response: e,
                        type: "read"
                    }), f.online()) {
                    if (e = f.reader.parse(e), f._handleCustomErrors(e)) return f._dequeueRequest(), t;
                    f._total = f.reader.total(e), f._pageSize > f._total && (f._pageSize = f._total), f._aggregate && c.serverAggregates && (f._aggregateResult = f._readAggregates(e)), r = arguments.length > 1 ? arguments[1] : t, e = f._readData(e, r), f._destroyed = []
                } else {
                    for (e = f._readData(e), i = [], n = {}, a = f.reader.model, s = a ? a.idField : "id", o = 0; o < this._destroyed.length; o++) u = this._destroyed[o][s], n[u] = u;
                    for (o = 0; o < e.length; o++) l = e[o], h = l.__state__, "destroy" == h ? n[l[s]] || this._destroyed.push(this._createNewModel(l)) : i.push(l);
                    e = i, f._total = e.length
                }
                if (f._pristineTotal = f._total, f._pristineData = e.slice(0), f._detachObservableParents(), f.options.endless) {
                    for (f._data.unbind(De, f._changeHandler), e = f._observe(e), d = 0; d < e.length; d++) f._data.push(e[d]);
                    f._data.bind(De, f._changeHandler)
                } else f._data = f._observe(e);
                f._markOfflineUpdatesAsDirty(), f._storeData(), f._addRange(f._data), f._process(f._data), f._dequeueRequest()
            },
            _detachObservableParents: function() {
                if (this._data && this._shouldDetachObservableParents)
                    for (var e = 0; e < this._data.length; e++) this._data[e].parent && (this._data[e].parent = ve)
            },
            _storeData: function(e) {
                function t(e) {
                    var r, i, n, a = [];
                    for (r = 0; r < e.length; r++) i = e.at(r), n = i.toJSON(), s && i.items ? n.items = t(i.items) : (n.uid = i.uid, o && (i.isNew() ? n.__state__ = "create" : i.dirty && (n.__state__ = "update"))), a.push(n);
                    return a
                }
                var r, i, n, a, s = this._isServerGrouped(),
                    o = this.reader.model;
                if (null != this.options.offlineStorage) {
                    for (r = t(this._data), i = [], n = 0; n < this._destroyed.length; n++) a = this._destroyed[n].toJSON(), a.__state__ = "destroy", i.push(a);
                    this.offlineData(r.concat(i)), e && (this._pristineData = this._readData(r))
                }
            },
            _addRange: function(e, r) {
                var i = this,
                    n = t !== r ? r : i._skip || 0,
                    a = n + i._flatData(e, !0).length;
                i._ranges.push({
                    start: n,
                    end: a,
                    data: e,
                    timestamp: (new Date).getTime()
                }), i._ranges.sort(function(e, t) {
                    return e.start - t.start
                })
            },
            error: function(e, t, r) {
                this._dequeueRequest(), this.trigger(Ae, {}), this.trigger(Pe, {
                    xhr: e,
                    status: t,
                    errorThrown: r
                })
            },
            _params: function(e) {
                var t = this,
                    r = le({
                        take: t.take(),
                        skip: t.skip(),
                        page: t.page(),
                        pageSize: t.pageSize(),
                        sort: t._sort,
                        filter: t._filter,
                        group: t._group,
                        aggregate: t._aggregate
                    }, e);
                return t.options.serverPaging || (delete r.take, delete r.skip, delete r.page, delete r.pageSize), t.options.serverGrouping ? t.reader.model && r.group && (r.group = N(r.group, t.reader.model)) : delete r.group, t.options.serverFiltering ? t.reader.model && r.filter && (r.filter = M(r.filter, t.reader.model)) : delete r.filter, t.options.serverSorting ? t.reader.model && r.sort && (r.sort = N(r.sort, t.reader.model)) : delete r.sort, t.options.serverAggregates ? t.reader.model && r.aggregate && (r.aggregate = N(r.aggregate, t.reader.model)) : delete r.aggregate, r
            },
            _queueRequest: function(e, r) {
                var i = this;
                i._requestInProgress ? i._pending = {
                    callback: he(r, i),
                    options: e
                } : (i._requestInProgress = !0, i._pending = t, r())
            },
            _dequeueRequest: function() {
                var e = this;
                e._requestInProgress = !1, e._pending && e._queueRequest(e._pending.options, e._pending.callback)
            },
            _handleCustomErrors: function(e) {
                if (this.reader.errors) {
                    var t = this.reader.errors(e);
                    if (t) return this.trigger(Pe, {
                        xhr: null,
                        status: "customerror",
                        errorThrown: "custom error",
                        errors: t
                    }), !0
                }
                return !1
            },
            _shouldWrap: function(e) {
                var t = this.reader.model;
                return !(!t || !e.length) && !(e[0] instanceof t)
            },
            _observe: function(e) {
                var t, r = this,
                    i = r.reader.model;
                return r._shouldDetachObservableParents = !0, e instanceof Ke ? (r._shouldDetachObservableParents = !1, r._shouldWrap(e) && (e.type = r.reader.model, e.wrapAll(e, e))) : (t = r.pageSize() && !r.options.serverPaging ? H : Ke, e = new t(e, r.reader.model), e.parent = function() {
                    return r.parent()
                }), r._isServerGrouped() && F(e, i), r._changeHandler && r._data && r._data instanceof Ke ? r._data.unbind(De, r._changeHandler) : r._changeHandler = he(r._change, r), e.bind(De, r._changeHandler)
            },
            _updateTotalForAction: function(e, t) {
                var r = this,
                    i = parseInt(r._total, 10);
                v(r._total) || (i = parseInt(r._pristineTotal, 10)), "add" === e ? i += t.length : "remove" === e ? i -= t.length : "itemchange" === e || "sync" === e || r.options.serverPaging ? "sync" === e && (i = r._pristineTotal = parseInt(r._total, 10)) : i = r._pristineTotal, r._total = i
            },
            _change: function(e) {
                var t, r, i, n = this,
                    a = e ? e.action : "";
                if ("remove" === a)
                    for (t = 0, r = e.items.length; t < r; t++) e.items[t].isNew && e.items[t].isNew() || n._destroyed.push(e.items[t]);
                !n.options.autoSync || "add" !== a && "remove" !== a && "itemchange" !== a ? (n._updateTotalForAction(a, e ? e.items : []), n._process(n._data, e)) : (i = function(t) {
                    "sync" === t.action && (n.unbind("change", i), n._updateTotalForAction(a, e.items))
                }, n.first("change", i), n.sync())
            },
            _calculateAggregates: function(e, t) {
                t = t || {};
                var r = new a(e),
                    i = t.aggregate,
                    n = t.filter;
                return n && (r = r.filter(n)), r.aggregate(i)
            },
            _process: function(e, r) {
                var i, n = this,
                    a = {};
                n.options.serverPaging !== !0 && (a.skip = n._skip, a.take = n._take || n._pageSize, a.skip === t && n._page !== t && n._pageSize !== t && (a.skip = (n._page - 1) * n._pageSize)), n.options.serverSorting !== !0 && (a.sort = n._sort), n.options.serverFiltering !== !0 && (a.filter = n._filter), n.options.serverGrouping !== !0 && (a.group = n._group), n.options.serverAggregates !== !0 && (a.aggregate = n._aggregate, n._aggregateResult = n._calculateAggregates(e, a)), i = n._queryProcess(e, a), n.view(i.data), i.total === t || n.options.serverFiltering || (n._total = i.total), r = r || {}, r.items = r.items || n._view, n.trigger(De, r)
            },
            _queryProcess: function(e, t) {
                return this.options.inPlaceSort ? a.process(e, t, this.options.inPlaceSort) : a.process(e, t)
            },
            _mergeState: function(e) {
                var r = this;
                return e !== t && (r._pageSize = e.pageSize, r._page = e.page, r._sort = e.sort, r._filter = e.filter, r._group = e.group, r._aggregate = e.aggregate, r._skip = r._currentRangeStart = e.skip, r._take = e.take, r._skip === t && (r._skip = r._currentRangeStart = r.skip(), e.skip = r.skip()), r._take === t && r._pageSize !== t && (r._take = r._pageSize, e.take = r._take), e.sort && (r._sort = e.sort = s(e.sort)), e.filter && (r._filter = e.filter = u(e.filter)), e.group && (r._group = e.group = g(e.group)), e.aggregate && (r._aggregate = e.aggregate = c(e.aggregate))), e
            },
            query: function(r) {
                var i, n, a, s = this.options.serverSorting || this.options.serverPaging || this.options.serverFiltering || this.options.serverGrouping || this.options.serverAggregates;
                return s || (this._data === t || 0 === this._data.length) && !this._destroyed.length ? (this.options.endless && (n = r.pageSize - this.pageSize(), n > 0 ? (n = this.pageSize(), r.page = r.pageSize / n, r.pageSize = n) : (r.page = 1, this.options.endless = !1)), this.read(this._mergeState(r))) : (a = this.trigger(Ce, {
                    type: "read"
                }), a || (this.trigger(Te), i = this._queryProcess(this._data, this._mergeState(r)), this.options.serverFiltering || (this._total = i.total !== t ? i.total : this._data.length), this._aggregateResult = this._calculateAggregates(this._data, r), this.view(i.data), this.trigger(Ae, {
                    type: "read"
                }), this.trigger(De, {
                    items: i.data
                })), e.Deferred().resolve(a).promise())
            },
            fetch: function(e) {
                var t = this,
                    r = function(r) {
                        r !== !0 && ye(e) && e.call(t)
                    };
                return this._query().then(r)
            },
            _query: function(e) {
                var t = this;
                return t.query(le({}, {
                    page: t.page(),
                    pageSize: t.pageSize(),
                    sort: t.sort(),
                    filter: t.filter(),
                    group: t.group(),
                    aggregate: t.aggregate()
                }, e))
            },
            next: function(e) {
                var t = this,
                    r = t.page(),
                    i = t.total();
                if (e = e || {}, r && !(i && r + 1 > t.totalPages())) return t._skip = t._currentRangeStart = r * t.take(), r += 1, e.page = r, t._query(e), r
            },
            prev: function(e) {
                var t = this,
                    r = t.page();
                if (e = e || {}, r && 1 !== r) return t._skip = t._currentRangeStart = t._skip - t.take(), r -= 1, e.page = r, t._query(e), r
            },
            page: function(e) {
                var r, i = this;
                return e !== t ? (e = Ge.max(Ge.min(Ge.max(e, 1), i.totalPages()), 1), i._query({
                    page: e
                }), t) : (r = i.skip(), r !== t ? Ge.round((r || 0) / (i.take() || 1)) + 1 : t)
            },
            pageSize: function(e) {
                var r = this;
                return e !== t ? (r._query({
                    pageSize: e,
                    page: 1
                }), t) : r.take()
            },
            sort: function(e) {
                var r = this;
                return e !== t ? (r._query({
                    sort: e
                }), t) : r._sort
            },
            filter: function(e) {
                var r = this;
                return e === t ? r._filter : (r.trigger("reset"), r._query({
                    filter: e,
                    page: 1
                }), t)
            },
            group: function(e) {
                var r = this;
                return e !== t ? (r._query({
                    group: e
                }), t) : r._group
            },
            total: function() {
                return parseInt(this._total || 0, 10)
            },
            aggregate: function(e) {
                var r = this;
                return e !== t ? (r._query({
                    aggregate: e
                }), t) : r._aggregate
            },
            aggregates: function() {
                var e = this._aggregateResult;
                return fe(e) && (e = this._emptyAggregates(this.aggregate())), e
            },
            _emptyAggregates: function(e) {
                var t, r, i = {};
                if (!fe(e))
                    for (t = {}, ce(e) || (e = [e]), r = 0; r < e.length; r++) t[e[r].aggregate] = 0, i[e[r].field] = t;
                return i
            },
            _wrapInEmptyGroup: function(e) {
                var t, r, i, n, a = this.group();
                for (i = a.length - 1, n = 0; i >= n; i--) r = a[i], t = {
                    value: e.get(r.field),
                    field: r.field,
                    items: t ? [t] : [e],
                    hasSubgroups: !!t,
                    aggregates: this._emptyAggregates(r.aggregates)
                };
                return t
            },
            totalPages: function() {
                var e = this,
                    t = e.pageSize() || e.total();
                return Ge.ceil((e.total() || 0) / t)
            },
            inRange: function(e, t) {
                var r = this,
                    i = Ge.min(e + t, r.total());
                return !r.options.serverPaging && r._data.length > 0 || r._findRange(e, i).length > 0
            },
            lastRange: function() {
                var e = this._ranges;
                return e[e.length - 1] || {
                    start: 0,
                    end: 0,
                    data: []
                }
            },
            firstItemUid: function() {
                var e = this._ranges;
                return e.length && e[0].data.length && e[0].data[0].uid
            },
            enableRequestsInProgress: function() {
                this._skipRequestsInProgress = !1
            },
            _timeStamp: function() {
                return (new Date).getTime()
            },
            range: function(e, r, i) {
                this._currentRequestTimeStamp = this._timeStamp(), this._skipRequestsInProgress = !0, e = Ge.min(e || 0, this.total()), i = ye(i) ? i : ve;
                var n, a = this,
                    s = Ge.max(Ge.floor(e / r), 0) * r,
                    o = Ge.min(s + r, a.total());
                return n = a._findRange(e, Ge.min(e + r, a.total())), n.length || 0 === a.total() ? (a._processRangeData(n, e, r, s, o), i(), t) : (r !== t && (a._rangeExists(s, o) ? s < e && a.prefetch(o, r, function() {
                    a.range(e, r, i)
                }) : a.prefetch(s, r, function() {
                    e > s && o < a.total() && !a._rangeExists(o, Ge.min(o + r, a.total())) ? a.prefetch(o, r, function() {
                        a.range(e, r, i)
                    }) : a.range(e, r, i)
                })), t)
            },
            _findRange: function(e, r) {
                var i, n, a, o, u, l, h, d, f, c, p, _, v = this,
                    m = v._ranges,
                    y = [],
                    S = v.options,
                    b = S.serverSorting || S.serverPaging || S.serverFiltering || S.serverGrouping || S.serverAggregates;
                for (n = 0, p = m.length; n < p; n++)
                    if (i = m[n], e >= i.start && e <= i.end) {
                        for (c = 0, a = n; a < p; a++)
                            if (i = m[a], f = v._flatData(i.data, !0), f.length && e + c >= i.start && (l = i.data, h = i.end, b || (S.inPlaceSort ? d = v._queryProcess(i.data, {
                                    filter: v.filter()
                                }) : (_ = g(v.group() || []).concat(s(v.sort() || [])), d = v._queryProcess(i.data, {
                                    sort: _,
                                    filter: v.filter()
                                })), f = l = d.data, d.total !== t && (h = d.total)), o = 0, e + c > i.start && (o = e + c - i.start), u = f.length, h > r && (u -= h - r), c += u - o, y = v._mergeGroups(y, l, o, u), r <= i.end && c == r - e)) return y;
                        break
                    }
                return []
            },
            _mergeGroups: function(e, t, r, i) {
                if (this._isServerGrouped()) {
                    var n, a = t.toJSON();
                    return e.length && (n = e[e.length - 1]), x(n, a, r, i), e.concat(a)
                }
                return e.concat(t.slice(r, i))
            },
            _processRangeData: function(e, r, i, n, a) {
                var s, o, u, l, h = this;
                h._pending = t, h._skip = r > h.skip() ? Ge.min(a, (h.totalPages() - 1) * h.take()) : n, h._currentRangeStart = r, h._take = i, s = h.options.serverPaging, o = h.options.serverSorting, u = h.options.serverFiltering, l = h.options.serverAggregates;
                try {
                    h.options.serverPaging = !0, h._isServerGrouped() || h.group() && h.group().length || (h.options.serverSorting = !0), h.options.serverFiltering = !0, h.options.serverPaging = !0, h.options.serverAggregates = !0, s && (h._detachObservableParents(), h._data = e = h._observe(e)), h._process(e)
                } finally {
                    h.options.serverPaging = s, h.options.serverSorting = o, h.options.serverFiltering = u, h.options.serverAggregates = l
                }
            },
            skip: function() {
                var e = this;
                return e._skip === t ? e._page !== t ? (e._page - 1) * (e.take() || 1) : t : e._skip
            },
            currentRangeStart: function() {
                return this._currentRangeStart || 0
            },
            take: function() {
                return this._take || this._pageSize
            },
            _prefetchSuccessHandler: function(e, t, r, i) {
                var n = this,
                    a = n._timeStamp();
                return function(s) {
                    var o, u, l, h = !1,
                        d = {
                            start: e,
                            end: t,
                            data: [],
                            timestamp: n._timeStamp()
                        };
                    if (n._dequeueRequest(), n.trigger(Ae, {
                            response: s,
                            type: "read"
                        }), s = n.reader.parse(s), l = n._readData(s), l.length) {
                        for (o = 0, u = n._ranges.length; o < u; o++)
                            if (n._ranges[o].start === e) {
                                h = !0, d = n._ranges[o];
                                break
                            }
                        h || n._ranges.push(d)
                    }
                    d.data = n._observe(l), d.end = d.start + n._flatData(d.data, !0).length, n._ranges.sort(function(e, t) {
                        return e.start - t.start
                    }), n._total = n.reader.total(s), (i || a >= n._currentRequestTimeStamp || !n._skipRequestsInProgress) && (r && l.length ? r() : n.trigger(De, {}))
                }
            },
            prefetch: function(e, t, r) {
                var i = this,
                    n = Ge.min(e + t, i.total()),
                    a = {
                        take: t,
                        skip: e,
                        page: e / t + 1,
                        pageSize: t,
                        sort: i._sort,
                        filter: i._filter,
                        group: i._group,
                        aggregate: i._aggregate
                    };
                i._rangeExists(e, n) ? r && r() : (clearTimeout(i._timeout), i._timeout = setTimeout(function() {
                    i._queueRequest(a, function() {
                        i.trigger(Ce, {
                            type: "read"
                        }) ? i._dequeueRequest() : i.transport.read({
                            data: i._params(a),
                            success: i._prefetchSuccessHandler(e, n, r),
                            error: function() {
                                var e = Je.call(arguments);
                                i.error.apply(i, e)
                            }
                        })
                    })
                }, 100))
            },
            _multiplePrefetch: function(e, t, r) {
                var i = this,
                    n = Ge.min(e + t, i.total()),
                    a = {
                        take: t,
                        skip: e,
                        page: e / t + 1,
                        pageSize: t,
                        sort: i._sort,
                        filter: i._filter,
                        group: i._group,
                        aggregate: i._aggregate
                    };
                i._rangeExists(e, n) ? r && r() : i.trigger(Ce, {
                    type: "read"
                }) || i.transport.read({
                    data: i._params(a),
                    success: i._prefetchSuccessHandler(e, n, r, !0)
                })
            },
            _rangeExists: function(e, t) {
                var r, i, n = this,
                    a = n._ranges;
                for (r = 0, i = a.length; r < i; r++)
                    if (a[r].start <= e && a[r].end >= t) return !0;
                return !1
            },
            _removeModelFromRanges: function(e) {
                var t, r, i, n, a = this;
                for (i = 0, n = this._ranges.length; i < n && (r = this._ranges[i], this._eachItem(r.data, function(r) {
                        t = P(r, e)
                    }), !t); i++);
                a._updateRangesLength()
            },
            _insertModelInRange: function(e, t) {
                var r, i, n = this,
                    a = n._ranges || [],
                    s = a.length;
                for (i = 0; i < s; i++)
                    if (r = a[i], r.start <= e && r.end >= e) {
                        n._getByUid(t.uid, r.data) || (n._isServerGrouped() ? r.data.splice(e, 0, n._wrapInEmptyGroup(t)) : r.data.splice(e, 0, t));
                        break
                    }
                n._updateRangesLength()
            },
            _updateRangesLength: function() {
                var e, t, r = this,
                    i = r._ranges || [],
                    n = i.length,
                    a = !1,
                    s = 0,
                    o = 0;
                for (t = 0; t < n; t++) e = i[t], o = r._flatData(e.data, !0).length - Ge.abs(e.end - e.start), a || 0 === o ? a && (e.start += s, e.end += s) : (a = !0, s = o, e.end += s)
            }
        }), ne = {}, ne.create = function(t, r, i) {
            var n, a = t.transport ? e.extend({}, t.transport) : null;
            return a ? (a.read = typeof a.read === we ? {
                url: a.read
            } : a.read, "jsdo" === t.type && (a.dataSource = i), t.type && (me.data.transports = me.data.transports || {}, me.data.schemas = me.data.schemas || {}, me.data.transports[t.type] ? de(me.data.transports[t.type]) ? a = le(!0, {}, me.data.transports[t.type], a) : n = new me.data.transports[t.type](le(a, {
                data: r
            })) : me.logToConsole("Unknown DataSource transport type '" + t.type + "'.\nVerify that registration scripts for this type are included after Kendo UI on the page.", "warn"), t.schema = le(!0, {}, me.data.schemas[t.type], t.schema)), n || (n = ye(a.read) ? a : new ee(a))) : n = new Z({
                data: t.data || []
            }), n
        }, ie.create = function(e) {
            (ce(e) || e instanceof Ke) && (e = {
                data: e
            });
            var r, i, n, a = e || {},
                s = a.data,
                o = a.fields,
                u = a.table,
                l = a.select,
                h = {};
            if (s || !o || a.transport || (u ? s = G(u, o) : l && (s = j(l, o), a.group === t && s[0] && s[0].optgroup !== t && (a.group = "optgroup"))), me.data.Model && o && (!a.schema || !a.schema.model)) {
                for (r = 0, i = o.length; r < i; r++) n = o[r], n.type && (h[n.field] = n);
                fe(h) || (a.schema = le(!0, a.schema, {
                    model: {
                        fields: h
                    }
                }))
            }
            return a.data = s, l = null, a.select = null, u = null, a.table = null, a instanceof ie ? a : new ie(a)
        }, ae = W.define({
            idField: "id",
            init: function(e) {
                var t, r = this,
                    i = r.hasChildren || e && e.hasChildren,
                    n = "items",
                    a = {};
                me.data.Model.fn.init.call(r, e), typeof r.children === we && (n = r.children), a = {
                    schema: {
                        data: n,
                        model: {
                            hasChildren: i,
                            id: r.idField,
                            fields: r.fields
                        }
                    }
                }, typeof r.children !== we && le(a, r.children), a.data = e, i || (i = a.schema.data), typeof i === we && (i = me.getter(i)), ye(i) && (t = i.call(r, r), r.hasChildren = (!t || 0 !== t.length) && !!t), r._childrenOptions = a, r.hasChildren && r._initChildren(), r._loaded = !(!e || !e._loaded)
            },
            _initChildren: function() {
                var e, t, r, i = this;
                i.children instanceof se || (e = i.children = new se(i._childrenOptions), t = e.transport, r = t.parameterMap, t.parameterMap = function(e, t) {
                    return e[i.idField || "id"] = i.id, r && (e = r(e, t)), e
                }, e.parent = function() {
                    return i
                }, e.bind(De, function(e) {
                    e.node = e.node || i, i.trigger(De, e)
                }), e.bind(Pe, function(e) {
                    var t = i.parent();
                    t && (e.node = e.node || i, t.trigger(Pe, e))
                }), i._updateChildrenField())
            },
            append: function(e) {
                this._initChildren(), this.loaded(!0), this.children.add(e)
            },
            hasChildren: !1,
            level: function() {
                for (var e = this.parentNode(), t = 0; e && e.parentNode;) t++, e = e.parentNode ? e.parentNode() : null;
                return t
            },
            _updateChildrenField: function() {
                var e = this._childrenOptions.schema.data;
                this[e || "items"] = this.children.data()
            },
            _childrenLoaded: function() {
                this._loaded = !0, this._updateChildrenField()
            },
            load: function() {
                var r, i, n = {},
                    a = "_query";
                return this.hasChildren ? (this._initChildren(), r = this.children, n[this.idField || "id"] = this.id, this._loaded || (r._data = t, a = "read"), r.one(De, he(this._childrenLoaded, this)), this._matchFilter && (n.filter = {
                    field: "_matchFilter",
                    operator: "eq",
                    value: !0
                }), i = r[a](n)) : this.loaded(!0), i || e.Deferred().resolve().promise()
            },
            parentNode: function() {
                var e = this.parent();
                return e.parent()
            },
            loaded: function(e) {
                return e === t ? this._loaded : (this._loaded = e, t)
            },
            shouldSerialize: function(e) {
                return W.fn.shouldSerialize.call(this, e) && "children" !== e && "_loaded" !== e && "hasChildren" !== e && "_childrenOptions" !== e
            }
        }), se = ie.extend({
            init: function(e) {
                var t = ae.define({
                    children: e
                });
                e.filter && !e.serverFiltering && (this._hierarchicalFilter = e.filter, e.filter = null), ie.fn.init.call(this, le(!0, {}, {
                    schema: {
                        modelBase: t,
                        model: t
                    }
                }, e)), this._attachBubbleHandlers()
            },
            _attachBubbleHandlers: function() {
                var e = this;
                e._data.bind(Pe, function(t) {
                    e.trigger(Pe, t)
                })
            },
            read: function(e) {
                var t = ie.fn.read.call(this, e);
                return this._hierarchicalFilter && (this._data && this._data.length > 0 ? this.filter(this._hierarchicalFilter) : (this.options.filter = this._hierarchicalFilter, this._filter = u(this.options.filter), this._hierarchicalFilter = null)), t
            },
            remove: function(e) {
                var t, r = e.parentNode(),
                    i = this;
                return r && r._initChildren && (i = r.children), t = ie.fn.remove.call(i, e), r && !i.data().length && (r.hasChildren = !1), t
            },
            success: B("success"),
            data: B("data"),
            insert: function(e, t) {
                var r = this.parent();
                return r && r._initChildren && (r.hasChildren = !0, r._initChildren()), ie.fn.insert.call(this, e, t)
            },
            filter: function(e) {
                return e === t ? this._filter : (!this.options.serverFiltering && this._markHierarchicalQuery(e) && (e = {
                    logic: "or",
                    filters: [e, {
                        field: "_matchFilter",
                        operator: "equals",
                        value: !0
                    }]
                }), this.trigger("reset"), this._query({
                    filter: e,
                    page: 1
                }), t)
            },
            _markHierarchicalQuery: function(e) {
                var t, r, i, n, s;
                return e = u(e), e && 0 !== e.filters.length ? (t = a.filterExpr(e), i = t.fields, n = t.operators, r = s = Function("d, __f, __o", "return " + t.expression), (i.length || n.length) && (s = function(e) {
                    return r(e, i, n)
                }), this._updateHierarchicalFilter(s), !0) : (this._updateHierarchicalFilter(function() {
                    return !0
                }), !1)
            },
            _updateHierarchicalFilter: function(e) {
                var t, r, i = this._data,
                    n = !1;
                for (r = 0; r < i.length; r++) t = i[r], t.hasChildren ? (t._matchFilter = t.children._updateHierarchicalFilter(e), t._matchFilter || (t._matchFilter = e(t))) : t._matchFilter = e(t), t._matchFilter && (n = !0);
                return n
            },
            _find: function(e, t) {
                var r, i, n, a, s = this._data;
                if (s) {
                    if (n = ie.fn[e].call(this, t)) return n;
                    for (s = this._flatData(this._data), r = 0, i = s.length; r < i; r++)
                        if (a = s[r].children, a instanceof se && (n = a[e](t))) return n
                }
            },
            get: function(e) {
                return this._find("get", e)
            },
            getByUid: function(e) {
                return this._find("getByUid", e)
            }
        }), se.create = function(e) {
            e = e && e.push ? {
                data: e
            } : e;
            var t = e || {},
                r = t.data,
                i = t.fields,
                n = t.list;
            return r && r._dataSource ? r._dataSource : (r || !i || t.transport || n && (r = E(n, i)), t.data = r, t instanceof se ? t : new se(t))
        }, oe = me.Observable.extend({
            init: function(e, t, r) {
                me.Observable.fn.init.call(this), this._prefetching = !1, this.dataSource = e, this.prefetch = !r;
                var i = this;
                e.bind("change", function() {
                    i._change()
                }), e.bind("reset", function() {
                    i._reset()
                }), this._syncWithDataSource(), this.setViewSize(t)
            },
            setViewSize: function(e) {
                this.viewSize = e, this._recalculate()
            },
            at: function(e) {
                var r = this.pageSize,
                    i = !0;
                return e >= this.total() ? (this.trigger("endreached", {
                    index: e
                }), null) : this.useRanges ? this.useRanges ? ((e < this.dataOffset || e >= this.skip + r) && (i = this.range(Math.floor(e / r) * r)), e === this.prefetchThreshold && this._prefetch(), e === this.midPageThreshold ? this.range(this.nextMidRange, !0) : e === this.nextPageThreshold ? this.range(this.nextFullRange) : e === this.pullBackThreshold && this.range(this.offset === this.skip ? this.previousMidRange : this.previousFullRange), i ? this.dataSource.at(e - this.dataOffset) : (this.trigger("endreached", {
                    index: e
                }), null)) : t : this.dataSource.view()[e]
            },
            indexOf: function(e) {
                return this.dataSource.data().indexOf(e) + this.dataOffset
            },
            total: function() {
                return parseInt(this.dataSource.total(), 10)
            },
            next: function() {
                var e = this,
                    t = e.pageSize,
                    r = e.skip - e.viewSize + t,
                    i = Ge.max(Ge.floor(r / t), 0) * t;
                this.offset = r, this.dataSource.prefetch(i, t, function() {
                    e._goToRange(r, !0)
                })
            },
            range: function(e, t) {
                if (this.offset === e) return !0;
                var r = this,
                    i = this.pageSize,
                    n = Ge.max(Ge.floor(e / i), 0) * i,
                    a = this.dataSource;
                return t && (n += i), a.inRange(e, i) ? (this.offset = e, this._recalculate(), this._goToRange(e), !0) : !this.prefetch || (a.prefetch(n, i, function() {
                    r.offset = e, r._recalculate(), r._goToRange(e, !0)
                }), !1)
            },
            syncDataSource: function() {
                var e = this.offset;
                this.offset = null, this.range(e)
            },
            destroy: function() {
                this.unbind()
            },
            _prefetch: function() {
                var e = this,
                    t = this.pageSize,
                    r = this.skip + t,
                    i = this.dataSource;
                i.inRange(r, t) || this._prefetching || !this.prefetch || (this._prefetching = !0, this.trigger("prefetching", {
                    skip: r,
                    take: t
                }), i.prefetch(r, t, function() {
                    e._prefetching = !1, e.trigger("prefetched", {
                        skip: r,
                        take: t
                    })
                }))
            },
            _goToRange: function(e, t) {
                this.offset === e && (this.dataOffset = e, this._expanding = t, this.dataSource.range(e, this.pageSize), this.dataSource.enableRequestsInProgress())
            },
            _reset: function() {
                this._syncPending = !0
            },
            _change: function() {
                var e = this.dataSource;
                this.length = this.useRanges ? e.lastRange().end : e.view().length, this._syncPending && (this._syncWithDataSource(), this._recalculate(), this._syncPending = !1, this.trigger("reset", {
                    offset: this.offset
                })), this.trigger("resize"), this._expanding && this.trigger("expand"), delete this._expanding
            },
            _syncWithDataSource: function() {
                var e = this.dataSource;
                this._firstItemUid = e.firstItemUid(), this.dataOffset = this.offset = e.skip() || 0, this.pageSize = e.pageSize(), this.useRanges = e.options.serverPaging
            },
            _recalculate: function() {
                var e = this.pageSize,
                    t = this.offset,
                    r = this.viewSize,
                    i = Math.ceil(t / e) * e;
                this.skip = i, this.midPageThreshold = i + e - 1, this.nextPageThreshold = i + r - 1, this.prefetchThreshold = i + Math.floor(e / 3 * 2), this.pullBackThreshold = this.offset - 1, this.nextMidRange = i + e - r, this.nextFullRange = i, this.previousMidRange = t - r, this.previousFullRange = i - e
            }
        }), ue = me.Observable.extend({
            init: function(e, t) {
                var r = this;
                me.Observable.fn.init.call(r), this.dataSource = e, this.batchSize = t, this._total = 0, this.buffer = new oe(e, 3 * t), this.buffer.bind({
                    endreached: function(e) {
                        r.trigger("endreached", {
                            index: e.index
                        })
                    },
                    prefetching: function(e) {
                        r.trigger("prefetching", {
                            skip: e.skip,
                            take: e.take
                        })
                    },
                    prefetched: function(e) {
                        r.trigger("prefetched", {
                            skip: e.skip,
                            take: e.take
                        })
                    },
                    reset: function() {
                        r._total = 0, r.trigger("reset")
                    },
                    resize: function() {
                        r._total = Math.ceil(this.length / r.batchSize), r.trigger("resize", {
                            total: r.total(),
                            offset: this.offset
                        })
                    }
                })
            },
            syncDataSource: function() {
                this.buffer.syncDataSource()
            },
            at: function(e) {
                var t, r, i = this.buffer,
                    n = e * this.batchSize,
                    a = this.batchSize,
                    s = [];
                for (i.offset > n && i.at(i.offset - 1), r = 0; r < a && (t = i.at(n + r), null !== t); r++) s.push(t);
                return s
            },
            total: function() {
                return this._total
            },
            destroy: function() {
                this.buffer.destroy(), this.unbind()
            }
        }), le(!0, me.data, {
            readers: {
                json: re
            },
            Query: a,
            DataSource: ie,
            HierarchicalDataSource: se,
            Node: ae,
            ObservableObject: U,
            ObservableArray: Ke,
            LazyObservableArray: H,
            LocalTransport: Z,
            RemoteTransport: ee,
            Cache: te,
            DataReader: re,
            Model: W,
            Buffer: oe,
            BatchBuffer: ue
        })
    }(window.kendo.jQuery), window.kendo
}, "function" == typeof define && define.amd ? define : function(e, t, r) {
    (r || t)()
});
//# sourceMappingURL=kendo.data.min.js.map;
/** 
 * Kendo UI v2018.1.221 (http://www.telerik.com/kendo-ui)                                                                                                                                               
 * Copyright 2018 Telerik AD. All rights reserved.                                                                                                                                                      
 *                                                                                                                                                                                                      
 * Kendo UI commercial licenses may be obtained at                                                                                                                                                      
 * http://www.telerik.com/purchase/license-agreement/kendo-ui-complete                                                                                                                                  
 * If you do not own a commercial license, this file shall be governed by the trial license terms.                                                                                                      

*/
! function(e, define) {
    define("kendo.popup.min", ["kendo.core.min"], e)
}(function() {
    return function(e, t) {
        function o(t, o) {
            return !(!t || !o) && (t === o || e.contains(t, o))
        }
        var n, i, s, r, a = window.kendo,
            l = a.ui,
            d = l.Widget,
            p = a.Class,
            c = a.support,
            f = a.getOffset,
            u = a._outerWidth,
            h = a._outerHeight,
            m = "open",
            g = "close",
            w = "deactivate",
            v = "activate",
            _ = "center",
            b = "left",
            y = "right",
            k = "top",
            x = "bottom",
            T = "absolute",
            z = "hidden",
            C = "body",
            S = "location",
            E = "position",
            I = "visible",
            P = "effects",
            R = "k-state-active",
            A = "k-state-border",
            D = /k-state-border-(\w+)/,
            O = ".k-picker-wrap, .k-dropdown-wrap, .k-link",
            F = "down",
            H = e(document.documentElement),
            N = e.proxy,
            W = e(window),
            L = "scroll",
            j = c.transitions.css,
            M = j + "transform",
            K = e.extend,
            U = ".kendoPopup",
            Y = ["font-size", "font-family", "font-stretch", "font-style", "font-weight", "line-height"],
            Q = d.extend({
                init: function(t, o) {
                    var n, i = this;
                    o = o || {}, o.isRtl && (o.origin = o.origin || x + " " + y, o.position = o.position || k + " " + y), d.fn.init.call(i, t, o), t = i.element, o = i.options, i.collisions = o.collision ? o.collision.split(" ") : [], i.downEvent = a.applyEventMap(F, a.guid()), 1 === i.collisions.length && i.collisions.push(i.collisions[0]), n = e(i.options.anchor).closest(".k-popup,.k-group").filter(":not([class^=km-])"), o.appendTo = e(e(o.appendTo)[0] || n[0] || document.body), i.element.hide().addClass("k-popup k-group k-reset").toggleClass("k-rtl", !!o.isRtl).css({
                        position: T
                    }).appendTo(o.appendTo).attr("aria-hidden", !0).on("mouseenter" + U, function() {
                        i._hovered = !0
                    }).on("wheel" + U, function(t) {
                        var o = e(t.target).find(".k-list"),
                            n = o.parent();
                        o.length && o.is(":visible") && (0 === n.scrollTop() && t.originalEvent.deltaY < 0 || n.scrollTop() === n.prop("scrollHeight") - n.prop("offsetHeight") && t.originalEvent.deltaY > 0) && t.preventDefault()
                    }).on("mouseleave" + U, function() {
                        i._hovered = !1
                    }), i.wrapper = e(), o.animation === !1 && (o.animation = {
                        open: {
                            effects: {}
                        },
                        close: {
                            hide: !0,
                            effects: {}
                        }
                    }), K(o.animation.open, {
                        complete: function() {
                            i.wrapper.css({
                                overflow: I
                            }), i._activated = !0, i._trigger(v)
                        }
                    }), K(o.animation.close, {
                        complete: function() {
                            i._animationClose()
                        }
                    }), i._mousedownProxy = function(e) {
                        i._mousedown(e)
                    }, i._resizeProxy = c.mobileOS.android ? function(e) {
                        setTimeout(function() {
                            i._resize(e)
                        }, 600)
                    } : function(e) {
                        i._resize(e)
                    }, o.toggleTarget && e(o.toggleTarget).on(o.toggleEvent + U, e.proxy(i.toggle, i))
                },
                events: [m, v, g, w],
                options: {
                    name: "Popup",
                    toggleEvent: "click",
                    origin: x + " " + b,
                    position: k + " " + b,
                    anchor: C,
                    appendTo: null,
                    collision: "flip fit",
                    viewport: window,
                    copyAnchorStyles: !0,
                    autosize: !1,
                    modal: !1,
                    adjustSize: {
                        width: 0,
                        height: 0
                    },
                    animation: {
                        open: {
                            effects: "slideIn:down",
                            transition: !0,
                            duration: 200
                        },
                        close: {
                            duration: 100,
                            hide: !0
                        }
                    }
                },
                _animationClose: function() {
                    var e = this,
                        t = e.wrapper.data(S);
                    e.wrapper.hide(), t && e.wrapper.css(t), e.options.anchor != C && e._hideDirClass(), e._closing = !1, e._trigger(w)
                },
                destroy: function() {
                    var t, o = this,
                        n = o.options,
                        i = o.element.off(U);
                    d.fn.destroy.call(o), n.toggleTarget && e(n.toggleTarget).off(U), n.modal || (H.unbind(o.downEvent, o._mousedownProxy), o._toggleResize(!1)), a.destroy(o.element.children()), i.removeData(), n.appendTo[0] === document.body && (t = i.parent(".k-animation-container"), t[0] ? t.remove() : i.remove())
                },
                open: function(t, o) {
                    var n, i, s = this,
                        r = {
                            isFixed: !isNaN(parseInt(o, 10)),
                            x: t,
                            y: o
                        },
                        l = s.element,
                        d = s.options,
                        p = e(d.anchor),
                        f = l[0] && l.hasClass("km-widget");
                    if (!s.visible()) {
                        if (d.copyAnchorStyles && (f && "font-size" == Y[0] && Y.shift(), l.css(a.getComputedStyles(p[0], Y))), l.data("animating") || s._trigger(m)) return;
                        s._activated = !1, d.modal || (H.unbind(s.downEvent, s._mousedownProxy).bind(s.downEvent, s._mousedownProxy), s._toggleResize(!1), s._toggleResize(!0)), s.wrapper = i = a.wrap(l, d.autosize).css({
                            overflow: z,
                            display: "block",
                            position: T
                        }).attr("aria-hidden", !1), c.mobileOS.android && i.css(M, "translatez(0)"), i.css(E), e(d.appendTo)[0] == document.body && i.css(k, "-10000px"), s.flipped = s._position(r), n = s._openAnimation(), d.anchor != C && s._showDirClass(n), l.data(P, n.effects).kendoStop(!0).kendoAnimate(n).attr("aria-hidden", !1)
                    }
                },
                _location: function(t) {
                    var o, n, i = this,
                        s = i.element,
                        r = i.options,
                        l = e(r.anchor),
                        d = s[0] && s.hasClass("km-widget");
                    return r.copyAnchorStyles && (d && "font-size" == Y[0] && Y.shift(), s.css(a.getComputedStyles(l[0], Y))), i.wrapper = o = a.wrap(s, r.autosize).css({
                        overflow: z,
                        display: "block",
                        position: T
                    }), c.mobileOS.android && o.css(M, "translatez(0)"), o.css(E), e(r.appendTo)[0] == document.body && o.css(k, "-10000px"), i._position(t || {}), n = o.offset(), {
                        width: a._outerWidth(o),
                        height: a._outerHeight(o),
                        left: n.left,
                        top: n.top
                    }
                },
                _openAnimation: function() {
                    var e = K(!0, {}, this.options.animation.open);
                    return e.effects = a.parseEffects(e.effects, this.flipped), e
                },
                _hideDirClass: function() {
                    var t = e(this.options.anchor),
                        o = ((t.attr("class") || "").match(D) || ["", "down"])[1],
                        n = A + "-" + o;
                    t.removeClass(n).children(O).removeClass(R).removeClass(n), this.element.removeClass(A + "-" + a.directions[o].reverse)
                },
                _showDirClass: function(t) {
                    var o = t.effects.slideIn ? t.effects.slideIn.direction : "down",
                        n = A + "-" + o;
                    e(this.options.anchor).addClass(n).children(O).addClass(R).addClass(n), this.element.addClass(A + "-" + a.directions[o].reverse)
                },
                position: function() {
                    this.visible() && (this.flipped = this._position())
                },
                toggle: function() {
                    var e = this;
                    e[e.visible() ? g : m]()
                },
                visible: function() {
                    return this.element.is(":" + I)
                },
                close: function(o) {
                    var n, i, s, r, l = this,
                        d = l.options;
                    if (l.visible()) {
                        if (n = l.wrapper[0] ? l.wrapper : a.wrap(l.element).hide(), l._toggleResize(!1), l._closing || l._trigger(g)) return l._toggleResize(!0), t;
                        l.element.find(".k-popup").each(function() {
                            var t = e(this),
                                n = t.data("kendoPopup");
                            n && n.close(o)
                        }), H.unbind(l.downEvent, l._mousedownProxy), o ? i = {
                            hide: !0,
                            effects: {}
                        } : (i = K(!0, {}, d.animation.close), s = l.element.data(P), r = i.effects, !r && !a.size(r) && s && a.size(s) && (i.effects = s, i.reverse = !0), l._closing = !0), l.element.kendoStop(!0).attr("aria-hidden", !0), n.css({
                            overflow: z
                        }).attr("aria-hidden", !0), l.element.kendoAnimate(i), o && l._animationClose()
                    }
                },
                _trigger: function(e) {
                    return this.trigger(e, {
                        type: e
                    })
                },
                _resize: function(e) {
                    var t = this;
                    c.resize.indexOf(e.type) !== -1 ? (clearTimeout(t._resizeTimeout), t._resizeTimeout = setTimeout(function() {
                        t._position(), t._resizeTimeout = null
                    }, 50)) : (!t._hovered || t._activated && t.element.hasClass("k-list-container")) && t.close()
                },
                _toggleResize: function(e) {
                    var t = e ? "on" : "off",
                        o = c.resize;
                    c.mobileOS.ios || c.mobileOS.android || (o += " " + L), this._scrollableParents()[t](L, this._resizeProxy), W[t](o, this._resizeProxy)
                },
                _mousedown: function(t) {
                    var n = this,
                        i = n.element[0],
                        s = n.options,
                        r = e(s.anchor)[0],
                        l = s.toggleTarget,
                        d = a.eventTarget(t),
                        p = e(d).closest(".k-popup"),
                        c = p.parent().parent(".km-shim").length;
                    p = p[0], !c && p && p !== n.element[0] || "popover" !== e(t.target).closest("a").data("rel") && (o(i, d) || o(r, d) || l && o(e(l)[0], d) || n.close())
                },
                _fit: function(e, t, o) {
                    var n = 0;
                    return e + t > o && (n = o - (e + t)), e < 0 && (n = -e), n
                },
                _flip: function(e, t, o, n, i, s, r) {
                    var a = 0;
                    return r = r || t, s !== i && s !== _ && i !== _ && (e + r > n && (a += -(o + t)), e + a < 0 && (a += o + t)), a
                },
                _scrollableParents: function() {
                    return e(this.options.anchor).parentsUntil("body").filter(function(e, t) {
                        return a.isScrollable(t)
                    })
                },
                _position: function(t) {
                    var o, n, i, s, r, l, d, p, m, g, w, v, _, b, y, k, x, z = this,
                        C = z.element,
                        I = z.wrapper,
                        P = z.options,
                        R = e(P.viewport),
                        A = c.zoomLevel(),
                        D = !!(R[0] == window && window.innerWidth && A <= 1.02),
                        O = e(P.anchor),
                        F = P.origin.toLowerCase().split(" "),
                        H = P.position.toLowerCase().split(" "),
                        N = z.collisions,
                        W = 10002,
                        L = 0,
                        j = document.documentElement;
                    if (r = P.viewport === window ? {
                            top: window.pageYOffset || document.documentElement.scrollTop || 0,
                            left: window.pageXOffset || document.documentElement.scrollLeft || 0
                        } : R.offset(), D ? (l = window.innerWidth, d = window.innerHeight) : (l = R.width(), d = R.height()), D && j.scrollHeight - j.clientHeight > 0 && (p = P.isRtl ? -1 : 1, l -= p * a.support.scrollbar()), o = O.parents().filter(I.siblings()), o[0])
                        if (i = Math.max(+o.css("zIndex"), 0)) W = i + 10;
                        else
                            for (n = O.parentsUntil(o), s = n.length; L < s; L++) i = +e(n[L]).css("zIndex"), i && W < i && (W = i + 10);
                    return I.css("zIndex", W), I.css(t && t.isFixed ? {
                        left: t.x,
                        top: t.y
                    } : z._align(F, H)), m = f(I, E, O[0] === I.offsetParent()[0]), g = f(I), w = O.offsetParent().parent(".k-animation-container,.k-popup,.k-group"), w.length && (m = f(I, E, !0), g = f(I)), g.top -= r.top, g.left -= r.left, z.wrapper.data(S) || I.data(S, K({}, m)), v = K({}, g), _ = K({}, m), b = P.adjustSize, "fit" === N[0] && (_.top += z._fit(v.top, h(I) + b.height, d / A)), "fit" === N[1] && (_.left += z._fit(v.left, u(I) + b.width, l / A)), y = K({}, _), k = h(C), x = h(I), !I.height() && k && (x += k), "flip" === N[0] && (_.top += z._flip(v.top, k, h(O), d / A, F[0], H[0], x)), "flip" === N[1] && (_.left += z._flip(v.left, u(C), u(O), l / A, F[1], H[1], u(I))), C.css(E, T), I.css(_), _.left != y.left || _.top != y.top
                },
                _align: function(t, o) {
                    var n, i = this,
                        s = i.wrapper,
                        r = e(i.options.anchor),
                        a = t[0],
                        l = t[1],
                        d = o[0],
                        p = o[1],
                        c = f(r),
                        m = e(i.options.appendTo),
                        g = u(s),
                        w = h(s) || h(s.children().first()),
                        v = u(r),
                        b = h(r),
                        k = c.top,
                        T = c.left,
                        z = Math.round;
                    return m[0] != document.body && (n = f(m), k -= n.top, T -= n.left), a === x && (k += b), a === _ && (k += z(b / 2)), d === x && (k -= w), d === _ && (k -= z(w / 2)), l === y && (T += v), l === _ && (T += z(v / 2)), p === y && (T -= g), p === _ && (T -= z(g / 2)), {
                        top: k,
                        left: T
                    }
                }
            });
        l.plugin(Q), n = a.support.stableSort, i = "kendoTabKeyTrap", s = "a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex], *[contenteditable]", r = p.extend({
            init: function(t) {
                this.element = e(t), this.element.autoApplyNS(i)
            },
            trap: function() {
                this.element.on("keydown", N(this._keepInTrap, this))
            },
            removeTrap: function() {
                this.element.kendoDestroy(i)
            },
            destroy: function() {
                this.element.kendoDestroy(i), this.element = t
            },
            shouldTrap: function() {
                return !0
            },
            _keepInTrap: function(e) {
                var t, o, n;
                9 === e.which && this.shouldTrap() && !e.isDefaultPrevented() && (t = this._focusableElements(), o = this._sortFocusableElements(t), n = this._nextFocusable(e, o), this._focus(n), e.preventDefault())
            },
            _focusableElements: function() {
                var t = this.element.find(s).filter(function(t, o) {
                    return o.tabIndex >= 0 && e(o).is(":visible") && !e(o).is("[disabled]")
                });
                return this.element.is("[tabindex]") && t.push(this.element[0]), t
            },
            _sortFocusableElements: function(e) {
                var t, o;
                return n ? t = e.sort(function(e, t) {
                    return e.tabIndex - t.tabIndex
                }) : (o = "__k_index", e.each(function(e, t) {
                    t.setAttribute(o, e)
                }), t = e.sort(function(e, t) {
                    return e.tabIndex === t.tabIndex ? parseInt(e.getAttribute(o), 10) - parseInt(t.getAttribute(o), 10) : e.tabIndex - t.tabIndex
                }), e.removeAttr(o)), t
            },
            _nextFocusable: function(e, t) {
                var o = t.length,
                    n = t.index(e.target);
                return t.get((n + (e.shiftKey ? -1 : 1)) % o)
            },
            _focus: function(e) {
                return "IFRAME" == e.nodeName ? (e.contentWindow.document.body.focus(), t) : (e.focus(), "INPUT" == e.nodeName && e.setSelectionRange && this._haveSelectionRange(e) && e.setSelectionRange(0, e.value.length), t)
            },
            _haveSelectionRange: function(e) {
                var t = e.type.toLowerCase();
                return "text" === t || "search" === t || "url" === t || "tel" === t || "password" === t
            }
        }), l.Popup.TabKeyTrap = r
    }(window.kendo.jQuery), window.kendo
}, "function" == typeof define && define.amd ? define : function(e, t, o) {
    (o || t)()
});
//# sourceMappingURL=kendo.popup.min.js.map;
/** 
 * Kendo UI v2018.1.221 (http://www.telerik.com/kendo-ui)                                                                                                                                               
 * Copyright 2018 Telerik AD. All rights reserved.                                                                                                                                                      
 *                                                                                                                                                                                                      
 * Kendo UI commercial licenses may be obtained at                                                                                                                                                      
 * http://www.telerik.com/purchase/license-agreement/kendo-ui-complete                                                                                                                                  
 * If you do not own a commercial license, this file shall be governed by the trial license terms.                                                                                                      

*/
! function(e, define) {
    define("kendo.list.min", ["kendo.data.min", "kendo.popup.min"], e)
}(function() {
    return function(e, t) {
        function i(e, i) {
            return e !== t && "" !== e && null !== e && ("boolean" === i ? e = !!e : "number" === i ? e = +e : "string" === i && (e = "" + e)), e
        }

        function a(e) {
            return e[e.length - 1]
        }

        function n(e) {
            var t = e.selectedIndex;
            return t > -1 ? e.options[t] : {}
        }

        function s(e, t) {
            var i, a, n, s, r = t.length,
                l = e.length,
                o = [],
                u = [];
            if (l)
                for (n = 0; n < l; n++) {
                    for (i = e[n], a = !1, s = 0; s < r; s++)
                        if (i === t[s]) {
                            a = !0, o.push({
                                index: n,
                                item: i
                            });
                            break
                        }
                    a || u.push(i)
                }
            return {
                changed: o,
                unchanged: u
            }
        }

        function r(t) {
            return !(!t || e.isEmptyObject(t)) && !(t.filters && !t.filters.length)
        }

        function l(t, i) {
            var a, n = !1;
            return t.filters && (a = e.grep(t.filters, function(e) {
                return n = l(e, i), e.filters ? e.filters.length : e.field != i
            }), n || t.filters.length === a.length || (n = !0), t.filters = a), n
        }
        var o, u, c = window.kendo,
            d = c.ui,
            h = c._outerWidth,
            f = c._outerHeight,
            _ = d.Widget,
            p = c.keys,
            v = c.support,
            g = c.htmlEncode,
            m = c._activeElement,
            b = c.data.ObservableArray,
            x = "id",
            I = "change",
            S = "k-state-focused",
            w = "k-state-hover",
            y = "k-i-loading",
            T = "k-hidden",
            V = ".k-group-header",
            F = "_label",
            k = "open",
            D = "close",
            C = "cascade",
            H = "select",
            B = "selected",
            E = "requestStart",
            P = "requestEnd",
            L = "width",
            W = e.extend,
            q = e.proxy,
            A = e.isArray,
            G = v.browser,
            O = G.msie,
            N = O && G.version < 9,
            R = /"/g,
            U = {
                ComboBox: "DropDownList",
                DropDownList: "ComboBox"
            },
            M = c.ui.DataBoundWidget.extend({
                init: function(t, i) {
                    var a, n = this,
                        s = n.ns;
                    _.fn.init.call(n, t, i), t = n.element, i = n.options, n._isSelect = t.is(H), n._isSelect && n.element[0].length && (i.dataSource || (i.dataTextField = i.dataTextField || "text", i.dataValueField = i.dataValueField || "value")), n.ul = e('<ul unselectable="on" class="k-list k-reset"/>').attr({
                        tabIndex: -1,
                        "aria-hidden": !0
                    }), n.list = e("<div class='k-list-container'/>").append(n.ul).on("mousedown" + s, q(n._listMousedown, n)), a = t.attr(x), a && (n.list.attr(x, a + "-list"), n.ul.attr(x, a + "_listbox")), n._header(), n._noData(), n._footer(), n._accessors(), n._initValue()
                },
                options: {
                    valuePrimitive: !1,
                    footerTemplate: "",
                    headerTemplate: "",
                    noDataTemplate: "No data found."
                },
                setOptions: function(e) {
                    _.fn.setOptions.call(this, e), e && e.enable !== t && (e.enabled = e.enable), this._header(), this._noData(), this._footer(), this._renderFooter(), this._renderNoData()
                },
                focus: function() {
                    this._focused.focus()
                },
                readonly: function(e) {
                    this._editable({
                        readonly: e === t || e,
                        disable: !1
                    })
                },
                enable: function(e) {
                    this._editable({
                        readonly: !1,
                        disable: !(e = e === t || e)
                    })
                },
                _listOptions: function(t) {
                    var i = this,
                        a = i.options,
                        n = a.virtual,
                        s = {
                            change: q(i._listChange, i)
                        },
                        r = q(i._listBound, i);
                    return n = "object" == typeof n ? n : {}, t = e.extend({
                        autoBind: !1,
                        selectable: !0,
                        dataSource: i.dataSource,
                        click: q(i._click, i),
                        activate: q(i._activateItem, i),
                        deactivate: q(i._deactivateItem, i),
                        dataBinding: function() {
                            i.trigger("dataBinding")
                        },
                        dataBound: r,
                        height: a.height,
                        dataValueField: a.dataValueField,
                        dataTextField: a.dataTextField,
                        groupTemplate: a.groupTemplate,
                        fixedGroupTemplate: a.fixedGroupTemplate,
                        template: a.template
                    }, t, n, s), t.template || (t.template = "#:" + c.expr(t.dataTextField, "data") + "#"), a.$angular && (t.$angular = a.$angular), t
                },
                _initList: function() {
                    var e = this,
                        t = e._listOptions({
                            selectedItemChange: q(e._listChange, e)
                        });
                    e.listView = e.options.virtual ? new c.ui.VirtualList(e.ul, t) : new c.ui.StaticList(e.ul, t), e.listView.bind("listBound", q(e._listBound, e)), e._setListValue()
                },
                _setListValue: function(e) {
                    e = e || this.options.value, e !== t && this.listView.value(e).done(q(this._updateSelectionState, this))
                },
                _updateSelectionState: e.noop,
                _listMousedown: function(e) {
                    this.filterInput && this.filterInput[0] === e.target || e.preventDefault()
                },
                _isFilterEnabled: function() {
                    var e = this.options.filter;
                    return e && "none" !== e
                },
                _hideClear: function() {
                    var e = this;
                    e._clear && this._clear.addClass(T)
                },
                _showClear: function() {
                    var e = this;
                    e._clear && this._clear.removeClass(T)
                },
                _clearValue: function() {
                    this._clearText(), this._accessor(""), this.listView.value([]), this._isFilterEnabled() && !this.options.enforceMinLength && this._filter({
                        word: "",
                        open: !1
                    }), this._change()
                },
                _clearText: function() {
                    this.text("")
                },
                _clearFilter: function() {
                    this.options.virtual || this.listView.bound(!1), this._filterSource()
                },
                _filterSource: function(t, i) {
                    var a, n, s = this,
                        o = s.options,
                        u = s.dataSource,
                        c = W({}, u.filter() || {}),
                        d = t || c.filters && c.filters.length && !t,
                        h = l(c, o.dataTextField);
                    if (!t && !h || !s.trigger("filtering", {
                            filter: t
                        })) return a = {
                        filters: [],
                        logic: "and"
                    }, r(t) && e.trim(t.value).length && a.filters.push(t), r(c) && (a.logic === c.logic ? a.filters = a.filters.concat(c.filters) : a.filters.push(c)), s._cascading && this.listView.setDSFilter(a), n = W({}, {
                        page: d ? 1 : u.page(),
                        pageSize: d ? u.options.pageSize : u.pageSize(),
                        sort: u.sort(),
                        filter: u.filter(),
                        group: u.group(),
                        aggregate: u.aggregate()
                    }, {
                        filter: a
                    }), u[i ? "read" : "query"](u._mergeState(n))
                },
                _angularElement: function(e, t) {
                    e && this.angular(t, function() {
                        return {
                            elements: e
                        }
                    })
                },
                _noData: function() {
                    var i = e(this.noData),
                        a = this.options.noDataTemplate;
                    return this.angular("cleanup", function() {
                        return {
                            elements: i
                        }
                    }), c.destroy(i), i.remove(), a ? (this.noData = e('<div class="k-nodata" style="display:none"><div></div></div>').appendTo(this.list), this.noDataTemplate = "function" != typeof a ? c.template(a) : a, t) : (this.noData = null, t)
                },
                _renderNoData: function() {
                    var e = this.noData;
                    e && (this._angularElement(e, "cleanup"), e.children(":first").html(this.noDataTemplate({
                        instance: this
                    })), this._angularElement(e, "compile"))
                },
                _toggleNoData: function(t) {
                    e(this.noData).toggle(t)
                },
                _toggleHeader: function(e) {
                    var t = this.listView.content.prev(V);
                    t.toggle(e)
                },
                _footer: function() {
                    var i = e(this.footer),
                        a = this.options.footerTemplate;
                    return this._angularElement(i, "cleanup"), c.destroy(i), i.remove(), a ? (this.footer = e('<div class="k-footer"></div>').appendTo(this.list), this.footerTemplate = "function" != typeof a ? c.template(a) : a, t) : (this.footer = null, t)
                },
                _renderFooter: function() {
                    var e = this.footer;
                    e && (this._angularElement(e, "cleanup"), e.html(this.footerTemplate({
                        instance: this
                    })), this._angularElement(e, "compile"))
                },
                _header: function() {
                    var i, a = e(this.header),
                        n = this.options.headerTemplate;
                    return this._angularElement(a, "cleanup"), c.destroy(a), a.remove(), n ? (i = "function" != typeof n ? c.template(n) : n, a = e(i({})), this.header = a[0] ? a : null, this.list.prepend(a), this._angularElement(this.header, "compile"), t) : (this.header = null, t)
                },
                _allowOpening: function() {
                    return this.options.noDataTemplate || this.dataSource.flatView().length
                },
                _initValue: function() {
                    var e = this,
                        t = e.options.value;
                    null !== t ? e.element.val(t) : (t = e._accessor(), e.options.value = t), e._old = t
                },
                _ignoreCase: function() {
                    var e, t = this,
                        i = t.dataSource.reader.model;
                    i && i.fields && (e = i.fields[t.options.dataTextField], e && e.type && "string" !== e.type && (t.options.ignoreCase = !1))
                },
                _focus: function(e) {
                    return this.listView.focus(e)
                },
                _filter: function(e) {
                    var t = this,
                        i = t.options,
                        a = i.ignoreCase,
                        n = i.dataTextField,
                        s = {
                            value: a ? e.word.toLowerCase() : e.word,
                            field: n,
                            operator: i.filter,
                            ignoreCase: a
                        };
                    t._open = e.open, t._filterSource(s)
                },
                _clearButton: function() {
                    this._clear || (this._clear = e('<span unselectable="on" class="k-icon k-clear-value k-i-close" title="clear"></span>').attr({
                        role: "button",
                        tabIndex: -1
                    })), this.options.clearButton || this._clear.remove()
                },
                search: function(t) {
                    var i = this.options;
                    t = "string" == typeof t ? t : this._inputValue(), clearTimeout(this._typingTimeout), (!i.enforceMinLength && !t.length || t.length >= i.minLength) && (this._state = "filter", this._isFilterEnabled() ? (this.listView._emptySearch = !e.trim(t).length || !this.listView, this._filter({
                        word: t,
                        open: !0
                    })) : this._searchByWord(t))
                },
                current: function(e) {
                    return this._focus(e)
                },
                items: function() {
                    return this.ul[0].children
                },
                destroy: function() {
                    var e = this,
                        t = e.ns;
                    _.fn.destroy.call(e), e._unbindDataSource(), e.listView.destroy(), e.list.off(t), e.popup.destroy(), e._form && e._form.off("reset", e._resetHandler)
                },
                dataItem: function(i) {
                    var a = this;
                    if (i === t) return a.listView.selectedDataItems()[0];
                    if ("number" != typeof i) {
                        if (a.options.virtual) return a.dataSource.getByUid(e(i).data("uid"));
                        i = e(a.items()).index(i)
                    }
                    return a.dataSource.flatView()[i]
                },
                _activateItem: function() {
                    var e = this.listView.focus();
                    e && this._focused.add(this.filterInput).attr("aria-activedescendant", e.attr("id"))
                },
                _deactivateItem: function() {
                    this._focused.add(this.filterInput).removeAttr("aria-activedescendant")
                },
                _accessors: function() {
                    var e = this,
                        t = e.element,
                        i = e.options,
                        a = c.getter,
                        n = t.attr(c.attr("text-field")),
                        s = t.attr(c.attr("value-field"));
                    !i.dataTextField && n && (i.dataTextField = n), !i.dataValueField && s && (i.dataValueField = s), e._text = a(i.dataTextField), e._value = a(i.dataValueField)
                },
                _aria: function(e) {
                    var i = this,
                        a = i.options,
                        n = i._focused.add(i.filterInput);
                    a.suggest !== t && n.attr("aria-autocomplete", a.suggest ? "both" : "list"), e = e ? e + " " + i.ul[0].id : i.ul[0].id, n.attr("aria-owns", e), i.ul.attr("aria-live", i._isFilterEnabled() ? "polite" : "off"), i._ariaLabel()
                },
                _ariaLabel: function() {
                    var t, i = this,
                        a = i._focused,
                        n = i.element,
                        s = n.attr("id"),
                        r = e('label[for="' + s + '"]'),
                        l = n.attr("aria-label"),
                        o = n.attr("aria-labelledby");
                    a !== n && (l ? a.attr("aria-label", l) : o ? a.attr("aria-labelledby", o) : r.length && (t = r.attr("id") || i._generateLabelId(r, s), a.attr("aria-labelledby", t)))
                },
                _generateLabelId: function(e, t) {
                    var i = t + F;
                    return e.attr("id", i), i
                },
                _blur: function() {
                    var e = this;
                    e._change(), e.close()
                },
                _change: function() {
                    var e, a = this,
                        n = a.selectedIndex,
                        s = a.options.value,
                        r = a.value();
                    a._isSelect && !a.listView.bound() && s && (r = s), r !== i(a._old, typeof r) ? e = !0 : a._valueBeforeCascade !== t && a._valueBeforeCascade !== i(a._old, typeof a._valueBeforeCascade) && a._userTriggered ? e = !0 : n === t || n === a._oldIndex || a.listView.isFiltered() || (e = !0), e && (a._valueBeforeCascade = a._old = null === a._old || "" === r ? r : a.dataItem() ? a.dataItem()[a.options.dataValueField] : null, a._oldIndex = n, a._typing || a.element.trigger(I), a.trigger(I)), a.typing = !1
                },
                _data: function() {
                    return this.dataSource.view()
                },
                _enable: function() {
                    var e = this,
                        i = e.options,
                        a = e.element.is("[disabled]");
                    i.enable !== t && (i.enabled = i.enable), !i.enabled || a ? e.enable(!1) : e.readonly(e.element.is("[readonly]"))
                },
                _dataValue: function(e) {
                    var i = this._value(e);
                    return i === t && (i = this._text(e)), i
                },
                _offsetHeight: function() {
                    var t = 0,
                        i = this.listView.content.prevAll(":visible");
                    return i.each(function() {
                        var i = e(this);
                        t += f(i, !0)
                    }), t
                },
                _height: function(i) {
                    var a, n, s, r = this,
                        l = r.list,
                        o = r.options.height,
                        u = r.popup.visible();
                    if (i || r.options.noDataTemplate) {
                        if (n = l.add(l.parent(".k-animation-container")).show(), !l.is(":visible")) return n.hide(), t;
                        o = r.listView.content[0].scrollHeight > o ? o : "auto", n.height(o), "auto" !== o && (a = r._offsetHeight(), s = f(e(r.footer)) || 0, o = o - a - s), r.listView.content.height(o), u || n.hide()
                    }
                    return o
                },
                _adjustListWidth: function() {
                    var e, t, i = this.list,
                        a = i[0].style.width,
                        n = this.wrapper;
                    if (i.data(L) || !a) return e = window.getComputedStyle ? window.getComputedStyle(n[0], null) : 0, t = parseFloat(e && e.width) || h(n), e && G.msie && (t += parseFloat(e.paddingLeft) + parseFloat(e.paddingRight) + parseFloat(e.borderLeftWidth) + parseFloat(e.borderRightWidth)), a = "border-box" !== i.css("box-sizing") ? t - (h(i) - i.width()) : t, i.css({
                        fontFamily: n.css("font-family"),
                        width: this.options.autoWidth ? "auto" : a,
                        minWidth: a,
                        whiteSpace: this.options.autoWidth ? "nowrap" : "normal"
                    }).data(L, a), !0
                },
                _openHandler: function(e) {
                    this._adjustListWidth(), this.trigger(k) ? e.preventDefault() : (this._focused.attr("aria-expanded", !0), this.ul.attr("aria-hidden", !1))
                },
                _closeHandler: function(e) {
                    this.trigger(D) ? e.preventDefault() : (this._focused.attr("aria-expanded", !1), this.ul.attr("aria-hidden", !0))
                },
                _focusItem: function() {
                    var e = this.listView,
                        i = !e.focus(),
                        n = a(e.select());
                    n === t && this.options.highlightFirst && i && (n = 0), n !== t ? e.focus(n) : i && e.scrollToIndex(0)
                },
                _calculateGroupPadding: function(e) {
                    var t = this.ul.children(".k-first:first"),
                        i = this.listView.content.prev(V),
                        a = 0;
                    i[0] && "none" !== i[0].style.display && ("auto" !== e && (a = c.support.scrollbar()), a += parseFloat(t.css("border-right-width"), 10) + parseFloat(t.children(".k-group").css("padding-right"), 10), i.css("padding-right", a))
                },
                _calculatePopupHeight: function(e) {
                    var t = this._height(this.dataSource.flatView().length || e);
                    this._calculateGroupPadding(t)
                },
                _resizePopup: function(e) {
                    this.options.virtual || (this.popup.element.is(":visible") ? this._calculatePopupHeight(e) : this.popup.one("open", function(e) {
                        return q(function() {
                            this._calculatePopupHeight(e)
                        }, this)
                    }.call(this, e)))
                },
                _popup: function() {
                    var e = this;
                    e.popup = new d.Popup(e.list, W({}, e.options.popup, {
                        anchor: e.wrapper,
                        open: q(e._openHandler, e),
                        close: q(e._closeHandler, e),
                        animation: e.options.animation,
                        isRtl: v.isRtl(e.wrapper),
                        autosize: e.options.autoWidth
                    }))
                },
                _makeUnselectable: function() {
                    N && this.list.find("*").not(".k-textbox").attr("unselectable", "on")
                },
                _toggleHover: function(t) {
                    e(t.currentTarget).toggleClass(w, "mouseenter" === t.type)
                },
                _toggle: function(e, i) {
                    var a = this,
                        n = v.mobileOS && (v.touch || v.MSPointers || v.pointers);
                    e = e !== t ? e : !a.popup.visible(), i || n || a._focused[0] === m() || (a._prevent = !0, a._focused.focus(), a._prevent = !1), a[e ? k : D]()
                },
                _triggerCascade: function() {
                    var e = this;
                    e._cascadeTriggered && e.value() === i(e._cascadedValue, typeof e.value()) || (e._cascadedValue = e.value(), e._cascadeTriggered = !0, e.trigger(C, {
                        userTriggered: e._userTriggered
                    }))
                },
                _triggerChange: function() {
                    this._valueBeforeCascade !== this.value() && this.trigger(I)
                },
                _unbindDataSource: function() {
                    var e = this;
                    e.dataSource.unbind(E, e._requestStartHandler).unbind(P, e._requestEndHandler).unbind("error", e._errorHandler)
                },
                requireValueMapper: function(e, t) {
                    var i = (e.value instanceof Array ? e.value.length : e.value) || (t instanceof Array ? t.length : t);
                    if (i && e.virtual && "function" != typeof e.virtual.valueMapper) throw Error("ValueMapper is not provided while the value is being set. See http://docs.telerik.com/kendo-ui/controls/editors/combobox/virtualization#the-valuemapper-function")
                }
            });
        W(M, {
            inArray: function(e, t) {
                var i, a, n = t.children;
                if (!e || e.parentNode !== t) return -1;
                for (i = 0, a = n.length; i < a; i++)
                    if (e === n[i]) return i;
                return -1
            },
            unifyType: i
        }), c.ui.List = M, d.Select = M.extend({
            init: function(e, t) {
                M.fn.init.call(this, e, t), this._initial = this.element.val()
            },
            setDataSource: function(e) {
                var t, i = this;
                i.options.dataSource = e, i._dataSource(), i.listView.bound() && (i._initialIndex = null, i.listView._current = null), i.listView.setDataSource(i.dataSource), i.options.autoBind && i.dataSource.fetch(), t = i._parentWidget(), t && i._cascadeSelect(t)
            },
            close: function() {
                this.popup.close()
            },
            select: function(e) {
                var i = this;
                return e === t ? i.selectedIndex : i._select(e).done(function() {
                    i._cascadeValue = i._old = i._accessor(), i._oldIndex = i.selectedIndex
                })
            },
            _accessor: function(e, t) {
                return this[this._isSelect ? "_accessorSelect" : "_accessorInput"](e, t)
            },
            _accessorInput: function(e) {
                var i = this.element[0];
                return e === t ? i.value : (null === e && (e = ""), i.value = e, t)
            },
            _accessorSelect: function(e, i) {
                var a, s = this.element[0];
                return e === t ? n(s).value || "" : (n(s).selected = !1, i === t && (i = -1), a = null !== e && "" !== e, a && i == -1 ? this._custom(e) : e ? s.value = e : s.selectedIndex = i, t)
            },
            _syncValueAndText: function() {
                return !0
            },
            _custom: function(t) {
                var i = this,
                    a = i.element,
                    n = i._customOption;
                n || (n = e("<option/>"), i._customOption = n, a.append(n)), n.text(t), n[0].selected = !0
            },
            _hideBusy: function() {
                var e = this;
                clearTimeout(e._busy), e._arrowIcon.removeClass(y), e._focused.attr("aria-busy", !1), e._busy = null, e._showClear()
            },
            _showBusy: function(e) {
                var t = this;
                e.isDefaultPrevented() || (t._request = !0, t._busy || (t._busy = setTimeout(function() {
                    t._arrowIcon && (t._focused.attr("aria-busy", !0), t._arrowIcon.addClass(y), t._hideClear())
                }, 100)))
            },
            _requestEnd: function() {
                this._request = !1, this._hideBusy()
            },
            _dataSource: function() {
                var t, i = this,
                    a = i.element,
                    n = i.options,
                    s = n.dataSource || {};
                s = e.isArray(s) ? {
                    data: s
                } : s, i._isSelect && (t = a[0].selectedIndex, t > -1 && (n.index = t), s.select = a, s.fields = [{
                    field: n.dataTextField
                }, {
                    field: n.dataValueField
                }]), i.dataSource ? i._unbindDataSource() : (i._requestStartHandler = q(i._showBusy, i), i._requestEndHandler = q(i._requestEnd, i), i._errorHandler = q(i._hideBusy, i)), i.dataSource = c.data.DataSource.create(s).bind(E, i._requestStartHandler).bind(P, i._requestEndHandler).bind("error", i._errorHandler)
            },
            _firstItem: function() {
                this.listView.focusFirst()
            },
            _lastItem: function() {
                this.listView.focusLast()
            },
            _nextItem: function() {
                this.listView.focusNext()
            },
            _prevItem: function() {
                this.listView.focusPrev()
            },
            _move: function(e) {
                var i, a, n, s, r, l, o = this,
                    u = o.listView,
                    c = e.keyCode,
                    d = c === p.DOWN;
                if (c === p.UP || d) {
                    if (e.altKey) o.toggle(d);
                    else {
                        if (!u.bound() && !o.ul[0].firstChild) return o._fetch || (o.dataSource.one(I, function() {
                            o._fetch = !1, o._move(e)
                        }), o._fetch = !0, o._filterSource()), e.preventDefault(), !0;
                        if (n = o._focus(), o._fetch || n && !n.hasClass("k-state-selected") || (d ? (o._nextItem(), o._focus() || o._lastItem()) : (o._prevItem(), o._focus() || o._firstItem())), i = u.dataItemByIndex(u.getElementIndex(o._focus())), o.trigger(H, {
                                dataItem: i,
                                item: o._focus()
                            })) return o._focus(n), t;
                        o._select(o._focus(), !0).done(function() {
                            o.popup.visible() || o._blur(), o._cascadedValue = null === o._cascadedValue ? o.value() : o.dataItem() ? o.dataItem()[o.options.dataValueField] : null
                        })
                    }
                    e.preventDefault(), a = !0
                } else if (c === p.ENTER || c === p.TAB) {
                    if (o.popup.visible() && e.preventDefault(), n = o._focus(), i = o.dataItem(), o.popup.visible() || i && o.text() === o._text(i) || (n = null), s = o.filterInput && o.filterInput[0] === m(), n) {
                        if (i = u.dataItemByIndex(u.getElementIndex(n)), r = !0, i && (r = o._value(i) !== M.unifyType(o.value(), typeof o._value(i))), r && o.trigger(H, {
                                dataItem: i,
                                item: n
                            })) return;
                        o._select(n)
                    } else o.input && ((o._syncValueAndText() || o._isSelect) && o._accessor(o.input.val()), o.listView.value(o.input.val()));
                    o._focusElement && o._focusElement(o.wrapper), s && c === p.TAB ? o.wrapper.focusout() : o._blur(), o.close(), a = !0
                } else c === p.ESC ? (o.popup.visible() && e.preventDefault(), o.close(), a = !0) : !o.popup.visible() || c !== p.PAGEDOWN && c !== p.PAGEUP || (e.preventDefault(), l = c === p.PAGEDOWN ? 1 : -1, u.scrollWith(l * u.screenHeight()), a = !0);
                return a
            },
            _fetchData: function() {
                var e = this,
                    t = !!e.dataSource.view().length;
                e._request || e.options.cascadeFrom || e.listView.bound() || e._fetch || t || (e._fetch = !0, e.dataSource.fetch().done(function() {
                    e._fetch = !1
                }))
            },
            _options: function(e, i, a) {
                var s, r, l, o, u = this,
                    c = u.element,
                    d = c[0],
                    h = e.length,
                    f = "",
                    _ = 0;
                for (i && (f = i); _ < h; _++) s = "<option", r = e[_], l = u._text(r), o = u._value(r), o !== t && (o += "", o.indexOf('"') !== -1 && (o = o.replace(R, "&quot;")), s += ' value="' + o + '"'), s += ">", l !== t && (s += g(l)), s += "</option>", f += s;
                c.html(f), a !== t && (d.value = a, d.value && !a && (d.selectedIndex = -1)), d.selectedIndex !== -1 && (s = n(d), s && s.setAttribute(B, B))
            },
            _reset: function() {
                var t = this,
                    i = t.element,
                    a = i.attr("form"),
                    n = a ? e("#" + a) : i.closest("form");
                n[0] && (t._resetHandler = function() {
                    setTimeout(function() {
                        t.value(t._initial)
                    })
                }, t._form = n.on("reset", t._resetHandler))
            },
            _parentWidget: function() {
                var t, i, a = this.options.name;
                if (this.options.cascadeFrom) return t = e("#" + this.options.cascadeFrom), i = t.data("kendo" + a), i || (i = t.data("kendo" + U[a])), i
            },
            _cascade: function() {
                var e, t = this,
                    i = t.options,
                    a = i.cascadeFrom;
                if (a) {
                    if (e = t._parentWidget(), !e) return;
                    t._cascadeHandlerProxy = q(t._cascadeHandler, t), t._cascadeFilterRequests = [], i.autoBind = !1, e.bind("set", function() {
                        t.one("set", function(e) {
                            t._selectedValue = e.value || t._accessor()
                        })
                    }), e.first(C, t._cascadeHandlerProxy), e.listView.bound() ? (t._toggleCascadeOnFocus(), t._cascadeSelect(e)) : (e.one("dataBound", function() {
                        t._toggleCascadeOnFocus(), e.popup.visible() && e._focused.focus()
                    }), e.value() || t.enable(!1))
                }
            },
            _toggleCascadeOnFocus: function() {
                var e = this,
                    t = e._parentWidget(),
                    i = O ? "blur" : "focusout";
                t._focused.add(t.filterInput).bind("focus", function() {
                    t.unbind(C, e._cascadeHandlerProxy), t.first(I, e._cascadeHandlerProxy)
                }), t._focused.add(t.filterInput).bind(i, function() {
                    t.unbind(I, e._cascadeHandlerProxy), t.first(C, e._cascadeHandlerProxy)
                })
            },
            _cascadeHandler: function(e) {
                var t = this._parentWidget(),
                    i = this.value();
                this._userTriggered = e.userTriggered, this.listView.bound() && this._clearSelection(t, !0), this._cascadeSelect(t, i)
            },
            _cascadeChange: function(e) {
                var t = this,
                    i = t._accessor() || t._selectedValue;
                t._cascadeFilterRequests.length || (t._selectedValue = null), t._userTriggered ? t._clearSelection(e, !0) : i ? (i !== t.listView.value()[0] && t.value(i), t.dataSource.view()[0] && t.selectedIndex !== -1 || t._clearSelection(e, !0)) : t.dataSource.flatView().length && t.select(t.options.index), t.enable(), t._triggerCascade(), t._triggerChange(), t._userTriggered = !1
            },
            _cascadeSelect: function(e, i) {
                var a, n, s = this,
                    r = e.dataItem(),
                    o = r ? r[s.options.cascadeFromField] || e._value(r) : null,
                    u = s.options.cascadeFromField || e.options.dataValueField;
                s._valueBeforeCascade = i !== t ? i : s.value(), o || 0 === o ? (a = s.dataSource.filter() || {}, l(a, u), n = function() {
                    var t = s._cascadeFilterRequests.shift();
                    t && s.unbind("dataBound", t), t = s._cascadeFilterRequests[0], t && s.first("dataBound", t), s._cascadeChange(e)
                }, s._cascadeFilterRequests.push(n), 1 === s._cascadeFilterRequests.length && s.first("dataBound", n), s._cascading = !0, s._filterSource({
                    field: u,
                    operator: "eq",
                    value: o
                }), s._cascading = !1) : (s.enable(!1), s._clearSelection(e), s._triggerCascade(), s._triggerChange(), s._userTriggered = !1)
            }
        }), o = ".StaticList", u = c.ui.DataBoundWidget.extend({
            init: function(t, i) {
                _.fn.init.call(this, t, i), this.element.attr("role", "listbox").on("click" + o, "li", q(this._click, this)).on("mouseenter" + o, "li", function() {
                    e(this).addClass(w)
                }).on("mouseleave" + o, "li", function() {
                    e(this).removeClass(w)
                }), "multiple" === this.options.selectable && this.element.attr("aria-multiselectable", !0), this.content = this.element.wrap("<div class='k-list-scroller' unselectable='on'></div>").parent(), this.header = this.content.before('<div class="k-group-header" style="display:none"></div>').prev(), this.bound(!1), this._optionID = c.guid(), this._selectedIndices = [], this._view = [], this._dataItems = [], this._values = [];
                var a = this.options.value;
                a && (this._values = e.isArray(a) ? a.slice(0) : [a]), this._getter(), this._templates(), this.setDataSource(this.options.dataSource), this._onScroll = q(function() {
                    var e = this;
                    clearTimeout(e._scrollId), e._scrollId = setTimeout(function() {
                        e._renderHeader()
                    }, 50)
                }, this)
            },
            options: {
                name: "StaticList",
                dataValueField: null,
                valuePrimitive: !1,
                selectable: !0,
                template: null,
                groupTemplate: null,
                fixedGroupTemplate: null
            },
            events: ["click", I, "activate", "deactivate", "dataBinding", "dataBound", "selectedItemChange"],
            setDataSource: function(t) {
                var i, a = this,
                    n = t || {};
                n = e.isArray(n) ? {
                    data: n
                } : n, n = c.data.DataSource.create(n), a.dataSource ? (a.dataSource.unbind(I, a._refreshHandler), i = a.value(), a.value([]), a.bound(!1), a.value(i)) : a._refreshHandler = q(a.refresh, a), a.setDSFilter(n.filter()), a.dataSource = n.bind(I, a._refreshHandler), a._fixedHeader()
            },
            skip: function() {
                return this.dataSource.skip()
            },
            setOptions: function(e) {
                _.fn.setOptions.call(this, e), this._getter(), this._templates(), this._render()
            },
            destroy: function() {
                this.element.off(o), this._refreshHandler && this.dataSource.unbind(I, this._refreshHandler), clearTimeout(this._scrollId), _.fn.destroy.call(this)
            },
            dataItemByIndex: function(e) {
                return this.dataSource.flatView()[e]
            },
            screenHeight: function() {
                return this.content[0].clientHeight
            },
            scrollToIndex: function(e) {
                var t = this.element[0].children[e];
                t && this.scroll(t)
            },
            scrollWith: function(e) {
                this.content.scrollTop(this.content.scrollTop() + e)
            },
            scroll: function(e) {
                if (e) {
                    e[0] && (e = e[0]);
                    var t = this.content[0],
                        i = e.offsetTop,
                        a = e.offsetHeight,
                        n = t.scrollTop,
                        s = t.clientHeight,
                        r = i + a;
                    n > i ? n = i : r > n + s && (n = r - s), t.scrollTop = n
                }
            },
            selectedDataItems: function(e) {
                return e === t ? this._dataItems.slice() : (this._dataItems = e, this._values = this._getValues(e), t)
            },
            _getValues: function(t) {
                var i = this._valueGetter;
                return e.map(t, function(e) {
                    return i(e)
                })
            },
            focusNext: function() {
                var e = this.focus();
                e = e ? e.next() : 0, this.focus(e)
            },
            focusPrev: function() {
                var e = this.focus();
                e = e ? e.prev() : this.element[0].children.length - 1, this.focus(e)
            },
            focusFirst: function() {
                this.focus(this.element[0].children[0])
            },
            focusLast: function() {
                this.focus(a(this.element[0].children))
            },
            focus: function(i) {
                var n, s = this,
                    r = s._optionID;
                return i === t ? s._current : (i = a(s._get(i)), i = e(this.element[0].children[i]), s._current && (s._current.removeClass(S).removeAttr(x), s.trigger("deactivate")), n = !!i[0], n && (i.addClass(S), s.scroll(i), i.attr("id", r)), s._current = n ? i : null, s.trigger("activate"), t)
            },
            focusIndex: function() {
                return this.focus() ? this.focus().index() : t
            },
            skipUpdate: function(e) {
                this._skipUpdate = e
            },
            select: function(i) {
                var n, s, r, l = this,
                    o = l.options.selectable,
                    u = "multiple" !== o && o !== !1,
                    c = l._selectedIndices,
                    d = [],
                    h = [];
                return i === t ? c.slice() : (i = l._get(i), 1 === i.length && i[0] === -1 && (i = []), s = e.Deferred().resolve(), r = l.isFiltered(), r && !u && l._deselectFiltered(i) ? s : u && !r && e.inArray(a(i), c) !== -1 ? (l._dataItems.length && l._view.length && (l._dataItems = [l._view[c[0]].item]), s) : (n = l._deselect(i), h = n.removed, i = n.indices, i.length && (u && (i = [a(i)]), d = l._select(i)), (d.length || h.length) && (l._valueComparer = null, l.trigger(I, {
                    added: d,
                    removed: h
                })), s))
            },
            removeAt: function(e) {
                return this._selectedIndices.splice(e, 1), this._values.splice(e, 1), this._valueComparer = null, {
                    position: e,
                    dataItem: this._dataItems.splice(e, 1)[0]
                }
            },
            setValue: function(t) {
                t = e.isArray(t) || t instanceof b ? t.slice(0) : [t], this._values = t, this._valueComparer = null
            },
            value: function(i) {
                var a, n = this,
                    s = n._valueDeferred;
                return i === t ? n._values.slice() : (n.setValue(i), s && "resolved" !== s.state() || (n._valueDeferred = s = e.Deferred()), n.bound() && (a = n._valueIndices(n._values), "multiple" === n.options.selectable && n.select(-1), n.select(a), s.resolve()), n._skipUpdate = !1, s)
            },
            items: function() {
                return this.element.children(".k-item")
            },
            _click: function(t) {
                t.isDefaultPrevented() || this.trigger("click", {
                    item: e(t.currentTarget)
                }) || this.select(t.currentTarget)
            },
            _valueExpr: function(e, t) {
                var a, n, s = this,
                    r = 0,
                    l = [];
                if (!s._valueComparer || s._valueType !== e) {
                    for (s._valueType = e; r < t.length; r++) l.push(i(t[r], e));
                    a = "for (var idx = 0; idx < " + l.length + "; idx++) { if (current === values[idx]) {   return idx; }} return -1;", n = Function("current", "values", a), s._valueComparer = function(e) {
                        return n(e, l)
                    }
                }
                return s._valueComparer
            },
            _dataItemPosition: function(e, t) {
                var i = this._valueGetter(e),
                    a = this._valueExpr(typeof i, t);
                return a(i)
            },
            _getter: function() {
                this._valueGetter = c.getter(this.options.dataValueField)
            },
            _deselect: function(t) {
                var i, a, n, s = this,
                    r = s.element[0].children,
                    l = s.options.selectable,
                    o = s._selectedIndices,
                    u = s._dataItems,
                    c = s._values,
                    d = [],
                    h = 0,
                    f = 0;
                if (t = t.slice(), l !== !0 && t.length) {
                    if ("multiple" === l)
                        for (; h < t.length; h++)
                            if (a = t[h], e(r[a]).hasClass("k-state-selected"))
                                for (i = 0; i < o.length; i++)
                                    if (n = o[i], n === a) {
                                        e(r[n]).removeClass("k-state-selected").attr("aria-selected", !1), d.push({
                                            position: i + f,
                                            dataItem: u.splice(i, 1)[0]
                                        }), o.splice(i, 1), t.splice(h, 1), c.splice(i, 1), f += 1, h -= 1, i -= 1;
                                        break
                                    }
                } else {
                    for (; h < o.length; h++) e(r[o[h]]).removeClass("k-state-selected").attr("aria-selected", !1), d.push({
                        position: h,
                        dataItem: u[h]
                    });
                    s._values = [], s._dataItems = [], s._selectedIndices = []
                }
                return {
                    indices: t,
                    removed: d
                }
            },
            _deselectFiltered: function(t) {
                for (var i, a, n, s = this.element[0].children, r = [], l = 0; l < t.length; l++) a = t[l], i = this._view[a].item, n = this._dataItemPosition(i, this._values), n > -1 && (r.push(this.removeAt(n)), e(s[a]).removeClass("k-state-selected"));
                return !!r.length && (this.trigger(I, {
                    added: [],
                    removed: r
                }), !0)
            },
            _select: function(t) {
                var i, n, s = this,
                    r = s.element[0].children,
                    l = s._view,
                    o = [],
                    u = 0;
                for (a(t) !== -1 && s.focus(t); u < t.length; u++) n = t[u], i = l[n], n !== -1 && i && (i = i.item, s._selectedIndices.push(n), s._dataItems.push(i), s._values.push(s._valueGetter(i)), e(r[n]).addClass("k-state-selected").attr("aria-selected", !0), o.push({
                    dataItem: i
                }));
                return o
            },
            getElementIndex: function(t) {
                return e(t).data("offset-index")
            },
            _get: function(e) {
                return "number" == typeof e ? e = [e] : A(e) || (e = this.getElementIndex(e), e = [e !== t ? e : -1]), e
            },
            _template: function() {
                var e = this,
                    t = e.options,
                    i = t.template;
                return i ? (i = c.template(i), i = function(e) {
                    return '<li tabindex="-1" role="option" unselectable="on" class="k-item">' + i(e) + "</li>"
                }) : i = c.template('<li tabindex="-1" role="option" unselectable="on" class="k-item">${' + c.expr(t.dataTextField, "data") + "}</li>", {
                    useWithBlock: !1
                }), i
            },
            _templates: function() {
                var e, t, i = this.options,
                    a = {
                        template: i.template,
                        groupTemplate: i.groupTemplate,
                        fixedGroupTemplate: i.fixedGroupTemplate
                    };
                for (t in a) e = a[t], e && "function" != typeof e && (a[t] = c.template(e));
                this.templates = a
            },
            _normalizeIndices: function(e) {
                for (var i = [], a = 0; a < e.length; a++) e[a] !== t && i.push(e[a]);
                return i
            },
            _valueIndices: function(e, t) {
                var i, a = this._view,
                    n = 0;
                if (t = t ? t.slice() : [], !e.length) return [];
                for (; n < a.length; n++) i = this._dataItemPosition(a[n].item, e), i !== -1 && (t[i] = n);
                return this._normalizeIndices(t)
            },
            _firstVisibleItem: function() {
                for (var t = this.element[0], i = this.content[0], a = i.scrollTop, n = e(t.children[0]).height(), s = Math.floor(a / n) || 0, r = t.children[s] || t.lastChild, l = r.offsetTop < a; r;)
                    if (l) {
                        if (r.offsetTop + n > a || !r.nextSibling) break;
                        r = r.nextSibling
                    } else {
                        if (r.offsetTop <= a || !r.previousSibling) break;
                        r = r.previousSibling
                    }
                return this._view[e(r).data("offset-index")]
            },
            _fixedHeader: function() {
                this.isGrouped() && this.templates.fixedGroupTemplate ? (this.header.show(), this.content.scroll(this._onScroll)) : (this.header.hide(), this.content.off("scroll", this._onScroll))
            },
            _renderHeader: function() {
                var e, t = this.templates.fixedGroupTemplate;
                t && (e = this._firstVisibleItem(), e && e.group && this.header.html(t(e.group)))
            },
            _renderItem: function(e) {
                var t = '<li tabindex="-1" role="option" unselectable="on" class="k-item',
                    i = e.item,
                    a = 0 !== e.index,
                    n = e.selected;
                return a && e.newGroup && (t += " k-first"), n && (t += " k-state-selected"), t += '" aria-selected="' + (n ? "true" : "false") + '" data-offset-index="' + e.index + '">', t += this.templates.template(i), a && e.newGroup && (t += '<div class="k-group">' + this.templates.groupTemplate(e.group) + "</div>"), t + "</li>"
            },
            _render: function() {
                var e, t, i, a, n = "",
                    s = 0,
                    r = 0,
                    l = [],
                    o = this.dataSource.view(),
                    u = this.value(),
                    c = this.isGrouped();
                if (c)
                    for (s = 0; s < o.length; s++)
                        for (t = o[s], i = !0, a = 0; a < t.items.length; a++) e = {
                            selected: this._selected(t.items[a], u),
                            item: t.items[a],
                            group: t.value,
                            newGroup: i,
                            index: r
                        }, l[r] = e, r += 1, n += this._renderItem(e), i = !1;
                else
                    for (s = 0; s < o.length; s++) e = {
                        selected: this._selected(o[s], u),
                        item: o[s],
                        index: s
                    }, l[s] = e, n += this._renderItem(e);
                this._view = l, this.element[0].innerHTML = n, c && l.length && this._renderHeader()
            },
            _selected: function(e, t) {
                var i = !this.isFiltered() || "multiple" === this.options.selectable;
                return i && this._dataItemPosition(e, t) !== -1
            },
            setDSFilter: function(e) {
                this._lastDSFilter = W({}, e)
            },
            isFiltered: function() {
                return this._lastDSFilter || this.setDSFilter(this.dataSource.filter()), !c.data.Query.compareFilters(this.dataSource.filter(), this._lastDSFilter)
            },
            refresh: function(e) {
                var t, i = this,
                    a = e && e.action,
                    n = i.options.skipUpdateOnBind,
                    r = "itemchange" === a;
                i.trigger("dataBinding"), i._angularItems("cleanup"), i._fixedHeader(), i._render(), i.bound(!0), r || "remove" === a ? (t = s(i._dataItems, e.items), t.changed.length && (r ? i.trigger("selectedItemChange", {
                    items: t.changed
                }) : i.value(i._getValues(t.unchanged)))) : i.isFiltered() || i._skipUpdate || i._emptySearch ? (i.focus(0), i._skipUpdate && (i._skipUpdate = !1, i._selectedIndices = i._valueIndices(i._values, i._selectedIndices))) : n || a && "add" !== a || i.value(i._values), i._valueDeferred && i._valueDeferred.resolve(), i._angularItems("compile"), i.trigger("dataBound")
            },
            bound: function(e) {
                return e === t ? this._bound : (this._bound = e, t)
            },
            isGrouped: function() {
                return (this.dataSource.group() || []).length
            }
        }), d.plugin(u)
    }(window.kendo.jQuery), window.kendo
}, "function" == typeof define && define.amd ? define : function(e, t, i) {
    (i || t)()
});
//# sourceMappingURL=kendo.list.min.js.map;
/** 
 * Kendo UI v2018.1.221 (http://www.telerik.com/kendo-ui)                                                                                                                                               
 * Copyright 2018 Telerik AD. All rights reserved.                                                                                                                                                      
 *                                                                                                                                                                                                      
 * Kendo UI commercial licenses may be obtained at                                                                                                                                                      
 * http://www.telerik.com/purchase/license-agreement/kendo-ui-complete                                                                                                                                  
 * If you do not own a commercial license, this file shall be governed by the trial license terms.                                                                                                      

*/
! function(t, define) {
    define("kendo.fx.min", ["kendo.core.min"], t)
}(function() {
    return function(t, e) {
        function i(t) {
            return parseInt(t, 10)
        }

        function r(t, e) {
            return i(t.css(e))
        }

        function n(t) {
            var e, i = [];
            for (e in t) i.push(e);
            return i
        }

        function s(t) {
            for (var e in t) L.indexOf(e) != -1 && Q.indexOf(e) == -1 && delete t[e];
            return t
        }

        function o(t, e) {
            var i, r, n, s, o = [],
                a = {};
            for (r in e) i = r.toLowerCase(), s = H && L.indexOf(i) != -1, !E.hasHW3D && s && Q.indexOf(i) == -1 ? delete e[r] : (n = e[r], s ? o.push(r + "(" + n + ")") : a[r] = n);
            return o.length && (a[at] = o.join(" ")), a
        }

        function a(t, e) {
            var r, n, s;
            return H ? (r = t.css(at), r == J ? "scale" == e ? 1 : 0 : (n = r.match(RegExp(e + "\\s*\\(([\\d\\w\\.]+)")), s = 0, n ? s = i(n[1]) : (n = r.match(S) || [0, 0, 0, 0, 0], e = e.toLowerCase(), V.test(e) ? s = parseFloat(n[3] / n[2]) : "translatey" == e ? s = parseFloat(n[4] / n[2]) : "scale" == e ? s = parseFloat(n[2]) : "rotate" == e && (s = parseFloat(Math.atan2(n[2], n[1])))), s)) : parseFloat(t.css(e))
        }

        function c(t) {
            return t.charAt(0).toUpperCase() + t.substring(1)
        }

        function l(t, e) {
            var i = h.extend(e),
                r = i.prototype.directions;
            T[c(t)] = i, T.Element.prototype[t] = function(t, e, r, n) {
                return new i(this.element, t, e, r, n)
            }, N(r, function(e, r) {
                T.Element.prototype[t + c(r)] = function(t, e, n) {
                    return new i(this.element, r, t, e, n)
                }
            })
        }

        function d(t, i, r, n) {
            l(t, {
                directions: v,
                startValue: function(t) {
                    return this._startValue = t, this
                },
                endValue: function(t) {
                    return this._endValue = t, this
                },
                shouldHide: function() {
                    return this._shouldHide
                },
                prepare: function(t, s) {
                    var o, a, c = this,
                        l = "out" === this._direction,
                        d = c.element.data(i),
                        u = !(isNaN(d) || d == r);
                    o = u ? d : e !== this._startValue ? this._startValue : l ? r : n, a = e !== this._endValue ? this._endValue : l ? n : r, this._reverse ? (t[i] = a, s[i] = o) : (t[i] = o, s[i] = a), c._shouldHide = s[i] === n
                }
            })
        }

        function u(t, e) {
            var i = C.directions[e].vertical,
                r = t[i ? Y : X]() / 2 + "px";
            return _[e].replace("$size", r)
        }
        var f, p, h, m, v, x, _, g, y, k, b, w, C = window.kendo,
            T = C.effects,
            N = t.each,
            P = t.extend,
            z = t.proxy,
            E = C.support,
            R = E.browser,
            H = E.transforms,
            D = E.transitions,
            O = {
                scale: 0,
                scalex: 0,
                scaley: 0,
                scale3d: 0
            },
            F = {
                translate: 0,
                translatex: 0,
                translatey: 0,
                translate3d: 0
            },
            I = e !== document.documentElement.style.zoom && !H,
            S = /matrix3?d?\s*\(.*,\s*([\d\.\-]+)\w*?,\s*([\d\.\-]+)\w*?,\s*([\d\.\-]+)\w*?,\s*([\d\.\-]+)\w*?/i,
            A = /^(-?[\d\.\-]+)?[\w\s]*,?\s*(-?[\d\.\-]+)?[\w\s]*/i,
            V = /translatex?$/i,
            q = /(zoom|fade|expand)(\w+)/,
            M = /(zoom|fade|expand)/,
            $ = /[xy]$/i,
            L = ["perspective", "rotate", "rotatex", "rotatey", "rotatez", "rotate3d", "scale", "scalex", "scaley", "scalez", "scale3d", "skew", "skewx", "skewy", "translate", "translatex", "translatey", "translatez", "translate3d", "matrix", "matrix3d"],
            Q = ["rotate", "scale", "scalex", "scaley", "skew", "skewx", "skewy", "translate", "translatex", "translatey", "matrix"],
            W = {
                rotate: "deg",
                scale: "",
                skew: "px",
                translate: "px"
            },
            j = H.css,
            B = Math.round,
            U = "",
            G = "px",
            J = "none",
            K = "auto",
            X = "width",
            Y = "height",
            Z = "hidden",
            tt = "origin",
            et = "abortId",
            it = "overflow",
            rt = "translate",
            nt = "position",
            st = "completeCallback",
            ot = j + "transition",
            at = j + "transform",
            ct = j + "backface-visibility",
            lt = j + "perspective",
            dt = "1500px",
            ut = "perspective(" + dt + ")",
            ft = {
                left: {
                    reverse: "right",
                    property: "left",
                    transition: "translatex",
                    vertical: !1,
                    modifier: -1
                },
                right: {
                    reverse: "left",
                    property: "left",
                    transition: "translatex",
                    vertical: !1,
                    modifier: 1
                },
                down: {
                    reverse: "up",
                    property: "top",
                    transition: "translatey",
                    vertical: !0,
                    modifier: 1
                },
                up: {
                    reverse: "down",
                    property: "top",
                    transition: "translatey",
                    vertical: !0,
                    modifier: -1
                },
                top: {
                    reverse: "bottom"
                },
                bottom: {
                    reverse: "top"
                },
                "in": {
                    reverse: "out",
                    modifier: -1
                },
                out: {
                    reverse: "in",
                    modifier: 1
                },
                vertical: {
                    reverse: "vertical"
                },
                horizontal: {
                    reverse: "horizontal"
                }
            };
        C.directions = ft, P(t.fn, {
            kendoStop: function(t, e) {
                return D ? T.stopQueue(this, t || !1, e || !1) : this.stop(t, e)
            }
        }), H && !D && (N(Q, function(i, r) {
            t.fn[r] = function(i) {
                if (e === i) return a(this, r);
                var n = t(this)[0],
                    s = r + "(" + i + W[r.replace($, "")] + ")";
                return n.style.cssText.indexOf(at) == -1 ? t(this).css(at, s) : n.style.cssText = n.style.cssText.replace(RegExp(r + "\\(.*?\\)", "i"), s), this
            }, t.fx.step[r] = function(e) {
                t(e.elem)[r](e.now)
            }
        }), f = t.fx.prototype.cur, t.fx.prototype.cur = function() {
            return Q.indexOf(this.prop) != -1 ? parseFloat(t(this.elem)[this.prop]()) : f.apply(this, arguments)
        }), C.toggleClass = function(t, e, i, r) {
            return e && (e = e.split(" "), D && (i = P({
                exclusive: "all",
                duration: 400,
                ease: "ease-out"
            }, i), t.css(ot, i.exclusive + " " + i.duration + "ms " + i.ease), setTimeout(function() {
                t.css(ot, "").css(Y)
            }, i.duration)), N(e, function(e, i) {
                t.toggleClass(i, r)
            })), t
        }, C.parseEffects = function(t, e) {
            var i = {};
            return "string" == typeof t ? N(t.split(" "), function(t, r) {
                var n = !M.test(r),
                    s = r.replace(q, function(t, e, i) {
                        return e + ":" + i.toLowerCase()
                    }),
                    o = s.split(":"),
                    a = o[1],
                    c = {};
                o.length > 1 && (c.direction = e && n ? ft[a].reverse : a), i[o[0]] = c
            }) : N(t, function(t) {
                var r = this.direction;
                r && e && !M.test(t) && (this.direction = ft[r].reverse), i[t] = this
            }), i
        }, D && P(T, {
            transition: function(e, i, r) {
                var s, a, c, l, d = 0,
                    u = e.data("keys") || [];
                r = P({
                    duration: 200,
                    ease: "ease-out",
                    complete: null,
                    exclusive: "all"
                }, r), c = !1, l = function() {
                    c || (c = !0, a && (clearTimeout(a), a = null), e.removeData(et).dequeue().css(ot, "").css(ot), r.complete.call(e))
                }, r.duration = t.fx ? t.fx.speeds[r.duration] || r.duration : r.duration, s = o(e, i), t.merge(u, n(s)), e.data("keys", t.unique(u)).height(), e.css(ot, r.exclusive + " " + r.duration + "ms " + r.ease).css(ot), e.css(s).css(at), D.event && (e.one(D.event, l), 0 !== r.duration && (d = 500)), a = setTimeout(l, r.duration + d), e.data(et, a), e.data(st, l)
            },
            stopQueue: function(t, e, i) {
                var r, n = t.data("keys"),
                    s = !i && n,
                    o = t.data(st);
                return s && (r = C.getComputedStyles(t[0], n)), o && o(), s && t.css(r), t.removeData("keys").stop(e)
            }
        }), p = C.Class.extend({
            init: function(t, e) {
                var i = this;
                i.element = t, i.effects = [], i.options = e, i.restore = []
            },
            run: function(e) {
                var i, r, n, a, c, l, d, u = this,
                    f = e.length,
                    p = u.element,
                    h = u.options,
                    m = t.Deferred(),
                    v = {},
                    x = {};
                for (u.effects = e, m.then(t.proxy(u, "complete")), p.data("animating", !0), r = 0; r < f; r++)
                    for (i = e[r], i.setReverse(h.reverse), i.setOptions(h), u.addRestoreProperties(i.restore), i.prepare(v, x), c = i.children(), n = 0, l = c.length; n < l; n++) c[n].duration(h.duration).run();
                for (d in h.effects) P(x, h.effects[d].properties);
                for (p.is(":visible") || P(v, {
                        display: p.data("olddisplay") || "block"
                    }), H && !h.reset && (a = p.data("targetTransform"), a && (v = P(a, v))), v = o(p, v), H && !D && (v = s(v)), p.css(v).css(at), r = 0; r < f; r++) e[r].setup();
                return h.init && h.init(), p.data("targetTransform", x), T.animate(p, x, P({}, h, {
                    complete: m.resolve
                })), m.promise()
            },
            stop: function() {
                t(this.element).kendoStop(!0, !0)
            },
            addRestoreProperties: function(t) {
                for (var e, i = this.element, r = 0, n = t.length; r < n; r++) e = t[r], this.restore.push(e), i.data(e) || i.data(e, i.css(e))
            },
            restoreCallback: function() {
                var t, e, i, r = this.element;
                for (t = 0, e = this.restore.length; t < e; t++) i = this.restore[t], r.css(i, r.data(i))
            },
            complete: function() {
                var e = this,
                    i = 0,
                    r = e.element,
                    n = e.options,
                    s = e.effects,
                    o = s.length;
                for (r.removeData("animating").dequeue(), n.hide && r.data("olddisplay", r.css("display")).hide(), this.restoreCallback(), I && !H && setTimeout(t.proxy(this, "restoreCallback"), 0); i < o; i++) s[i].teardown();
                n.completeCallback && n.completeCallback(r)
            }
        }), T.promise = function(t, e) {
            var i, r, n, s = [],
                o = new p(t, e),
                a = C.parseEffects(e.effects);
            e.effects = a;
            for (n in a) i = T[c(n)], i && (r = new i(t, a[n].direction), s.push(r));
            s[0] ? o.run(s) : (t.is(":visible") || t.css({
                display: t.data("olddisplay") || "block"
            }).css("display"), e.init && e.init(), t.dequeue(), o.complete())
        }, P(T, {
            animate: function(i, n, o) {
                var a = o.transition !== !1;
                delete o.transition, D && "transition" in T && a ? T.transition(i, n, o) : H ? i.animate(s(n), {
                    queue: !1,
                    show: !1,
                    hide: !1,
                    duration: o.duration,
                    complete: o.complete
                }) : i.each(function() {
                    var i = t(this),
                        s = {};
                    N(L, function(t, o) {
                        var a, c, l, d, u, f, p, h = n ? n[o] + " " : null;
                        h && (c = n, o in O && n[o] !== e ? (a = h.match(A), H && P(c, {
                            scale: +a[0]
                        })) : o in F && n[o] !== e && (l = i.css(nt), d = "absolute" == l || "fixed" == l, i.data(rt) || (d ? i.data(rt, {
                            top: r(i, "top") || 0,
                            left: r(i, "left") || 0,
                            bottom: r(i, "bottom"),
                            right: r(i, "right")
                        }) : i.data(rt, {
                            top: r(i, "marginTop") || 0,
                            left: r(i, "marginLeft") || 0
                        })), u = i.data(rt), a = h.match(A), a && (f = o == rt + "y" ? 0 : +a[1], p = o == rt + "y" ? +a[1] : +a[2], d ? (isNaN(u.right) ? isNaN(f) || P(c, {
                            left: u.left + f
                        }) : isNaN(f) || P(c, {
                            right: u.right - f
                        }), isNaN(u.bottom) ? isNaN(p) || P(c, {
                            top: u.top + p
                        }) : isNaN(p) || P(c, {
                            bottom: u.bottom - p
                        })) : (isNaN(f) || P(c, {
                            marginLeft: u.left + f
                        }), isNaN(p) || P(c, {
                            marginTop: u.top + p
                        })))), !H && "scale" != o && o in c && delete c[o], c && P(s, c))
                    }), R.msie && delete s.scale, i.animate(s, {
                        queue: !1,
                        show: !1,
                        hide: !1,
                        duration: o.duration,
                        complete: o.complete
                    })
                })
            }
        }), T.animatedPromise = T.promise, h = C.Class.extend({
            init: function(t, e) {
                var i = this;
                i.element = t, i._direction = e, i.options = {}, i._additionalEffects = [], i.restore || (i.restore = [])
            },
            reverse: function() {
                return this._reverse = !0, this.run()
            },
            play: function() {
                return this._reverse = !1, this.run()
            },
            add: function(t) {
                return this._additionalEffects.push(t), this
            },
            direction: function(t) {
                return this._direction = t, this
            },
            duration: function(t) {
                return this._duration = t, this
            },
            compositeRun: function() {
                var t = this,
                    e = new p(t.element, {
                        reverse: t._reverse,
                        duration: t._duration
                    }),
                    i = t._additionalEffects.concat([t]);
                return e.run(i)
            },
            run: function() {
                if (this._additionalEffects && this._additionalEffects[0]) return this.compositeRun();
                var e, i, r = this,
                    n = r.element,
                    a = 0,
                    c = r.restore,
                    l = c.length,
                    d = t.Deferred(),
                    u = {},
                    f = {},
                    p = r.children(),
                    h = p.length;
                for (d.then(t.proxy(r, "_complete")), n.data("animating", !0), a = 0; a < l; a++) e = c[a], n.data(e) || n.data(e, n.css(e));
                for (a = 0; a < h; a++) p[a].duration(r._duration).run();
                return r.prepare(u, f), n.is(":visible") || P(u, {
                    display: n.data("olddisplay") || "block"
                }), H && (i = n.data("targetTransform"), i && (u = P(i, u))), u = o(n, u), H && !D && (u = s(u)), n.css(u).css(at), r.setup(), n.data("targetTransform", f), T.animate(n, f, {
                    duration: r._duration,
                    complete: d.resolve
                }), d.promise()
            },
            stop: function() {
                var e = 0,
                    i = this.children(),
                    r = i.length;
                for (e = 0; e < r; e++) i[e].stop();
                return t(this.element).kendoStop(!0, !0), this
            },
            restoreCallback: function() {
                var t, e, i, r = this.element;
                for (t = 0, e = this.restore.length; t < e; t++) i = this.restore[t], r.css(i, r.data(i))
            },
            _complete: function() {
                var e = this,
                    i = e.element;
                i.removeData("animating").dequeue(), e.restoreCallback(), e.shouldHide() && i.data("olddisplay", i.css("display")).hide(), I && !H && setTimeout(t.proxy(e, "restoreCallback"), 0), e.teardown()
            },
            setOptions: function(t) {
                P(!0, this.options, t)
            },
            children: function() {
                return []
            },
            shouldHide: t.noop,
            setup: t.noop,
            prepare: t.noop,
            teardown: t.noop,
            directions: [],
            setReverse: function(t) {
                return this._reverse = t, this
            }
        }), m = ["left", "right", "up", "down"], v = ["in", "out"], l("slideIn", {
            directions: m,
            divisor: function(t) {
                return this.options.divisor = t, this
            },
            prepare: function(t, e) {
                var i, r = this,
                    n = r.element,
                    s = C._outerWidth,
                    o = C._outerHeight,
                    a = ft[r._direction],
                    c = -a.modifier * (a.vertical ? o(n) : s(n)),
                    l = c / (r.options && r.options.divisor || 1) + G,
                    d = "0px";
                r._reverse && (i = t, t = e, e = i), H ? (t[a.transition] = l, e[a.transition] = d) : (t[a.property] = l, e[a.property] = d)
            }
        }), l("tile", {
            directions: m,
            init: function(t, e, i) {
                h.prototype.init.call(this, t, e), this.options = {
                    previous: i
                }
            },
            previousDivisor: function(t) {
                return this.options.previousDivisor = t, this
            },
            children: function() {
                var t = this,
                    e = t._reverse,
                    i = t.options.previous,
                    r = t.options.previousDivisor || 1,
                    n = t._direction,
                    s = [C.fx(t.element).slideIn(n).setReverse(e)];
                return i && s.push(C.fx(i).slideIn(ft[n].reverse).divisor(r).setReverse(!e)), s
            }
        }), d("fade", "opacity", 1, 0), d("zoom", "scale", 1, .01), l("slideMargin", {
            prepare: function(t, e) {
                var i, r = this,
                    n = r.element,
                    s = r.options,
                    o = n.data(tt),
                    a = s.offset,
                    c = r._reverse;
                c || null !== o || n.data(tt, parseFloat(n.css("margin-" + s.axis))), i = n.data(tt) || 0, e["margin-" + s.axis] = c ? i : i + a
            }
        }), l("slideTo", {
            prepare: function(t, e) {
                var i = this,
                    r = i.element,
                    n = i.options,
                    s = n.offset.split(","),
                    o = i._reverse;
                H ? (e.translatex = o ? 0 : s[0], e.translatey = o ? 0 : s[1]) : (e.left = o ? 0 : s[0], e.top = o ? 0 : s[1]), r.css("left")
            }
        }), l("expand", {
            directions: ["horizontal", "vertical"],
            restore: [it],
            prepare: function(t, i) {
                var r = this,
                    n = r.element,
                    s = r.options,
                    o = r._reverse,
                    a = "vertical" === r._direction ? Y : X,
                    c = n[0].style[a],
                    l = n.data(a),
                    d = parseFloat(l || c),
                    u = B(n.css(a, K)[a]());
                t.overflow = Z, d = s && s.reset ? u || d : d || u, i[a] = (o ? 0 : d) + G, t[a] = (o ? d : 0) + G, l === e && n.data(a, c)
            },
            shouldHide: function() {
                return this._reverse
            },
            teardown: function() {
                var t = this,
                    e = t.element,
                    i = "vertical" === t._direction ? Y : X,
                    r = e.data(i);
                r != K && r !== U || setTimeout(function() {
                    e.css(i, K).css(i)
                }, 0)
            }
        }), x = {
            position: "absolute",
            marginLeft: 0,
            marginTop: 0,
            scale: 1
        }, l("transfer", {
            init: function(t, e) {
                this.element = t, this.options = {
                    target: e
                }, this.restore = []
            },
            setup: function() {
                this.element.appendTo(document.body)
            },
            prepare: function(t, e) {
                var i = this,
                    r = i.element,
                    n = T.box(r),
                    s = T.box(i.options.target),
                    o = a(r, "scale"),
                    c = T.fillScale(s, n),
                    l = T.transformOrigin(s, n);
                P(t, x), e.scale = 1, r.css(at, "scale(1)").css(at), r.css(at, "scale(" + o + ")"), t.top = n.top, t.left = n.left, t.transformOrigin = l.x + G + " " + l.y + G, i._reverse ? t.scale = c : e.scale = c
            }
        }), _ = {
            top: "rect(auto auto $size auto)",
            bottom: "rect($size auto auto auto)",
            left: "rect(auto $size auto auto)",
            right: "rect(auto auto auto $size)"
        }, g = {
            top: {
                start: "rotatex(0deg)",
                end: "rotatex(180deg)"
            },
            bottom: {
                start: "rotatex(-180deg)",
                end: "rotatex(0deg)"
            },
            left: {
                start: "rotatey(0deg)",
                end: "rotatey(-180deg)"
            },
            right: {
                start: "rotatey(180deg)",
                end: "rotatey(0deg)"
            }
        }, l("turningPage", {
            directions: m,
            init: function(t, e, i) {
                h.prototype.init.call(this, t, e), this._container = i
            },
            prepare: function(t, e) {
                var i = this,
                    r = i._reverse,
                    n = r ? ft[i._direction].reverse : i._direction,
                    s = g[n];
                t.zIndex = 1, i._clipInHalf && (t.clip = u(i._container, C.directions[n].reverse)), t[ct] = Z, e[at] = ut + (r ? s.start : s.end), t[at] = ut + (r ? s.end : s.start)
            },
            setup: function() {
                this._container.append(this.element)
            },
            face: function(t) {
                return this._face = t, this
            },
            shouldHide: function() {
                var t = this,
                    e = t._reverse,
                    i = t._face;
                return e && !i || !e && i
            },
            clipInHalf: function(t) {
                return this._clipInHalf = t, this
            },
            temporary: function() {
                return this.element.addClass("temp-page"), this
            }
        }), l("staticPage", {
            directions: m,
            init: function(t, e, i) {
                h.prototype.init.call(this, t, e), this._container = i
            },
            restore: ["clip"],
            prepare: function(t, e) {
                var i = this,
                    r = i._reverse ? ft[i._direction].reverse : i._direction;
                t.clip = u(i._container, r), t.opacity = .999, e.opacity = 1
            },
            shouldHide: function() {
                var t = this,
                    e = t._reverse,
                    i = t._face;
                return e && !i || !e && i
            },
            face: function(t) {
                return this._face = t, this
            }
        }), l("pageturn", {
            directions: ["horizontal", "vertical"],
            init: function(t, e, i, r) {
                h.prototype.init.call(this, t, e), this.options = {}, this.options.face = i, this.options.back = r
            },
            children: function() {
                var t, e = this,
                    i = e.options,
                    r = "horizontal" === e._direction ? "left" : "top",
                    n = C.directions[r].reverse,
                    s = e._reverse,
                    o = i.face.clone(!0).removeAttr("id"),
                    a = i.back.clone(!0).removeAttr("id"),
                    c = e.element;
                return s && (t = r, r = n, n = t), [C.fx(i.face).staticPage(r, c).face(!0).setReverse(s), C.fx(i.back).staticPage(n, c).setReverse(s), C.fx(o).turningPage(r, c).face(!0).clipInHalf(!0).temporary().setReverse(s), C.fx(a).turningPage(n, c).clipInHalf(!0).temporary().setReverse(s)]
            },
            prepare: function(t, e) {
                t[lt] = dt, t.transformStyle = "preserve-3d", t.opacity = .999, e.opacity = 1
            },
            teardown: function() {
                this.element.find(".temp-page").remove()
            }
        }), l("flip", {
            directions: ["horizontal", "vertical"],
            init: function(t, e, i, r) {
                h.prototype.init.call(this, t, e), this.options = {}, this.options.face = i, this.options.back = r
            },
            children: function() {
                var t, e = this,
                    i = e.options,
                    r = "horizontal" === e._direction ? "left" : "top",
                    n = C.directions[r].reverse,
                    s = e._reverse,
                    o = e.element;
                return s && (t = r, r = n, n = t), [C.fx(i.face).turningPage(r, o).face(!0).setReverse(s), C.fx(i.back).turningPage(n, o).setReverse(s)]
            },
            prepare: function(t) {
                t[lt] = dt, t.transformStyle = "preserve-3d"
            }
        }), y = !E.mobileOS.android, k = ".km-touch-scrollbar, .km-actionsheet-wrapper", l("replace", {
            _before: t.noop,
            _after: t.noop,
            init: function(e, i, r) {
                h.prototype.init.call(this, e), this._previous = t(i), this._transitionClass = r
            },
            duration: function() {
                throw Error("The replace effect does not support duration setting; the effect duration may be customized through the transition class rule")
            },
            beforeTransition: function(t) {
                return this._before = t, this
            },
            afterTransition: function(t) {
                return this._after = t, this
            },
            _both: function() {
                return t().add(this._element).add(this._previous)
            },
            _containerClass: function() {
                var t = this._direction,
                    e = "k-fx k-fx-start k-fx-" + this._transitionClass;
                return t && (e += " k-fx-" + t), this._reverse && (e += " k-fx-reverse"), e
            },
            complete: function(e) {
                if (!(!this.deferred || e && t(e.target).is(k))) {
                    var i = this.container;
                    i.removeClass("k-fx-end").removeClass(this._containerClass()).off(D.event, this.completeProxy), this._previous.hide().removeClass("k-fx-current"), this.element.removeClass("k-fx-next"), y && i.css(it, ""), this.isAbsolute || this._both().css(nt, ""), this.deferred.resolve(), delete this.deferred
                }
            },
            run: function() {
                if (this._additionalEffects && this._additionalEffects[0]) return this.compositeRun();
                var e, i = this,
                    r = i.element,
                    n = i._previous,
                    s = r.parents().filter(n.parents()).first(),
                    o = i._both(),
                    a = t.Deferred(),
                    c = r.css(nt);
                return s.length || (s = r.parent()), this.container = s, this.deferred = a, this.isAbsolute = "absolute" == c, this.isAbsolute || o.css(nt, "absolute"), y && (e = s.css(it), s.css(it, "hidden")), D ? (r.addClass("k-fx-hidden"), s.addClass(this._containerClass()), this.completeProxy = t.proxy(this, "complete"), s.on(D.event, this.completeProxy), C.animationFrame(function() {
                    r.removeClass("k-fx-hidden").addClass("k-fx-next"), n.css("display", "").addClass("k-fx-current"), i._before(n, r), C.animationFrame(function() {
                        s.removeClass("k-fx-start").addClass("k-fx-end"), i._after(n, r)
                    })
                })) : this.complete(), a.promise()
            },
            stop: function() {
                this.complete()
            }
        }), b = C.Class.extend({
            init: function() {
                var t = this;
                t._tickProxy = z(t._tick, t), t._started = !1
            },
            tick: t.noop,
            done: t.noop,
            onEnd: t.noop,
            onCancel: t.noop,
            start: function() {
                this.enabled() && (this.done() ? this.onEnd() : (this._started = !0, C.animationFrame(this._tickProxy)))
            },
            enabled: function() {
                return !0
            },
            cancel: function() {
                this._started = !1, this.onCancel()
            },
            _tick: function() {
                var t = this;
                t._started && (t.tick(), t.done() ? (t._started = !1, t.onEnd()) : C.animationFrame(t._tickProxy))
            }
        }), w = b.extend({
            init: function(t) {
                var e = this;
                P(e, t), b.fn.init.call(e)
            },
            done: function() {
                return this.timePassed() >= this.duration
            },
            timePassed: function() {
                return Math.min(this.duration, new Date - this.startDate)
            },
            moveTo: function(t) {
                var e = this,
                    i = e.movable;
                e.initial = i[e.axis], e.delta = t.location - e.initial, e.duration = "number" == typeof t.duration ? t.duration : 300, e.tick = e._easeProxy(t.ease), e.startDate = new Date, e.start()
            },
            _easeProxy: function(t) {
                var e = this;
                return function() {
                    e.movable.moveAxis(e.axis, t(e.timePassed(), e.initial, e.delta, e.duration))
                }
            }
        }), P(w, {
            easeOutExpo: function(t, e, i, r) {
                return t == r ? e + i : i * (-Math.pow(2, -10 * t / r) + 1) + e
            },
            easeOutBack: function(t, e, i, r, n) {
                return n = 1.70158, i * ((t = t / r - 1) * t * ((n + 1) * t + n) + 1) + e
            }
        }), T.Animation = b, T.Transition = w, T.createEffect = l, T.box = function(e) {
            e = t(e);
            var i = e.offset();
            return i.width = C._outerWidth(e), i.height = C._outerHeight(e), i
        }, T.transformOrigin = function(t, e) {
            var i = (t.left - e.left) * e.width / (e.width - t.width),
                r = (t.top - e.top) * e.height / (e.height - t.height);
            return {
                x: isNaN(i) ? 0 : i,
                y: isNaN(r) ? 0 : r
            }
        }, T.fillScale = function(t, e) {
            return Math.min(t.width / e.width, t.height / e.height)
        }, T.fitScale = function(t, e) {
            return Math.max(t.width / e.width, t.height / e.height)
        }
    }(window.kendo.jQuery), window.kendo
}, "function" == typeof define && define.amd ? define : function(t, e, i) {
    (i || e)()
});
//# sourceMappingURL=kendo.fx.min.js.map;
/** 
 * Kendo UI v2018.1.221 (http://www.telerik.com/kendo-ui)                                                                                                                                               
 * Copyright 2018 Telerik AD. All rights reserved.                                                                                                                                                      
 *                                                                                                                                                                                                      
 * Kendo UI commercial licenses may be obtained at                                                                                                                                                      
 * http://www.telerik.com/purchase/license-agreement/kendo-ui-complete                                                                                                                                  
 * If you do not own a commercial license, this file shall be governed by the trial license terms.                                                                                                      

*/
! function(e, define) {
    define("kendo.userevents.min", ["kendo.core.min"], e)
}(function() {
    return function(e, t) {
        function n(e, t) {
            var n = e.x.location,
                i = e.y.location,
                o = t.x.location,
                r = t.y.location,
                s = n - o,
                a = i - r;
            return {
                center: {
                    x: (n + o) / 2,
                    y: (i + r) / 2
                },
                distance: Math.sqrt(s * s + a * a)
            }
        }

        function i(e) {
            var t, n, i, o = [],
                r = e.originalEvent,
                a = e.currentTarget,
                c = 0;
            if (e.api) o.push({
                id: 2,
                event: e,
                target: e.target,
                currentTarget: e.target,
                location: e,
                type: "api"
            });
            else if (e.type.match(/touch/))
                for (n = r ? r.changedTouches : [], t = n.length; c < t; c++) i = n[c], o.push({
                    location: i,
                    event: e,
                    target: i.target,
                    currentTarget: a,
                    id: i.identifier,
                    type: "touch"
                });
            else o.push(s.pointers || s.msPointers ? {
                location: r,
                event: e,
                target: e.target,
                currentTarget: a,
                id: r.pointerId,
                type: "pointer"
            } : {
                id: 1,
                event: e,
                target: e.target,
                currentTarget: a,
                location: e,
                type: "mouse"
            });
            return o
        }

        function o(e) {
            for (var t = r.eventMap.up.split(" "), n = 0, i = t.length; n < i; n++) e(t[n])
        }
        var r = window.kendo,
            s = r.support,
            a = r.Class,
            c = r.Observable,
            u = e.now,
            h = e.extend,
            l = s.mobileOS,
            p = l && l.android,
            d = 800,
            f = s.browser.msie ? 5 : 0,
            v = "press",
            g = "hold",
            m = "select",
            _ = "start",
            T = "move",
            y = "end",
            x = "cancel",
            M = "tap",
            w = "release",
            E = "gesturestart",
            k = "gesturechange",
            D = "gestureend",
            A = "gesturetap",
            C = {
                api: 0,
                touch: 0,
                mouse: 9,
                pointer: 9
            },
            b = !s.touch || s.mouseAndTouchPresent,
            I = a.extend({
                init: function(e, t) {
                    var n = this;
                    n.axis = e, n._updateLocationData(t), n.startLocation = n.location, n.velocity = n.delta = 0, n.timeStamp = u()
                },
                move: function(e) {
                    var t = this,
                        n = e["page" + t.axis],
                        i = u(),
                        o = i - t.timeStamp || 1;
                    !n && p || (t.delta = n - t.location, t._updateLocationData(e), t.initialDelta = n - t.startLocation, t.velocity = t.delta / o, t.timeStamp = i)
                },
                _updateLocationData: function(e) {
                    var t = this,
                        n = t.axis;
                    t.location = e["page" + n], t.client = e["client" + n], t.screen = e["screen" + n]
                }
            }),
            S = a.extend({
                init: function(e, t, n) {
                    h(this, {
                        x: new I("X", n.location),
                        y: new I("Y", n.location),
                        type: n.type,
                        useClickAsTap: e.useClickAsTap,
                        threshold: e.threshold || C[n.type],
                        userEvents: e,
                        target: t,
                        currentTarget: n.currentTarget,
                        initialTouch: n.target,
                        id: n.id,
                        pressEvent: n,
                        _moved: !1,
                        _finished: !1
                    })
                },
                press: function() {
                    this._holdTimeout = setTimeout(e.proxy(this, "_hold"), this.userEvents.minHold), this._trigger(v, this.pressEvent)
                },
                _hold: function() {
                    this._trigger(g, this.pressEvent)
                },
                move: function(e) {
                    var t = this;
                    if (!t._finished) {
                        if (t.x.move(e.location), t.y.move(e.location), !t._moved) {
                            if (t._withinIgnoreThreshold()) return;
                            if (P.current && P.current !== t.userEvents) return t.dispose();
                            t._start(e)
                        }
                        t._finished || t._trigger(T, e)
                    }
                },
                end: function(e) {
                    this.endTime = u(), this._finished || (this._finished = !0, this._trigger(w, e), this._moved ? this._trigger(y, e) : this.useClickAsTap || this._trigger(M, e), clearTimeout(this._holdTimeout), this.dispose())
                },
                dispose: function() {
                    var t = this.userEvents,
                        n = t.touches;
                    this._finished = !0, this.pressEvent = null, clearTimeout(this._holdTimeout), n.splice(e.inArray(this, n), 1)
                },
                skip: function() {
                    this.dispose()
                },
                cancel: function() {
                    this.dispose()
                },
                isMoved: function() {
                    return this._moved
                },
                _start: function(e) {
                    clearTimeout(this._holdTimeout), this.startTime = u(), this._moved = !0, this._trigger(_, e)
                },
                _trigger: function(e, t) {
                    var n = this,
                        i = t.event,
                        o = {
                            touch: n,
                            x: n.x,
                            y: n.y,
                            target: n.target,
                            event: i
                        };
                    n.userEvents.notify(e, o) && i.preventDefault()
                },
                _withinIgnoreThreshold: function() {
                    var e = this.x.initialDelta,
                        t = this.y.initialDelta;
                    return Math.sqrt(e * e + t * t) <= this.threshold
                }
            }),
            P = c.extend({
                init: function(t, n) {
                    var i, a, u, l, p = this,
                        C = r.guid();
                    n = n || {}, i = p.filter = n.filter, p.threshold = n.threshold || f, p.minHold = n.minHold || d, p.touches = [], p._maxTouches = n.multiTouch ? 2 : 1, p.allowSelection = n.allowSelection, p.captureUpIfMoved = n.captureUpIfMoved, p.useClickAsTap = !n.fastTap && !s.delayedClick(), p.eventNS = C, t = e(t).handler(p), c.fn.init.call(p), h(p, {
                        element: t,
                        surface: e(n.global && b ? t[0].ownerDocument.documentElement : n.surface || t),
                        stopPropagation: n.stopPropagation,
                        pressed: !1
                    }), p.surface.handler(p).on(r.applyEventMap("move", C), "_move").on(r.applyEventMap("up cancel", C), "_end"), t.on(r.applyEventMap("down", C), i, "_start"), p.useClickAsTap && t.on(r.applyEventMap("click", C), i, "_click"), (s.pointers || s.msPointers) && (s.browser.version < 11 ? (a = "pinch-zoom double-tap-zoom", t.css("-ms-touch-action", n.touchAction && "none" != n.touchAction ? a + " " + n.touchAction : a)) : t.css("touch-action", n.touchAction || "none")), n.preventDragEvent && t.on(r.applyEventMap("dragstart", C), r.preventDefault), t.on(r.applyEventMap("mousedown", C), i, {
                        root: t
                    }, "_select"), p.captureUpIfMoved && s.eventCapture && (u = p.surface[0], l = e.proxy(p.preventIfMoving, p), o(function(e) {
                        u.addEventListener(e, l, !0)
                    })), p.bind([v, g, M, _, T, y, w, x, E, k, D, A, m], n)
                },
                preventIfMoving: function(e) {
                    this._isMoved() && e.preventDefault()
                },
                destroy: function() {
                    var e, t = this;
                    t._destroyed || (t._destroyed = !0, t.captureUpIfMoved && s.eventCapture && (e = t.surface[0], o(function(n) {
                        e.removeEventListener(n, t.preventIfMoving)
                    })), t.element.kendoDestroy(t.eventNS), t.surface.kendoDestroy(t.eventNS), t.element.removeData("handler"), t.surface.removeData("handler"), t._disposeAll(), t.unbind(), delete t.surface, delete t.element, delete t.currentTarget)
                },
                capture: function() {
                    P.current = this
                },
                cancel: function() {
                    this._disposeAll(), this.trigger(x)
                },
                notify: function(e, t) {
                    var i = this,
                        o = i.touches;
                    if (this._isMultiTouch()) {
                        switch (e) {
                            case T:
                                e = k;
                                break;
                            case y:
                                e = D;
                                break;
                            case M:
                                e = A
                        }
                        h(t, {
                            touches: o
                        }, n(o[0], o[1]))
                    }
                    return this.trigger(e, h(t, {
                        type: e
                    }))
                },
                press: function(e, t, n) {
                    this._apiCall("_start", e, t, n)
                },
                move: function(e, t) {
                    this._apiCall("_move", e, t)
                },
                end: function(e, t) {
                    this._apiCall("_end", e, t)
                },
                _isMultiTouch: function() {
                    return this.touches.length > 1
                },
                _maxTouchesReached: function() {
                    return this.touches.length >= this._maxTouches
                },
                _disposeAll: function() {
                    for (var e = this.touches; e.length > 0;) e.pop().dispose()
                },
                _isMoved: function() {
                    return e.grep(this.touches, function(e) {
                        return e.isMoved()
                    }).length
                },
                _select: function(e) {
                    this.allowSelection && !this.trigger(m, {
                        event: e
                    }) || e.preventDefault()
                },
                _start: function(t) {
                    var n, o, r = this,
                        s = 0,
                        a = r.filter,
                        c = i(t),
                        u = c.length,
                        h = t.which;
                    if (!(h && h > 1 || r._maxTouchesReached()))
                        for (P.current = null, r.currentTarget = t.currentTarget, r.stopPropagation && t.stopPropagation(); s < u && !r._maxTouchesReached(); s++) o = c[s], n = a ? e(o.currentTarget) : r.element, n.length && (o = new S(r, n, o), r.touches.push(o), o.press(), r._isMultiTouch() && r.notify("gesturestart", {}))
                },
                _move: function(e) {
                    this._eachTouch("move", e)
                },
                _end: function(e) {
                    this._eachTouch("end", e)
                },
                _click: function(t) {
                    var n = {
                        touch: {
                            initialTouch: t.target,
                            target: e(t.currentTarget),
                            endTime: u(),
                            x: {
                                location: t.pageX,
                                client: t.clientX
                            },
                            y: {
                                location: t.pageY,
                                client: t.clientY
                            }
                        },
                        x: t.pageX,
                        y: t.pageY,
                        target: e(t.currentTarget),
                        event: t,
                        type: "tap"
                    };
                    this.trigger("tap", n) && t.preventDefault()
                },
                _eachTouch: function(e, t) {
                    var n, o, r, s, a = this,
                        c = {},
                        u = i(t),
                        h = a.touches;
                    for (n = 0; n < h.length; n++) o = h[n], c[o.id] = o;
                    for (n = 0; n < u.length; n++) r = u[n], s = c[r.id], s && s[e](r)
                },
                _apiCall: function(t, n, i, o) {
                    this[t]({
                        api: !0,
                        pageX: n,
                        pageY: i,
                        clientX: n,
                        clientY: i,
                        target: e(o || this.element)[0],
                        stopPropagation: e.noop,
                        preventDefault: e.noop
                    })
                }
            });
        P.defaultThreshold = function(e) {
            f = e
        }, P.minHold = function(e) {
            d = e
        }, r.getTouches = i, r.touchDelta = n, r.UserEvents = P
    }(window.kendo.jQuery), window.kendo
}, "function" == typeof define && define.amd ? define : function(e, t, n) {
    (n || t)()
});
//# sourceMappingURL=kendo.userevents.min.js.map;
/** 
 * Kendo UI v2018.1.221 (http://www.telerik.com/kendo-ui)                                                                                                                                               
 * Copyright 2018 Telerik AD. All rights reserved.                                                                                                                                                      
 *                                                                                                                                                                                                      
 * Kendo UI commercial licenses may be obtained at                                                                                                                                                      
 * http://www.telerik.com/purchase/license-agreement/kendo-ui-complete                                                                                                                                  
 * If you do not own a commercial license, this file shall be governed by the trial license terms.                                                                                                      

*/
! function(t, define) {
    define("kendo.draganddrop.min", ["kendo.core.min", "kendo.userevents.min"], t)
}(function() {
    return function(t, e) {
        function n(e, n) {
            try {
                return t.contains(e, n) || e == n
            } catch (r) {
                return !1
            }
        }

        function r(t, e) {
            return parseInt(t.css(e), 10) || 0
        }

        function i(t, e) {
            return Math.min(Math.max(t, e.min), e.max)
        }

        function o(t, e) {
            var n = D(t),
                i = _._outerWidth,
                o = _._outerHeight,
                a = n.left + r(t, "borderLeftWidth") + r(t, "paddingLeft"),
                s = n.top + r(t, "borderTopWidth") + r(t, "paddingTop"),
                l = a + t.width() - i(e, !0),
                c = s + t.height() - o(e, !0);
            return {
                x: {
                    min: a,
                    max: l
                },
                y: {
                    min: s,
                    max: c
                }
            }
        }

        function a(n, r, i) {
            for (var o, a, s = 0, l = r && r.length, c = i && i.length; n && n.parentNode;) {
                for (s = 0; s < l; s++)
                    if (o = r[s], o.element[0] === n) return {
                        target: o,
                        targetElement: n
                    };
                for (s = 0; s < c; s++)
                    if (a = i[s], t.contains(a.element[0], n) && x.matchesSelector.call(n, a.options.filter)) return {
                        target: a,
                        targetElement: n
                    };
                n = n.parentNode
            }
            return e
        }

        function s(t, e) {
            var n, r = e.options.group,
                i = t[r];
            if (T.fn.destroy.call(e), i.length > 1) {
                for (n = 0; n < i.length; n++)
                    if (i[n] == e) {
                        i.splice(n, 1);
                        break
                    }
            } else i.length = 0, delete t[r]
        }

        function l(t) {
            var e, n, r, i = c()[0];
            return t[0] === i ? (n = i.scrollTop, r = i.scrollLeft, {
                top: n,
                left: r,
                bottom: n + b.height(),
                right: r + b.width()
            }) : (e = t.offset(), e.bottom = e.top + t.height(), e.right = e.left + t.width(), e)
        }

        function c() {
            return t(_.support.browser.chrome ? y.body : y.documentElement)
        }

        function u(e) {
            var n, r = c();
            if (!e || e === y.body || e === y.documentElement) return r;
            for (n = t(e)[0]; n && !_.isScrollable(n) && n !== y.body;) n = n.parentNode;
            return n === y.body ? r : t(n)
        }

        function h(t, e, n) {
            var r = {
                    x: 0,
                    y: 0
                },
                i = 50;
            return t - n.left < i ? r.x = -(i - (t - n.left)) : n.right - t < i && (r.x = i - (n.right - t)), e - n.top < i ? r.y = -(i - (e - n.top)) : n.bottom - e < i && (r.y = i - (n.bottom - e)), r
        }
        var d, f, p, g, v, m, _ = window.kendo,
            x = _.support,
            y = window.document,
            b = t(window),
            E = _.Class,
            T = _.ui.Widget,
            S = _.Observable,
            M = _.UserEvents,
            w = t.proxy,
            C = t.extend,
            D = _.getOffset,
            O = {},
            k = {},
            I = {},
            H = _.elementUnderCursor,
            W = "keyup",
            z = "change",
            P = "dragstart",
            U = "hold",
            L = "drag",
            A = "dragend",
            N = "dragcancel",
            V = "hintDestroyed",
            B = "dragenter",
            $ = "dragleave",
            F = "drop",
            j = S.extend({
                init: function(e, n) {
                    var r = this,
                        i = e[0];
                    r.capture = !1, i.addEventListener ? (t.each(_.eventMap.down.split(" "), function() {
                        i.addEventListener(this, w(r._press, r), !0)
                    }), t.each(_.eventMap.up.split(" "), function() {
                        i.addEventListener(this, w(r._release, r), !0)
                    })) : (t.each(_.eventMap.down.split(" "), function() {
                        i.attachEvent(this, w(r._press, r))
                    }), t.each(_.eventMap.up.split(" "), function() {
                        i.attachEvent(this, w(r._release, r))
                    })), S.fn.init.call(r), r.bind(["press", "release"], n || {})
                },
                captureNext: function() {
                    this.capture = !0
                },
                cancelCapture: function() {
                    this.capture = !1
                },
                _press: function(t) {
                    var e = this;
                    e.trigger("press"), e.capture && t.preventDefault()
                },
                _release: function(t) {
                    var e = this;
                    e.trigger("release"), e.capture && (t.preventDefault(), e.cancelCapture())
                }
            }),
            G = S.extend({
                init: function(e) {
                    var n = this;
                    S.fn.init.call(n), n.forcedEnabled = !1, t.extend(n, e), n.scale = 1, n.horizontal ? (n.measure = "offsetWidth", n.scrollSize = "scrollWidth", n.axis = "x") : (n.measure = "offsetHeight", n.scrollSize = "scrollHeight", n.axis = "y")
                },
                makeVirtual: function() {
                    t.extend(this, {
                        virtual: !0,
                        forcedEnabled: !0,
                        _virtualMin: 0,
                        _virtualMax: 0
                    })
                },
                virtualSize: function(t, e) {
                    this._virtualMin === t && this._virtualMax === e || (this._virtualMin = t, this._virtualMax = e, this.update())
                },
                outOfBounds: function(t) {
                    return t > this.max || t < this.min
                },
                forceEnabled: function() {
                    this.forcedEnabled = !0
                },
                getSize: function() {
                    return this.container[0][this.measure]
                },
                getTotal: function() {
                    return this.element[0][this.scrollSize]
                },
                rescale: function(t) {
                    this.scale = t
                },
                update: function(t) {
                    var e = this,
                        n = e.virtual ? e._virtualMax : e.getTotal(),
                        r = n * e.scale,
                        i = e.getSize();
                    (0 !== n || e.forcedEnabled) && (e.max = e.virtual ? -e._virtualMin : 0, e.size = i, e.total = r, e.min = Math.min(e.max, i - r), e.minScale = i / n, e.centerOffset = (r - i) / 2, e.enabled = e.forcedEnabled || r > i, t || e.trigger(z, e))
                }
            }),
            Q = S.extend({
                init: function(t) {
                    var e = this;
                    S.fn.init.call(e), e.x = new G(C({
                        horizontal: !0
                    }, t)), e.y = new G(C({
                        horizontal: !1
                    }, t)), e.container = t.container, e.forcedMinScale = t.minScale, e.maxScale = t.maxScale || 100, e.bind(z, t)
                },
                rescale: function(t) {
                    this.x.rescale(t), this.y.rescale(t), this.refresh()
                },
                centerCoordinates: function() {
                    return {
                        x: Math.min(0, -this.x.centerOffset),
                        y: Math.min(0, -this.y.centerOffset)
                    }
                },
                refresh: function() {
                    var t = this;
                    t.x.update(), t.y.update(), t.enabled = t.x.enabled || t.y.enabled, t.minScale = t.forcedMinScale || Math.min(t.x.minScale, t.y.minScale), t.fitScale = Math.max(t.x.minScale, t.y.minScale), t.trigger(z)
                }
            }),
            q = S.extend({
                init: function(t) {
                    var e = this;
                    C(e, t), S.fn.init.call(e)
                },
                outOfBounds: function() {
                    return this.dimension.outOfBounds(this.movable[this.axis])
                },
                dragMove: function(t) {
                    var e = this,
                        n = e.dimension,
                        r = e.axis,
                        i = e.movable,
                        o = i[r] + t;
                    n.enabled && ((o < n.min && t < 0 || o > n.max && t > 0) && (t *= e.resistance), i.translateAxis(r, t), e.trigger(z, e))
                }
            }),
            J = E.extend({
                init: function(e) {
                    var n, r, i, o, a = this;
                    C(a, {
                        elastic: !0
                    }, e), i = a.elastic ? .5 : 0, o = a.movable, a.x = n = new q({
                        axis: "x",
                        dimension: a.dimensions.x,
                        resistance: i,
                        movable: o
                    }), a.y = r = new q({
                        axis: "y",
                        dimension: a.dimensions.y,
                        resistance: i,
                        movable: o
                    }), a.userEvents.bind(["press", "move", "end", "gesturestart", "gesturechange"], {
                        gesturestart: function(t) {
                            a.gesture = t, a.offset = a.dimensions.container.offset()
                        },
                        press: function(e) {
                            t(e.event.target).closest("a").is("[data-navigate-on-press=true]") && e.sender.cancel()
                        },
                        gesturechange: function(t) {
                            var e, i, s, l = a.gesture,
                                c = l.center,
                                u = t.center,
                                h = t.distance / l.distance,
                                d = a.dimensions.minScale,
                                f = a.dimensions.maxScale;
                            o.scale <= d && h < 1 && (h += .8 * (1 - h)), o.scale * h >= f && (h = f / o.scale), i = o.x + a.offset.left, s = o.y + a.offset.top, e = {
                                x: (i - c.x) * h + u.x - i,
                                y: (s - c.y) * h + u.y - s
                            }, o.scaleWith(h), n.dragMove(e.x), r.dragMove(e.y), a.dimensions.rescale(o.scale), a.gesture = t, t.preventDefault()
                        },
                        move: function(t) {
                            t.event.target.tagName.match(/textarea|input/i) || (n.dimension.enabled || r.dimension.enabled ? (n.dragMove(t.x.delta), r.dragMove(t.y.delta), t.preventDefault()) : t.touch.skip())
                        },
                        end: function(t) {
                            t.preventDefault()
                        }
                    })
                }
            }),
            K = x.transitions.prefix + "Transform";
        f = x.hasHW3D ? function(t, e, n) {
            return "translate3d(" + t + "px," + e + "px,0) scale(" + n + ")"
        } : function(t, e, n) {
            return "translate(" + t + "px," + e + "px) scale(" + n + ")"
        }, p = S.extend({
            init: function(e) {
                var n = this;
                S.fn.init.call(n), n.element = t(e), n.element[0].style.webkitTransformOrigin = "left top", n.x = 0, n.y = 0, n.scale = 1, n._saveCoordinates(f(n.x, n.y, n.scale))
            },
            translateAxis: function(t, e) {
                this[t] += e, this.refresh()
            },
            scaleTo: function(t) {
                this.scale = t, this.refresh()
            },
            scaleWith: function(t) {
                this.scale *= t, this.refresh()
            },
            translate: function(t) {
                this.x += t.x, this.y += t.y, this.refresh()
            },
            moveAxis: function(t, e) {
                this[t] = e, this.refresh()
            },
            moveTo: function(t) {
                C(this, t), this.refresh()
            },
            refresh: function() {
                var t, e = this,
                    n = e.x,
                    r = e.y;
                e.round && (n = Math.round(n), r = Math.round(r)), t = f(n, r, e.scale), t != e.coordinates && (_.support.browser.msie && _.support.browser.version < 10 ? (e.element[0].style.position = "absolute", e.element[0].style.left = e.x + "px", e.element[0].style.top = e.y + "px") : e.element[0].style[K] = t, e._saveCoordinates(t), e.trigger(z))
            },
            _saveCoordinates: function(t) {
                this.coordinates = t
            }
        }), g = T.extend({
            init: function(t, e) {
                var n, r = this;
                T.fn.init.call(r, t, e), n = r.options.group, n in k ? k[n].push(r) : k[n] = [r]
            },
            events: [B, $, F],
            options: {
                name: "DropTarget",
                group: "default"
            },
            destroy: function() {
                s(k, this)
            },
            _trigger: function(t, e) {
                var n = this,
                    r = O[n.options.group];
                if (r) return n.trigger(t, C({}, e.event, {
                    draggable: r,
                    dropTarget: e.dropTarget
                }))
            },
            _over: function(t) {
                this._trigger(B, t)
            },
            _out: function(t) {
                this._trigger($, t)
            },
            _drop: function(t) {
                var e = this,
                    n = O[e.options.group];
                n && (n.dropped = !e._trigger(F, t))
            }
        }), g.destroyGroup = function(t) {
            var e, n = k[t] || I[t];
            if (n) {
                for (e = 0; e < n.length; e++) T.fn.destroy.call(n[e]);
                n.length = 0, delete k[t], delete I[t]
            }
        }, g._cache = k, v = g.extend({
            init: function(t, e) {
                var n, r = this;
                T.fn.init.call(r, t, e), n = r.options.group, n in I ? I[n].push(r) : I[n] = [r]
            },
            destroy: function() {
                s(I, this)
            },
            options: {
                name: "DropTargetArea",
                group: "default",
                filter: null
            }
        }), m = T.extend({
            init: function(t, e) {
                var n = this;
                T.fn.init.call(n, t, e), n._activated = !1, n.userEvents = new M(n.element, {
                    global: !0,
                    allowSelection: !0,
                    filter: n.options.filter,
                    threshold: n.options.distance,
                    start: w(n._start, n),
                    hold: w(n._hold, n),
                    move: w(n._drag, n),
                    end: w(n._end, n),
                    cancel: w(n._cancel, n),
                    select: w(n._select, n)
                }), n._afterEndHandler = w(n._afterEnd, n), n._captureEscape = w(n._captureEscape, n)
            },
            events: [U, P, L, A, N, V],
            options: {
                name: "Draggable",
                distance: _.support.touch ? 0 : 5,
                group: "default",
                cursorOffset: null,
                axis: null,
                container: null,
                filter: null,
                ignore: null,
                holdToDrag: !1,
                autoScroll: !1,
                dropped: !1
            },
            cancelHold: function() {
                this._activated = !1
            },
            _captureEscape: function(t) {
                var e = this;
                t.keyCode === _.keys.ESC && (e._trigger(N, {
                    event: t
                }), e.userEvents.cancel())
            },
            _updateHint: function(e) {
                var n, r = this,
                    o = r.options,
                    a = r.boundaries,
                    s = o.axis,
                    l = r.options.cursorOffset;
                l ? n = {
                    left: e.x.location + l.left,
                    top: e.y.location + l.top
                } : (r.hintOffset.left += e.x.delta, r.hintOffset.top += e.y.delta, n = t.extend({}, r.hintOffset)), a && (n.top = i(n.top, a.y), n.left = i(n.left, a.x)), "x" === s ? delete n.top : "y" === s && delete n.left, r.hint.css(n)
            },
            _shouldIgnoreTarget: function(e) {
                var n = this.options.ignore;
                return n && t(e).is(n)
            },
            _select: function(t) {
                this._shouldIgnoreTarget(t.event.target) || t.preventDefault()
            },
            _start: function(n) {
                var r, i = this,
                    a = i.options,
                    s = a.container ? t(a.container) : null,
                    l = a.hint;
                return this._shouldIgnoreTarget(n.touch.initialTouch) || a.holdToDrag && !i._activated ? (i.userEvents.cancel(), e) : (i.currentTarget = n.target, i.currentTargetOffset = D(i.currentTarget), l && (i.hint && i.hint.stop(!0, !0).remove(), i.hint = _.isFunction(l) ? t(l.call(i, i.currentTarget)) : l, r = D(i.currentTarget), i.hintOffset = r, i.hint.css({
                    position: "absolute",
                    zIndex: 2e4,
                    left: r.left,
                    top: r.top
                }).appendTo(y.body), i.angular("compile", function() {
                    i.hint.removeAttr("ng-repeat");
                    for (var e = t(n.target); !e.data("$$kendoScope") && e.length;) e = e.parent();
                    return {
                        elements: i.hint.get(),
                        scopeFrom: e.data("$$kendoScope")
                    }
                })), O[a.group] = i, i.dropped = !1, s && (i.boundaries = o(s, i.hint)), t(y).on(W, i._captureEscape), i._trigger(P, n) && (i.userEvents.cancel(), i._afterEnd()), i.userEvents.capture(), e)
            },
            _hold: function(t) {
                this.currentTarget = t.target, this._trigger(U, t) ? this.userEvents.cancel() : this._activated = !0
            },
            _drag: function(e) {
                var n, r;
                e.preventDefault(), n = this._elementUnderCursor(e), this.options.autoScroll && this._cursorElement !== n && (this._scrollableParent = u(n), this._cursorElement = n), this._lastEvent = e, this._processMovement(e, n), this.options.autoScroll && this._scrollableParent[0] && (r = h(e.x.location, e.y.location, l(this._scrollableParent)), this._scrollCompenstation = t.extend({}, this.hintOffset), this._scrollVelocity = r, 0 === r.y && 0 === r.x ? (clearInterval(this._scrollInterval), this._scrollInterval = null) : this._scrollInterval || (this._scrollInterval = setInterval(t.proxy(this, "_autoScroll"), 50))), this.hint && this._updateHint(e)
            },
            _processMovement: function(n, r) {
                this._withDropTarget(r, function(r, i) {
                    if (!r) return d && (d._trigger($, C(n, {
                        dropTarget: t(d.targetElement)
                    })), d = null), e;
                    if (d) {
                        if (i === d.targetElement) return;
                        d._trigger($, C(n, {
                            dropTarget: t(d.targetElement)
                        }))
                    }
                    r._trigger(B, C(n, {
                        dropTarget: t(i)
                    })), d = C(r, {
                        targetElement: i
                    })
                }), this._trigger(L, C(n, {
                    dropTarget: d,
                    elementUnderCursor: r
                }))
            },
            _autoScroll: function() {
                var t, e, n, r, i, o, a, s, l = this._scrollableParent[0],
                    u = this._scrollVelocity,
                    h = this._scrollCompenstation;
                l && (t = this._elementUnderCursor(this._lastEvent), this._processMovement(this._lastEvent, t), r = l === c()[0], r ? (e = y.body.scrollHeight > b.height(), n = y.body.scrollWidth > b.width()) : (e = l.offsetHeight <= l.scrollHeight, n = l.offsetWidth <= l.scrollWidth), i = l.scrollTop + u.y, o = e && i > 0 && i < l.scrollHeight, a = l.scrollLeft + u.x, s = n && a > 0 && a < l.scrollWidth, o && (l.scrollTop += u.y), s && (l.scrollLeft += u.x), this.hint && r && (s || o) && (o && (h.top += u.y), s && (h.left += u.x), this.hint.css(h)))
            },
            _end: function(e) {
                this._withDropTarget(this._elementUnderCursor(e), function(n, r) {
                    n && (n._drop(C({}, e, {
                        dropTarget: t(r)
                    })), d = null)
                }), this._cancel(this._trigger(A, e))
            },
            _cancel: function(t) {
                var e = this;
                e._scrollableParent = null, this._cursorElement = null, clearInterval(this._scrollInterval), e._activated = !1, e.hint && !e.dropped ? setTimeout(function() {
                    e.hint.stop(!0, !0), t ? e._afterEndHandler() : e.hint.animate(e.currentTargetOffset, "fast", e._afterEndHandler)
                }, 0) : e._afterEnd()
            },
            _trigger: function(t, e) {
                var n = this;
                return n.trigger(t, C({}, e.event, {
                    x: e.x,
                    y: e.y,
                    currentTarget: n.currentTarget,
                    initialTarget: e.touch ? e.touch.initialTouch : null,
                    dropTarget: e.dropTarget,
                    elementUnderCursor: e.elementUnderCursor
                }))
            },
            _elementUnderCursor: function(t) {
                var e = H(t),
                    r = this.hint;
                return r && n(r[0], e) && (r.hide(), e = H(t), e || (e = H(t)), r.show()), e
            },
            _withDropTarget: function(t, e) {
                var n, r = this.options.group,
                    i = k[r],
                    o = I[r];
                (i && i.length || o && o.length) && (n = a(t, i, o), n ? e(n.target, n.targetElement) : e())
            },
            destroy: function() {
                var t = this;
                T.fn.destroy.call(t), t._afterEnd(), t.userEvents.destroy(), this._scrollableParent = null, this._cursorElement = null, clearInterval(this._scrollInterval), t.currentTarget = null
            },
            _afterEnd: function() {
                var e = this;
                e.hint && e.hint.remove(), delete O[e.options.group], e.trigger("destroy"), e.trigger(V), t(y).off(W, e._captureEscape)
            }
        }), _.ui.plugin(g), _.ui.plugin(v), _.ui.plugin(m), _.TapCapture = j, _.containerBoundaries = o, C(_.ui, {
            Pane: J,
            PaneDimensions: Q,
            Movable: p
        }), _.ui.Draggable.utils = {
            autoScrollVelocity: h,
            scrollableViewPort: l,
            findScrollableParent: u
        }
    }(window.kendo.jQuery), window.kendo
}, "function" == typeof define && define.amd ? define : function(t, e, n) {
    (n || e)()
});
//# sourceMappingURL=kendo.draganddrop.min.js.map;
/** 
 * Kendo UI v2018.1.221 (http://www.telerik.com/kendo-ui)                                                                                                                                               
 * Copyright 2018 Telerik AD. All rights reserved.                                                                                                                                                      
 *                                                                                                                                                                                                      
 * Kendo UI commercial licenses may be obtained at                                                                                                                                                      
 * http://www.telerik.com/purchase/license-agreement/kendo-ui-complete                                                                                                                                  
 * If you do not own a commercial license, this file shall be governed by the trial license terms.                                                                                                      

*/
! function(i, define) {
    define("kendo.window.min", ["kendo.draganddrop.min", "kendo.popup.min"], i)
}(function() {
    return function(i, t) {
        function e(i) {
            return t !== i
        }

        function n(i, t, e) {
            return Math.max(Math.min(parseInt(i, 10), e === 1 / 0 ? e : parseInt(e, 10)), parseInt(t, 10))
        }

        function o() {
            return !this.type || this.type.toLowerCase().indexOf("script") >= 0
        }

        function s(i) {
            var t = this;
            t.owner = i, t._preventDragging = !1, t._draggable = new p(i.wrapper, {
                filter: ">" + M,
                group: i.wrapper.id + "-resizing",
                dragstart: m(t.dragstart, t),
                drag: m(t.drag, t),
                dragend: m(t.dragend, t)
            }), t._draggable.userEvents.bind("press", m(t.addOverlay, t)), t._draggable.userEvents.bind("release", m(t.removeOverlay, t))
        }

        function r(t) {
            for (var e, n, o = {
                    top: t.offsetTop,
                    left: t.offsetLeft
                }, s = t.offsetParent; s;) o.top += s.offsetTop, o.left += s.offsetLeft, e = i(s).css("overflowX"), n = i(s).css("overflowY"), "auto" !== n && "scroll" !== n || (o.top -= s.scrollTop), "auto" !== e && "scroll" !== e || (o.left -= s.scrollLeft), s = s.offsetParent;
            return o
        }

        function a(i, t) {
            var e = this;
            e.owner = i, e._preventDragging = !1, e._draggable = new p(i.wrapper, {
                filter: t,
                group: i.wrapper.id + "-moving",
                dragstart: m(e.dragstart, e),
                drag: m(e.drag, e),
                dragend: m(e.dragend, e),
                dragcancel: m(e.dragcancel, e)
            }), e._draggable.userEvents.stopPropagation = !1
        }
        var l = window.kendo,
            d = l.ui.Widget,
            c = l.ui.Popup.TabKeyTrap,
            p = l.ui.Draggable,
            h = i.isPlainObject,
            f = l._activeElement,
            u = l._outerWidth,
            g = l._outerHeight,
            m = i.proxy,
            w = i.extend,
            _ = i.each,
            v = l.template,
            b = "body",
            k = ".kendoWindow",
            z = ".k-window",
            x = ".k-window-title",
            y = x + "bar",
            T = ".k-window-content",
            O = ".k-dialog-content",
            M = ".k-resize-handle",
            S = ".k-overlay",
            P = "k-content-frame",
            L = "k-i-loading",
            I = "k-state-hover",
            D = "k-state-focused",
            C = "k-window-maximized",
            H = ":visible",
            W = "hidden",
            E = "cursor",
            F = "open",
            N = "activate",
            R = "deactivate",
            j = "close",
            A = "refresh",
            K = "minimize",
            q = "maximize",
            U = "resizeStart",
            G = "resize",
            J = "resizeEnd",
            V = "dragstart",
            B = "dragend",
            Q = "error",
            X = "overflow",
            Y = "original-overflow-rule",
            $ = "zIndex",
            Z = ".k-window-actions .k-i-window-minimize,.k-window-actions .k-i-window-maximize",
            ii = ".k-i-pin",
            ti = ".k-i-unpin",
            ei = ii + "," + ti,
            ni = ".k-window-titlebar .k-window-action",
            oi = ".k-window-titlebar .k-i-refresh",
            si = "WindowEventsHandled",
            ri = /^0[a-z]*$/i,
            ai = l.isLocalUrl,
            li = d.extend({
                init: function(n, s) {
                    var r, a, p, f, u, g, w, _, v, b = this,
                        M = {},
                        S = !1,
                        L = s && s.actions && !s.actions.length;
                    d.fn.init.call(b, n, s), s = b.options, f = s.position, n = b.element, u = s.content, _ = i(window), L && (s.actions = []), b.appendTo = i(s.appendTo), u && !h(u) && (u = s.content = {
                        url: u
                    }), n.find("script").filter(o).remove(), n.parent().is(b.appendTo) || f.top !== t && f.left !== t || (n.is(H) ? (M = n.offset(), S = !0) : (a = n.css("visibility"), p = n.css("display"), n.css({
                        visibility: W,
                        display: ""
                    }), M = n.offset(), n.css({
                        visibility: a,
                        display: p
                    })), f.top === t && (f.top = M.top), f.left === t && (f.left = M.left)), e(s.visible) && null !== s.visible || (s.visible = n.is(H)), r = b.wrapper = n.closest(z), n.is(".k-content") && r[0] || (n.addClass("k-window-content k-content"), b._createWindow(n, s), r = b.wrapper = n.closest(z), b._dimensions()), b._position(), u && b.refresh(u), s.visible && b.toFront(), g = r.children(T), b._tabindex(g), s.visible && s.modal && b._overlay(r.is(H)).css({
                        opacity: .5
                    }), r.on("mouseenter" + k, ni, m(b._buttonEnter, b)).on("mouseleave" + k, ni, m(b._buttonLeave, b)).on("click" + k, "> " + ni, m(b._windowActionHandler, b)).on("keydown" + k, m(b._keydown, b)).on("focus" + k, m(b._focus, b)).on("blur" + k, m(b._blur, b)), g.on("keydown" + k, m(b._keydown, b)).on("focus" + k, m(b._focus, b)).on("blur" + k, m(b._blur, b)), w = g.find("." + P)[0], w && !_.data(si) && (_.on("blur" + k, function() {
                        var t, e = i(document.activeElement).parent(T);
                        e.length && (t = l.widgetInstance(e), t._focus())
                    }), _.on("focus" + k, function() {
                        i(T).not(O).each(function(t, e) {
                            l.widgetInstance(i(e))._blur()
                        })
                    }), _.data(si, !0)), this._resizable(), this._draggable(), s.pinned && this.wrapper.is(":visible") && b.pin(), v = n.attr("id"), v && (v += "_wnd_title", r.children(y).children(x).attr("id", v), g.attr({
                        role: "dialog",
                        "aria-labelledby": v
                    })), r.add(r.children(".k-resize-handle," + y)).on("mousedown" + k, m(b.toFront, b)), b.touchScroller = l.touchScroller(n), b._resizeHandler = m(b._onDocumentResize, b), b._marker = l.guid().substring(0, 8), i(window).on("resize" + k + b._marker, b._resizeHandler), s.visible && (b.trigger(F), b.trigger(N)), l.notify(b), this.options.modal && (this._tabKeyTrap = new c(r), this._tabKeyTrap.trap(), this._tabKeyTrap.shouldTrap = function() {
                        return g.data("isFront")
                    })
                },
                _buttonEnter: function(t) {
                    i(t.currentTarget).addClass(I)
                },
                _buttonLeave: function(t) {
                    i(t.currentTarget).removeClass(I)
                },
                _focus: function() {
                    this.wrapper.addClass(D)
                },
                _blur: function() {
                    this.wrapper.removeClass(D)
                },
                _dimensions: function() {
                    var i, t, e = this.wrapper,
                        o = this.options,
                        s = o.width,
                        r = o.height,
                        a = o.maxHeight,
                        l = ["minWidth", "minHeight", "maxWidth", "maxHeight"];
                    for (this.title(o.title), i = 0; i < l.length; i++) t = o[l[i]] || "", t != 1 / 0 && e.css(l[i], t);
                    a != 1 / 0 && this.element.css("maxHeight", a), e.width(s ? isNaN(s) && ("" + s).indexOf("px") < 0 ? s : n(s, o.minWidth, o.maxWidth) : ""), e.height(r ? isNaN(r) && ("" + r).indexOf("px") < 0 ? r : n(r, o.minHeight, o.maxHeight) : ""), o.visible || e.hide()
                },
                _position: function() {
                    var i = this.wrapper,
                        t = this.options.position;
                    0 === t.top && (t.top = "" + t.top), 0 === t.left && (t.left = "" + t.left), i.css({
                        top: t.top || "",
                        left: t.left || ""
                    })
                },
                _animationOptions: function(i) {
                    var t = this.options.animation,
                        e = {
                            open: {
                                effects: {}
                            },
                            close: {
                                hide: !0,
                                effects: {}
                            }
                        };
                    return t && t[i] || e[i]
                },
                _resize: function() {
                    l.resize(this.element.children())
                },
                _resizable: function() {
                    var t = this.options.resizable,
                        e = this.wrapper;
                    this.resizing && (e.off("dblclick" + k).children(M).remove(), this.resizing.destroy(), this.resizing = null), t && (e.on("dblclick" + k, y, m(function(t) {
                        i(t.target).closest(".k-window-action").length || this.toggleMaximization()
                    }, this)), _("n e s w se sw ne nw".split(" "), function(i, t) {
                        e.append(di.resizeHandle(t))
                    }), this.resizing = new s(this)), e = null
                },
                _draggable: function() {
                    var i = this.options.draggable;
                    this.dragging && (this.dragging.destroy(), this.dragging = null), i && (this.dragging = new a(this, i.dragHandle || y))
                },
                _actions: function() {
                    var t = this.options,
                        e = t.actions,
                        n = t.pinned,
                        o = this.wrapper.children(y),
                        s = o.find(".k-window-actions"),
                        r = ["maximize", "minimize"];
                    e = i.map(e, function(i) {
                        return i = n && "pin" === i.toLowerCase() ? "unpin" : i, {
                            name: r.indexOf(i.toLowerCase()) > -1 ? "window-" + i : i
                        }
                    }), s.html(l.render(di.action, e))
                },
                setOptions: function(i) {
                    var e, n, o = JSON.parse(JSON.stringify(i));
                    w(i.position, this.options.position), w(i.position, o.position), d.fn.setOptions.call(this, i), e = this.options.scrollable !== !1, this.restore(), this._dimensions(), this._position(), this._resizable(), this._draggable(), this._actions(), t !== i.modal && (n = this.options.visible !== !1, this._overlay(i.modal && n)), this.element.css(X, e ? "" : "hidden")
                },
                events: [F, N, R, j, K, q, A, U, G, J, V, B, Q],
                options: {
                    name: "Window",
                    animation: {
                        open: {
                            effects: {
                                zoom: {
                                    direction: "in"
                                },
                                fade: {
                                    direction: "in"
                                }
                            },
                            duration: 350
                        },
                        close: {
                            effects: {
                                zoom: {
                                    direction: "out",
                                    properties: {
                                        scale: .7
                                    }
                                },
                                fade: {
                                    direction: "out"
                                }
                            },
                            duration: 350,
                            hide: !0
                        }
                    },
                    title: "",
                    actions: ["Close"],
                    autoFocus: !0,
                    modal: !1,
                    resizable: !0,
                    draggable: !0,
                    minWidth: 90,
                    minHeight: 50,
                    maxWidth: 1 / 0,
                    maxHeight: 1 / 0,
                    pinned: !1,
                    scrollable: !0,
                    position: {},
                    content: null,
                    visible: null,
                    height: null,
                    width: null,
                    appendTo: "body",
                    isMaximized: !1,
                    isMinimized: !1
                },
                _closable: function() {
                    return i.inArray("close", i.map(this.options.actions, function(i) {
                        return i.toLowerCase()
                    })) > -1
                },
                _keydown: function(i) {
                    var t, e, o, s, r, a, d = this,
                        c = d.options,
                        p = l.keys,
                        h = i.keyCode,
                        f = d.wrapper,
                        u = 10,
                        g = d.options.isMaximized,
                        m = d.options.isMinimized;
                    h == p.ESC && d._closable() && d._close(!1), i.target != i.currentTarget || d._closing || (i.altKey && 82 == h && d.refresh(), i.altKey && 80 == h && (d.options.pinned ? d.unpin() : d.pin()), i.altKey && h == p.UP ? m ? (d.restore(), d.element.focus()) : g || (d.maximize(), d.element.focus()) : i.altKey && h == p.DOWN && (m || g ? g && (d.restore(), d.element.focus()) : (d.minimize(), d.wrapper.focus())), !c.draggable || i.ctrlKey || i.altKey || g || (t = l.getOffset(f), h == p.UP ? e = f.css("top", t.top - u) : h == p.DOWN ? e = f.css("top", t.top + u) : h == p.LEFT ? e = f.css("left", t.left - u) : h == p.RIGHT && (e = f.css("left", t.left + u))), c.resizable && i.ctrlKey && !g && !m && (h == p.UP ? (e = !0, s = f.height() - u) : h == p.DOWN && (e = !0, s = f.height() + u), h == p.LEFT ? (e = !0, o = f.width() - u) : h == p.RIGHT && (e = !0, o = f.width() + u), e && (r = n(o, c.minWidth, c.maxWidth), a = n(s, c.minHeight, c.maxHeight), isNaN(r) || (f.width(r), d.options.width = r + "px"), isNaN(a) || (f.height(a), d.options.height = a + "px"), d.resize())), e && i.preventDefault())
                },
                _overlay: function(t) {
                    var e = this.appendTo.children(S),
                        n = this.wrapper;
                    return e.length || (e = i("<div class='k-overlay' />")), e.insertBefore(n[0]).toggle(t).css($, parseInt(n.css($), 10) - 1), e
                },
                _actionForIcon: function(i) {
                    var t = /\bk-i(-\w+)+\b/.exec(i[0].className)[0];
                    return {
                        "k-i-close": "_close",
                        "k-i-window-maximize": "maximize",
                        "k-i-window-minimize": "minimize",
                        "k-i-window-restore": "restore",
                        "k-i-refresh": "refresh",
                        "k-i-pin": "pin",
                        "k-i-unpin": "unpin"
                    }[t]
                },
                _windowActionHandler: function(e) {
                    var n, o;
                    if (!this._closing) return n = i(e.target).closest(".k-window-action").find(".k-icon"), o = this._actionForIcon(n), o ? (e.preventDefault(), this[o](), !1) : t
                },
                _modals: function() {
                    var t = this,
                        e = i(z).filter(function() {
                            var e = i(this),
                                n = t._object(e),
                                o = n && n.options;
                            return o && o.modal && o.visible && o.appendTo === t.options.appendTo && e.is(H)
                        }).sort(function(t, e) {
                            return +i(t).css("zIndex") - +i(e).css("zIndex")
                        });
                    return t = null, e
                },
                _object: function(i) {
                    var e = i.children(T),
                        n = l.widgetInstance(e);
                    return n ? n : t
                },
                center: function() {
                    var t, e, n = this,
                        o = n.options.position,
                        s = n.wrapper,
                        r = i(window),
                        a = 0,
                        l = 0;
                    return n.options.isMaximized ? n : (n.options.pinned && !n._isPinned && n.pin(), n.options.pinned || (a = r.scrollTop(), l = r.scrollLeft()), e = l + Math.max(0, (r.width() - s.width()) / 2), t = a + Math.max(0, (r.height() - s.height() - parseInt(s.css("paddingTop"), 10)) / 2), s.css({
                        left: e,
                        top: t
                    }), o.top = t, o.left = e, n)
                },
                title: function(i) {
                    var t, e = this,
                        n = e.wrapper,
                        o = e.options,
                        s = n.children(y),
                        r = s.children(x);
                    return arguments.length ? (i === !1 ? (n.addClass("k-window-titleless"), s.remove()) : (s.length ? r.html(l.htmlEncode(i)) : (n.prepend(di.titlebar(o)), e._actions(), s = n.children(y)), t = parseInt(g(s), 10), n.css("padding-top", t), s.css("margin-top", -t)), e.options.title = i, e) : r.html()
                },
                content: function(i, t) {
                    var n = this.wrapper.children(T),
                        o = n.children(".km-scroll-container");
                    return n = o[0] ? o : n, e(i) ? (this.angular("cleanup", function() {
                        return {
                            elements: n.children()
                        }
                    }), l.destroy(this.element.children()), n.empty().html(i), this.angular("compile", function() {
                        var i, e = [];
                        for (i = n.length; --i >= 0;) e.push({
                            dataItem: t
                        });
                        return {
                            elements: n.children(),
                            data: e
                        }
                    }), this) : n.html()
                },
                open: function() {
                    var t, e, n, o = this,
                        s = o.wrapper,
                        r = o.options,
                        a = this._animationOptions("open"),
                        d = s.children(T),
                        c = i(document);
                    return o.trigger(F) || (o._closing && s.kendoStop(!0, !0), o._closing = !1, o.toFront(), r.autoFocus && o.element.focus(), r.visible = !0, r.modal && (e = !!o._modals().length, t = o._overlay(e), t.kendoStop(!0, !0), a.duration && l.effects.Fade && !e ? (n = l.fx(t).fadeIn(), n.duration(a.duration || 0), n.endValue(.5), n.play()) : t.css("opacity", .5), t.show(), i(window).on("focus", function() {
                        d.data("isFront") && !i(document.activeElement).closest(d).length && o.element.focus()
                    })), s.is(H) || (d.css(X, W), s.show().kendoStop().kendoAnimate({
                        effects: a.effects,
                        duration: a.duration,
                        complete: m(this._activate, this)
                    }))), r.isMaximized && (o._documentScrollTop = c.scrollTop(), o._documentScrollLeft = c.scrollLeft(), o._stopDocumentScrolling()), r.pinned && !o._isPinned && o.pin(), o
                },
                _activate: function() {
                    var i = this.options.scrollable !== !1;
                    this.options.autoFocus && this.element.focus(), this.element.css(X, i ? "" : "hidden"), l.resize(this.element.children()), this.trigger(N)
                },
                _removeOverlay: function(e) {
                    var n, o = this._modals(),
                        s = this.options,
                        r = s.modal && !o.length,
                        a = s.modal ? this._overlay(!0) : i(t),
                        d = this._animationOptions("close");
                    r ? !e && d.duration && l.effects.Fade ? (n = l.fx(a).fadeOut(), n.duration(d.duration || 0), n.startValue(.5), n.play()) : this._overlay(!1).remove() : o.length && this._object(o.last())._overlay(!0)
                },
                _close: function(t) {
                    var e, n = this,
                        o = n.wrapper,
                        s = n.options,
                        r = this._animationOptions("open"),
                        a = this._animationOptions("close"),
                        l = i(document);
                    n._closing || (e = n.trigger(j, {
                        userTriggered: !t
                    }), n._closing = !e, o.is(H) && !e && (s.visible = !1, i(z).each(function(t, e) {
                        var n = i(e).children(T);
                        e != o && n.find("> ." + P).length > 0 && n.children(S).remove()
                    }), this._removeOverlay(), o.kendoStop().kendoAnimate({
                        effects: a.effects || r.effects,
                        reverse: a.reverse === !0,
                        duration: a.duration,
                        complete: m(this._deactivate, this)
                    })), n.options.isMaximized && (n._enableDocumentScrolling(), n._documentScrollTop && n._documentScrollTop > 0 && l.scrollTop(n._documentScrollTop), n._documentScrollLeft && n._documentScrollLeft > 0 && l.scrollLeft(n._documentScrollLeft)))
                },
                _deactivate: function() {
                    var i, t = this;
                    t.wrapper.hide().css("opacity", ""), t.trigger(R), t.options.modal && (i = t._object(t._modals().last()), i && i.toFront())
                },
                close: function() {
                    return this._close(!0), this
                },
                _actionable: function(t) {
                    return i(t).is(ni + "," + ni + " .k-icon,:input,a")
                },
                _shouldFocus: function(t) {
                    var e = f(),
                        n = this.element;
                    return this.options.autoFocus && !i(e).is(n) && !this._actionable(t) && (!n.find(e).length || !n.find(t).length)
                },
                toFront: function(t) {
                    var e, n, o = this,
                        s = o.wrapper,
                        r = s[0],
                        a = +s.css($),
                        l = a,
                        d = t && t.target || null;
                    return i(z).each(function(t, e) {
                        var n = i(e),
                            o = n.css($),
                            s = n.children(T);
                        isNaN(o) || (a = Math.max(+o, a)), s.data("isFront", e == r), e != r && s.find("> ." + P).length > 0 && s.append(di.overlay)
                    }), (!s[0].style.zIndex || l < a) && s.css($, a + 2), o.element.find("> .k-overlay").remove(), o._shouldFocus(d) && (o.isMinimized() ? o.wrapper.focus() : i(d).is(S) ? setTimeout(function() {
                        o.element.focus()
                    }) : o.element.focus(), e = i(window).scrollTop(), n = parseInt(s.position().top, 10), !o.options.pinned && n > 0 && n < e && (e > 0 ? i(window).scrollTop(n) : s.css("top", e))), s = null, o
                },
                toggleMaximization: function() {
                    return this._closing ? this : this[this.options.isMaximized ? "restore" : "maximize"]()
                },
                restore: function() {
                    var t = this,
                        e = t.options,
                        n = e.minHeight,
                        o = t.restoreOptions,
                        s = i(document);
                    return e.isMaximized || e.isMinimized ? (n && n != 1 / 0 && t.wrapper.css("min-height", n), t.wrapper.css({
                        position: e.pinned ? "fixed" : "absolute",
                        left: o.left,
                        top: o.top,
                        width: o.width,
                        height: o.height
                    }).removeClass(C).find(".k-window-content,.k-resize-handle").show().end().find(".k-window-titlebar .k-i-window-restore").parent().remove().end().end().find(Z).parent().show().end().end().find(ei).parent().show(), e.isMaximized ? t.wrapper.find(".k-i-window-maximize").parent().focus() : e.isMinimized && t.wrapper.find(".k-i-window-minimize").parent().focus(), t.options.width = o.width, t.options.height = o.height, t._enableDocumentScrolling(), this._documentScrollTop && this._documentScrollTop > 0 && s.scrollTop(this._documentScrollTop), this._documentScrollLeft && this._documentScrollLeft > 0 && s.scrollLeft(this._documentScrollLeft), e.isMaximized = e.isMinimized = !1, this.wrapper.removeAttr("tabindex"), this.wrapper.removeAttr("aria-labelled-by"), t.resize(), t) : t
                },
                _sizingAction: function(i, t) {
                    var e = this,
                        n = e.wrapper,
                        o = n[0].style,
                        s = e.options;
                    return s.isMaximized || s.isMinimized ? e : (e.restoreOptions = {
                        width: o.width,
                        height: o.height
                    }, n.children(M).hide().end().children(y).find(Z).parent().hide().eq(0).before(di.action({
                        name: "window-restore"
                    })), t.call(e), e.wrapper.children(y).find(ei).parent().toggle("maximize" !== i), e.trigger(i), n.find(".k-i-window-restore").parent().focus(), e)
                },
                maximize: function() {
                    return this._sizingAction("maximize", function() {
                        var t = this,
                            e = t.wrapper,
                            n = e.position(),
                            o = i(document);
                        w(t.restoreOptions, {
                            left: n.left,
                            top: n.top
                        }), e.css({
                            left: 0,
                            top: 0,
                            position: "fixed"
                        }).addClass(C), this._documentScrollTop = o.scrollTop(), this._documentScrollLeft = o.scrollLeft(), t._stopDocumentScrolling(), t.options.isMaximized = !0, t._onDocumentResize()
                    }), this
                },
                _stopDocumentScrolling: function() {
                    var t, e = this,
                        n = i("body");
                    e._storeOverflowRule(n), n.css(X, W), t = i("html"), e._storeOverflowRule(t), t.css(X, W)
                },
                _enableDocumentScrolling: function() {
                    var t = this;
                    t._restoreOverflowRule(i(document.body)), t._restoreOverflowRule(i("html"))
                },
                _storeOverflowRule: function(i) {
                    if (!this._isOverflowStored(i)) {
                        var t = i.get(0).style.overflow;
                        "string" == typeof t && i.data(Y, t)
                    }
                },
                _isOverflowStored: function(i) {
                    return "string" == typeof i.data(Y)
                },
                _restoreOverflowRule: function(i) {
                    var e = i.data(Y);
                    null !== e && e !== t ? (i.css(X, e), i.removeData(Y)) : i.css(X, "")
                },
                isMaximized: function() {
                    return this.options.isMaximized
                },
                minimize: function() {
                    return this._sizingAction("minimize", function() {
                        var i = this;
                        i.wrapper.css({
                            height: "",
                            minHeight: ""
                        }), i.element.hide(), i.options.isMinimized = !0
                    }), this.wrapper.attr("tabindex", 0), this.wrapper.attr("aria-labelled-by", this.element.attr("aria-labelled-by")), this
                },
                isMinimized: function() {
                    return this.options.isMinimized
                },
                pin: function() {
                    var t = this,
                        e = i(window),
                        n = t.wrapper,
                        o = parseInt(n.css("top"), 10),
                        s = parseInt(n.css("left"), 10);
                    t.options.isMaximized || (n.css({
                        position: "fixed",
                        top: o - e.scrollTop(),
                        left: s - e.scrollLeft()
                    }), n.children(y).find(ii).addClass("k-i-unpin").removeClass("k-i-pin"), t._isPinned = !0, t.options.pinned = !0)
                },
                unpin: function() {
                    var t = this,
                        e = i(window),
                        n = t.wrapper,
                        o = parseInt(n.css("top"), 10),
                        s = parseInt(n.css("left"), 10);
                    t.options.isMaximized || (n.css({
                        position: "",
                        top: o + e.scrollTop(),
                        left: s + e.scrollLeft()
                    }), n.children(y).find(ti).addClass("k-i-pin").removeClass("k-i-unpin"), t._isPinned = !1, t.options.pinned = !1)
                },
                _onDocumentResize: function() {
                    var t, e, n, o, s = this,
                        r = s.wrapper,
                        a = i(window),
                        d = l.support.zoomLevel();
                    s.options.isMaximized && (n = parseInt(r.css("border-left-width"), 10) + parseInt(r.css("border-right-width"), 10), o = parseInt(r.css("border-top-width"), 10) + parseInt(r.css("border-bottom-width"), 10), t = a.width() / d - n, e = a.height() / d - parseInt(r.css("padding-top"), 10) - o, r.css({
                        width: t,
                        height: e
                    }), s.options.width = t, s.options.height = e, s.resize())
                },
                refresh: function(t) {
                    var n, o, s, r = this,
                        a = r.options,
                        l = i(r.element);
                    return h(t) || (t = {
                        url: t
                    }), t = w({}, a.content, t), o = e(a.iframe) ? a.iframe : t.iframe, s = t.url, s ? (e(o) || (o = !ai(s)), o ? (n = l.find("." + P)[0], n ? n.src = s || n.src : l.html(di.contentFrame(w({}, a, {
                        content: t
                    }))), l.find("." + P).unbind("load" + k).on("load" + k, m(this._triggerRefresh, this))) : r._ajaxRequest(t)) : (t.template && r.content(v(t.template)({})), r.trigger(A)), l.toggleClass("k-window-iframecontent", !!o), r
                },
                _triggerRefresh: function() {
                    this.trigger(A)
                },
                _ajaxComplete: function() {
                    clearTimeout(this._loadingIconTimeout), this.wrapper.find(oi).removeClass(L)
                },
                _ajaxError: function(i, t) {
                    this.trigger(Q, {
                        status: t,
                        xhr: i
                    })
                },
                _ajaxSuccess: function(i) {
                    return function(t) {
                        var e = t;
                        i && (e = v(i)(t || {})), this.content(e, t), this.element.prop("scrollTop", 0), this.trigger(A)
                    }
                },
                _showLoading: function() {
                    this.wrapper.find(oi).addClass(L)
                },
                _ajaxRequest: function(t) {
                    this._loadingIconTimeout = setTimeout(m(this._showLoading, this), 100), i.ajax(w({
                        type: "GET",
                        dataType: "html",
                        cache: !1,
                        error: m(this._ajaxError, this),
                        complete: m(this._ajaxComplete, this),
                        success: m(this._ajaxSuccess(t.template), this)
                    }, t))
                },
                _destroy: function() {
                    this.resizing && this.resizing.destroy(), this.dragging && this.dragging.destroy(), this.wrapper.off(k).children(T).off(k).end().find(".k-resize-handle,.k-window-titlebar").off(k), i(window).off("resize" + k + this._marker), i(window).off(k), clearTimeout(this._loadingIconTimeout), d.fn.destroy.call(this), this.unbind(t), l.destroy(this.wrapper), this._removeOverlay(!0)
                },
                destroy: function() {
                    this._destroy(), this.wrapper.empty().remove(), this.wrapper = this.appendTo = this.element = i()
                },
                _createWindow: function() {
                    var t, e, n = this.element,
                        o = this.options,
                        s = l.support.isRtl(n);
                    o.scrollable === !1 && n.css("overflow", "hidden"), e = i(di.wrapper(o)), t = n.find("iframe:not(.k-content)").map(function() {
                        var i = this.getAttribute("src");
                        return this.src = "", i
                    }), e.toggleClass("k-rtl", s).appendTo(this.appendTo).append(n).find("iframe:not(.k-content)").each(function(i) {
                        this.src = t[i]
                    }), e.find(".k-window-title").css(s ? "left" : "right", u(e.find(".k-window-actions")) + 10), n.css("visibility", "").show(), n.find("[data-role=editor]").each(function() {
                        var t = i(this).data("kendoEditor");
                        t && t.refresh()
                    }), e = n = null
                }
            }),
            di = {
                wrapper: v("<div class='k-widget k-window' />"),
                action: v("<a role='button' href='\\#' class='k-button k-bare k-button-icon k-window-action' aria-label='#= name #'><span class='k-icon k-i-#= name.toLowerCase() #'></span></a>"),
                titlebar: v("<div class='k-window-titlebar k-header'>&nbsp;<span class='k-window-title'>#: title #</span><div class='k-window-actions' /></div>"),
                overlay: "<div class='k-overlay' />",
                contentFrame: v("<iframe frameborder='0' title='#= title #' class='" + P + "' src='#= content.url #'>This page requires frames in order to show content</iframe>"),
                resizeHandle: v("<div class='k-resize-handle k-resize-#= data #'></div>")
            };
        s.prototype = {
            addOverlay: function() {
                this.owner.wrapper.append(di.overlay)
            },
            removeOverlay: function() {
                this.owner.wrapper.find(S).remove()
            },
            dragstart: function(t) {
                var e, n, o, s, a, d, c, p = this,
                    h = p.owner,
                    f = h.wrapper;
                p._preventDragging = h.trigger(U), p._preventDragging || (p.elementPadding = parseInt(f.css("padding-top"), 10), p.initialPosition = l.getOffset(f, "position"), p.resizeDirection = t.currentTarget.prop("className").replace("k-resize-handle k-resize-", ""), p.initialSize = {
                    width: f.width(),
                    height: f.height()
                }, p.containerOffset = l.getOffset(h.appendTo, "position"), e = f.offsetParent(), e.is("html") ? p.containerOffset.top = p.containerOffset.left = 0 : (n = e.css("margin-top"), o = e.css("margin-left"), s = !ri.test(n) || !ri.test(o), s && (a = r(f[0]), d = a.left - p.containerOffset.left - p.initialPosition.left, c = a.top - p.containerOffset.top - p.initialPosition.top, p._relativeElMarginLeft = d > 1 ? d : 0, p._relativeElMarginTop = c > 1 ? c : 0, p.initialPosition.left += p._relativeElMarginLeft, p.initialPosition.top += p._relativeElMarginTop)), f.children(M).not(t.currentTarget).hide(), i(b).css(E, t.currentTarget.css(E)))
            },
            drag: function(t) {
                var e, o, s, r, a, l, d, c, p, h, f, u, g, m, w;
                this._preventDragging || (e = this, o = e.owner, s = o.wrapper, r = o.options, a = e.resizeDirection, l = e.containerOffset, d = e.initialPosition, c = e.initialSize, g = Math.max(t.x.location, 0), m = Math.max(t.y.location, 0), a.indexOf("e") >= 0 ? (p = g - d.left - l.left, s.width(n(p, r.minWidth, r.maxWidth))) : a.indexOf("w") >= 0 && (u = d.left + c.width + l.left, p = n(u - g, r.minWidth, r.maxWidth), s.css({
                    left: u - p - l.left - (e._relativeElMarginLeft || 0),
                    width: p
                })), w = m, o.options.pinned && (w -= i(window).scrollTop()), a.indexOf("s") >= 0 ? (h = w - d.top - e.elementPadding - l.top, s.height(n(h, r.minHeight, r.maxHeight))) : a.indexOf("n") >= 0 && (f = d.top + c.height + l.top, h = n(f - w, r.minHeight, r.maxHeight), s.css({
                    top: f - h - l.top - (e._relativeElMarginTop || 0),
                    height: h
                })), p && (o.options.width = p + "px"), h && (o.options.height = h + "px"), o.resize())
            },
            dragend: function(t) {
                if (!this._preventDragging) {
                    var e = this,
                        n = e.owner,
                        o = n.wrapper;
                    return o.children(M).not(t.currentTarget).show(), i(b).css(E, ""), n.touchScroller && n.touchScroller.reset(), 27 == t.keyCode && o.css(e.initialPosition).css(e.initialSize), n.trigger(J), !1
                }
            },
            destroy: function() {
                this._draggable && this._draggable.destroy(), this._draggable = this.owner = null
            }
        }, a.prototype = {
            dragstart: function(t) {
                var e = this.owner,
                    n = e.element,
                    o = n.find(".k-window-actions"),
                    s = l.getOffset(e.appendTo);
                this._preventDragging = e.trigger(V) || !e.options.draggable, this._preventDragging || (e.initialWindowPosition = l.getOffset(e.wrapper, "position"), e.initialPointerPosition = {
                    left: t.x.client,
                    top: t.y.client
                }, e.startPosition = {
                    left: t.x.client - e.initialWindowPosition.left,
                    top: t.y.client - e.initialWindowPosition.top
                }, e.minLeftPosition = o.length > 0 ? u(o) + parseInt(o.css("right"), 10) - u(n) : 20 - u(n), e.minLeftPosition -= s.left, e.minTopPosition = -s.top, e.wrapper.append(di.overlay).children(M).hide(), i(b).css(E, t.currentTarget.css(E)))
            },
            drag: function(t) {
                var e, n;
                this._preventDragging || (e = this.owner, n = e.options.position, n.top = Math.max(t.y.client - e.startPosition.top, e.minTopPosition), n.left = Math.max(t.x.client - e.startPosition.left, e.minLeftPosition), l.support.transforms ? i(e.wrapper).css("transform", "translate(" + (t.x.client - e.initialPointerPosition.left) + "px, " + (t.y.client - e.initialPointerPosition.top) + "px)") : i(e.wrapper).css(n))
            },
            _finishDrag: function() {
                var t = this.owner;
                t.wrapper.children(M).toggle(!t.options.isMinimized).end().find(S).remove(), i(b).css(E, "")
            },
            dragcancel: function(i) {
                this._preventDragging || (this._finishDrag(), i.currentTarget.closest(z).css(this.owner.initialWindowPosition))
            },
            dragend: function() {
                if (!this._preventDragging) return i(this.owner.wrapper).css(this.owner.options.position).css("transform", ""), this._finishDrag(), this.owner.trigger(B), !1
            },
            destroy: function() {
                this._draggable && this._draggable.destroy(), this._draggable = this.owner = null
            }
        }, l.ui.plugin(li)
    }(window.kendo.jQuery), window.kendo
}, "function" == typeof define && define.amd ? define : function(i, t, e) {
    (e || t)()
});
//# sourceMappingURL=kendo.window.min.js.map;
/** 
 * Kendo UI v2018.1.221 (http://www.telerik.com/kendo-ui)                                                                                                                                               
 * Copyright 2018 Telerik AD. All rights reserved.                                                                                                                                                      
 *                                                                                                                                                                                                      
 * Kendo UI commercial licenses may be obtained at                                                                                                                                                      
 * http://www.telerik.com/purchase/license-agreement/kendo-ui-complete                                                                                                                                  
 * If you do not own a commercial license, this file shall be governed by the trial license terms.                                                                                                      

*/
! function(e, define) {
    define("kendo.mobile.scroller.min", ["kendo.fx.min", "kendo.draganddrop.min"], e)
}(function() {
    return function(e, i) {
        var n = window.kendo,
            t = n.mobile,
            s = n.effects,
            o = t.ui,
            l = e.proxy,
            a = e.extend,
            r = o.Widget,
            c = n.Class,
            h = n.ui.Movable,
            u = n.ui.Pane,
            d = n.ui.PaneDimensions,
            m = s.Transition,
            f = s.Animation,
            p = Math.abs,
            v = 500,
            b = .7,
            x = .96,
            y = 10,
            T = 55,
            w = .5,
            g = 5,
            _ = "km-scroller-release",
            E = "km-scroller-refresh",
            C = "pull",
            k = "change",
            S = "resize",
            z = "scroll",
            M = 2,
            A = f.extend({
                init: function(e) {
                    var i = this;
                    f.fn.init.call(i), a(i, e), i.userEvents.bind("gestureend", l(i.start, i)), i.tapCapture.bind("press", l(i.cancel, i))
                },
                enabled: function() {
                    return this.movable.scale < this.dimensions.minScale
                },
                done: function() {
                    return this.dimensions.minScale - this.movable.scale < .01
                },
                tick: function() {
                    var e = this.movable;
                    e.scaleWith(1.1), this.dimensions.rescale(e.scale)
                },
                onEnd: function() {
                    var e = this.movable;
                    e.scaleTo(this.dimensions.minScale), this.dimensions.rescale(e.scale)
                }
            }),
            O = f.extend({
                init: function(e) {
                    var i = this;
                    f.fn.init.call(i), a(i, e, {
                        transition: new m({
                            axis: e.axis,
                            movable: e.movable,
                            onEnd: function() {
                                i._end()
                            }
                        })
                    }), i.tapCapture.bind("press", function() {
                        i.cancel()
                    }), i.userEvents.bind("end", l(i.start, i)), i.userEvents.bind("gestureend", l(i.start, i)), i.userEvents.bind("tap", l(i.onEnd, i))
                },
                onCancel: function() {
                    this.transition.cancel()
                },
                freeze: function(e) {
                    var i = this;
                    i.cancel(), i._moveTo(e)
                },
                onEnd: function() {
                    var e = this;
                    e.paneAxis.outOfBounds() ? e._snapBack() : e._end()
                },
                done: function() {
                    return p(this.velocity) < 1
                },
                start: function(e) {
                    var i, n = this;
                    n.dimension.enabled && (n.paneAxis.outOfBounds() ? n._snapBack() : (i = e.touch.id === M ? 0 : e.touch[n.axis].velocity, n.velocity = Math.max(Math.min(i * n.velocityMultiplier, T), -T), n.tapCapture.captureNext(), f.fn.start.call(n)))
                },
                tick: function() {
                    var e = this,
                        i = e.dimension,
                        n = e.paneAxis.outOfBounds() ? w : e.friction,
                        t = e.velocity *= n,
                        s = e.movable[e.axis] + t;
                    !e.elastic && i.outOfBounds(s) && (s = Math.max(Math.min(s, i.max), i.min), e.velocity = 0), e.movable.moveAxis(e.axis, s)
                },
                _end: function() {
                    this.tapCapture.cancelCapture(), this.end()
                },
                _snapBack: function() {
                    var e = this,
                        i = e.dimension,
                        n = e.movable[e.axis] > i.max ? i.max : i.min;
                    e._moveTo(n)
                },
                _moveTo: function(e) {
                    this.transition.moveTo({
                        location: e,
                        duration: v,
                        ease: m.easeOutExpo
                    })
                }
            }),
            H = f.extend({
                init: function(e) {
                    var i = this;
                    n.effects.Animation.fn.init.call(this), a(i, e, {
                        origin: {},
                        destination: {},
                        offset: {}
                    })
                },
                tick: function() {
                    this._updateCoordinates(), this.moveTo(this.origin)
                },
                done: function() {
                    return p(this.offset.y) < g && p(this.offset.x) < g
                },
                onEnd: function() {
                    this.moveTo(this.destination), this.callback && this.callback.call()
                },
                setCoordinates: function(e, i) {
                    this.offset = {}, this.origin = e, this.destination = i
                },
                setCallback: function(e) {
                    e && n.isFunction(e) ? this.callback = e : e = i
                },
                _updateCoordinates: function() {
                    this.offset = {
                        x: (this.destination.x - this.origin.x) / 4,
                        y: (this.destination.y - this.origin.y) / 4
                    }, this.origin = {
                        y: this.origin.y + this.offset.y,
                        x: this.origin.x + this.offset.x
                    }
                }
            }),
            B = c.extend({
                init: function(i) {
                    var n = this,
                        t = "x" === i.axis,
                        s = e('<div class="km-touch-scrollbar km-' + (t ? "horizontal" : "vertical") + '-scrollbar" />');
                    a(n, i, {
                        element: s,
                        elementSize: 0,
                        movable: new h(s),
                        scrollMovable: i.movable,
                        alwaysVisible: i.alwaysVisible,
                        size: t ? "width" : "height"
                    }), n.scrollMovable.bind(k, l(n.refresh, n)), n.container.append(s), i.alwaysVisible && n.show()
                },
                refresh: function() {
                    var e = this,
                        i = e.axis,
                        n = e.dimension,
                        t = n.size,
                        s = e.scrollMovable,
                        o = t / n.total,
                        l = Math.round(-s[i] * o),
                        a = Math.round(t * o);
                    o >= 1 ? this.element.css("display", "none") : this.element.css("display", ""), l + a > t ? a = t - l : l < 0 && (a += l, l = 0), e.elementSize != a && (e.element.css(e.size, a + "px"), e.elementSize = a), e.movable.moveAxis(i, l)
                },
                show: function() {
                    this.element.css({
                        opacity: b,
                        visibility: "visible"
                    })
                },
                hide: function() {
                    this.alwaysVisible || this.element.css({
                        opacity: 0
                    })
                }
            }),
            R = r.extend({
                init: function(t, s) {
                    var o, c, m, f, v, b, x, y, T, w = this;
                    return r.fn.init.call(w, t, s), t = w.element, (w._native = w.options.useNative && n.support.hasNativeScrolling) ? (t.addClass("km-native-scroller").prepend('<div class="km-scroll-header"/>'), a(w, {
                        scrollElement: t,
                        fixedContainer: t.children().first()
                    }), i) : (t.css("overflow", "hidden").addClass("km-scroll-wrapper").wrapInner('<div class="km-scroll-container"/>').prepend('<div class="km-scroll-header"/>'), o = t.children().eq(1), c = new n.TapCapture(t), m = new h(o), f = new d({
                        element: o,
                        container: t,
                        forcedEnabled: w.options.zoom
                    }), v = this.options.avoidScrolling, b = new n.UserEvents(t, {
                        touchAction: "pan-y",
                        fastTap: !0,
                        allowSelection: !0,
                        preventDragEvent: !0,
                        captureUpIfMoved: !0,
                        multiTouch: w.options.zoom,
                        start: function(i) {
                            f.refresh();
                            var n = p(i.x.velocity),
                                t = p(i.y.velocity),
                                s = 2 * n >= t,
                                o = e.contains(w.fixedContainer[0], i.event.target),
                                l = 2 * t >= n;
                            !o && !v(i) && w.enabled && (f.x.enabled && s || f.y.enabled && l) ? b.capture() : b.cancel()
                        }
                    }), x = new u({
                        movable: m,
                        dimensions: f,
                        userEvents: b,
                        elastic: w.options.elastic
                    }), y = new A({
                        movable: m,
                        dimensions: f,
                        userEvents: b,
                        tapCapture: c
                    }), T = new H({
                        moveTo: function(e) {
                            w.scrollTo(e.x, e.y)
                        }
                    }), m.bind(k, function() {
                        w.scrollTop = -m.y, w.scrollLeft = -m.x, w.trigger(z, {
                            scrollTop: w.scrollTop,
                            scrollLeft: w.scrollLeft
                        })
                    }), w.options.mousewheelScrolling && t.on("DOMMouseScroll mousewheel", l(this, "_wheelScroll")), a(w, {
                        movable: m,
                        dimensions: f,
                        zoomSnapBack: y,
                        animatedScroller: T,
                        userEvents: b,
                        pane: x,
                        tapCapture: c,
                        pulled: !1,
                        enabled: !0,
                        scrollElement: o,
                        scrollTop: 0,
                        scrollLeft: 0,
                        fixedContainer: t.children().first()
                    }), w._initAxis("x"), w._initAxis("y"), w._wheelEnd = function() {
                        w._wheel = !1, w.userEvents.end(0, w._wheelY)
                    }, f.refresh(), w.options.pullToRefresh && w._initPullToRefresh(), i)
                },
                _wheelScroll: function(e) {
                    this._wheel || (this._wheel = !0, this._wheelY = 0, this.userEvents.press(0, this._wheelY)), clearTimeout(this._wheelTimeout), this._wheelTimeout = setTimeout(this._wheelEnd, 50);
                    var i = n.wheelDeltaY(e);
                    i && (this._wheelY += i, this.userEvents.move(0, this._wheelY)), e.preventDefault()
                },
                makeVirtual: function() {
                    this.dimensions.y.makeVirtual()
                },
                virtualSize: function(e, i) {
                    this.dimensions.y.virtualSize(e, i)
                },
                height: function() {
                    return this.dimensions.y.size
                },
                scrollHeight: function() {
                    return this.scrollElement[0].scrollHeight
                },
                scrollWidth: function() {
                    return this.scrollElement[0].scrollWidth
                },
                options: {
                    name: "Scroller",
                    zoom: !1,
                    pullOffset: 140,
                    visibleScrollHints: !1,
                    elastic: !0,
                    useNative: !1,
                    mousewheelScrolling: !0,
                    avoidScrolling: function() {
                        return !1
                    },
                    pullToRefresh: !1,
                    messages: {
                        pullTemplate: "Pull to refresh",
                        releaseTemplate: "Release to refresh",
                        refreshTemplate: "Refreshing"
                    }
                },
                events: [C, z, S],
                _resize: function() {
                    this._native || this.contentResized()
                },
                setOptions: function(e) {
                    var i = this;
                    r.fn.setOptions.call(i, e), e.pullToRefresh && i._initPullToRefresh()
                },
                reset: function() {
                    this._native ? this.scrollElement.scrollTop(0) : (this.movable.moveTo({
                        x: 0,
                        y: 0
                    }), this._scale(1))
                },
                contentResized: function() {
                    this.dimensions.refresh(), this.pane.x.outOfBounds() && this.movable.moveAxis("x", this.dimensions.x.min), this.pane.y.outOfBounds() && this.movable.moveAxis("y", this.dimensions.y.min)
                },
                zoomOut: function() {
                    var e = this.dimensions;
                    e.refresh(), this._scale(e.fitScale), this.movable.moveTo(e.centerCoordinates())
                },
                enable: function() {
                    this.enabled = !0
                },
                disable: function() {
                    this.enabled = !1
                },
                scrollTo: function(e, i) {
                    this._native ? (this.scrollElement.scrollLeft(p(e)), this.scrollElement.scrollTop(p(i))) : (this.dimensions.refresh(), this.movable.moveTo({
                        x: e,
                        y: i
                    }))
                },
                animatedScrollTo: function(e, i, n) {
                    var t, s;
                    this._native ? this.scrollTo(e, i) : (t = {
                        x: this.movable.x,
                        y: this.movable.y
                    }, s = {
                        x: e,
                        y: i
                    }, this.animatedScroller.setCoordinates(t, s), this.animatedScroller.setCallback(n), this.animatedScroller.start())
                },
                pullHandled: function() {
                    var e = this;
                    e.refreshHint.removeClass(E), e.hintContainer.html(e.pullTemplate({})), e.yinertia.onEnd(), e.xinertia.onEnd(), e.userEvents.cancel()
                },
                destroy: function() {
                    r.fn.destroy.call(this), this.userEvents && this.userEvents.destroy()
                },
                _scale: function(e) {
                    this.dimensions.rescale(e), this.movable.scaleTo(e)
                },
                _initPullToRefresh: function() {
                    var e = this;
                    e.dimensions.y.forceEnabled(), e.pullTemplate = n.template(e.options.messages.pullTemplate), e.releaseTemplate = n.template(e.options.messages.releaseTemplate), e.refreshTemplate = n.template(e.options.messages.refreshTemplate), e.scrollElement.prepend('<span class="km-scroller-pull"><span class="km-icon"></span><span class="km-loading-left"></span><span class="km-loading-right"></span><span class="km-template">' + e.pullTemplate({}) + "</span></span>"), e.refreshHint = e.scrollElement.children().first(), e.hintContainer = e.refreshHint.children(".km-template"), e.pane.y.bind("change", l(e._paneChange, e)), e.userEvents.bind("end", l(e._dragEnd, e))
                },
                _dragEnd: function() {
                    var e = this;
                    e.pulled && (e.pulled = !1, e.refreshHint.removeClass(_).addClass(E), e.hintContainer.html(e.refreshTemplate({})), e.yinertia.freeze(e.options.pullOffset / 2), e.trigger("pull"))
                },
                _paneChange: function() {
                    var e = this;
                    e.movable.y / w > e.options.pullOffset ? e.pulled || (e.pulled = !0, e.refreshHint.removeClass(E).addClass(_), e.hintContainer.html(e.releaseTemplate({}))) : e.pulled && (e.pulled = !1, e.refreshHint.removeClass(_), e.hintContainer.html(e.pullTemplate({})))
                },
                _initAxis: function(e) {
                    var i = this,
                        n = i.movable,
                        t = i.dimensions[e],
                        s = i.tapCapture,
                        o = i.pane[e],
                        l = new B({
                            axis: e,
                            movable: n,
                            dimension: t,
                            container: i.element,
                            alwaysVisible: i.options.visibleScrollHints
                        });
                    t.bind(k, function() {
                        l.refresh()
                    }), o.bind(k, function() {
                        l.show()
                    }), i[e + "inertia"] = new O({
                        axis: e,
                        paneAxis: o,
                        movable: n,
                        tapCapture: s,
                        userEvents: i.userEvents,
                        dimension: t,
                        elastic: i.options.elastic,
                        friction: i.options.friction || x,
                        velocityMultiplier: i.options.velocityMultiplier || y,
                        end: function() {
                            l.hide(), i.trigger("scrollEnd", {
                                axis: e,
                                scrollTop: i.scrollTop,
                                scrollLeft: i.scrollLeft
                            })
                        }
                    })
                }
            });
        o.plugin(R)
    }(window.kendo.jQuery), window.kendo
}, "function" == typeof define && define.amd ? define : function(e, i, n) {
    (n || i)()
});
//# sourceMappingURL=kendo.mobile.scroller.min.js.map;
/** 
 * Kendo UI v2018.1.221 (http://www.telerik.com/kendo-ui)                                                                                                                                               
 * Copyright 2018 Telerik AD. All rights reserved.                                                                                                                                                      
 *                                                                                                                                                                                                      
 * Kendo UI commercial licenses may be obtained at                                                                                                                                                      
 * http://www.telerik.com/purchase/license-agreement/kendo-ui-complete                                                                                                                                  
 * If you do not own a commercial license, this file shall be governed by the trial license terms.                                                                                                      

*/
! function(e, define) {
    define("kendo.virtuallist.min", ["kendo.data.min"], e)
}(function() {
    return function(e, t) {
        function i(e) {
            return e[e.length - 1]
        }

        function n(e) {
            return e instanceof Array ? e : [e]
        }

        function s(e) {
            return "string" == typeof e || "number" == typeof e || "boolean" == typeof e
        }

        function r(e, t, i) {
            return Math.ceil(e * t / i)
        }

        function a(e, t, i) {
            var n = document.createElement(i || "div");
            return t && (n.className = t), e.appendChild(n), n
        }

        function o() {
            var t, i = e('<div class="k-popup"><ul class="k-list"><li class="k-item"><li></ul></div>');
            return i.css({
                position: "absolute",
                left: "-200000px",
                visibility: "hidden"
            }), i.appendTo(document.body), t = parseFloat(v.getComputedStyles(i.find(".k-item")[0], ["line-height"])["line-height"]), i.remove(), t
        }

        function l(e, t, i) {
            return {
                down: e * i,
                up: e * (t - 1 - i)
            }
        }

        function u(e, t) {
            var i = (e.listScreens - 1 - e.threshold) * t,
                n = e.threshold * t;
            return function(e, t, s) {
                return t > s ? t - e.top < i : 0 === e.top || t - e.top > n
            }
        }

        function h(e, t) {
            return function(i) {
                return t(e.scrollTop, i)
            }
        }

        function c(e) {
            return function(t, i) {
                return e(t.items, t.index, i), t
            }
        }

        function d(e, t) {
            v.support.browser.msie && v.support.browser.version < 10 ? e.style.top = t + "px" : (e.style.webkitTransform = "translateY(" + t + "px)", e.style.transform = "translateY(" + t + "px)")
        }

        function f(t, i) {
            return function(n, s) {
                for (var r = 0, a = n.length; r < a; r++) t(n[r], s[r], i), s[r].item && this.trigger(P, {
                    item: e(n[r]),
                    data: s[r].item,
                    ns: v.ui
                })
            }
        }

        function p(e, t) {
            var i;
            return t > 0 ? (i = e.splice(0, t), e.push.apply(e, i)) : (i = e.splice(t, -t), e.unshift.apply(e, i)), i
        }

        function g(i, n, s) {
            var r = s.template;
            i = e(i), n.item || (r = s.placeholderTemplate), 0 === n.index && this.header && n.group && this.header.html(s.fixedGroupTemplate(n.group)), this.angular("cleanup", function() {
                return {
                    elements: [i]
                }
            }), i.attr("data-uid", n.item ? n.item.uid : "").attr("data-offset-index", n.index).html(r(n.item || {})), i.toggleClass(L, n.current), i.toggleClass(G, n.selected), i.toggleClass("k-first", n.newGroup), i.toggleClass("k-loading-item", !n.item), 0 !== n.index && n.newGroup && e("<div class=" + B + "></div>").appendTo(i).html(s.groupTemplate(n.group)), n.top !== t && d(i[0], n.top), this.angular("compile", function() {
                return {
                    elements: [i],
                    data: [{
                        dataItem: n.item,
                        group: n.group,
                        newGroup: n.newGroup
                    }]
                }
            })
        }

        function m(e, t) {
            var i, n, s, r, a = t.length,
                o = e.length,
                l = [],
                u = [];
            if (o)
                for (s = 0; s < o; s++) {
                    for (i = e[s], n = !1, r = 0; r < a; r++)
                        if (i === t[r]) {
                            n = !0, l.push({
                                index: s,
                                item: i
                            });
                            break
                        }
                    n || u.push(i)
                }
            return {
                changed: l,
                unchanged: u
            }
        }

        function _(e) {
            return e && "resolved" !== e.state()
        }
        var v = window.kendo,
            I = v.ui,
            x = I.Widget,
            D = I.DataBoundWidget,
            y = e.proxy,
            S = "k-virtual-wrap",
            C = "k-virtual-list",
            b = "k-virtual-content",
            k = "k-list",
            T = "k-group-header",
            w = "k-virtual-item",
            H = "k-item",
            V = "k-height-container",
            B = "k-group",
            G = "k-state-selected",
            L = "k-state-focused",
            F = "k-state-hover",
            M = "change",
            E = "click",
            N = "listBound",
            P = "itemChange",
            R = "activate",
            A = "deactivate",
            z = ".VirtualList",
            j = D.extend({
                init: function(t, i) {
                    var s = this;
                    s.bound(!1), s._fetching = !1, x.fn.init.call(s, t, i), s.options.itemHeight || (s.options.itemHeight = o()), i = s.options, s.element.addClass(k + " " + C).attr("role", "listbox"), s.content = s.element.wrap("<div unselectable='on' class='" + b + "'></div>").parent(), s.wrapper = s.content.wrap("<div class='" + S + "'></div>").parent(), s.header = s.content.before("<div class='" + T + "'></div>").prev(), s.element.on("mouseenter" + z, "li:not(.k-loading-item)", function() {
                        e(this).addClass(F)
                    }).on("mouseleave" + z, "li", function() {
                        e(this).removeClass(F)
                    }), s._values = n(s.options.value), s._selectedDataItems = [], s._selectedIndexes = [], s._rangesList = {}, s._promisesList = [], s._optionID = v.guid(), s._templates(), s.setDataSource(i.dataSource), s.content.on("scroll" + z, v.throttle(function() {
                        s._renderItems(), s._triggerListBound()
                    }, i.delay)), s._selectable()
                },
                options: {
                    name: "VirtualList",
                    autoBind: !0,
                    delay: 100,
                    height: null,
                    listScreens: 4,
                    threshold: .5,
                    itemHeight: null,
                    oppositeBuffer: 1,
                    type: "flat",
                    selectable: !1,
                    value: [],
                    dataValueField: null,
                    template: "#:data#",
                    placeholderTemplate: "loading...",
                    groupTemplate: "#:data#",
                    fixedGroupTemplate: "#:data#",
                    mapValueTo: "index",
                    valueMapper: null
                },
                events: [M, E, N, P, R, A],
                setOptions: function(e) {
                    x.fn.setOptions.call(this, e), this._selectProxy && this.options.selectable === !1 ? this.element.off(E, "." + w, this._selectProxy) : !this._selectProxy && this.options.selectable && this._selectable(), this._templates(), this.refresh()
                },
                items: function() {
                    return e(this._items)
                },
                destroy: function() {
                    this.wrapper.off(z), this.dataSource.unbind(M, this._refreshHandler), x.fn.destroy.call(this)
                },
                setDataSource: function(t) {
                    var i, n = this,
                        s = t || {};
                    s = e.isArray(s) ? {
                        data: s
                    } : s, s = v.data.DataSource.create(s), n.dataSource ? (n.dataSource.unbind(M, n._refreshHandler), n._clean(), n.bound(!1), n._deferValueSet = !0, i = n.value(), n.value([]), n.mute(function() {
                        n.value(i)
                    })) : n._refreshHandler = e.proxy(n.refresh, n), n.dataSource = s.bind(M, n._refreshHandler), n.setDSFilter(s.filter()), 0 !== s.view().length ? n.refresh() : n.options.autoBind && s.fetch()
                },
                skip: function() {
                    return this.dataSource.currentRangeStart()
                },
                _triggerListBound: function() {
                    var e = this,
                        t = e.skip();
                    e.bound() && !e._selectingValue && e._skip !== t && (e._skip = t, e.trigger(N))
                },
                _getValues: function(t) {
                    var i = this._valueGetter;
                    return e.map(t, function(e) {
                        return i(e)
                    })
                },
                refresh: function(e) {
                    var t, i = this,
                        n = e && e.action,
                        s = "itemchange" === n,
                        r = this.isFiltered();
                    i._mute || (i._deferValueSet = !1, i._fetching ? (i._renderItems && i._renderItems(!0), i._triggerListBound()) : (r && i.focus(0), i._createList(), n || !i._values.length || r || i.options.skipUpdateOnBind ? (i.bound(!0), i._triggerListBound()) : (i._selectingValue = !0, i.value(i._values, !0).done(function() {
                        i.bound(!0), i._selectingValue = !1, i._triggerListBound()
                    }))), (s || "remove" === n) && (t = m(i._selectedDataItems, e.items), t.changed.length && (s ? i.trigger("selectedItemChange", {
                        items: t.changed
                    }) : i.value(i._getValues(t.unchanged)))), i._fetching = !1)
                },
                removeAt: function(e) {
                    return this._selectedIndexes.splice(e, 1), this._values.splice(e, 1), {
                        position: e,
                        dataItem: this._selectedDataItems.splice(e, 1)[0]
                    }
                },
                setValue: function(e) {
                    this._values = n(e)
                },
                value: function(i, s) {
                    var r, a = this;
                    return i === t ? a._values.slice() : (null === i && (i = []), i = n(i), a._valueDeferred && "resolved" !== a._valueDeferred.state() || (a._valueDeferred = e.Deferred()), r = "multiple" === a.options.selectable && a.select().length && i.length, !r && i.length || a.select(-1), a._values = i, (a.bound() && !a._mute && !a._deferValueSet || s) && a._prefetchByValue(i), a._valueDeferred)
                },
                _prefetchByValue: function(e) {
                    var i, n, r, a = this,
                        o = a._dataView,
                        l = a._valueGetter,
                        u = a.options.mapValueTo,
                        h = !1,
                        c = [];
                    for (n = 0; n < e.length; n++)
                        for (r = 0; r < o.length; r++) i = o[r].item, i && (h = s(i) ? e[n] === i : e[n] === l(i), h && c.push(o[r].index));
                    return c.length === e.length ? (a._values = [], a.select(c), t) : ("function" == typeof a.options.valueMapper ? a.options.valueMapper({
                        value: "multiple" === this.options.selectable ? e : e[0],
                        success: function(e) {
                            "index" === u ? a.mapValueToIndex(e) : "dataItem" === u && a.mapValueToDataItem(e)
                        }
                    }) : a.select([-1]), t)
                },
                mapValueToIndex: function(e) {
                    if (e = e === t || e === -1 || null === e ? [] : n(e), e.length) {
                        var i = this._deselect([]).removed;
                        i.length && this._triggerChange(i, [])
                    } else e = [-1];
                    this.select(e)
                },
                mapValueToDataItem: function(i) {
                    var s, r, a, o;
                    if (i = i === t || null === i ? [] : n(i), i.length) {
                        for (s = e.map(this._selectedDataItems, function(e, t) {
                                return {
                                    index: t,
                                    dataItem: e
                                }
                            }), r = e.map(i, function(e, t) {
                                return {
                                    index: t,
                                    dataItem: e
                                }
                            }), this._selectedDataItems = i, this._selectedIndexes = [], a = 0; a < this._selectedDataItems.length; a++) o = this._getElementByDataItem(this._selectedDataItems[a]), this._selectedIndexes.push(this._getIndecies(o)[0]), o.addClass(G);
                        this._triggerChange(s, r), this._valueDeferred && this._valueDeferred.resolve()
                    } else this.select([-1])
                },
                deferredRange: function(t) {
                    var i = this.dataSource,
                        n = this.itemCount,
                        s = this._rangesList,
                        r = e.Deferred(),
                        a = [],
                        o = Math.floor(t / n) * n,
                        l = Math.ceil(t / n) * n,
                        u = l === o ? [l] : [o, l];
                    return e.each(u, function(t, r) {
                        var o, l = r + n,
                            u = s[r];
                        u && u.end === l ? o = u.deferred : (o = e.Deferred(), s[r] = {
                            end: l,
                            deferred: o
                        }, i._multiplePrefetch(r, n, function() {
                            o.resolve()
                        })), a.push(o)
                    }), e.when.apply(e, a).then(function() {
                        r.resolve()
                    }), r
                },
                prefetch: function(t) {
                    var i = this,
                        n = this.itemCount,
                        s = !i._promisesList.length;
                    return _(i._activeDeferred) || (i._activeDeferred = e.Deferred(), i._promisesList = []), e.each(t, function(e, t) {
                        i._promisesList.push(i.deferredRange(i._getSkip(t, n)))
                    }), s && e.when.apply(e, i._promisesList).done(function() {
                        i._promisesList = [], i._activeDeferred.resolve()
                    }), i._activeDeferred
                },
                _findDataItem: function(e, t) {
                    var i, n;
                    if ("group" === this.options.type)
                        for (n = 0; n < e.length; n++) {
                            if (i = e[n].items, !(i.length <= t)) return i[t];
                            t -= i.length
                        }
                    return e[t]
                },
                _getRange: function(e, t) {
                    return this.dataSource._findRange(e, Math.min(e + t, this.dataSource.total()))
                },
                dataItemByIndex: function(e) {
                    var t = this,
                        i = t.itemCount,
                        n = t._getSkip(e, i);
                    return t._getRange(n, i).length ? (t.mute(function() {
                        t.dataSource.range(n, i)
                    }), t._findDataItem(t.dataSource.view(), [e - n])) : null
                },
                selectedDataItems: function() {
                    return this._selectedDataItems.slice()
                },
                scrollWith: function(e) {
                    this.content.scrollTop(this.content.scrollTop() + e)
                },
                scrollTo: function(e) {
                    this.content.scrollTop(e)
                },
                scrollToIndex: function(e) {
                    this.scrollTo(e * this.options.itemHeight)
                },
                focus: function(n) {
                    var s, r, a, o, l, u, h = this.options.itemHeight,
                        c = this._optionID,
                        d = !0;
                    if (n === t) return o = this.element.find("." + L), o.length ? o : null;
                    if ("function" == typeof n)
                        for (a = this.dataSource.flatView(), l = 0; l < a.length; l++)
                            if (n(a[l])) {
                                n = l;
                                break
                            }
                    return n instanceof Array && (n = i(n)), isNaN(n) ? (s = e(n), r = parseInt(e(s).attr("data-offset-index"), 10)) : (r = n, s = this._getElementByIndex(r)), r === -1 ? (this.element.find("." + L).removeClass(L), this._focusedIndex = t, t) : (s.length ? (s.hasClass(L) && (d = !1), this._focusedIndex !== t && (o = this._getElementByIndex(this._focusedIndex), o.removeClass(L).removeAttr("id"), d && this.trigger(A)), this._focusedIndex = r, s.addClass(L).attr("id", c), u = this._getElementLocation(r), "top" === u ? this.scrollTo(r * h) : "bottom" === u ? this.scrollTo(r * h + h - this._screenHeight) : "outScreen" === u && this.scrollTo(r * h), d && this.trigger(R)) : (this._focusedIndex = r, this.items().removeClass(L), this.scrollToIndex(r)), t)
                },
                focusIndex: function() {
                    return this._focusedIndex
                },
                focusFirst: function() {
                    this.scrollTo(0), this.focus(0)
                },
                focusLast: function() {
                    var e = this.dataSource.total();
                    this.scrollTo(this.heightContainer.offsetHeight), this.focus(e)
                },
                focusPrev: function() {
                    var e, t = this._focusedIndex;
                    return !isNaN(t) && t > 0 ? (t -= 1, this.focus(t), e = this.focus(), e && e.hasClass("k-loading-item") && (t += 1, this.focus(t)), t) : (t = this.dataSource.total() - 1, this.focus(t), t)
                },
                focusNext: function() {
                    var e, t = this._focusedIndex,
                        i = this.dataSource.total() - 1;
                    return !isNaN(t) && t < i ? (t += 1, this.focus(t), e = this.focus(), e && e.hasClass("k-loading-item") && (t -= 1, this.focus(t)), t) : (t = 0, this.focus(t), t)
                },
                _triggerChange: function(e, t) {
                    e = e || [], t = t || [], (e.length || t.length) && this.trigger(M, {
                        removed: e,
                        added: t
                    })
                },
                select: function(n) {
                    var s, r, a, o, l, u = this,
                        h = "multiple" !== u.options.selectable,
                        c = _(u._activeDeferred),
                        d = this.isFiltered(),
                        f = [];
                    return n === t ? u._selectedIndexes.slice() : (u._selectDeferred && "resolved" !== u._selectDeferred.state() || (u._selectDeferred = e.Deferred()), s = u._getIndecies(n), r = h && !d && i(s) === i(this._selectedIndexes), f = u._deselectCurrentValues(s), f.length || !s.length || r ? (u._triggerChange(f), u._valueDeferred && u._valueDeferred.resolve(), u._selectDeferred.resolve().promise()) : (1 === s.length && s[0] === -1 && (s = []), o = u._deselect(s), f = o.removed, s = o.indices, h && (c = !1, s.length && (s = [i(s)])), l = function() {
                        var e = u._select(s);
                        u.focus(s), u._triggerChange(f, e), u._valueDeferred && u._valueDeferred.resolve(), u._selectDeferred.resolve()
                    }, a = u.prefetch(s), c || (a ? a.done(l) : l()), u._selectDeferred.promise()))
                },
                bound: function(e) {
                    return e === t ? this._listCreated : (this._listCreated = e, t)
                },
                mute: function(e) {
                    this._mute = !0, y(e(), this), this._mute = !1
                },
                setDSFilter: function(t) {
                    this._lastDSFilter = e.extend({}, t)
                },
                isFiltered: function() {
                    return this._lastDSFilter || this.setDSFilter(this.dataSource.filter()), !v.data.Query.compareFilters(this.dataSource.filter(), this._lastDSFilter)
                },
                skipUpdate: e.noop,
                _getElementByIndex: function(t) {
                    return this.items().filter(function(i, n) {
                        return t === parseInt(e(n).attr("data-offset-index"), 10)
                    })
                },
                _getElementByDataItem: function(t) {
                    var i, n, r, a = this._dataView,
                        o = this._valueGetter;
                    for (r = 0; r < a.length; r++)
                        if (n = a[r].item && s(a[r].item) ? a[r].item === t : a[r].item && t && o(a[r].item) == o(t)) {
                            i = a[r];
                            break
                        }
                    return i ? this._getElementByIndex(i.index) : e()
                },
                _clean: function() {
                    this.result = t, this._lastScrollTop = t, this._skip = t, e(this.heightContainer).remove(), this.heightContainer = t, this.element.empty()
                },
                _height: function() {
                    var e = !!this.dataSource.view().length,
                        t = this.options.height,
                        i = this.options.itemHeight,
                        n = this.dataSource.total();
                    return e ? t / i > n && (t = n * i) : t = 0, t
                },
                setScreenHeight: function() {
                    var e = this._height();
                    this.content.height(e), this._screenHeight = e
                },
                screenHeight: function() {
                    return this._screenHeight
                },
                _getElementLocation: function(e) {
                    var t, i = this.content.scrollTop(),
                        n = this._screenHeight,
                        s = this.options.itemHeight,
                        r = e * s,
                        a = r + s,
                        o = i + n;
                    return t = r === i - s || a > i && r < i ? "top" : r === o || r < o && o < a ? "bottom" : r >= i && r <= i + (n - s) ? "inScreen" : "outScreen"
                },
                _templates: function() {
                    var e, t = this.options,
                        i = {
                            template: t.template,
                            placeholderTemplate: t.placeholderTemplate,
                            groupTemplate: t.groupTemplate,
                            fixedGroupTemplate: t.fixedGroupTemplate
                        };
                    for (e in i) "function" != typeof i[e] && (i[e] = v.template(i[e] || ""));
                    this.templates = i
                },
                _generateItems: function(e, t) {
                    for (var i, n = [], s = this.options.itemHeight + "px"; t-- > 0;) i = document.createElement("li"), i.tabIndex = -1, i.className = w + " " + H, i.setAttribute("role", "option"), i.style.height = s, i.style.minHeight = s, e.appendChild(i), n.push(i);
                    return n
                },
                _saveInitialRanges: function() {
                    var t, i = this.dataSource._ranges,
                        n = e.Deferred();
                    for (n.resolve(), this._rangesList = {}, t = 0; t < i.length; t++) this._rangesList[i[t].start] = {
                        end: i[t].end,
                        deferred: n
                    }
                },
                _createList: function() {
                    var t = this,
                        i = t.content.get(0),
                        n = t.options,
                        s = t.dataSource;
                    t.bound() && t._clean(), t._saveInitialRanges(), t._buildValueGetter(), t.setScreenHeight(), t.itemCount = r(t._screenHeight, n.listScreens, n.itemHeight), t.itemCount > s.total() && (t.itemCount = s.total()), t._items = t._generateItems(t.element[0], t.itemCount), t._setHeight(n.itemHeight * s.total()), t.options.type = (s.group() || []).length ? "group" : "flat", "flat" === t.options.type ? t.header.hide() : t.header.show(), t.getter = t._getter(function() {
                        t._renderItems(!0)
                    }), t._onScroll = function(e, i) {
                        var n = t._listItems(t.getter);
                        return t._fixedHeader(e, n(e, i))
                    }, t._renderItems = t._whenChanged(h(i, t._onScroll), c(t._reorderList(t._items, e.proxy(g, t)))), t._renderItems(), t._calculateGroupPadding(t._screenHeight)
                },
                _setHeight: function(e) {
                    var t, i, n = this.heightContainer;
                    if (n ? t = n.offsetHeight : n = this.heightContainer = a(this.content[0], V), e !== t)
                        for (n.innerHTML = ""; e > 0;) i = Math.min(e, 25e4), a(n).style.height = i + "px", e -= i
                },
                _getter: function() {
                    var e = null,
                        t = this.dataSource,
                        i = t.skip(),
                        n = this.options.type,
                        s = this.itemCount,
                        r = {};
                    return t.pageSize() < s && this.mute(function() {
                            t.pageSize(s)
                        }),
                        function(a, o) {
                            var l, u, h, c, d, f, p, g, m = this;
                            if (t.inRange(o, s)) {
                                if (i !== o && this.mute(function() {
                                        t.range(o, s), i = o
                                    }), "group" === n) {
                                    if (!r[o])
                                        for (u = r[o] = [], h = t.view(), c = 0, d = h.length; c < d; c++)
                                            for (f = h[c], p = 0, g = f.items.length; p < g; p++) u.push({
                                                item: f.items[p],
                                                group: f.value
                                            });
                                    l = r[o][a - o]
                                } else l = t.view()[a - o];
                                return l
                            }
                            return e !== o && (e = o, i = o, m._getterDeferred && m._getterDeferred.reject(), m._getterDeferred = m.deferredRange(o), m._getterDeferred.then(function() {
                                var e = m._indexConstraint(m.content[0].scrollTop);
                                m._getterDeferred = null, o <= e && e <= o + s && (m._fetching = !0, t.range(o, s))
                            })), null
                        }
                },
                _fixedHeader: function(e, t) {
                    var i, n, s = this.currentVisibleGroup,
                        r = this.options.itemHeight,
                        a = Math.floor((e - t.top) / r),
                        o = t.items[a];
                    return o && o.item && (i = o.group, i !== s && (n = i || "", this.header.html(this.templates.fixedGroupTemplate(n)), this.currentVisibleGroup = i)), t
                },
                _itemMapper: function(e, t, i) {
                    var n, r = this.options.type,
                        a = this.options.itemHeight,
                        o = this._focusedIndex,
                        l = !1,
                        u = !1,
                        h = !1,
                        c = null,
                        d = !1,
                        f = this._valueGetter;
                    if ("group" === r && (e && (h = 0 === t || this._currentGroup && this._currentGroup !== e.group, this._currentGroup = e.group), c = e ? e.group : null, e = e ? e.item : null), !this.isFiltered() && i.length && e)
                        for (n = 0; n < i.length; n++)
                            if (d = s(e) ? i[n] === e : i[n] === f(e)) {
                                i.splice(n, 1), l = !0;
                                break
                            }
                    return o === t && (u = !0), {
                        item: e ? e : null,
                        group: c,
                        newGroup: h,
                        selected: l,
                        current: u,
                        index: t,
                        top: t * a
                    }
                },
                _range: function(e) {
                    var t, i, n, s = this.itemCount,
                        r = this._values.slice(),
                        a = [];
                    for (this._view = {}, this._currentGroup = null, i = e, n = e + s; i < n; i++) t = this._itemMapper(this.getter(i, e), i, r), a.push(t), this._view[t.index] = t;
                    return this._dataView = a, a
                },
                _getDataItemsCollection: function(e, t) {
                    var i = this._range(this._listIndex(e, t));
                    return {
                        index: i.length ? i[0].index : 0,
                        top: i.length ? i[0].top : 0,
                        items: i
                    }
                },
                _listItems: function() {
                    var t = this._screenHeight,
                        i = this.options,
                        n = u(i, t);
                    return e.proxy(function(e, t) {
                        var i = this.result,
                            s = this._lastScrollTop;
                        return !t && i && n(i, e, s) || (i = this._getDataItemsCollection(e, s)), this._lastScrollTop = e, this.result = i, i
                    }, this)
                },
                _whenChanged: function(e, t) {
                    var i;
                    return function(n) {
                        var s = e(n);
                        s !== i && (i = s, t(s, n))
                    }
                },
                _reorderList: function(t, i) {
                    var n = this,
                        s = t.length,
                        r = -(1 / 0);
                    return i = e.proxy(f(i, this.templates), this),
                        function(e, a, o) {
                            var l, u, h = a - r;
                            o || Math.abs(h) >= s ? (l = t, u = e) : (l = p(t, h), u = h > 0 ? e.slice(-h) : e.slice(0, -h)), i(l, u, n.bound()), r = a
                        }
                },
                _bufferSizes: function() {
                    var e = this.options;
                    return l(this._screenHeight, e.listScreens, e.oppositeBuffer)
                },
                _indexConstraint: function(e) {
                    var t = this.itemCount,
                        i = this.options.itemHeight,
                        n = this.dataSource.total();
                    return Math.min(Math.max(n - t, 0), Math.max(0, Math.floor(e / i)))
                },
                _listIndex: function(e, t) {
                    var i, n = this._bufferSizes();
                    return i = e - (e > t ? n.down : n.up), this._indexConstraint(i)
                },
                _selectable: function() {
                    this.options.selectable && (this._selectProxy = e.proxy(this, "_clickHandler"), this.element.on(E + z, "." + w, this._selectProxy))
                },
                getElementIndex: function(e) {
                    return e instanceof jQuery ? parseInt(e.attr("data-offset-index"), 10) : t
                },
                _getIndecies: function(e) {
                    var t, i, n, s = [];
                    if ("function" == typeof e)
                        for (t = this.dataSource.flatView(), i = 0; i < t.length; i++)
                            if (e(t[i])) {
                                s.push(i);
                                break
                            }
                    return "number" == typeof e && s.push(e), n = this.getElementIndex(e), isNaN(n) || s.push(n), e instanceof Array && (s = e), s
                },
                _deselect: function(i) {
                    var n, r, a, o, l, u, h, c = [],
                        d = this._selectedIndexes,
                        f = this._selectedDataItems,
                        p = 0,
                        g = this.options.selectable,
                        m = 0,
                        _ = this._valueGetter,
                        v = null;
                    if (i = i.slice(), g !== !0 && i.length) {
                        if ("multiple" === g)
                            for (u = 0; u < i.length; u++) {
                                if (v = null, p = e.inArray(i[u], d), r = this.dataItemByIndex(i[u]), p === -1 && r)
                                    for (h = 0; h < f.length; h++) o = s(r) ? f[h] === r : _(f[h]) === _(r), o && (a = this._getElementByIndex(i[u]), v = this._deselectSingleItem(a, h, i[u], m));
                                else n = d[p], n !== t && (a = this._getElementByIndex(n), v = this._deselectSingleItem(a, p, n, m));
                                v && (i.splice(u, 1), c.push(v), m++, u--)
                            }
                    } else {
                        for (l = 0; l < d.length; l++) d[l] !== t ? this._getElementByIndex(d[l]).removeClass(G) : f[l] && this._getElementByDataItem(f[l]).removeClass(G), c.push({
                            index: d[l],
                            position: l,
                            dataItem: f[l]
                        });
                        this._values = [], this._selectedDataItems = [], this._selectedIndexes = []
                    }
                    return {
                        indices: i,
                        removed: c
                    }
                },
                _deselectSingleItem: function(e, t, i, n) {
                    var s;
                    if (e.hasClass("k-state-selected")) return e.removeClass(G), this._values.splice(t, 1), this._selectedIndexes.splice(t, 1), s = this._selectedDataItems.splice(t, 1)[0], {
                        index: i,
                        position: t + n,
                        dataItem: s
                    }
                },
                _deselectCurrentValues: function(t) {
                    var i, n, s, r, a = this.element[0].children,
                        o = this._values,
                        l = [],
                        u = 0;
                    if ("multiple" !== this.options.selectable || !this.isFiltered()) return [];
                    if (t[0] === -1) return e(a).removeClass("k-state-selected"), l = e.map(this._selectedDataItems.slice(0), function(e, t) {
                        return {
                            dataItem: e,
                            position: t
                        }
                    }), this._selectedIndexes = [], this._selectedDataItems = [], this._values = [], l;
                    for (; u < t.length; u++) {
                        for (s = -1, n = t[u], i = this._valueGetter(this.dataItemByIndex(n)), r = 0; r < o.length; r++)
                            if (i == o[r]) {
                                s = r;
                                break
                            }
                        s > -1 && (l.push(this.removeAt(s)), e(a[n]).removeClass("k-state-selected"))
                    }
                    return l
                },
                _getSkip: function(e, t) {
                    var i = e < t ? 1 : Math.floor(e / t) + 1;
                    return (i - 1) * t
                },
                _select: function(t) {
                    var i, n, r = this,
                        a = "multiple" !== this.options.selectable,
                        o = this.dataSource,
                        l = this.itemCount,
                        u = this._valueGetter,
                        h = [];
                    return a && (r._selectedIndexes = [], r._selectedDataItems = [], r._values = []), n = o.skip(), e.each(t, function(e, t) {
                        var a = r._getSkip(t, l);
                        r.mute(function() {
                            o.range(a, l), i = r._findDataItem(o.view(), [t - a]), r._selectedIndexes.push(t), r._selectedDataItems.push(i), r._values.push(s(i) ? i : u(i)), h.push({
                                index: t,
                                dataItem: i
                            }), r._getElementByIndex(t).addClass(G), o.range(n, l)
                        })
                    }), h
                },
                _clickHandler: function(t) {
                    var i = e(t.currentTarget);
                    !t.isDefaultPrevented() && i.attr("data-uid") && this.trigger(E, {
                        item: i
                    })
                },
                _buildValueGetter: function() {
                    this._valueGetter = v.getter(this.options.dataValueField)
                },
                _calculateGroupPadding: function(e) {
                    var t = this.items().first(),
                        i = this.header,
                        n = 0;
                    i[0] && "none" !== i[0].style.display && ("auto" !== e && (n = v.support.scrollbar()), n += parseFloat(t.css("border-right-width"), 10) + parseFloat(t.children(".k-group").css("right"), 10), i.css("padding-right", n))
                }
            });
        v.ui.VirtualList = j, v.ui.plugin(j)
    }(window.kendo.jQuery), window.kendo
}, "function" == typeof define && define.amd ? define : function(e, t, i) {
    (i || t)()
});
//# sourceMappingURL=kendo.virtuallist.min.js.map;
/** 
 * Kendo UI v2018.1.221 (http://www.telerik.com/kendo-ui)                                                                                                                                               
 * Copyright 2018 Telerik AD. All rights reserved.                                                                                                                                                      
 *                                                                                                                                                                                                      
 * Kendo UI commercial licenses may be obtained at                                                                                                                                                      
 * http://www.telerik.com/purchase/license-agreement/kendo-ui-complete                                                                                                                                  
 * If you do not own a commercial license, this file shall be governed by the trial license terms.                                                                                                      

*/
! function(e, define) {
    define("kendo.multiselect.min", ["kendo.list.min", "kendo.mobile.scroller.min", "kendo.virtuallist.min"], e)
}(function() {
    return function(e, t) {
        function i(e, t) {
            var i;
            if (null === e && null !== t || null !== e && null === t) return !1;
            if (i = e.length, i !== t.length) return !1;
            for (; i--;)
                if (e[i] !== t[i]) return !1;
            return !0
        }
        var a = window.kendo,
            s = a.ui,
            n = s.List,
            l = e.extend({
                A: 65
            }, a.keys),
            o = a._activeElement,
            r = a.data.ObservableArray,
            c = e.proxy,
            u = "id",
            d = "li",
            p = "accept",
            h = "filter",
            _ = "rebind",
            f = "open",
            g = "close",
            m = "change",
            v = "progress",
            T = "select",
            w = "deselect",
            I = "aria-disabled",
            b = "k-state-focused",
            y = "k-state-selected",
            k = "k-hidden",
            x = "k-state-hover",
            C = "k-state-disabled",
            V = "disabled",
            S = "readonly",
            L = ".kendoMultiSelect",
            D = "click" + L,
            O = "keydown" + L,
            B = "mouseenter" + L,
            E = "mouseleave" + L,
            F = B + " " + E,
            A = /"/g,
            M = e.isArray,
            P = ["font-family", "font-size", "font-stretch", "font-style", "font-weight", "letter-spacing", "text-transform", "line-height"],
            H = n.extend({
                init: function(t, i) {
                    var s, l, o = this;
                    o.ns = L, n.fn.init.call(o, t, i), o._optionsMap = {}, o._customOptions = {}, o._wrapper(), o._tagList(), o._input(), o._textContainer(), o._loader(), o._clearButton(), o._tabindex(o.input), t = o.element.attr("multiple", "multiple").hide(), i = o.options, i.placeholder || (i.placeholder = t.data("placeholder")), s = t.attr(u), s && (o._tagID = s + "_tag_active", s += "_taglist", o.tagList.attr(u, s)), o._initialOpen = !0, o._aria(s), o._dataSource(), o._ignoreCase(), o._popup(), o._tagTemplate(), o.requireValueMapper(o.options), o._initList(), o._reset(), o._enable(), o._placeholder(), i.autoBind ? o.dataSource.fetch() : i.value && o._preselect(i.value), l = e(o.element).parents("fieldset").is(":disabled"), l && o.enable(!1), a.notify(o), o._toggleCloseVisibility()
                },
                options: {
                    name: "MultiSelect",
                    tagMode: "multiple",
                    enabled: !0,
                    autoBind: !0,
                    autoClose: !0,
                    highlightFirst: !0,
                    dataTextField: "",
                    dataValueField: "",
                    filter: "startswith",
                    ignoreCase: !0,
                    minLength: 1,
                    enforceMinLength: !1,
                    delay: 100,
                    value: null,
                    maxSelectedItems: null,
                    placeholder: "",
                    height: 200,
                    animation: {},
                    virtual: !1,
                    itemTemplate: "",
                    tagTemplate: "",
                    groupTemplate: "#:data#",
                    fixedGroupTemplate: "#:data#",
                    clearButton: !0,
                    autoWidth: !1
                },
                events: [f, g, m, T, w, "filtering", "dataBinding", "dataBound"],
                setDataSource: function(e) {
                    this.options.dataSource = e, this._state = "", this._dataSource(), this.persistTagList = !1, this.listView.setDataSource(this.dataSource), this.options.autoBind && this.dataSource.fetch()
                },
                setOptions: function(e) {
                    var t = this._listOptions(e);
                    n.fn.setOptions.call(this, e), this.listView.setOptions(t), this._accessors(), this._aria(this.tagList.attr(u)), this._tagTemplate(), this._placeholder(), this._clearButton()
                },
                currentTag: function(e) {
                    var i = this;
                    return e === t ? i._currentTag : (i._currentTag && (i._currentTag.removeClass(b).removeAttr(u), i.input.removeAttr("aria-activedescendant")), e && (e.addClass(b).attr(u, i._tagID), i.input.attr("aria-activedescendant", i._tagID)), i._currentTag = e, t)
                },
                dataItems: function() {
                    return this.listView.selectedDataItems()
                },
                destroy: function() {
                    var e = this,
                        t = e.ns;
                    clearTimeout(e._busy), clearTimeout(e._typingTimeout), e.wrapper.off(t), e.tagList.off(t), e.input.off(t), e._clear.off(t), n.fn.destroy.call(e)
                },
                _activateItem: function() {
                    n.fn._activateItem.call(this), this.currentTag(null)
                },
                _listOptions: function(t) {
                    var i = this,
                        s = n.fn._listOptions.call(i, e.extend(t, {
                            selectedItemChange: c(i._selectedItemChange, i),
                            selectable: "multiple"
                        })),
                        l = this.options.itemTemplate || this.options.template,
                        o = s.itemTemplate || l || s.template;
                    return o || (o = "#:" + a.expr(s.dataTextField, "data") + "#"), s.template = o, s
                },
                _setListValue: function() {
                    n.fn._setListValue.call(this, this._initialValues.slice(0))
                },
                _listChange: function(e) {
                    var i, a = this.dataSource.flatView(),
                        s = this._optionsMap,
                        n = this._value;
                    for (this._state === _ && (this._state = ""), i = 0; i < e.added.length; i++)
                        if (s[n(e.added[i].dataItem)] === t) {
                            this._render(a);
                            break
                        }
                    this.persistTagList = !1, this._selectValue(e.added, e.removed)
                },
                _selectedItemChange: function(e) {
                    var t, i, a = e.items;
                    for (i = 0; i < a.length; i++) t = a[i], this.tagList.children().eq(t.index).children("span:first").html(this.tagTextTemplate(t.item))
                },
                _wrapperMousedown: function(t) {
                    var i = this,
                        s = "input" !== t.target.nodeName.toLowerCase(),
                        n = e(t.target),
                        l = n.hasClass("k-select") || n.hasClass("k-icon");
                    l && (l = !n.closest(".k-select").children(".k-i-arrow-60-down").length), !s || l && a.support.mobileOS || t.preventDefault(), l || (i.input[0] !== o() && s && i.input.focus(), 1 === i.options.minLength && i.open())
                },
                _inputFocus: function() {
                    this._placeholder(!1), this.wrapper.addClass(b)
                },
                _inputFocusout: function() {
                    var e = this;
                    clearTimeout(e._typingTimeout), e.wrapper.removeClass(b), e._placeholder(!e.listView.selectedDataItems()[0], !0), e.close(), e._state === h && (e._state = p, e.listView.skipUpdate(!0)), e.listView.bound() && e.listView.isFiltered() && (e.persistTagList = !0, e._clearFilter()), e.element.blur()
                },
                _removeTag: function(e, i) {
                    var a, s, n = this,
                        l = n._state,
                        o = e.index(),
                        r = n.listView,
                        c = r.value()[o],
                        u = n.listView.selectedDataItems()[o],
                        d = n._customOptions[c];
                    return n.trigger(w, {
                        dataItem: u,
                        item: e
                    }) ? (n._close(), t) : (d !== t || l !== p && l !== h || (d = n._optionsMap[c]), s = function() {
                        n.currentTag(null), i && n._change(), n._close()
                    }, d === t ? (n.persistTagList = !1, r.select(r.select()[o]).done(s)) : (a = n.element[0].children[d], a.selected = !1, r.removeAt(o), e.remove(), s()), t)
                },
                _tagListClick: function(t) {
                    var i = e(t.currentTarget);
                    i.children(".k-i-arrow-60-down").length || this._removeTag(i.closest(d), !0)
                },
                _clearClick: function() {
                    var t = this;
                    "single" === t.options.tagMode ? t.value([]) : t.tagList.children().each(function(i, a) {
                        t._removeTag(e(a), !1)
                    }), t.input.val(""), t._search(), t.trigger(m), t.focus()
                },
                _editable: function(t) {
                    var i = this,
                        a = t.disable,
                        s = t.readonly,
                        n = i.wrapper.off(L),
                        l = i.tagList.off(L),
                        o = i.element.add(i.input.off(L));
                    s || a ? (a ? n.addClass(C) : n.removeClass(C), o.attr(V, a).attr(S, s).attr(I, a)) : (n.removeClass(C).on(F, i._toggleHover).on("mousedown" + L + " touchend" + L, c(i._wrapperMousedown, i)), i.input.on(O, c(i._keydown, i)).on("paste" + L, c(i._search, i)).on("focus" + L, c(i._inputFocus, i)).on("focusout" + L, c(i._inputFocusout, i)), i._clear.on("click" + L, c(i._clearClick, i)), o.removeAttr(V).removeAttr(S).attr(I, !1), l.on(B, d, function() {
                        e(this).addClass(x)
                    }).on(E, d, function() {
                        e(this).removeClass(x)
                    }).on(D, "li.k-button .k-select", c(i._tagListClick, i)))
                },
                _close: function() {
                    var e = this;
                    e.options.autoClose ? e.close() : e.popup.position()
                },
                _filterSource: function(e, t) {
                    t || (t = this._retrieveData), this._retrieveData = !1, n.fn._filterSource.call(this, e, t)
                },
                close: function() {
                    this._activeItem = null, this.popup.close()
                },
                open: function() {
                    var t = this;
                    t._request && (t._retrieveData = !1), t._retrieveData || !t.listView.bound() || t._state === p ? (t._open = !0, t._state = _, t.listView.skipUpdate(!0), t.persistTagList = !0, t._filterSource(), t._focusItem()) : t._allowOpening() && (t.options.autoBind || t.options.virtual || !t.options.value || e.isPlainObject(t.options.value[0]) || (t.value(t._initialValues), t._initialOpen = !1), t.popup._hovered = !0, t.popup.open(), t._focusItem())
                },
                toggle: function(e) {
                    e = e !== t ? e : !this.popup.visible(), this[e ? f : g]()
                },
                refresh: function() {
                    this.listView.refresh()
                },
                _listBound: function() {
                    var e = this,
                        i = e.dataSource.flatView(),
                        a = e.listView.skip();
                    e._render(i), e._renderFooter(), e._renderNoData(), e._toggleNoData(!i.length), e._resizePopup(), e._open && (e._open = !1, e.toggle(e._allowOpening())), e.popup.position(), !e.options.highlightFirst || a !== t && 0 !== a || e.listView.focusFirst(), e._touchScroller && e._touchScroller.reset(), e._hideBusy(), e._makeUnselectable(), e.trigger("dataBound")
                },
                _inputValue: function() {
                    var e = this,
                        t = e.input.val();
                    return e.options.placeholder === t && (t = ""), t
                },
                value: function(e) {
                    var i = this,
                        a = i.listView,
                        s = a.value().slice(),
                        n = i.options.maxSelectedItems,
                        l = a.bound() && a.isFiltered();
                    return e === t ? s : (i._toggleCloseVisibility(), i.persistTagList = !1, i.requireValueMapper(i.options, e), e = i._normalizeValues(e), null !== n && e.length > n && (e = e.slice(0, n)), l && i._clearFilter(), a.value(e), i._old = a.value(), l || i._fetchData(), t)
                },
                _preselect: function(t, i) {
                    var s = this;
                    M(t) || t instanceof a.data.ObservableArray || (t = [t]), (e.isPlainObject(t[0]) || t[0] instanceof a.data.ObservableObject || !s.options.dataValueField) && (s.dataSource.data(t), s.value(i || s._initialValues), s._retrieveData = !0)
                },
                _setOption: function(e, t) {
                    var i = this.element[0].children[this._optionsMap[e]];
                    i && (i.selected = t)
                },
                _fetchData: function() {
                    var e = this,
                        t = !!e.dataSource.view().length,
                        i = 0 === e.listView.value().length;
                    i || e._request || (e._retrieveData || !e._fetch && !t) && (e._fetch = !0, e._retrieveData = !1, e.dataSource.read().done(function() {
                        e._fetch = !1
                    }))
                },
                _isBound: function() {
                    return this.listView.bound() && !this._retrieveData
                },
                _dataSource: function() {
                    var e = this,
                        t = e.element,
                        i = e.options,
                        s = i.dataSource || {};
                    s = M(s) ? {
                        data: s
                    } : s, s.select = t, s.fields = [{
                        field: i.dataTextField
                    }, {
                        field: i.dataValueField
                    }], e.dataSource && e._refreshHandler ? e._unbindDataSource() : (e._progressHandler = c(e._showBusy, e), e._errorHandler = c(e._hideBusy, e)), e.dataSource = a.data.DataSource.create(s).bind(v, e._progressHandler).bind("error", e._errorHandler)
                },
                _reset: function() {
                    var t = this,
                        i = t.element,
                        a = i.attr("form"),
                        s = a ? e("#" + a) : i.closest("form");
                    s[0] && (t._resetHandler = function() {
                        setTimeout(function() {
                            t.value(t._initialValues), t._placeholder()
                        })
                    }, t._form = s.on("reset", t._resetHandler))
                },
                _initValue: function() {
                    var e = this.options.value || this.element.val();
                    this._old = this._initialValues = this._normalizeValues(e)
                },
                _normalizeValues: function(t) {
                    var i = this;
                    return null === t ? t = [] : t && e.isPlainObject(t) ? t = [i._value(t)] : t && e.isPlainObject(t[0]) ? t = e.map(t, function(e) {
                        return i._value(e)
                    }) : M(t) || t instanceof r ? M(t) && (t = t.slice()) : t = [t], t
                },
                _change: function() {
                    var e = this,
                        t = e.value();
                    i(t, e._old) || (e._old = t.slice(), e.trigger(m), e.element.trigger(m)), e._toggleCloseVisibility()
                },
                _click: function(e) {
                    var t = this,
                        i = e.item;
                    e.preventDefault(), t._select(i).done(function() {
                        t._activeItem = i, t._change(), t._close()
                    })
                },
                _getActiveItem: function() {
                    return this._activeItem || e(this.listView.items()[this._getSelectedIndices().length - 1]) || this.listView.focus()
                },
                _getSelectedIndices: function() {
                    return this.listView._selectedIndices || this.listView._selectedIndexes
                },
                _keydown: function(i) {
                    var s, n, o, r, c = this,
                        u = i.keyCode,
                        d = c._currentTag,
                        h = c.listView,
                        _ = c.input.val(),
                        f = a.support.isRtl(c.wrapper),
                        g = c.popup.visible(),
                        v = 0;
                    if (u === l.DOWN) {
                        if (i.preventDefault(), !g) return c.open(), h.focus() || h.focusFirst(), t;
                        h.focus() ? (!c._activeItem && i.shiftKey && (c._activeItem = h.focus(), v = -1), s = h.getElementIndex(c._getActiveItem()[0]), h.focusNext(), h.focus() ? i.shiftKey && c._selectRange(s, h.getElementIndex(h.focus()[0]) + v) : h.focusLast()) : h.focusFirst()
                    } else if (u === l.UP) g && (!c._activeItem && i.shiftKey && (c._activeItem = h.focus(), v = 1), s = h.getElementIndex(c._getActiveItem()[0]), h.focusPrev(), h.focus() ? i.shiftKey && c._selectRange(s, h.getElementIndex(h.focus()[0]) + v) : c.close()), i.preventDefault();
                    else if (u === l.LEFT && !f || u === l.RIGHT && f) _ || (d = d ? d.prev() : e(c.tagList[0].lastChild), d[0] && c.currentTag(d));
                    else if (u === l.RIGHT && !f || u === l.LEFT && f) !_ && d && (d = d.next(), c.currentTag(d[0] ? d : null));
                    else if (i.ctrlKey && u === l.A && g) this._getSelectedIndices().length === h.items().length && (c._activeItem = null), h.items().length && c._selectRange(0, h.items().length - 1);
                    else if (u === l.ENTER && g) {
                        if (i.preventDefault(), h.focus().hasClass(y)) return c._close(), t;
                        c._select(h.focus()).done(function() {
                            c._change(), c._close()
                        })
                    } else if (u === l.SPACEBAR && i.ctrlKey && g) c._activeItem && h.focus() && h.focus()[0] === c._activeItem[0] && (c._activeItem = null), e(h.focus()).hasClass(y) || (c._activeItem = h.focus()), c._select(h.focus()).done(function() {
                        c._change()
                    }), i.preventDefault();
                    else if (u === l.SPACEBAR && i.shiftKey && g) n = h.getElementIndex(c._getActiveItem()), o = h.getElementIndex(h.focus()), n !== t && o !== t && c._selectRange(n, o), i.preventDefault();
                    else if (u === l.ESC) g ? i.preventDefault() : (c.tagList.children().each(function(t, i) {
                        c._removeTag(e(i), !1)
                    }), c.trigger(m)), c.close();
                    else if (u === l.HOME) g ? h.focus() ? (i.ctrlKey && i.shiftKey && c._selectRange(h.getElementIndex(h.focus()[0]), 0), h.focusFirst()) : c.close() : _ || (d = c.tagList[0].firstChild, d && c.currentTag(e(d)));
                    else if (u === l.END) g ? h.focus() ? (i.ctrlKey && i.shiftKey && c._selectRange(h.getElementIndex(h.focus()[0]), h.element.children().length - 1), h.focusLast()) : c.close() : _ || (d = c.tagList[0].lastChild, d && c.currentTag(e(d)));
                    else if (u !== l.DELETE && u !== l.BACKSPACE || _) !c.popup.visible() || u !== l.PAGEDOWN && u !== l.PAGEUP ? (clearTimeout(c._typingTimeout), setTimeout(function() {
                        c._scale()
                    }), c._search()) : (i.preventDefault(), r = u === l.PAGEDOWN ? 1 : -1, h.scrollWith(r * h.screenHeight()));
                    else {
                        if (c._state = p, "single" === c.options.tagMode) return h.value([]), c._change(), c._close(), t;
                        u !== l.BACKSPACE || d || (d = e(c.tagList[0].lastChild)), d && d[0] && c._removeTag(d, !0)
                    }
                },
                _hideBusy: function() {
                    var e = this;
                    clearTimeout(e._busy), e.input.attr("aria-busy", !1), e._loading.addClass(k), e._request = !1, e._busy = null, e._toggleCloseVisibility()
                },
                _showBusyHandler: function() {
                    this.input.attr("aria-busy", !0), this._loading.removeClass(k), this._hideClear()
                },
                _showBusy: function() {
                    var e = this;
                    e._request = !0, e._busy || (e._busy = setTimeout(c(e._showBusyHandler, e), 100))
                },
                _placeholder: function(e, i) {
                    var s = this,
                        n = s.input,
                        l = o(),
                        r = s.options.placeholder,
                        c = n.val(),
                        u = n[0] === l,
                        d = c.length;
                    u && !s.options.autoClose && c !== r || (d = 0, c = ""), e === t && (e = !1, n[0] !== l && (e = !s.listView.selectedDataItems()[0])), s._prev = c, n.toggleClass("k-readonly", e).val(e ? r : c), u && !i && a.caret(n[0], d, d), s._scale()
                },
                _scale: function() {
                    var e, t = this,
                        i = t.wrapper.find(".k-multiselect-wrap"),
                        a = i.width(),
                        s = t._span.text(t.input.val());
                    i.is(":visible") ? e = s.width() + 25 : (s.appendTo(document.documentElement), a = e = s.width() + 25, s.appendTo(i)), t.input.width(e > a ? a : e)
                },
                _option: function(e, i, s) {
                    var n = "<option";
                    return e !== t && (e += "", e.indexOf('"') !== -1 && (e = e.replace(A, "&quot;")), n += ' value="' + e + '"'), s && (n += " selected"), n += ">", i !== t && (n += a.htmlEncode(i)), n += "</option>"
                },
                _render: function(e) {
                    var t, i, a, s, n, l, o = this.listView.selectedDataItems(),
                        r = this.listView.value(),
                        c = e.length,
                        u = "";
                    for (r.length !== o.length && (o = this._buildSelectedItems(r)), n = {}, l = {}, s = 0; s < c; s++) i = e[s], a = this._value(i), t = this._selectedItemIndex(a, o), t !== -1 && o.splice(t, 1), l[a] = s, u += this._option(a, this._text(i), t !== -1);
                    if (o.length)
                        for (s = 0; s < o.length; s++) i = o[s], a = this._value(i), n[a] = c, l[a] = c, c += 1, u += this._option(a, this._text(i), !0);
                    this._customOptions = n, this._optionsMap = l, this.element.html(u)
                },
                _buildSelectedItems: function(e) {
                    var t, i, a = this.options.dataValueField,
                        s = this.options.dataTextField,
                        n = [];
                    for (i = 0; i < e.length; i++) t = {}, t[a] = e[i], t[s] = e[i], n.push(t);
                    return n
                },
                _selectedItemIndex: function(e, t) {
                    for (var i = this._value, a = 0; a < t.length; a++)
                        if (e === i(t[a])) return a;
                    return -1
                },
                _search: function() {
                    var e = this;
                    e._typingTimeout = setTimeout(function() {
                        var t = e._inputValue();
                        e._prev !== t && (e._prev = t, e.search(t), e._toggleCloseVisibility())
                    }, e.options.delay)
                },
                _toggleCloseVisibility: function() {
                    this.value().length || this.input.val() && this.input.val() !== this.options.placeholder ? this._showClear() : this._hideClear()
                },
                _allowOpening: function() {
                    return this._allowSelection() && n.fn._allowOpening.call(this)
                },
                _allowSelection: function() {
                    var e = this.options.maxSelectedItems;
                    return null === e || e > this.listView.value().length
                },
                _angularTagItems: function(t) {
                    var i = this;
                    i.angular(t, function() {
                        return {
                            elements: i.tagList[0].children,
                            data: e.map(i.dataItems(), function(e) {
                                return {
                                    dataItem: e
                                }
                            })
                        }
                    })
                },
                updatePersistTagList: function(e, t) {
                    this.persistTagList = (!this.persistTagList.added || this.persistTagList.added.length !== t.length || !this.persistTagList.removed || this.persistTagList.removed.length !== e.length) && {
                        added: e,
                        removed: t
                    }
                },
                _selectValue: function(e, i) {
                    var a, s, n, l = this,
                        o = l.value(),
                        r = l.dataSource.total(),
                        c = l.tagList,
                        u = l._value;
                    if (this.persistTagList) return this.updatePersistTagList(e, i), t;
                    if (l._angularTagItems("cleanup"), "multiple" === l.options.tagMode) {
                        for (n = i.length - 1; n > -1; n--) a = i[n], c.children().length && (c[0].removeChild(c[0].children[a.position]), l._setOption(u(a.dataItem), !1));
                        for (n = 0; n < e.length; n++) s = e[n], c.append(l.tagTemplate(s.dataItem)), l._setOption(u(s.dataItem), !0)
                    } else {
                        for ((!l._maxTotal || l._maxTotal < r) && (l._maxTotal = r), c.html(""), o.length && c.append(l.tagTemplate({
                                values: o,
                                dataItems: l.dataItems(),
                                maxTotal: l._maxTotal,
                                currentTotal: r
                            })), n = i.length - 1; n > -1; n--) l._setOption(u(i[n].dataItem), !1);
                        for (n = 0; n < e.length; n++) l._setOption(u(e[n].dataItem), !0)
                    }
                    l._angularTagItems("compile"), l._placeholder()
                },
                _select: function(t) {
                    var i, a, s, n, l = e.Deferred().resolve();
                    return t ? (i = this, a = i.listView, s = a.dataItemByIndex(a.getElementIndex(t)), n = t.hasClass("k-state-selected"), i._state === _ && (i._state = ""), i._allowSelection() || n ? i.trigger(n ? w : T, {
                        dataItem: s,
                        item: t
                    }) ? (i._close(), l) : (i.persistTagList = !1, a.select(t).done(function() {
                        i._placeholder(), i._state === h && (i._state = p, a.skipUpdate(!0))
                    })) : l) : l
                },
                _selectRange: function(i, a) {
                    var s, n, l = this,
                        o = this.listView,
                        r = this.options.maxSelectedItems,
                        c = this._getSelectedIndices().slice(),
                        u = [],
                        d = function(t) {
                            o.select(t).done(function() {
                                t.forEach(function(t) {
                                    var i = o.dataItemByIndex(t),
                                        a = o.element.children()[t],
                                        s = e(a).hasClass("k-state-selected");
                                    l.trigger(s ? T : w, {
                                        dataItem: i,
                                        item: a
                                    })
                                }), l._change()
                            })
                        };
                    if (c.length - 1 === a - i) return d(c);
                    if (i < a)
                        for (s = i; s <= a; s++) u.push(s);
                    else
                        for (s = i; s >= a; s--) u.push(s);
                    for (null !== r && u.length > r && (u = u.slice(0, r)), s = 0; s < u.length; s++) n = u[s], this._getSelectedIndices().indexOf(n) == -1 ? c.push(n) : c.splice(c.indexOf(n), 1);
                    return c.length ? (l.persistTagList = !1, d(c)) : t
                },
                _input: function() {
                    var t = this,
                        i = t.element,
                        a = i[0].accessKey,
                        s = t._innerWrapper.children("input.k-input");
                    s[0] || (s = e('<input class="k-input" style="width: 25px" />').appendTo(t._innerWrapper)), i.removeAttr("accesskey"), t._focused = t.input = s.attr({
                        accesskey: a,
                        autocomplete: "off",
                        role: "listbox",
                        title: i[0].title,
                        "aria-expanded": !1
                    })
                },
                _tagList: function() {
                    var t = this,
                        i = t._innerWrapper.children("ul");
                    i[0] || (i = e('<ul role="listbox" deselectable="on" class="k-reset"/>').appendTo(t._innerWrapper)), t.tagList = i
                },
                _tagTemplate: function() {
                    var e, t = this,
                        i = t.options,
                        s = i.tagTemplate,
                        n = i.dataSource,
                        l = "multiple" === i.tagMode;
                    t.element[0].length && !n && (i.dataTextField = i.dataTextField || "text", i.dataValueField = i.dataValueField || "value"), e = l ? a.template("#:" + a.expr(i.dataTextField, "data") + "#", {
                        useWithBlock: !1
                    }) : a.template("#:values.length# item(s) selected"), t.tagTextTemplate = s = s ? a.template(s) : e, t.tagTemplate = function(e) {
                        return '<li class="k-button" deselectable="on"><span deselectable="on">' + s(e) + '</span><span unselectable="on" aria-label="' + (l ? "delete" : "open") + '" class="k-select"><span class="k-icon ' + (l ? "k-i-close" : "k-i-arrow-60-down") + '"></span></span></li>'
                    }
                },
                _loader: function() {
                    this._loading = e('<span class="k-icon k-i-loading ' + k + '"></span>').insertAfter(this.input)
                },
                _clearButton: function() {
                    n.fn._clearButton.call(this), this.options.clearButton && (this._clear.insertAfter(this.input), this.wrapper.addClass("k-multiselect-clearable"))
                },
                _textContainer: function() {
                    var t = a.getComputedStyles(this.input[0], P);
                    t.position = "absolute", t.visibility = "hidden", t.top = -3333, t.left = -3333, this._span = e("<span/>").css(t).appendTo(this.wrapper)
                },
                _wrapper: function() {
                    var t = this,
                        i = t.element,
                        a = i.parent("span.k-multiselect");
                    a[0] || (a = i.wrap('<div class="k-widget k-multiselect k-header" deselectable="on" />').parent(), a[0].style.cssText = i[0].style.cssText, a[0].title = i[0].title, e('<div class="k-multiselect-wrap k-floatwrap" deselectable="on" />').insertBefore(i)), t.wrapper = a.addClass(i[0].className).css("display", ""), t._innerWrapper = e(a[0].firstChild)
                }
            });
        s.plugin(H)
    }(window.kendo.jQuery), window.kendo
}, "function" == typeof define && define.amd ? define : function(e, t, i) {
    (i || t)()
});
//# sourceMappingURL=kendo.multiselect.min.js.map;
/** 
 * Kendo UI v2018.1.221 (http://www.telerik.com/kendo-ui)                                                                                                                                               
 * Copyright 2018 Telerik AD. All rights reserved.                                                                                                                                                      
 *                                                                                                                                                                                                      
 * Kendo UI commercial licenses may be obtained at                                                                                                                                                      
 * http://www.telerik.com/purchase/license-agreement/kendo-ui-complete                                                                                                                                  
 * If you do not own a commercial license, this file shall be governed by the trial license terms.                                                                                                      

*/
! function(e, define) {
    define("kendo.dropdownlist.min", ["kendo.list.min", "kendo.mobile.scroller.min", "kendo.virtuallist.min"], e)
}(function() {
    return function(e, t) {
        function i(e, t, i) {
            for (var n, s = 0, o = t.length - 1; s < o; ++s) n = t[s], n in e || (e[n] = {}), e = e[n];
            e[t[o]] = i
        }

        function n(e, t) {
            return e >= t && (e -= t), e
        }

        function s(e, t) {
            for (var i = 0; i < e.length; i++)
                if (e.charAt(i) !== t) return !1;
            return !0
        }
        var o = window.kendo,
            a = o.ui,
            l = a.List,
            r = a.Select,
            p = o.support,
            u = o._activeElement,
            c = o.data.ObservableObject,
            d = o.keys,
            f = ".kendoDropDownList",
            _ = "disabled",
            h = "readonly",
            m = "change",
            b = "k-state-focused",
            v = "k-state-default",
            w = "k-state-disabled",
            g = "aria-disabled",
            I = "mouseenter" + f + " mouseleave" + f,
            x = "tabindex",
            L = "filter",
            k = "accept",
            y = "The `optionLabel` option is not valid due to missing fields. Define a custom optionLabel as shown here http://docs.telerik.com/kendo-ui/api/javascript/ui/dropdownlist#configuration-optionLabel",
            T = e.proxy,
            V = r.extend({
                init: function(i, n) {
                    var s, a, l, p = this,
                        u = n && n.index;
                    p.ns = f, n = e.isArray(n) ? {
                        dataSource: n
                    } : n, r.fn.init.call(p, i, n), n = p.options, i = p.element.on("focus" + f, T(p._focusHandler, p)), p._focusInputHandler = e.proxy(p._focusInput, p), p.optionLabel = e(), p._optionLabel(), p._inputTemplate(), p._reset(), p._prev = "", p._word = "", p._wrapper(), p._tabindex(), p.wrapper.data(x, p.wrapper.attr(x)), p._span(), p._popup(), p._mobile(), p._dataSource(), p._ignoreCase(), p._filterHeader(), p._aria(), p.wrapper.attr("aria-live", "polite"), p._enable(), p._oldIndex = p.selectedIndex = -1, u !== t && (n.index = u), p._initialIndex = n.index, p.requireValueMapper(p.options), p._initList(), p._cascade(), p.one("set", function(e) {
                        !e.sender.listView.bound() && p.hasOptionLabel() && p._textAccessor(p._optionLabelText())
                    }), n.autoBind ? p.dataSource.fetch() : p.selectedIndex === -1 && (a = n.text || "", a || (s = n.optionLabel, s && 0 === n.index ? a = s : p._isSelect && (a = i.children(":selected").text())), p._textAccessor(a)), l = e(p.element).parents("fieldset").is(":disabled"), l && p.enable(!1), p.listView.bind("click", function(e) {
                        e.preventDefault()
                    }), o.notify(p)
                },
                options: {
                    name: "DropDownList",
                    enabled: !0,
                    autoBind: !0,
                    index: 0,
                    text: null,
                    value: null,
                    delay: 500,
                    height: 200,
                    dataTextField: "",
                    dataValueField: "",
                    optionLabel: "",
                    cascadeFrom: "",
                    cascadeFromField: "",
                    ignoreCase: !0,
                    animation: {},
                    filter: "none",
                    minLength: 1,
                    enforceMinLength: !1,
                    virtual: !1,
                    template: null,
                    valueTemplate: null,
                    optionLabelTemplate: null,
                    groupTemplate: "#:data#",
                    fixedGroupTemplate: "#:data#",
                    autoWidth: !1
                },
                events: ["open", "close", m, "select", "filtering", "dataBinding", "dataBound", "cascade", "set"],
                setOptions: function(e) {
                    r.fn.setOptions.call(this, e), this.listView.setOptions(this._listOptions(e)), this._optionLabel(), this._inputTemplate(), this._accessors(), this._filterHeader(), this._enable(), this._aria(), !this.value() && this.hasOptionLabel() && this.select(0)
                },
                destroy: function() {
                    var e = this;
                    r.fn.destroy.call(e), e.wrapper.off(f), e.element.off(f), e._inputWrapper.off(f), e._arrow.off(), e._arrow = null, e._arrowIcon = null, e.optionLabel.off()
                },
                open: function() {
                    var e = this,
                        t = !!e.dataSource.filter() && e.dataSource.filter().filters.length > 0;
                    e.popup.visible() || (e.listView.bound() && e._state !== k ? e._allowOpening() && (e._focusFilter = !0, e.popup.one("activate", e._focusInputHandler), e.popup._hovered = !0, e.popup.open(), e._resizeFilterInput(), e._focusItem()) : (e._open = !0, e._state = "rebind", e.filterInput && (e.filterInput.val(""), e._prev = ""), e.filterInput && 1 !== e.options.minLength && !t ? (e.refresh(), e.popup.one("activate", e._focusInputHandler), e.popup.open(), e._resizeFilterInput()) : e._filterSource()))
                },
                _focusInput: function() {
                    this._focusElement(this.filterInput)
                },
                _resizeFilterInput: function() {
                    var e, t, i = this.filterInput,
                        n = this._prevent;
                    i && (e = this.filterInput[0] === u(), t = o.caret(this.filterInput[0])[0], this._prevent = !0, i.css("display", "none").css("width", this.popup.element.css("width")).css("display", "inline-block"), e && (i.focus(), o.caret(i[0], t)), this._prevent = n)
                },
                _allowOpening: function() {
                    return this.hasOptionLabel() || this.filterInput || r.fn._allowOpening.call(this)
                },
                toggle: function(e) {
                    this._toggle(e, !0)
                },
                current: function(e) {
                    var i;
                    return e === t ? (i = this.listView.focus(), !i && 0 === this.selectedIndex && this.hasOptionLabel() ? this.optionLabel : i) : (this._focus(e), t)
                },
                dataItem: function(i) {
                    var n = this,
                        s = null;
                    if (null === i) return i;
                    if (i === t) s = n.listView.selectedDataItems()[0];
                    else {
                        if ("number" != typeof i) {
                            if (n.options.virtual) return n.dataSource.getByUid(e(i).data("uid"));
                            i = i.hasClass("k-list-optionlabel") ? -1 : e(n.items()).index(i)
                        } else n.hasOptionLabel() && (i -= 1);
                        s = n.dataSource.flatView()[i]
                    }
                    return s || (s = n._optionLabelDataItem()), s
                },
                refresh: function() {
                    this.listView.refresh()
                },
                text: function(e) {
                    var i, n = this,
                        s = n.options.ignoreCase;
                    return e = null === e ? "" : e, e === t ? n._textAccessor() : "string" != typeof e ? (n._textAccessor(e), t) : (i = s ? e.toLowerCase() : e, n._select(function(e) {
                        return e = n._text(e), s && (e = (e + "").toLowerCase()), e === i
                    }).done(function() {
                        n._textAccessor(n.dataItem() || e)
                    }), t)
                },
                _clearFilter: function() {
                    e(this.filterInput).val(""), r.fn._clearFilter.call(this)
                },
                value: function(e) {
                    var i = this,
                        n = i.listView,
                        s = i.dataSource;
                    return e === t ? (e = i._accessor() || i.listView.value()[0], e === t || null === e ? "" : e) : (i.requireValueMapper(i.options, e), !e && i.hasOptionLabel() || (i._initialIndex = null), this.trigger("set", {
                        value: e
                    }), i._request && i.options.cascadeFrom && i.listView.bound() ? (i._valueSetter && s.unbind(m, i._valueSetter), i._valueSetter = T(function() {
                        i.value(e)
                    }, i), s.one(m, i._valueSetter), t) : (i._isFilterEnabled() && n.bound() && n.isFiltered() ? i._clearFilter() : i._fetchData(), n.value(e).done(function() {
                        i._old = i._accessor(), i._oldIndex = i.selectedIndex
                    }), t))
                },
                hasOptionLabel: function() {
                    return this.optionLabel && !!this.optionLabel[0]
                },
                _optionLabel: function() {
                    var i = this,
                        n = i.options,
                        s = n.optionLabel,
                        a = n.optionLabelTemplate;
                    return s ? (a || (a = "#:", a += "string" == typeof s ? "data" : o.expr(n.dataTextField, "data"), a += "#"), "function" != typeof a && (a = o.template(a)), i.optionLabelTemplate = a, i.hasOptionLabel() || (i.optionLabel = e('<div class="k-list-optionlabel"></div>').prependTo(i.list)), i.optionLabel.html(a(s)).off().click(T(i._click, i)).on(I, i._toggleHover), i.angular("compile", function() {
                        return {
                            elements: i.optionLabel,
                            data: [{
                                dataItem: i._optionLabelDataItem()
                            }]
                        }
                    }), t) : (i.optionLabel.off().remove(), i.optionLabel = e(), t)
                },
                _optionLabelText: function() {
                    var e = this.options.optionLabel;
                    return "string" == typeof e ? e : this._text(e)
                },
                _optionLabelDataItem: function() {
                    var i = this,
                        n = i.options.optionLabel;
                    return i.hasOptionLabel() ? e.isPlainObject(n) ? new c(n) : i._assignInstance(i._optionLabelText(), "") : t
                },
                _buildOptions: function(e) {
                    var i, n, s, o = this;
                    o._isSelect && (i = o.listView.value()[0], n = o._optionLabelDataItem(), s = n && o._value(n), i !== t && null !== i || (i = ""), n && (s !== t && null !== s || (s = ""), n = '<option value="' + s + '">' + o._text(n) + "</option>"), o._options(e, n, i), i !== l.unifyType(o._accessor(), typeof i) && (o._customOption = null, o._custom(i)))
                },
                _listBound: function() {
                    var e, t = this,
                        i = t._initialIndex,
                        n = t._state === L,
                        s = t.dataSource.flatView();
                    t._presetValue = !1, t._renderFooter(), t._renderNoData(), t._toggleNoData(!s.length), t._resizePopup(!0), t.popup.position(), t._buildOptions(s), t._makeUnselectable(), n || (t._open && t.toggle(t._allowOpening()), t._open = !1, t._fetch || (s.length ? (!t.listView.value().length && i > -1 && null !== i && t.select(i), t._initialIndex = null, e = t.listView.selectedDataItems()[0], e && t.text() !== t._text(e) && t._selectValue(e)) : t._textAccessor() !== t._optionLabelText() && (t.listView.value(""), t._selectValue(null), t._oldIndex = t.selectedIndex))), t._hideBusy(), t.trigger("dataBound")
                },
                _listChange: function() {
                    this._selectValue(this.listView.selectedDataItems()[0]), (this._presetValue || this._old && this._oldIndex === -1) && (this._oldIndex = this.selectedIndex)
                },
                _filterPaste: function() {
                    this._search()
                },
                _focusHandler: function() {
                    this.wrapper.focus()
                },
                _focusinHandler: function() {
                    this._inputWrapper.addClass(b), this._prevent = !1
                },
                _focusoutHandler: function() {
                    var e = this,
                        t = window.self !== window.top;
                    e._prevent || (clearTimeout(e._typingTimeout), p.mobileOS.ios && t ? e._change() : e._blur(), e._inputWrapper.removeClass(b), e._prevent = !0, e._open = !1, e.element.blur())
                },
                _wrapperMousedown: function() {
                    this._prevent = !!this.filterInput
                },
                _wrapperClick: function(e) {
                    e.preventDefault(), this.popup.unbind("activate", this._focusInputHandler), this._focused = this.wrapper, this._prevent = !1, this._toggle()
                },
                _editable: function(e) {
                    var t = this,
                        i = t.element,
                        n = e.disable,
                        s = e.readonly,
                        o = t.wrapper.add(t.filterInput).off(f),
                        a = t._inputWrapper.off(I);
                    s || n ? n ? (o.removeAttr(x), a.addClass(w).removeClass(v)) : (a.addClass(v).removeClass(w), o.on("focusin" + f, T(t._focusinHandler, t)).on("focusout" + f, T(t._focusoutHandler, t))) : (i.removeAttr(_).removeAttr(h), a.addClass(v).removeClass(w).on(I, t._toggleHover), o.attr(x, o.data(x)).attr(g, !1).on("keydown" + f, T(t._keydown, t)).on("focusin" + f, T(t._focusinHandler, t)).on("focusout" + f, T(t._focusoutHandler, t)).on("mousedown" + f, T(t._wrapperMousedown, t)).on("paste" + f, T(t._filterPaste, t)), t.wrapper.on("click" + f, T(t._wrapperClick, t)), t.filterInput || o.on("keypress" + f, T(t._keypress, t))), i.attr(_, n).attr(h, s), o.attr(g, n)
                },
                _keydown: function(e) {
                    var i, n, s, o = this,
                        a = e.keyCode,
                        l = e.altKey,
                        r = o.popup.visible();
                    if (o.filterInput && (i = o.filterInput[0] === u()), a === d.LEFT ? (a = d.UP, n = !0) : a === d.RIGHT && (a = d.DOWN, n = !0), !n || !i) {
                        if (e.keyCode = a, (l && a === d.UP || a === d.ESC) && o._focusElement(o.wrapper), o._state === L && a === d.ESC && (o._clearFilter(), o._open = !1, o._state = k), a === d.ENTER && o._typingTimeout && o.filterInput && r) return e.preventDefault(), t;
                        if (a !== d.SPACEBAR || i || (o.toggle(!r), e.preventDefault()), n = o._move(e), !n) {
                            if ((!r || !o.filterInput) && (s = o._focus(), a === d.HOME ? (n = !0, o._firstItem()) : a === d.END && (n = !0, o._lastItem()), n)) {
                                if (o.trigger("select", {
                                        dataItem: o._getElementDataItem(o._focus()),
                                        item: o._focus()
                                    })) return o._focus(s), t;
                                o._select(o._focus(), !0).done(function() {
                                    r || o._blur()
                                }), e.preventDefault()
                            }
                            l || n || !o.filterInput || o._search()
                        }
                    }
                },
                _matchText: function(e, i) {
                    var n = this.options.ignoreCase;
                    return e !== t && null !== e && (e += "", n && (e = e.toLowerCase()), 0 === e.indexOf(i))
                },
                _shuffleData: function(e, t) {
                    var i = this._optionLabelDataItem();
                    return i && (e = [i].concat(e)), e.slice(t).concat(e.slice(0, t))
                },
                _selectNext: function() {
                    var e, t, i, o = this,
                        a = o.dataSource.flatView(),
                        l = a.length + (o.hasOptionLabel() ? 1 : 0),
                        r = s(o._word, o._last),
                        p = o.selectedIndex;
                    for (p === -1 ? p = 0 : (p += r ? 1 : 0, p = n(p, l)), a = a.toJSON ? a.toJSON() : a.slice(), a = o._shuffleData(a, p), i = 0; i < l && (t = o._text(a[i]), !r || !o._matchText(t, o._last)) && !o._matchText(t, o._word); i++);
                    i !== l && (e = o._focus(), o._select(n(p + i, l)).done(function() {
                        var t = function() {
                            o.popup.visible() || o._change()
                        };
                        o.trigger("select", {
                            dataItem: o._getElementDataItem(o._focus()),
                            item: o._focus()
                        }) ? o._select(e).done(t) : t()
                    }))
                },
                _keypress: function(e) {
                    var t, i = this;
                    0 !== e.which && e.keyCode !== o.keys.ENTER && (t = String.fromCharCode(e.charCode || e.keyCode), i.options.ignoreCase && (t = t.toLowerCase()), " " === t && e.preventDefault(), i._word += t, i._last = t, i._search())
                },
                _popupOpen: function() {
                    var e = this.popup;
                    e.wrapper = o.wrap(e.element), e.element.closest(".km-root")[0] && (e.wrapper.addClass("km-popup km-widget"), this.wrapper.addClass("km-widget"))
                },
                _popup: function() {
                    r.fn._popup.call(this), this.popup.one("open", T(this._popupOpen, this))
                },
                _getElementDataItem: function(e) {
                    return e && e[0] ? e[0] === this.optionLabel[0] ? this._optionLabelDataItem() : this.listView.dataItemByIndex(this.listView.getElementIndex(e)) : null
                },
                _click: function(i) {
                    var n = this,
                        s = i.item || e(i.currentTarget);
                    return i.preventDefault(), n.trigger("select", {
                        dataItem: n._getElementDataItem(s),
                        item: s
                    }) ? (n.close(), t) : (n._userTriggered = !0, n._select(s).done(function() {
                        n._focusElement(n.wrapper), n._blur()
                    }), t)
                },
                _focusElement: function(e) {
                    var t = u(),
                        i = this.wrapper,
                        n = this.filterInput,
                        s = e === n ? i : n,
                        o = p.mobileOS && (p.touch || p.MSPointers || p.pointers);
                    n && n[0] === e[0] && o || n && (s[0] === t || this._focusFilter) && (this._focusFilter = !1, this._prevent = !0, this._focused = e.focus())
                },
                _searchByWord: function(e) {
                    var t, i;
                    e && (t = this, i = t.options.ignoreCase, i && (e = e.toLowerCase()), t._select(function(i) {
                        return t._matchText(t._text(i), e)
                    }))
                },
                _inputValue: function() {
                    return this.text()
                },
                _search: function() {
                    var e = this,
                        i = e.dataSource;
                    if (clearTimeout(e._typingTimeout), e._isFilterEnabled()) e._typingTimeout = setTimeout(function() {
                        var t = e.filterInput.val();
                        e._prev !== t && (e._prev = t, e.search(t), e._resizeFilterInput()), e._typingTimeout = null
                    }, e.options.delay);
                    else {
                        if (e._typingTimeout = setTimeout(function() {
                                e._word = ""
                            }, e.options.delay), !e.listView.bound()) return i.fetch().done(function() {
                            e._selectNext()
                        }), t;
                        e._selectNext()
                    }
                },
                _get: function(t) {
                    var i, n, s, o = "function" == typeof t,
                        a = o ? e() : e(t);
                    if (this.hasOptionLabel() && ("number" == typeof t ? t > -1 && (t -= 1) : a.hasClass("k-list-optionlabel") && (t = -1)), o) {
                        for (i = this.dataSource.flatView(), s = 0; s < i.length; s++)
                            if (t(i[s])) {
                                t = s, n = !0;
                                break
                            }
                        n || (t = -1)
                    }
                    return t
                },
                _firstItem: function() {
                    this.hasOptionLabel() ? this._focus(this.optionLabel) : this.listView.focusFirst()
                },
                _lastItem: function() {
                    this._resetOptionLabel(), this.listView.focusLast()
                },
                _nextItem: function() {
                    this.optionLabel.hasClass("k-state-focused") ? (this._resetOptionLabel(), this.listView.focusFirst()) : this.listView.focusNext()
                },
                _prevItem: function() {
                    this.optionLabel.hasClass("k-state-focused") || (this.listView.focusPrev(), this.listView.focus() || this._focus(this.optionLabel))
                },
                _focusItem: function() {
                    var e = this.options,
                        i = this.listView,
                        n = i.focus(),
                        s = i.select();
                    s = s[s.length - 1], s === t && e.highlightFirst && !n && (s = 0), s !== t ? i.focus(s) : !e.optionLabel || e.virtual && "dataItem" === e.virtual.mapValueTo ? i.scrollToIndex(0) : (this._focus(this.optionLabel), this._select(this.optionLabel))
                },
                _resetOptionLabel: function(e) {
                    this.optionLabel.removeClass("k-state-focused" + (e || "")).removeAttr("id")
                },
                _focus: function(e) {
                    var i = this.listView,
                        n = this.optionLabel;
                    return e === t ? (e = i.focus(), !e && n.hasClass("k-state-focused") && (e = n), e) : (this._resetOptionLabel(), e = this._get(e), i.focus(e), e === -1 && (n.addClass("k-state-focused").attr("id", i._optionID), this._focused.add(this.filterInput).removeAttr("aria-activedescendant").attr("aria-activedescendant", i._optionID)), t)
                },
                _select: function(e, t) {
                    var i = this;
                    return e = i._get(e), i.listView.select(e).done(function() {
                        t || i._state !== L || (i._state = k), e === -1 && i._selectValue(null)
                    })
                },
                _selectValue: function(e) {
                    var i = this,
                        n = i.options.optionLabel,
                        s = i.listView.select(),
                        o = "",
                        a = "";
                    s = s[s.length - 1], s === t && (s = -1), this._resetOptionLabel(" k-state-selected"), e || 0 === e ? (a = e, o = i._dataValue(e), n && (s += 1)) : n && (i._focus(i.optionLabel.addClass("k-state-selected")), a = i._optionLabelText(), o = "string" == typeof n ? "" : i._value(n), s = 0), i.selectedIndex = s, null === o && (o = ""), i._textAccessor(a), i._accessor(o, s), i._triggerCascade()
                },
                _mobile: function() {
                    var e = this,
                        t = e.popup,
                        i = p.mobileOS,
                        n = t.element.parents(".km-root").eq(0);
                    n.length && i && (t.options.animation.open.effects = i.android || i.meego ? "fadeIn" : i.ios || i.wp ? "slideIn:up" : t.options.animation.open.effects)
                },
                _filterHeader: function() {
                    var t;
                    this.filterInput && (this.filterInput.off(f).parent().remove(), this.filterInput = null), this._isFilterEnabled() && (t = '<span class="k-icon k-i-zoom"></span>', this.filterInput = e('<input class="k-textbox"/>').attr({
                        placeholder: this.element.attr("placeholder"),
                        title: this.element.attr("title"),
                        role: "listbox",
                        "aria-haspopup": !0,
                        "aria-expanded": !1
                    }), this.list.prepend(e('<span class="k-list-filter" />').append(this.filterInput.add(t))))
                },
                _span: function() {
                    var t, i = this,
                        n = i.wrapper,
                        s = "span.k-input";
                    t = n.find(s), t[0] || (n.append('<span unselectable="on" class="k-dropdown-wrap k-state-default"><span unselectable="on" class="k-input">&nbsp;</span><span unselectable="on" class="k-select" aria-label="select"><span class="k-icon k-i-arrow-60-down"></span></span></span>').append(i.element), t = n.find(s)), i.span = t, i._inputWrapper = e(n[0].firstChild), i._arrow = n.find(".k-select"), i._arrowIcon = i._arrow.find(".k-icon")
                },
                _wrapper: function() {
                    var e, t = this,
                        i = t.element,
                        n = i[0];
                    e = i.parent(), e.is("span.k-widget") || (e = i.wrap("<span />").parent(), e[0].style.cssText = n.style.cssText, e[0].title = n.title), t._focused = t.wrapper = e.addClass("k-widget k-dropdown k-header").addClass(n.className).css("display", "").attr({
                        accesskey: i.attr("accesskey"),
                        unselectable: "on",
                        role: "listbox",
                        "aria-haspopup": !0,
                        "aria-expanded": !1
                    }), i.hide().removeAttr("accesskey")
                },
                _clearSelection: function(e) {
                    this.select(e.value() ? 0 : -1)
                },
                _inputTemplate: function() {
                    var t = this,
                        i = t.options.valueTemplate;
                    if (i = i ? o.template(i) : e.proxy(o.template("#:this._text(data)#", {
                            useWithBlock: !1
                        }), t), t.valueTemplate = i, t.hasOptionLabel() && !t.options.optionLabelTemplate) try {
                        t.valueTemplate(t._optionLabelDataItem())
                    } catch (n) {
                        throw Error(y)
                    }
                },
                _textAccessor: function(i) {
                    var n, s = null,
                        o = this.valueTemplate,
                        a = this._optionLabelText(),
                        l = this.span;
                    if (i === t) return l.text();
                    e.isPlainObject(i) || i instanceof c ? s = i : a && a === i && (s = this.options.optionLabel), s || (s = this._assignInstance(i, this._accessor())), this.hasOptionLabel() && (s !== a && this._text(s) !== a || (o = this.optionLabelTemplate, "string" != typeof this.options.optionLabel || this.options.optionLabelTemplate || (s = a))), n = function() {
                        return {
                            elements: l.get(),
                            data: [{
                                dataItem: s
                            }]
                        }
                    }, this.angular("cleanup", n);
                    try {
                        l.html(o(s))
                    } catch (r) {
                        l.html("")
                    }
                    this.angular("compile", n)
                },
                _preselect: function(e, t) {
                    e || t || (t = this._optionLabelText()), this._accessor(e), this._textAccessor(t), this._old = this._accessor(), this._oldIndex = this.selectedIndex, this.listView.setValue(e), this._initialIndex = null, this._presetValue = !0
                },
                _assignInstance: function(e, t) {
                    var n = this.options.dataTextField,
                        s = {};
                    return n ? (i(s, n.split("."), e), i(s, this.options.dataValueField.split("."), t), s = new c(s)) : s = e, s
                }
            });
        a.plugin(V)
    }(window.kendo.jQuery), window.kendo
}, "function" == typeof define && define.amd ? define : function(e, t, i) {
    (i || t)()
});
//# sourceMappingURL=kendo.dropdownlist.min.js.map;
/** 
 * Kendo UI v2018.1.221 (http://www.telerik.com/kendo-ui)                                                                                                                                               
 * Copyright 2018 Telerik AD. All rights reserved.                                                                                                                                                      
 *                                                                                                                                                                                                      
 * Kendo UI commercial licenses may be obtained at                                                                                                                                                      
 * http://www.telerik.com/purchase/license-agreement/kendo-ui-complete                                                                                                                                  
 * If you do not own a commercial license, this file shall be governed by the trial license terms.                                                                                                      

*/
! function(e, define) {
    define("kendo.autocomplete.min", ["kendo.list.min", "kendo.mobile.scroller.min", "kendo.virtuallist.min"], e)
}(function() {
    return function(e, t) {
        function s(e, t, s) {
            return s ? t.substring(0, e).split(s).length - 1 : 0
        }

        function i(e, t, i) {
            return t.split(i)[s(e, t, i)]
        }

        function o(e, t, i, o, a) {
            var n = t.split(o);
            return n.splice(s(e, t, o), 1, i), o && "" !== n[n.length - 1] && n.push(""), n.join(a)
        }
        var a = window.kendo,
            n = a.support,
            r = a.caret,
            l = a._activeElement,
            u = n.placeholder,
            c = a.ui,
            d = c.List,
            p = a.keys,
            _ = a.data.DataSource,
            h = "aria-disabled",
            f = "aria-readonly",
            g = "change",
            v = "k-state-default",
            m = "disabled",
            y = "readonly",
            w = "k-state-focused",
            C = "k-state-selected",
            b = "k-state-disabled",
            k = "k-state-hover",
            x = ".kendoAutoComplete",
            T = "mouseenter" + x + " mouseleave" + x,
            V = e.proxy,
            S = d.extend({
                init: function(t, s) {
                    var i, o, n = this;
                    n.ns = x, s = e.isArray(s) ? {
                        dataSource: s
                    } : s, d.fn.init.call(n, t, s), t = n.element, s = n.options, s.placeholder = s.placeholder || t.attr("placeholder"), u && t.attr("placeholder", s.placeholder), n._wrapper(), n._loader(), n._clearButton(), n._dataSource(), n._ignoreCase(), t[0].type = "text", i = n.wrapper, n._popup(), t.addClass("k-input").on("keydown" + x, V(n._keydown, n)).on("keypress" + x, V(n._keypress, n)).on("paste" + x, V(n._search, n)).on("focus" + x, function() {
                        n._prev = n._accessor(), n._oldText = n._prev, n._placeholder(!1), i.addClass(w)
                    }).on("focusout" + x, function() {
                        n._change(), n._placeholder(), n.close(), i.removeClass(w)
                    }).attr({
                        autocomplete: "off",
                        role: "textbox",
                        "aria-haspopup": !0
                    }), n._clear.on("click" + x, V(n._clearValue, n)), n._enable(), n._old = n._accessor(), t[0].id && t.attr("aria-owns", n.ul[0].id), n._aria(), n._placeholder(), n._initList(), o = e(n.element).parents("fieldset").is(":disabled"), o && n.enable(!1), n.listView.bind("click", function(e) {
                        e.preventDefault()
                    }), n._resetFocusItemHandler = e.proxy(n._resetFocusItem, n), a.notify(n), n._toggleCloseVisibility()
                },
                options: {
                    name: "AutoComplete",
                    enabled: !0,
                    suggest: !1,
                    template: "",
                    groupTemplate: "#:data#",
                    fixedGroupTemplate: "#:data#",
                    dataTextField: "",
                    minLength: 1,
                    enforceMinLength: !1,
                    delay: 200,
                    height: 200,
                    filter: "startswith",
                    ignoreCase: !0,
                    highlightFirst: !1,
                    separator: null,
                    placeholder: "",
                    animation: {},
                    virtual: !1,
                    value: null,
                    clearButton: !0,
                    autoWidth: !1
                },
                _dataSource: function() {
                    var e = this;
                    e.dataSource && e._refreshHandler ? e._unbindDataSource() : (e._progressHandler = V(e._showBusy, e), e._errorHandler = V(e._hideBusy, e)), e.dataSource = _.create(e.options.dataSource).bind("progress", e._progressHandler).bind("error", e._errorHandler)
                },
                setDataSource: function(e) {
                    this.options.dataSource = e, this._dataSource(), this.listView.setDataSource(this.dataSource)
                },
                events: ["open", "close", g, "select", "filtering", "dataBinding", "dataBound"],
                setOptions: function(e) {
                    var t = this._listOptions(e);
                    d.fn.setOptions.call(this, e), this.listView.setOptions(t), this._accessors(), this._aria(), this._clearButton()
                },
                _listOptions: function(t) {
                    var s = d.fn._listOptions.call(this, e.extend(t, {
                        skipUpdateOnBind: !0
                    }));
                    return s.dataValueField = s.dataTextField, s.selectedItemChange = null, s
                },
                _editable: function(e) {
                    var t = this,
                        s = t.element,
                        i = t.wrapper.off(x),
                        o = e.readonly,
                        a = e.disable;
                    o || a ? (i.addClass(a ? b : v).removeClass(a ? v : b), s.attr(m, a).attr(y, o).attr(h, a).attr(f, o)) : (i.addClass(v).removeClass(b).on(T, t._toggleHover), s.removeAttr(m).removeAttr(y).attr(h, !1).attr(f, !1))
                },
                close: function() {
                    var e = this,
                        t = e.listView.focus();
                    t && t.removeClass(C), e.popup.close()
                },
                destroy: function() {
                    var e = this;
                    e.element.off(x), e._clear.off(x), e.wrapper.off(x), d.fn.destroy.call(e)
                },
                refresh: function() {
                    this.listView.refresh()
                },
                select: function(e) {
                    this._select(e)
                },
                search: function(t) {
                    var s, o = this,
                        a = o.options,
                        n = a.ignoreCase,
                        l = o._separator();
                    t = t || o._accessor(), clearTimeout(o._typingTimeout), l && (t = i(r(o.element)[0], t, l)), s = t.length, (!a.enforceMinLength && !s || s >= a.minLength) && (o._open = !0, o._mute(function() {
                        this.listView.value([])
                    }), o._filterSource({
                        value: n ? t.toLowerCase() : t,
                        operator: a.filter,
                        field: a.dataTextField,
                        ignoreCase: n
                    }), o.one("close", e.proxy(o._unifySeparators, o))), o._toggleCloseVisibility()
                },
                suggest: function(e) {
                    var i, o = this,
                        a = o._last,
                        n = o._accessor(),
                        u = o.element[0],
                        c = r(u)[0],
                        _ = o._separator(),
                        h = n.split(_),
                        f = s(c, n, _),
                        g = c;
                    return a == p.BACKSPACE || a == p.DELETE ? (o._last = t, t) : (e = e || "", "string" != typeof e && (e[0] && (e = o.dataSource.view()[d.inArray(e[0], o.ul[0])]), e = e ? o._text(e) : ""), c <= 0 && (c = n.toLowerCase().indexOf(e.toLowerCase()) + 1), i = n.substring(0, c).lastIndexOf(_), i = i > -1 ? c - (i + _.length) : c, n = h[f].substring(0, i), e && (e = "" + e, i = e.toLowerCase().indexOf(n.toLowerCase()), i > -1 && (e = e.substring(i + n.length), g = c + e.length, n += e), _ && "" !== h[h.length - 1] && h.push("")), h[f] = n, o._accessor(h.join(_ || "")), u === l() && r(u, c, g), t)
                },
                value: function(e) {
                    return e === t ? this._accessor() : (this.listView.value(e), this._accessor(e), this._old = this._accessor(), this._oldText = this._accessor(), t)
                },
                _click: function(e) {
                    var s = e.item,
                        i = this,
                        o = i.element,
                        a = i.listView.dataItemByIndex(i.listView.getElementIndex(s));
                    return e.preventDefault(), i._active = !0, i.trigger("select", {
                        dataItem: a,
                        item: s
                    }) ? (i.close(), t) : (i._oldText = o.val(), i._select(s).done(function() {
                        i._blur(), r(o, o.val().length)
                    }), t)
                },
                _clearText: e.noop,
                _resetFocusItem: function() {
                    var e = this.options.highlightFirst ? 0 : -1;
                    this.options.virtual && this.listView.scrollTo(0), this.listView.focus(e)
                },
                _listBound: function() {
                    var e, s = this,
                        i = s.popup,
                        o = s.options,
                        a = s.dataSource.flatView(),
                        n = a.length,
                        r = s.dataSource._group.length,
                        u = s.element[0] === l();
                    s._renderFooter(), s._renderNoData(), s._toggleNoData(!n), s._toggleHeader(!!r && !!n), s._resizePopup(), i.position(), n && o.suggest && u && s.suggest(a[0]), s._open && (s._open = !1, e = s._allowOpening() ? "open" : "close", s._typingTimeout && !u && (e = "close"), n && (s._resetFocusItem(), o.virtual && s.popup.unbind("activate", s._resetFocusItemHandler).one("activate", s._resetFocusItemHandler)), i[e](), s._typingTimeout = t), s._touchScroller && s._touchScroller.reset(), s._hideBusy(), s._makeUnselectable(), s.trigger("dataBound")
                },
                _mute: function(e) {
                    this._muted = !0, e.call(this), this._muted = !1
                },
                _listChange: function() {
                    var e = this._active || this.element[0] === l();
                    e && !this._muted && this._selectValue(this.listView.selectedDataItems()[0])
                },
                _selectValue: function(e) {
                    var t = this._separator(),
                        s = "";
                    e && (s = this._text(e)), null === s && (s = ""), t && (s = o(r(this.element)[0], this._accessor(), s, t, this._defaultSeparator())), this._prev = s, this._accessor(s), this._placeholder()
                },
                _unifySeparators: function() {
                    return this._accessor(this.value().split(this._separator()).join(this._defaultSeparator())), this
                },
                _preselect: function(e, t) {
                    this._inputValue(t), this._accessor(e), this._old = this.oldText = this._accessor(), this.listView.setValue(e), this._placeholder()
                },
                _change: function() {
                    var e = this,
                        t = e._unifySeparators().value(),
                        s = t !== d.unifyType(e._old, typeof t),
                        i = s && !e._typing,
                        o = e._oldText !== t;
                    e._old = t, e._oldText = t, (i || o) && e.element.trigger(g), s && e.trigger(g), e.typing = !1, e._toggleCloseVisibility()
                },
                _accessor: function(e) {
                    var s = this,
                        i = s.element[0];
                    return e === t ? (e = i.value, i.className.indexOf("k-readonly") > -1 && e === s.options.placeholder ? "" : e) : (i.value = null === e ? "" : e, s._placeholder(), t)
                },
                _keydown: function(e) {
                    var t, s, i = this,
                        o = e.keyCode,
                        a = i.listView,
                        n = i.popup.visible(),
                        r = a.focus();
                    if (i._last = o, o === p.DOWN) n ? this._move(r ? "focusNext" : "focusFirst") : i.value() && i._filterSource({
                        value: i.ignoreCase ? i.value().toLowerCase() : i.value(),
                        operator: i.options.filter,
                        field: i.options.dataTextField,
                        ignoreCase: i.ignoreCase
                    }).done(function() {
                        i._resetFocusItem(), i.popup.open()
                    }), e.preventDefault();
                    else if (o === p.UP) n && this._move(r ? "focusPrev" : "focusLast"), e.preventDefault();
                    else if (o === p.HOME) this._move("focusFirst");
                    else if (o === p.END) this._move("focusLast");
                    else if (o === p.ENTER || o === p.TAB) {
                        if (o === p.ENTER && n && e.preventDefault(), n && r) {
                            if (t = a.dataItemByIndex(a.getElementIndex(r)), i.trigger("select", {
                                    dataItem: t,
                                    item: r
                                })) return;
                            this._select(r)
                        }
                        this._blur()
                    } else o === p.ESC ? (n ? e.preventDefault() : i._clearValue(), i.close()) : !i.popup.visible() || o !== p.PAGEDOWN && o !== p.PAGEUP ? (i.popup._hovered = !0, i._search()) : (e.preventDefault(), s = o === p.PAGEDOWN ? 1 : -1, a.scrollWith(s * a.screenHeight()))
                },
                _keypress: function() {
                    this._oldText = this.element.val(), this._typing = !0
                },
                _move: function(e) {
                    this.listView[e](), this.options.suggest && this.suggest(this.listView.focus())
                },
                _hideBusy: function() {
                    var e = this;
                    clearTimeout(e._busy), e._loading.hide(), e.element.attr("aria-busy", !1), e._busy = null, e._showClear()
                },
                _showBusy: function() {
                    var e = this;
                    e._busy || (e._busy = setTimeout(function() {
                        e.element.attr("aria-busy", !0), e._loading.show(), e._hideClear()
                    }, 100))
                },
                _placeholder: function(e) {
                    if (!u) {
                        var s, i = this,
                            o = i.element,
                            a = i.options.placeholder;
                        if (a) {
                            if (s = o.val(), e === t && (e = !s), e || (a = s !== a ? s : ""), s === i._old && !e) return;
                            o.toggleClass("k-readonly", e).val(a), a || o[0] !== document.activeElement || r(o[0], 0, 0)
                        }
                    }
                },
                _separator: function() {
                    var e = this.options.separator;
                    return e instanceof Array ? RegExp(e.join("|"), "gi") : e
                },
                _defaultSeparator: function() {
                    var e = this.options.separator;
                    return e instanceof Array ? e[0] : e
                },
                _inputValue: function() {
                    return this.element.val()
                },
                _search: function() {
                    var e = this;
                    clearTimeout(e._typingTimeout), e._typingTimeout = setTimeout(function() {
                        e._prev !== e._accessor() && (e._prev = e._accessor(), e.search())
                    }, e.options.delay)
                },
                _select: function(e) {
                    var t = this;
                    return t._active = !0, t.listView.select(e).done(function() {
                        t._active = !1
                    })
                },
                _loader: function() {
                    this._loading = e('<span class="k-icon k-i-loading" style="display:none"></span>').insertAfter(this.element)
                },
                _clearButton: function() {
                    d.fn._clearButton.call(this), this.options.clearButton && (this._clear.insertAfter(this.element), this.wrapper.addClass("k-autocomplete-clearable"))
                },
                _toggleHover: function(t) {
                    e(t.currentTarget).toggleClass(k, "mouseenter" === t.type)
                },
                _toggleCloseVisibility: function() {
                    this.value() ? this._showClear() : this._hideClear()
                },
                _wrapper: function() {
                    var e, t = this,
                        s = t.element,
                        i = s[0];
                    e = s.parent(), e.is("span.k-widget") || (e = s.wrap("<span />").parent()), e.attr("tabindex", -1), e.attr("role", "presentation"), e[0].style.cssText = i.style.cssText, s.css({
                        width: "",
                        height: i.style.height
                    }), t._focused = t.element, t.wrapper = e.addClass("k-widget k-autocomplete k-header").addClass(i.className)
                }
            });
        c.plugin(S)
    }(window.kendo.jQuery), window.kendo
}, "function" == typeof define && define.amd ? define : function(e, t, s) {
    (s || t)()
});
//# sourceMappingURL=kendo.autocomplete.min.js.map;
// helper.js
! function(t) {
    function e(t, e, n, a) {
        if ("addEventListener" in t) try {
            t.addEventListener(e, n, a)
        } catch (i) {
            if ("object" != typeof n || !n.handleEvent) throw i;
            t.addEventListener(e, function(t) {
                n.handleEvent.call(n, t)
            }, a)
        } else "attachEvent" in t && ("object" == typeof n && n.handleEvent ? t.attachEvent("on" + e, function() {
            n.handleEvent.call(n)
        }) : t.attachEvent("on" + e, n))
    }

    function n(t, e, n, a) {
        if ("removeEventListener" in t) try {
            t.removeEventListener(e, n, a)
        } catch (i) {
            if ("object" != typeof n || !n.handleEvent) throw i;
            t.removeEventListener(e, function(t) {
                n.handleEvent.call(n, t)
            }, a)
        } else "detachEvent" in t && ("object" == typeof n && n.handleEvent ? t.detachEvent("on" + e, function() {
            n.handleEvent.call(n)
        }) : t.detachEvent("on" + e, n))
    }
    window.MBP = window.MBP || {}, MBP.viewportmeta = t.querySelector && t.querySelector('meta[name="viewport"]'), MBP.ua = navigator.userAgent, MBP.scaleFix = function() {
        MBP.viewportmeta && /iPhone|iPad|iPod/.test(MBP.ua) && !/Opera Mini/.test(MBP.ua) && (MBP.viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0", t.addEventListener("gesturestart", MBP.gestureStart, !1))
    }, MBP.gestureStart = function() {
        MBP.viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6"
    }, MBP.BODY_SCROLL_TOP = !1, MBP.getScrollTop = function() {
        var e = window,
            n = t;
        return e.pageYOffset || "CSS1Compat" === n.compatMode && n.documentElement.scrollTop || n.body.scrollTop || 0
    }, MBP.hideUrlBar = function() {
        var t = window;
        location.hash || MBP.BODY_SCROLL_TOP === !1 || t.scrollTo(0, 1 === MBP.BODY_SCROLL_TOP ? 0 : 1)
    }, MBP.hideUrlBarOnLoad = function() {
        var t, e = window,
            n = e.document;
        e.navigator.standalone || location.hash || !e.addEventListener || (window.scrollTo(0, 1), MBP.BODY_SCROLL_TOP = 1, t = setInterval(function() {
            n.body && (clearInterval(t), MBP.BODY_SCROLL_TOP = MBP.getScrollTop(), MBP.hideUrlBar())
        }, 15), e.addEventListener("load", function() {
            setTimeout(function() {
                MBP.getScrollTop() < 20 && MBP.hideUrlBar()
            }, 0)
        }, !1))
    }, MBP.fastButton = function(t, e, n) {
        if (this.handler = e, this.pressedClass = "undefined" == typeof n ? "pressed" : n, MBP.listenForGhostClicks(), t.length && t.length > 1)
            for (var a in t) this.addClickEvent(t[a]);
        else this.addClickEvent(t)
    }, MBP.fastButton.prototype.handleEvent = function(t) {
        switch (t = t || window.event, t.type) {
            case "touchstart":
                this.onTouchStart(t);
                break;
            case "touchmove":
                this.onTouchMove(t);
                break;
            case "touchend":
                this.onClick(t);
                break;
            case "click":
                this.onClick(t)
        }
    }, MBP.fastButton.prototype.onTouchStart = function(e) {
        var n = e.target || e.srcElement;
        e.stopPropagation(), n.addEventListener("touchend", this, !1), t.body.addEventListener("touchmove", this, !1), this.startX = e.touches[0].clientX, this.startY = e.touches[0].clientY, n.className += " " + this.pressedClass
    }, MBP.fastButton.prototype.onTouchMove = function(t) {
        (Math.abs(t.touches[0].clientX - this.startX) > 10 || Math.abs(t.touches[0].clientY - this.startY) > 10) && this.reset(t)
    }, MBP.fastButton.prototype.onClick = function(t) {
        t = t || window.event;
        var e = t.target || t.srcElement;
        t.stopPropagation && t.stopPropagation(), this.reset(t), this.handler.apply(t.currentTarget, [t]), "touchend" == t.type && MBP.preventGhostClick(this.startX, this.startY);
        var n = new RegExp(" ?" + this.pressedClass, "gi");
        e.className = e.className.replace(n, "")
    }, MBP.fastButton.prototype.reset = function(e) {
        var a = e.target || e.srcElement;
        n(a, "touchend", this, !1), n(t.body, "touchmove", this, !1);
        var i = new RegExp(" ?" + this.pressedClass, "gi");
        a.className = a.className.replace(i, "")
    }, MBP.fastButton.prototype.addClickEvent = function(t) {
        e(t, "touchstart", this, !1), e(t, "click", this, !1)
    }, MBP.preventGhostClick = function(t, e) {
        MBP.coords.push(t, e), window.setTimeout(function() {
            MBP.coords.splice(0, 2)
        }, 2500)
    }, MBP.ghostClickHandler = function(t) {
        if (!MBP.hadTouchEvent && MBP.dodgyAndroid) return t.stopPropagation(), void t.preventDefault();
        for (var e = 0, n = MBP.coords.length; n > e; e += 2) {
            var a = MBP.coords[e],
                i = MBP.coords[e + 1];
            Math.abs(t.clientX - a) < 25 && Math.abs(t.clientY - i) < 25 && (t.stopPropagation(), t.preventDefault())
        }
    }, MBP.dodgyAndroid = "ontouchstart" in window && -1 != navigator.userAgent.indexOf("Android 2.3"), MBP.listenForGhostClicks = function() {
        var n = !1;
        return function() {
            n || (t.addEventListener && t.addEventListener("click", MBP.ghostClickHandler, !0), e(t.documentElement, "touchstart", function() {
                MBP.hadTouchEvent = !0
            }, !1), n = !0)
        }
    }(), MBP.coords = [], MBP.autogrow = function(t, e) {
        function n() {
            var t = this.scrollHeight,
                e = this.clientHeight;
            t > e && (this.style.height = t + 3 * i + "px")
        }
        var a = e ? e : 12,
            i = t.currentStyle ? t.currentStyle.lineHeight : getComputedStyle(t, null).lineHeight;
        i = -1 == i.indexOf("px") ? a : parseInt(i, 10), t.style.overflow = "hidden", t.addEventListener ? t.addEventListener("input", n, !1) : t.attachEvent("onpropertychange", n)
    }, MBP.enableActive = function() {
        t.addEventListener("touchstart", function() {}, !1)
    }, MBP.preventScrolling = function() {
        t.addEventListener("touchmove", function(t) {
            "range" !== t.target.type && t.preventDefault()
        }, !1)
    }, MBP.preventZoom = function() {
        if (MBP.viewportmeta && navigator.platform.match(/iPad|iPhone|iPod/i))
            for (var e = t.querySelectorAll("input, select, textarea"), n = "width=device-width,initial-scale=1,maximum-scale=", a = 0, i = e.length, o = function() {
                    MBP.viewportmeta.content = n + "1"
                }, r = function() {
                    MBP.viewportmeta.content = n + "10"
                }; i > a; a++) e[a].onfocus = o, e[a].onblur = r
    }, MBP.startupImage = function() {
        var e, n, a, i, o, r;
        a = window.devicePixelRatio, i = t.getElementsByTagName("head")[0], "iPad" === navigator.platform ? (e = 2 === a ? "img/startup/startup-tablet-portrait-retina.png" : "img/startup/startup-tablet-portrait.png", n = 2 === a ? "img/startup/startup-tablet-landscape-retina.png" : "img/startup/startup-tablet-landscape.png", o = t.createElement("link"), o.setAttribute("rel", "apple-touch-startup-image"), o.setAttribute("media", "screen and (orientation: portrait)"), o.setAttribute("href", e), i.appendChild(o), r = t.createElement("link"), r.setAttribute("rel", "apple-touch-startup-image"), r.setAttribute("media", "screen and (orientation: landscape)"), r.setAttribute("href", n), i.appendChild(r)) : (e = 2 === a ? "img/startup/startup-retina.png" : "img/startup/startup.png", e = 568 === screen.height ? "img/startup/startup-retina-4in.png" : e, o = t.createElement("link"), o.setAttribute("rel", "apple-touch-startup-image"), o.setAttribute("href", e), i.appendChild(o)), navigator.platform.match(/iPhone|iPod/i) && 568 === screen.height && navigator.userAgent.match(/\bOS 6_/) && MBP.viewportmeta && (MBP.viewportmeta.content = MBP.viewportmeta.content.replace(/\bwidth\s*=\s*320\b/, "width=320.1").replace(/\bwidth\s*=\s*device-width\b/, ""))
    }
}(document);
var accent_map = {
    'áº': 'a',
    'Ã': 'a',
    'Ã¡': 'a',
    'Ã': 'a',
    'Ã ': 'a',
    'Ä': 'a',
    'Ä': 'a',
    'áº®': 'a',
    'áº¯': 'a',
    'áº°': 'a',
    'áº±': 'a',
    'áº´': 'a',
    'áºµ': 'a',
    'áº²': 'a',
    'áº³': 'a',
    'Ã': 'a',
    'Ã¢': 'a',
    'áº¤': 'a',
    'áº¥': 'a',
    'áº¦': 'a',
    'áº§': 'a',
    'áºª': 'a',
    'áº«': 'a',
    'áº¨': 'a',
    'áº©': 'a',
    'Ç': 'a',
    'Ç': 'a',
    'Ã': 'a',
    'Ã¥': 'a',
    'Çº': 'a',
    'Ç»': 'a',
    'Ã': 'a',
    'Ã¤': 'a',
    'Ç': 'a',
    'Ç': 'a',
    'Ã': 'a',
    'Ã£': 'a',
    'È¦': 'a',
    'È§': 'a',
    'Ç ': 'a',
    'Ç¡': 'a',
    'Ä': 'a',
    'Ä': 'a',
    'Ä': 'a',
    'Ä': 'a',
    'áº¢': 'a',
    'áº£': 'a',
    'È': 'a',
    'È': 'a',
    'È': 'a',
    'È': 'a',
    'áº ': 'a',
    'áº¡': 'a',
    'áº¶': 'a',
    'áº·': 'a',
    'áº¬': 'a',
    'áº­': 'a',
    'á¸': 'a',
    'á¸': 'a',
    'Èº': 'a',
    'â±¥': 'a',
    'Ç¼': 'a',
    'Ç½': 'a',
    'Ç¢': 'a',
    'Ç£': 'a',
    'á¸': 'b',
    'á¸': 'b',
    'á¸': 'b',
    'á¸': 'b',
    'á¸': 'b',
    'á¸': 'b',
    'É': 'b',
    'Æ': 'b',
    'áµ¬': 'b',
    'Æ': 'b',
    'É': 'b',
    'Æ': 'b',
    'Æ': 'b',
    'Ä': 'c',
    'Ä': 'c',
    'Ä': 'c',
    'Ä': 'c',
    'Ä': 'c',
    'Ä': 'c',
    'Ä': 'c',
    'Ä': 'c',
    'Ã': 'c',
    'Ã§': 'c',
    'á¸': 'c',
    'á¸': 'c',
    'È»': 'c',
    'È¼': 'c',
    'Æ': 'c',
    'Æ': 'c',
    'É': 'c',
    'Ä': 'd',
    'Ä': 'd',
    'á¸': 'd',
    'á¸': 'd',
    'á¸': 'd',
    'á¸': 'd',
    'á¸': 'd',
    'á¸': 'd',
    'á¸': 'd',
    'á¸': 'd',
    'á¸': 'd',
    'á¸': 'd',
    'Ä': 'd',
    'Ä': 'd',
    'áµ­': 'd',
    'Æ': 'd',
    'É': 'd',
    'Æ': 'd',
    'É': 'd',
    'Æ': 'd',
    'Æ': 'd',
    'È¡': 'd',
    'Ã°': 'd',
    'Ã': 'e',
    'Æ': 'e',
    'Æ': 'e',
    'Ç': 'e',
    'Ã©': 'e',
    'Ã': 'e',
    'Ã¨': 'e',
    'Ä': 'e',
    'Ä': 'e',
    'Ã': 'e',
    'Ãª': 'e',
    'áº¾': 'e',
    'áº¿': 'e',
    'á»': 'e',
    'á»': 'e',
    'á»': 'e',
    'á»': 'e',
    'á»': 'e',
    'á»': 'e',
    'Ä': 'e',
    'Ä': 'e',
    'Ã': 'e',
    'Ã«': 'e',
    'áº¼': 'e',
    'áº½': 'e',
    'Ä': 'e',
    'Ä': 'e',
    'È¨': 'e',
    'È©': 'e',
    'á¸': 'e',
    'á¸': 'e',
    'Ä': 'e',
    'Ä': 'e',
    'Ä': 'e',
    'Ä': 'e',
    'á¸': 'e',
    'á¸': 'e',
    'á¸': 'e',
    'á¸': 'e',
    'áºº': 'e',
    'áº»': 'e',
    'È': 'e',
    'È': 'e',
    'È': 'e',
    'È': 'e',
    'áº¸': 'e',
    'áº¹': 'e',
    'á»': 'e',
    'á»': 'e',
    'á¸': 'e',
    'á¸': 'e',
    'á¸': 'e',
    'á¸': 'e',
    'É': 'e',
    'É': 'e',
    'É': 'e',
    'É': 'e',
    'á¸': 'f',
    'á¸': 'f',
    'áµ®': 'f',
    'Æ': 'f',
    'Æ': 'f',
    'Ç´': 'g',
    'Çµ': 'g',
    'Ä': 'g',
    'Ä': 'g',
    'Ä': 'g',
    'Ä': 'g',
    'Ç¦': 'g',
    'Ç§': 'g',
    'Ä ': 'g',
    'Ä¡': 'g',
    'Ä¢': 'g',
    'Ä£': 'g',
    'á¸ ': 'g',
    'á¸¡': 'g',
    'Ç¤': 'g',
    'Ç¥': 'g',
    'Æ': 'g',
    'É ': 'g',
    'Ä¤': 'h',
    'Ä¥': 'h',
    'È': 'h',
    'È': 'h',
    'á¸¦': 'h',
    'á¸§': 'h',
    'á¸¢': 'h',
    'á¸£': 'h',
    'á¸¨': 'h',
    'á¸©': 'h',
    'á¸¤': 'h',
    'á¸¥': 'h',
    'á¸ª': 'h',
    'á¸«': 'h',
    'H': 'h',
    'Ì±': 'h',
    'áº': 'h',
    'Ä¦': 'h',
    'Ä§': 'h',
    'â±§': 'h',
    'â±¨': 'h',
    'Ã': 'i',
    'Ã­': 'i',
    'Ã': 'i',
    'Ã¬': 'i',
    'Ä¬': 'i',
    'Ä­': 'i',
    'Ã': 'i',
    'Ã®': 'i',
    'Ç': 'i',
    'Ç': 'i',
    'Ã': 'i',
    'Ã¯': 'i',
    'á¸®': 'i',
    'á¸¯': 'i',
    'Ä¨': 'i',
    'Ä©': 'i',
    'Ä°': 'i',
    'i': 'i',
    'Ä®': 'i',
    'Ä¯': 'i',
    'Äª': 'i',
    'Ä«': 'i',
    'á»': 'i',
    'á»': 'i',
    'È': 'i',
    'È': 'i',
    'È': 'i',
    'È': 'i',
    'á»': 'i',
    'á»': 'i',
    'á¸¬': 'i',
    'á¸­': 'i',
    'I': 'i',
    'Ä±': 'i',
    'Æ': 'i',
    'É¨': 'i',
    'Ä´': 'j',
    'Äµ': 'j',
    'J': 'j',
    'Ì': 'j',
    'Ç°': 'j',
    'È·': 'j',
    'É': 'j',
    'É': 'j',
    'Ê': 'j',
    'É': 'j',
    'Ê': 'j',
    'á¸°': 'k',
    'á¸±': 'k',
    'Ç¨': 'k',
    'Ç©': 'k',
    'Ä¶': 'k',
    'Ä·': 'k',
    'á¸²': 'k',
    'á¸³': 'k',
    'á¸´': 'k',
    'á¸µ': 'k',
    'Æ': 'k',
    'Æ': 'k',
    'â±©': 'k',
    'â±ª': 'k',
    'Ä¹': 'l',
    'Äº': 'l',
    'Ä½': 'l',
    'Ä¾': 'l',
    'Ä»': 'l',
    'Ä¼': 'l',
    'á¸¶': 'l',
    'á¸·': 'l',
    'á¸¸': 'l',
    'á¸¹': 'l',
    'á¸¼': 'l',
    'á¸½': 'l',
    'á¸º': 'l',
    'á¸»': 'l',
    'Å': 'l',
    'Å': 'l',
    'Å': 'l',
    'Ì£': 'l',
    'Å': 'l',
    'Ì£': 'l',
    'Ä¿': 'l',
    'Å': 'l',
    'È½': 'l',
    'Æ': 'l',
    'â± ': 'l',
    'â±¡': 'l',
    'â±¢': 'l',
    'É«': 'l',
    'É¬': 'l',
    'É­': 'l',
    'È´': 'l',
    'á¸¾': 'm',
    'á¸¿': 'm',
    'á¹': 'm',
    'á¹': 'm',
    'á¹': 'm',
    'á¹': 'm',
    'É±': 'm',
    'Å': 'n',
    'Å': 'n',
    'Ç¸': 'n',
    'Ç¹': 'n',
    'Å': 'n',
    'Å': 'n',
    'Ã': 'n',
    'Ã±': 'n',
    'á¹': 'n',
    'á¹': 'n',
    'Å': 'n',
    'Å': 'n',
    'á¹': 'n',
    'á¹': 'n',
    'á¹': 'n',
    'á¹': 'n',
    'á¹': 'n',
    'á¹': 'n',
    'Æ': 'n',
    'É²': 'n',
    'È ': 'n',
    'Æ': 'n',
    'É³': 'n',
    'Èµ': 'n',
    'N': 'n',
    'Ì': 'n',
    'n': 'n',
    'Ì': 'n',
    'Ã': 'o',
    'Ã³': 'o',
    'Ã': 'o',
    'Ã²': 'o',
    'Å': 'o',
    'Å': 'o',
    'Ã': 'o',
    'Ã´': 'o',
    'á»': 'o',
    'á»': 'o',
    'á»': 'o',
    'á»': 'o',
    'á»': 'o',
    'á»': 'o',
    'á»': 'o',
    'á»': 'o',
    'Ç': 'o',
    'Ç': 'o',
    'Ã': 'o',
    'Ã¶': 'o',
    'Èª': 'o',
    'È«': 'o',
    'Å': 'o',
    'Å': 'o',
    'Ã': 'o',
    'Ãµ': 'o',
    'á¹': 'o',
    'á¹': 'o',
    'á¹': 'o',
    'á¹': 'o',
    'È¬': 'o',
    'È­': 'o',
    'È®': 'o',
    'È¯': 'o',
    'È°': 'o',
    'È±': 'o',
    'Ã': 'o',
    'Ã¸': 'o',
    'Ç¾': 'o',
    'Ç¿': 'o',
    'Çª': 'o',
    'Ç«': 'o',
    'Ç¬': 'o',
    'Ç­': 'o',
    'Å': 'o',
    'Å': 'o',
    'á¹': 'o',
    'á¹': 'o',
    'á¹': 'o',
    'á¹': 'o',
    'á»': 'o',
    'á»': 'o',
    'È': 'o',
    'È': 'o',
    'È': 'o',
    'È': 'o',
    'Æ ': 'o',
    'Æ¡': 'o',
    'á»': 'o',
    'á»': 'o',
    'á»': 'o',
    'á»': 'o',
    'á» ': 'o',
    'á»¡': 'o',
    'á»': 'o',
    'á»': 'o',
    'á»¢': 'o',
    'á»£': 'o',
    'á»': 'o',
    'á»': 'o',
    'á»': 'o',
    'á»': 'o',
    'Æ': 'o',
    'Éµ': 'o',
    'á¹': 'p',
    'á¹': 'p',
    'á¹': 'p',
    'á¹': 'p',
    'â±£': 'p',
    'Æ¤': 'p',
    'Æ¥': 'p',
    'P': 'p',
    'Ì': 'p',
    'p': 'p',
    'Ì': 'p',
    'Ê ': 'q',
    'É': 'q',
    'É': 'q',
    'Å': 'r',
    'Å': 'r',
    'Å': 'r',
    'Å': 'r',
    'á¹': 'r',
    'á¹': 'r',
    'Å': 'r',
    'Å': 'r',
    'È': 'r',
    'È': 'r',
    'È': 'r',
    'È': 'r',
    'á¹': 'r',
    'á¹': 'r',
    'á¹': 'r',
    'á¹': 'r',
    'á¹': 'r',
    'á¹': 'r',
    'É': 'r',
    'É': 'r',
    'áµ²': 'r',
    'É¼': 'r',
    'â±¤': 'r',
    'É½': 'r',
    'É¾': 'r',
    'áµ³': 'r',
    'Ã': 's',
    'Å': 's',
    'Å': 's',
    'á¹¤': 's',
    'á¹¥': 's',
    'Å': 's',
    'Å': 's',
    'Å ': 's',
    'Å¡': 's',
    'á¹¦': 's',
    'á¹§': 's',
    'á¹ ': 's',
    'á¹¡': 's',
    'áº': 's',
    'Å': 's',
    'Å': 's',
    'á¹¢': 's',
    'á¹£': 's',
    'á¹¨': 's',
    'á¹©': 's',
    'È': 's',
    'È': 's',
    'Ê': 's',
    'S': 's',
    'Ì©': 's',
    's': 's',
    'Ì©': 's',
    'Ã': 't',
    'Ã¾': 't',
    'Å¤': 't',
    'Å¥': 't',
    'T': 't',
    'Ì': 't',
    'áº': 't',
    'á¹ª': 't',
    'á¹«': 't',
    'Å¢': 't',
    'Å£': 't',
    'á¹¬': 't',
    'á¹­': 't',
    'È': 't',
    'È': 't',
    'á¹°': 't',
    'á¹±': 't',
    'á¹®': 't',
    'á¹¯': 't',
    'Å¦': 't',
    'Å§': 't',
    'È¾': 't',
    'â±¦': 't',
    'áµµ': 't',
    'Æ«': 't',
    'Æ¬': 't',
    'Æ­': 't',
    'Æ®': 't',
    'Ê': 't',
    'È¶': 't',
    'Ã': 'u',
    'Ãº': 'u',
    'Ã': 'u',
    'Ã¹': 'u',
    'Å¬': 'u',
    'Å­': 'u',
    'Ã': 'u',
    'Ã»': 'u',
    'Ç': 'u',
    'Ç': 'u',
    'Å®': 'u',
    'Å¯': 'u',
    'Ã': 'u',
    'Ã¼': 'u',
    'Ç': 'u',
    'Ç': 'u',
    'Ç': 'u',
    'Ç': 'u',
    'Ç': 'u',
    'Ç': 'u',
    'Ç': 'u',
    'Ç': 'u',
    'Å°': 'u',
    'Å±': 'u',
    'Å¨': 'u',
    'Å©': 'u',
    'á¹¸': 'u',
    'á¹¹': 'u',
    'Å²': 'u',
    'Å³': 'u',
    'Åª': 'u',
    'Å«': 'u',
    'á¹º': 'u',
    'á¹»': 'u',
    'á»¦': 'u',
    'á»§': 'u',
    'È': 'u',
    'È': 'u',
    'È': 'u',
    'È': 'u',
    'Æ¯': 'u',
    'Æ°': 'u',
    'á»¨': 'u',
    'á»©': 'u',
    'á»ª': 'u',
    'á»«': 'u',
    'á»®': 'u',
    'á»¯': 'u',
    'á»¬': 'u',
    'á»­': 'u',
    'á»°': 'u',
    'á»±': 'u',
    'á»¤': 'u',
    'á»¥': 'u',
    'á¹²': 'u',
    'á¹³': 'u',
    'á¹¶': 'u',
    'á¹·': 'u',
    'á¹´': 'u',
    'á¹µ': 'u',
    'É': 'u',
    'Ê': 'u',
    'á¹¼': 'v',
    'á¹½': 'v',
    'á¹¾': 'v',
    'á¹¿': 'v',
    'Æ²': 'v',
    'Ê': 'v',
    'áº': 'w',
    'áº': 'w',
    'áº': 'w',
    'áº': 'w',
    'Å´': 'w',
    'Åµ': 'w',
    'W': 'w',
    'Ì': 'w',
    'áº': 'w',
    'áº': 'w',
    'áº': 'w',
    'áº': 'w',
    'áº': 'w',
    'áº': 'w',
    'áº': 'w',
    'áº': 'x',
    'áº': 'x',
    'áº': 'x',
    'áº': 'x',
    'Ã': 'y',
    'Ã½': 'y',
    'á»²': 'y',
    'á»³': 'y',
    'Å¶': 'y',
    'Å·': 'y',
    'Y': 'y',
    'Ì': 'y',
    'áº': 'y',
    'Å¸': 'y',
    'Ã¿': 'y',
    'á»¸': 'y',
    'á»¹': 'y',
    'áº': 'y',
    'áº': 'y',
    'È²': 'y',
    'È³': 'y',
    'á»¶': 'y',
    'á»·': 'y',
    'á»´': 'y',
    'á»µ': 'y',
    'Ê': 'y',
    'É': 'y',
    'É': 'y',
    'Æ³': 'y',
    'Æ´': 'y',
    'Å¹': 'z',
    'Åº': 'z',
    'áº': 'z',
    'áº': 'z',
    'Å½': 'z',
    'Å¾': 'z',
    'Å»': 'z',
    'Å¼': 'z',
    'áº': 'z',
    'áº': 'z',
    'áº': 'z',
    'áº': 'z',
    'Æµ': 'z',
    'Æ¶': 'z',
    'È¤': 'z',
    'È¥': 'z',
    'Ê': 'z',
    'Ê': 'z',
    'â±«': 'z',
    'â±¬': 'z',
    'Ç®': 'z',
    'Ç¯': 'z',
    'Æº': 'z',
    "Ò": "G",
    "Ð": "YO",
    "Ð": "E",
    "Ð": "YI",
    "Ð": "I",
    "Ð": "A",
    "Ð": "B",
    "Ð": "V",
    "Ð": "G",
    "Ð": "D",
    "Ð": "E",
    "Ð": "ZH",
    "Ð": "Z",
    "Ð": "I",
    "Ð": "Y",
    "Ð": "K",
    "Ð": "L",
    "Ð": "M",
    "Ð": "N",
    "Ð": "O",
    "Ð": "P",
    "Ð ": "R",
    "Ð¡": "S",
    "Ð¢": "T",
    "Ð£": "U",
    "Ð¤": "F",
    "Ð¥": "H",
    "Ð¦": "TS",
    "Ð§": "CH",
    "Ð¨": "SH",
    "Ð©": "SCH",
    "Ðª": "'",
    "Ð«": "Y",
    "Ð¬": " ",
    "Ð­": "E",
    "Ð®": "YU",
    "Ð¯": "YA",
    "Ñ": "i",
    "Ò": "g",
    "Ñ": "yo",
    "â": "#",
    "Ñ": "e",
    "Ñ": "yi",
    "Ð°": "a",
    "Ð±": "b",
    "Ð²": "v",
    "Ð³": "g",
    "Ð´": "d",
    "Ðµ": "e",
    "Ð¶": "zh",
    "Ð·": "z",
    "Ð¸": "i",
    "Ð¹": "y",
    "Ðº": "k",
    "Ð»": "l",
    "Ð¼": "m",
    "Ð½": "n",
    "Ð¾": "o",
    "Ð¿": "p",
    "Ñ": "r",
    "Ñ": "s",
    "Ñ": "t",
    "Ñ": "u",
    "Ñ": "f",
    "Ñ": "h",
    "Ñ": "ts",
    "Ñ": "ch",
    "Ñ": "sh",
    "Ñ": "sch",
    "Ñ": "'",
    "Ñ": "y",
    "Ñ": " ",
    "Ñ": "e",
    "Ñ": "yu",
    "Ñ": "ya",
    "Ð¬Ð": "IE",
    "Ð¬Ð": "IE",
    "ÑÐµ": "ie",
    "ÑÑ": "ie",
    'Î±': 'a',
    'Î¬': 'a',
    'Î': 'a',
    'Î': 'a',
    'Î²': 'v',
    'Î': 'v',
    'Î³': 'g',
    'Î': 'g',
    'Î´': 'd',
    'Î': 'd',
    'Îµ': 'e',
    'Î­': 'e',
    'Î': 'e',
    'Î': 'e',
    'Î¶': 'z',
    'Î': 'z',
    'Î·': 'h',
    'Î®': 'h',
    'Î': 'h',
    'Î': 'h',
    'Î¸': 'th',
    'Î': 'th',
    'Î¹': 'I',
    'Î¯': 'I',
    'Î': 'I',
    'Î': 'I',
    'Îº': 'k',
    'Î': 'k',
    'Î»': 'l',
    'Î': 'l',
    'Î¼': 'm',
    'Î': 'm',
    'Î½': 'n',
    'Î': 'n',
    'Î¾': 'x',
    'Î': 'x',
    'Î¿': 'o',
    'Î': 'o',
    'Ï': 'o',
    'Î': 'o',
    'Ï': 'p',
    'Î ': 'p',
    'Ï': 'r',
    'Î¡': 'r',
    'Ï': 's',
    'Î£': 's',
    'Ï': 's',
    'Ï': 't',
    'Î¤': 't',
    'Ï': 'y',
    'Ï': 'y',
    'Î¥': 'y',
    'Î': 'y',
    'Ï': 'f',
    'Î¦': 'f',
    'Ï': 'ch',
    'Î§': 'ch',
    'Ï': 'ps',
    'Î¨': 'ps',
    'Ï': 'w',
    'Ï': 'w',
    'Î': 'w',
    'Î©': 'w',
    'Î±Î¹': 'ai',
    'Î±Î¯': 'ai',
    'Î¬Î¹': 'ai',
    'ÎÎ': 'ai',
    'ÎÎ': 'ai',
    'ÎÎ': 'ai',
    'Î¿Î¹': 'oi',
    'Î¿Î¯': 'oi',
    'ÏÎ¹': 'oi',
    'ÎÎ': 'oi',
    'ÎÎ': 'oi',
    'ÎÎ': 'oi',
    'ÎµÎ¹': 'ei',
    'Î­Î¹': 'ei',
    'ÎÎ': 'ei',
    'ÎÎ': 'ei',
    'ÎÎ': 'ei',
    'Î¿Ï': 'u',
    'ÏÏ': 'u',
    'Î¿Ï': 'u',
    'ÎÎ¥': 'u',
    'ÎÎ¥': 'u',
    'ÎÎ': 'u',
    'Î±Ï': 'av',
    'Î±Ï': 'av',
    'Î¬Ï': 'av',
    'ÎÎ¥': 'av',
    'ÎÎ': 'av',
    'ÎÎ¥': 'av',
    'ÎµÏ': 'ev',
    'ÎµÏ': 'ev',
    'Î­Ï': 'ev',
    'ÎÎ¥': 'ev',
    'ÎÎ': 'ev',
    'ÎÎ¥': 'ev',
    'Ï': 'i',
    'Ï': 'i'
};

function accent_fold(s) {
    if (!s) {
        return '';
    }
    var ret = '';
    for (var i = 0; i < s.length; i++) {
        ret += accent_map[s.charAt(i)] || s.charAt(i);
    }
    return ret;
}

Date.prototype.stdTimezoneOffset = function() {
    var jan = new Date(this.getFullYear(), 0, 1);
    var jul = new Date(this.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}

Date.prototype.dst = function() {
    return this.getTimezoneOffset() < this.stdTimezoneOffset();
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function attachLoggedInAccountTooltips(query_ids, account_username_url, account_id, add_fav_msg, add_watch_msg, remove_fav_msg, remove_watch_msg) {
    var favourite_ids = [];
    var watchlist_ids = [];
    var rated_ids = [];

    $.ajax({
        url: '/account/' + account_username_url + '/remote/account-list-check?account_id=' + account_id + '&ids=' + query_ids.join(),
        dataType: 'json'
    }).done(function(response) {

        for (var i = 0; i < response.favourite_ids.length; i++) {
            $('#favourite_' + response.favourite_ids[i]).toggleClass('selected');
            $('#favourite_' + response.favourite_ids[i] + '_value').html(remove_fav_msg)
        }
        for (var i = 0; i < response.watchlist_ids.length; i++) {
            $('#watchlist_' + response.watchlist_ids[i]).toggleClass('selected');
            $('#watchlist_' + response.watchlist_ids[i] + '_value').html(remove_watch_msg)
        }
        for (var i = 0; i < response.rated_ids.length; i++) {
            $('#rating_' + response.rated_ids[i]).toggleClass('selected');
        }
    });
}

function attachFavouriteActions(query_ids, account_username_url, add_fav_msg, remove_fav_msg) {
    var favourite_ids = [];
    $.ajax({
        url: '/u/' + account_username_url + '/remote/account-list-check',
        method: 'POST',
        dataType: 'json',
        data: {
            list_type: {
                account_list_item: ['favourite']
            },
            ids: query_ids
        }
    }).done(function(response) {
        for (var i = 0; i < response.favourite_ids.length; i++) {
            $('#favourite_' + response.favourite_ids[i]).toggleClass('selected');
            $('#favourite_' + response.favourite_ids[i] + '_value').html(remove_fav_msg)
        }
    });
}

var loggedInRatingTooltipLoaded = false;

function attachRatingTooltip(mobile) {
    if (loggedInRatingTooltipLoaded == false) {
        loggedInRatingTooltipLoaded = true;

        $("main").kendoTooltip({
            filter: 'a.list_item_rating',
            width: (mobile) ? 170 : 214,
            height: 40,
            position: 'right',
            callout: true,
            showOn: 'click',
            autoHide: false,
            content: {
                url: '/'
            },
            requestStart: function(e) {
                e.options.url = kendo.format('{0}', e.target.data("rating-url"));
            },
            show: function() {
                $("div.k-tooltip-button").addClass('hide');
                this.popup.element.addClass("tmdb_theme no_pad flex");
            }
        }).data("kendoTooltip");
    }
}

var loggedInRatingTooltipWithDateLoaded = false;

function attachRatingTooltipWithDate(mobile) {
    if (loggedInRatingTooltipWithDateLoaded == false) {
        loggedInRatingTooltipWithDateLoaded = true;

        $("main").kendoTooltip({
            filter: 'a.list_item_rating_with_date',
            width: (mobile) ? 170 : 214,
            height: (mobile) ? 60 : 70,
            position: 'right',
            callout: true,
            showOn: 'click',
            autoHide: false,
            content: {
                url: '/'
            },
            requestStart: function(e) {
                e.options.url = kendo.format('{0}', e.target.data("rating-url"));
            },
            show: function() {
                $("div.k-tooltip-button").addClass('hide');
                this.popup.element.addClass("tmdb_theme no_pad flex");
            }
        }).data("kendoTooltip");
    }
}

var loggedInListTooltipLoaded = false;

function attachListTooltip(mobile) {
    if (loggedInListTooltipLoaded == false) {
        loggedInListTooltipLoaded = true;

        $("main").kendoTooltip({
            filter: 'a.add_media_to_list',
            showOn: 'click',
            autoHide: false,
            content: {
                url: '/',
                cache: true,
                headers: {
                    'Accept-Language': '<%= choose_valid_i18n[0] %>',
                    'Content-Type': 'text/html;charset=utf-8'
                }
            },
            width: '300',
            position: (mobile ? 'bottom' : 'right'),
            requestStart: function(e) {
                e.options.url = kendo.format('{0}', e.target.attr("href"));
            },
            show: function(e) {
                $("div.k-tooltip-button").addClass('hide');
                this.popup.element.addClass("tmdb_theme_light");
                this.popup.element.addClass("min_100px_height");

                var loading_div = $('div.k-loading-mask');
                loading_div.html('<div class="loading_wrapper"><div class="ball-scale-multiple white"><div></div><div></div><div></div></div></div>')
            }
        });
    }
}

var loggedInTooltipActionsLoaded = false;

function enableLoggedInAccountTooltipActions(media_type, account_username_url, add_fav_msg, add_watch_msg, remove_fav_msg, remove_watch_msg) {
    if (loggedInTooltipActionsLoaded == false) {
        loggedInTooltipActionsLoaded = true;

        switch (media_type) {
            case null:
                var itemSelector = 'a.account_list_action, span.list_action';
                break;
            default:
                var itemSelector = 'span.list_action[data-media-type=' + media_type + ']';
        }

        $("body").on('click', itemSelector, function(e) {
            e.preventDefault();

            $.ajax({
                url: '/u/' + account_username_url + '/remote/toggle-list-item',
                type: 'PUT',
                data: {
                    media_type: $(this).attr('data-media-type'),
                    selected_object: this.id
                }
            }).fail(function() {
                showError('There was a problem marking this item.')
            }).done(function(response) {
                if (response.failure) {
                    showError('There was a problem marking this item.')
                }

                if (response.success) {
                    $('#' + response.list_type + '_' + response.media_id).toggleClass('selected');

                    if (response.action == 'created' && response.list_type == 'watchlist') {
                        $('#' + response.list_type + '_' + response.media_id + '_value').html(remove_watch_msg)
                    } else if (response.action == 'created' && response.list_type == 'favourite') {
                        $('#' + response.list_type + '_' + response.media_id + '_value').html(remove_fav_msg)
                    } else if (response.action == 'destroyed' && response.list_type == 'watchlist') {
                        $('#' + response.list_type + '_' + response.media_id + '_value').html(add_watch_msg)
                    } else if (response.action == 'destroyed' && response.list_type == 'favourite') {
                        $('#' + response.list_type + '_' + response.media_id + '_value').html(add_fav_msg)
                    }
                }
            });
        });
    }
}

function attachAccountListTooltips() {
    $("div.meta").kendoTooltip({
        filter: "span.favourite, span.watchlist",
        width: 200,
        position: "top",
        content: function(e) {
            return $('#' + e.target.attr('id') + '_value').html();
        }
    }).data("kendoTooltip");
}

function attachMovieListPopularityTooltip() {
    $("div.meta").kendoTooltip({
        filter: "span.popularity_rank",
        position: "top",
        width: 170,
        content: function(e) {
            return $('#' + e.target.attr('id') + '_value').html();
        },
        show: function(e) {
            $("div.k-tooltip-button").addClass('hide');
            this.popup.element.addClass("tmdb_theme");

            var loading_div = $('div.k-loading-mask');
            loading_div.html('<div class="loading_wrapper"><div class="ball-scale-multiple white"><div></div><div></div><div></div></div></div>')
        }
    });
}

function changeSearchTabs(media_type) {
    $('div.search_results').addClass('hide');
    $('div.' + media_type).removeClass('hide');
    $('section.search_results ul li a').removeClass('active');
    $('#' + media_type).addClass('active');
}

function attachViewStyleTooltips() {
    $("div.right").kendoTooltip({
        filter: "a.view_style",
        width: 'auto',
        position: "top"
    }).data("kendoTooltip");
}

function getImageWindowDimensions(inner_height, aspect_ratio) {
    var poster_width = 640;
    var window_width = (1000 - poster_width);
    var poster_height = Math.round(poster_width / aspect_ratio);

    if (poster_height > (inner_height - 100)) {
        var poster_height = (inner_height - 100);
        var poster_width = Math.round(poster_height * aspect_ratio);
    }

    return [window_width, poster_width, poster_height]
};
