//HTML elements
var $body           = document.body
var $progressPerc   = document.querySelector('#progressPerc')
var $numPeers       = document.querySelector('#numPeers')
var $uploadSpeed    = document.querySelector('#uploadSpeed')
var $downloadSpeed  = document.querySelector('#downloadSpeed')
var $downloadStatus = document.querySelector('#downloadStatus')

function prettyBytes(num) {
  var exponent, unit, neg = num < 0, units = ['b', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
  if (neg) num = -num
  if (num < 1) return (neg ? '-' : '') + num + ' b'
  exponent = Math.min(Math.floor(Math.log(num) / Math.log(1000)), units.length - 1)
  num = Number((num / Math.pow(1000, exponent)).toFixed(2))
  unit = units[exponent]
  return (neg ? '-' : '') + num + ' ' + unit
}

var client = new WebTorrent()
//console.log(document.querySelectorAll('video'))

Array.from(document.querySelectorAll('video')).forEach(elem =>
    client.add(elem.childNodes[1].src, function (torrent) {
        //torrents can contain many files. Let's use the .mp4 file
        var file = torrent.files.find(function (file) {
            //console.log(file)
            return file.name.endsWith('.mp4')
        })

        //replace video element
        file.renderTo(elem)

        //generate download link
        file.getBlobURL(function(err, url) {
            if (err) return console.log(err)
            $downloadStatus.innerHTML = '<a href="' +
                url + '" target="_blank">Descarga directa: ' + file.name + '</a>'
        })

        //trigger statistics refresh
        torrent.on('done', onDone)
        setInterval(onProgress, 500)
        onProgress()

        //statistics
        function onProgress () {
          $progressPerc.innerHTML = (torrent.progress * 100).toFixed(2) + " %"

          $numPeers.innerHTML = torrent.numPeers + (torrent.numPeers === 1 ? ' nodo' : ' nodos')

          $downloadSpeed.innerHTML = prettyBytes(torrent.downloadSpeed) + '/s'
          $uploadSpeed.innerHTML   = prettyBytes(torrent.uploadSpeed) + '/s'
        }

        function onDone () {
          $body.className += ' is-seed'
          onProgress()
        }
    }
  )
)
