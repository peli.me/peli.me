from app.encode   import b64_encode, b64_decode
from urllib.parse import urlparse
import zlib, urllib, re

def encode_uri(uri):
    if not uri:
        return "404"

    encoded_uri = b64_encode(zlib.compress(uri.encode(),9))
    encoded_uri = urllib.parse.quote_plus(encoded_uri.decode())
    return encoded_uri

def decode_uri(encoded_uri):
    if re.search("^magnet:", encoded_uri) or re.search("^http(s)?://", encoded_uri):
        uri = encoded_uri
    else:
        try:
            encoded_uri = urllib.parse.unquote_plus(encoded_uri)
            uri         = zlib.decompress(b64_decode(encoded_uri)).decode()
        except:
            return None
    return uri

def sanitazer_uri(dirty_uri):
    #in  => https://www.themoviedb.org/movie/299536-avengers-infinity-war?language=es_MX
    #out => https://www.themoviedb.org/movie/299536-avengers-infinity-war
    uri = re.sub('\?.*$', '', dirty_uri)
    return uri

def parse_provider(uri):
    if uri.startswith('magnet:?'):
        return 'MAGNET'
    else:
        parsed_uri = urlparse(uri)
        provider = '{uri.netloc}'.format(uri=parsed_uri).split('.')[-2]
        return provider.upper()
