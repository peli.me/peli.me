from flask_wtf import FlaskForm, RecaptchaField
from wtforms   import RadioField, StringField, IntegerField, FloatField
from wtforms   import PasswordField, BooleanField, SubmitField
from wtforms   import SelectField, DateField, TextAreaField
from wtforms.validators import InputRequired, Email, Length, EqualTo, NumberRange, Regexp

from config    import Config

class LoginForm(FlaskForm):
    email       = StringField('Email',
                  # render_kw={"placeholder": "correo electrónico"},
                  validators=[
                      InputRequired(message="campo requerido"),
                      Email(message='correo inválido'),
                  ])

    password    = PasswordField('Contraseña',
                  # render_kw={"placeholder": "contraseña"},
                  validators=[InputRequired(message="campo requerido")])

    submit      = SubmitField('Iniciar Sesion')

class RegisterForm(FlaskForm):
    username   = StringField('Nombre de usuario',
                 validators=[
                     InputRequired(message="campo requerido"),
                     Length(max=128, message="el usuario no puede tener más de %(max) caracteres")
                 ])

    email      = StringField('Email',
                 # render_kw={"placeholder": "Correo electrónico"},
                 validators=[
                     InputRequired(message="campo requerido"),
                     Email(message='correo inválido'),
                     Length(max=128, message="el correo no puede tener más de %(max) caracteres"),
                 ])

    password   = PasswordField('Contraseña (4 caracteres mínimo)',
                 # render_kw={"placeholder": "Contraseña"},
                 validators=[
                     InputRequired(message="campo requerido"),
                     Length(max=1024, message="la contraseña debe ser entre 8 y %(max) caracteres"),
                     EqualTo('confirm', message='las contraseñas no coinciden'),
                 ])

    confirm    = PasswordField('Confirmar Contraseña',
                 # render_kw={"placeholder": "Confirmar contraseña"},
                 validators=[
                     InputRequired(message="campo requerido"),
                     Length(max=500, message="la contraseña debe ser entre 8 y %(max) caracteres"),
               ])

    # accept_tos = BooleanField('Términos y condiciones',
                 # validators=[
                     # InputRequired(message="campo requerido"),
                 # ],
                 # default=True,
                 # )

    captcha    = RecaptchaField()

    submit     = SubmitField('Registrarse')

class PasswordRecoverForm(FlaskForm):
    email  = StringField('Correo',
             validators=[
                 InputRequired(message="campo requerido"),
                 Email(message='correo inválido'),
             ])
    captcha = RecaptchaField()
    submit = SubmitField('Recuperar cuenta')

class PasswordResetForm(FlaskForm):
    password = PasswordField('Nueva Contraseña',
               # render_kw={"placeholder": "Nueva contraseña"},
               validators=[
                   InputRequired(message="campo requerido"),
                   Length(max=1024, message="la contraseña debe ser entre 8 y %(max) caracteres"),
                   EqualTo('confirm', message='las contraseñas no coinciden'),
               ])
    confirm  = PasswordField('Repetir contraseña',
               # render_kw={"placeholder": "Repetir la nueva contraseña"},
               validators=[
                   InputRequired(message="campo requerido"),
                   Length(max=1024, message="la contraseña debe ser entre 8 y %(max) caracteres"),
               ])
    submit   = SubmitField('Guardar')


class EditUserForm(FlaskForm):
    username    = StringField('Usuario', validators=[
                     InputRequired(message="campo requerido"),
                 ])

    email       = StringField('Correo', validators=[
                     InputRequired(message="campo requerido"),
                     Email(message='correo inválido'),
                 ])

    password   = PasswordField('Nueva contraseña',
                 validators=[
                     Length(max=1024, message="la contraseña debe ser entre 8 y %(max) caracteres"),
                     EqualTo('confirm', message='las contraseñas no coinciden'),
                 ])

    confirm    = PasswordField('Repetir contraseña',
                 validators=[
                     Length(max=1024, message="la contraseña debe ser entre 8 y %(max) caracteres"),
                 ])

    about_me    = StringField('Sobre mi',
                 validators=[
                     Length(max=1024, message="el campo no puede tener más de %(max) caracteres"),
                 ])

    phrase    = StringField('Frase',
                 validators=[
                     Length(max=1024, message="el campo no puede tener más de %(max) caracteres"),
                 ])

    submit     = SubmitField('Guardar')

class MovieForm(FlaskForm):
    moviedb_uri = StringField('URL de los metadatos desde <a href="https://www.themoviedb.org" target="_blank">https://www.themoviedb.org</a>',
                   validators=[
                       InputRequired(message="campo requerido"),
                       Length(max=1024, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^https://www.themoviedb.org/(movie|tv)/[0-9]+',
                       message='URL inválida, debe ser https://www.themoviedb.org/movie/[id]'),
                   ])

    uri = StringField('URL de la pelí',
                   validators=[
                       InputRequired(message="campo requerido"),
                       Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^[http://|https://|magnet:]',
                              message='URL inválida, sólo se permiten los protocolos "http", "https" y "magnet"'),
                   ])

    download_uri = StringField('URL interna de la pelí',
                   validators=[
                       Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^[http://|https://|magnet:]',
                              message='URL inválida, sólo se permiten los protocolos "http", "https" y "magnet"'),
                   ])

    quality = SelectField("Calidad",
               choices=[
                   ('FULL_HD', 'FULL_HD'),
                   ('HD',      'HD'),
                   ('SD',      'SD'),
                   ('CAMARA',  'CAMARA'),
               ],
               default='HD',
               validators=[
                    InputRequired(message='campo requerido'),
                ])

    audio = SelectField("Audio",
               choices=[
                   ('LATAM', 'LATAM'),
                   ('ES',    'ES'),
                   ('EN',    'EN'),
               ],
               validators=[
                    InputRequired(message='campo requerido'),
                ])

    subtitle = SelectField("Subtitulos",
               choices=[
                   ('NINGUNO', 'NINGUNO'),
                   ('ES',    'ES'),
                   ('EN',    'EN'),
               ],
               validators=[
                    InputRequired(message='campo requerido'),
                ])

    submit = SubmitField('Guardar')

class SerieForm(FlaskForm):
    moviedb_uri = StringField('URL de los metadatos desde <a href="https://www.themoviedb.org" target="_blank">https://www.themoviedb.org</a>',
                   validators=[
                       InputRequired(message="campo requerido"),
                       Length(max=1024, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^https://www.themoviedb.org/(movie|tv)/[0-9]+',
                       message='URL inválida, debe ser https://www.themoviedb.org/tv/[id]'),
                   ])

    season = IntegerField('Temporada (1-20)',
                   validators=[
                       InputRequired(message="campo requerido"),
                       NumberRange(min=0, max=50, message="el capítulo debe ser un número entre %(min)d y %(max)d"),
                   ])

    chapter = IntegerField('Capítulo (1-20)',
                   validators=[
                       InputRequired(message="campo requerido"),
                       NumberRange(min=1, max=1000, message="el capítulo debe ser un número entre %(min)d y %(max)d"),
                   ])

    uri = StringField('URL de la serie',
                   validators=[
                       InputRequired(message="campo requerido"),
                       Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^[http://|https://|magnet:]',
                              message='URL inválido, sólo se permiten los protocolos "http", "https" y "magnet"'),
                   ])

    download_uri = StringField('URL interna de la serie',
                   validators=[
                       Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^[http://|https://|magnet:]',
                              message='URL inválido, sólo se permiten los protocolos "http", "https" y "magnet"'),
                   ])

    quality = SelectField("Calidad",
               choices=[
                   ('FULL_HD', 'FULL_HD'),
                   ('HD',      'HD'),
                   ('SD',      'SD'),
                   ('CAMARA',  'CAMARA'),
               ],
               default='HD',
               validators=[
                    InputRequired(message='campo requerido'),
                ])

    audio = SelectField("Audio",
               choices=[
                   ('LATAM', 'LATAM'),
                   ('ES',    'ES'),
                   ('EN',    'EN'),
               ],
               validators=[
                    InputRequired(message='campo requerido'),
                ])

    subtitle = SelectField("Subtitulos",
               choices=[
                   ('NINGUNO', 'NINGUNO'),
                   ('ES',    'ES'),
                   ('EN',    'EN'),
               ],
               validators=[
                    InputRequired(message='campo requerido'),
                ])

    submit = SubmitField('Guardar')

class AnimeForm(FlaskForm):
    moviedb_uri = StringField('URL de los metadatos desde <a href="https://www.themoviedb.org" target="_blank">https://www.themoviedb.org</a>',
                   validators=[
                       InputRequired(message="campo requerido"),
                       Length(max=1024, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^https://www.themoviedb.org/(movie|tv)/[0-9]+',
                       message='URL inválida, debe ser https://www.themoviedb.org/tv/[id]'),
                   ])

    season = IntegerField('Temporada (1-20)',
                   validators=[
                       InputRequired(message="campo requerido"),
                       NumberRange(min=0, max=50, message="el capítulo debe ser un número entre %(min)d y %(max)d"),
                   ])

    chapter = IntegerField('Capítulo (1-20)',
                   validators=[
                       InputRequired(message="campo requerido"),
                       NumberRange(min=1, max=1000, message="el capítulo debe ser un número entre %(min)d y %(max)d"),
                   ])

    uri = StringField('URL del anime',
                   validators=[
                       InputRequired(message="campo requerido"),
                       Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^[http://|https://|magnet:]',
                              message='URL inválido, sólo se permiten los protocolos "http", "https" y "magnet"'),
                   ])

    download_uri = StringField('URL interna del anime',
                   validators=[
                       Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^[http://|https://|magnet:]',
                              message='URL inválido, sólo se permiten los protocolos "http", "https" y "magnet"'),
                   ])

    quality = SelectField("Calidad",
               choices=[
                   ('FULL_HD', 'FULL_HD'),
                   ('HD',      'HD'),
                   ('SD',      'SD'),
                   ('CAMARA',  'CAMARA'),
               ],
               default='HD',
               validators=[
                    InputRequired(message='campo requerido'),
                ])

    audio = SelectField("Audio",
               choices=[
                   ('LATAM', 'LATAM'),
                   ('ES',    'ES'),
                   ('EN',    'EN'),
               ],
               validators=[
                    InputRequired(message='campo requerido'),
                ])

    subtitle = SelectField("Subtitulos",
               choices=[
                   ('NINGUNO', 'NINGUNO'),
                   ('ES',    'ES'),
                   ('EN',    'EN'),
               ],
               validators=[
                    InputRequired(message='campo requerido'),
                ])

    submit = SubmitField('Guardar')

class DocumentalForm(FlaskForm):
    moviedb_uri = StringField('URL de los metadatos desde <a href="https://www.themoviedb.org" target="_blank">https://www.themoviedb.org</a>',
                   validators=[
                       InputRequired(message="campo requerido"),
                       Length(max=1024, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^https://www.themoviedb.org/(movie|tv)/[0-9]+',
                       message='URL inválida, debe ser https://www.themoviedb.org/tv/[id]'),
                   ])

    season = IntegerField('Temporada (1-20)',
                   validators=[
                       InputRequired(message="campo requerido"),
                       NumberRange(min=0, max=50, message="el capítulo debe ser un número entre %(min)d y %(max)d"),
                   ])

    chapter = IntegerField('Capítulo (1-20)',
                   validators=[
                       InputRequired(message="campo requerido"),
                       NumberRange(min=1, max=1000, message="el capítulo debe ser un número entre %(min)d y %(max)d"),
                   ])

    uri = StringField('URL del documental',
                   validators=[
                       InputRequired(message="campo requerido"),
                       Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^[http://|https://|magnet:]',
                              message='URL inválido, sólo se permiten los protocolos "http", "https" y "magnet"'),
                   ])

    download_uri = StringField('URL interna del documental',
                   validators=[
                       Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^[http://|https://|magnet:]',
                              message='URL inválido, sólo se permiten los protocolos "http", "https" y "magnet"'),
                   ])

    quality = SelectField("Calidad",
               choices=[
                   ('FULL_HD', 'FULL_HD'),
                   ('HD',      'HD'),
                   ('SD',      'SD'),
                   ('CAMARA',  'CAMARA'),
               ],
               default='HD',
               validators=[
                    InputRequired(message='campo requerido'),
                ])

    audio = SelectField("Audio",
               choices=[
                   ('LATAM', 'LATAM'),
                   ('ES',    'ES'),
                   ('EN',    'EN'),
               ],
               validators=[
                    InputRequired(message='campo requerido'),
                ])

    subtitle = SelectField("Subtitulos",
               choices=[
                   ('NINGUNO', 'NINGUNO'),
                   ('ES',    'ES'),
                   ('EN',    'EN'),
               ],
               validators=[
                    InputRequired(message='campo requerido'),
                ])

    submit = SubmitField('Guardar')

class MetaDataForm(FlaskForm):
    title = StringField('Título',
                   validators=[
                       InputRequired(message="campo requerido"),
                       Length(max=1024, message="el título no puede ser mayor a %(max) caracteres"),
                   ])

    description = TextAreaField('Descripción',
           render_kw={'style': 'height:240px'},
           validators=[
                 InputRequired(message="campo requerido"),
                 Length(max=20000, message="el campo no puede contener más de 100 líneas"),
           ])

    poster_uri  = StringField('URL de la imagen principal (500x281 px)',
                   validators=[
                       InputRequired(message="campo requerido"),
                       Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^[http://|https://]',
                              message='URL inválida, sólo se permiten los protocolos "http" y "https"'),
                   ])

    fposter_uri = StringField('URL de la imagen secundaria (300x450 px)',
                   validators=[
                       InputRequired(message="campo requerido"),
                       Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^[http://|https://]',
                              message='URL inválida, sólo se permiten los protocolos "http" y "https"'),
                   ])

    bposter_uri = StringField('URL de la imagen de fondo (1400x450 px)',
                   validators=[
                       InputRequired(message="campo requerido"),
                       Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^[http://|https://]',
                              message='URL inválida, sólo se permiten los protocolos "http" y "https"'),
                   ])

    lposter_uri = StringField('URL de la imagen miniatura (150x225 px)',
                   validators=[
                       InputRequired(message="campo requerido"),
                       Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
                       Regexp(regex=r'^[http://|https://]',
                              message='URL inválida, sólo se permiten los protocolos "http" y "https"'),
                   ])

    score  = FloatField('Score',
                   validators=[
                       InputRequired(message="campo requerido"),
                       NumberRange(min=0, max=100, message='El valor debe estar entre 0 y 100'),
                   ])

    counter  = FloatField('Counter',
                   validators=[
                       InputRequired(message="campo requerido"),
                   ])

    submit = SubmitField('Guardar')

class MovieBulkForm(FlaskForm):
    placeholder = "URL_METADATOS URL_PELI CALIDAD AUDIO SUBTITULOS"
    bulk = TextAreaField('Una entrada por línea, MAX {} líneas<br><br>Formato: {}'.format(
                Config.APP_MAX_UPLOADS_BULK, placeholder),
           render_kw={'style': 'height:240px'},
           validators=[
                 InputRequired(message="campo requerido"),
                 Length(max=20000, message="el campo no puede contener mas de 100 líneas"),
           ])

    submit = SubmitField('Guardar')

class SerieBulkForm(FlaskForm):
    placeholder = "URL_METADATOS TEMPORADA CAPITULO URL_CONTENIDO CALIDAD AUDIO SUBTITULOS"
    bulk = TextAreaField('Una entrada por línea, MAX {} líneas<br><br>Formato: {}'.format(
                Config.APP_MAX_UPLOADS_BULK, placeholder),
           render_kw={'style': 'height:240px'},
           validators=[
               InputRequired(message="campo requerido"),
               Length(max=20000, message="el campo no puede contener mas de 100 líneas"),
           ])

    submit = SubmitField('Guardar')

class DirectViewForm(FlaskForm):
    uri = StringField('URL',
          validators=[
              InputRequired(message="campo requerido"),
              Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
              Regexp(regex=r'^(magnet:\?|http(s)?://)',
                     message='URL inválida, solo links "magnet:" y "https://" son válidos"'),
          ])

    submit = SubmitField('Ver')

class DirectDownloadForm(FlaskForm):
    uri = StringField('WEBRTC MAGNET URL',
          validators=[
              InputRequired(message="campo requerido"),
              Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
              Regexp(regex=r'^(magnet:\?|http(s)?://)',
                     message='URL inválida, solo links "magnet:" y "https://" son válidos"'),
          ])

    submit = SubmitField('Descargar')

class DcmaForm(FlaskForm):
    kind = SelectField("Soy",
          choices=[
              ('PROPIETARIO', 'PROPIETARIO'),
              ('AGENTE',      'AGENTE'),
          ],
          validators=[
               InputRequired(message='campo requerido'),
          ])

    name = StringField('Su nombre completo',
          validators=[
              InputRequired(message="campo requerido"),
              Length(max=2048, message="el nombre no puede ser mayor a %(max) caracteres"),
          ])

    email = StringField('Correo de contacto',
          validators=[
              InputRequired(message="campo requerido"),
              Email(message='correo inválido'),
          ])

    phone_number = StringField('Teléfono de contacto',
          validators=[
              InputRequired(message="campo requerido"),
              Length(max=2048, message="el nombre no puede ser mayor a %(max) caracteres"),
          ])

    organization = StringField('Organización',
          validators=[
              InputRequired(message="campo requerido"),
              Length(max=2048, message="el campo no puede tener más de %(max) caracteres"),
          ])

    position = StringField('Su posición en la organización representada',
          validators=[
              InputRequired(message="campo requerido"),
              Length(max=2048, message="el campo no puede tener más de %(max) caracteres"),
          ])

    address = StringField('Dirección de la organización representada',
          validators=[
              InputRequired(message="campo requerido"),
              Length(max=2048, message="el campo no puede tener más de %(max) caracteres"),
          ])

    zipcode = StringField('Código postal de la organización representada',
          validators=[
              InputRequired(message="campo requerido"),
              Length(max=2048, message="el campo no puede tener más de %(max) caracteres"),
          ])

    rightholder_name = StringField('Nombre legal del títular de los derechos',
          validators=[
              InputRequired(message="campo requerido"),
              Length(max=2048, message="el nombre no puede ser mayor a %(max) caracteres"),
          ])

    rightholder_country = StringField('País del títular de los derechos',
          validators=[
              InputRequired(message="campo requerido"),
              Length(max=2048, message="el nombre no puede ser mayor a %(max) caracteres"),
          ])

    local_uri = StringField('URL local donde se encontró contenido infractor',
          validators=[
              InputRequired(message="campo requerido"),
              Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
              Regexp(regex=r'^(http(s)?://[peli.me|localhost])',
                     message='URL inválida, solo links hospedados localmente son válidos"'),
          ])

    remote_uri = StringField('URL externa con contenido infractor',
          validators=[
              InputRequired(message="campo requerido"),
              Length(max=2048, message="la URL no puede ser mayor a %(max) caracteres"),
              Regexp(regex=r'^(http(s)?://)',
                     message='URL inválida, solo links "https://" son válidos"'),
          ])

    captcha = RecaptchaField()

    submit = SubmitField('Enviar')
