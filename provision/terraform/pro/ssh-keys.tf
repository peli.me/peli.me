resource "digitalocean_ssh_key" "peli-me-key" {
  name       = "ssh key"
  public_key = "${file(var.public_key)}"
}
