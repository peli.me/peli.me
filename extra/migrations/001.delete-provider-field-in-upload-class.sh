#!/bin/sh
set -e #exit on error

mongodb_id="$(docker ps | awk '/mongo-auth/{print $1;exit;}')"
mongo="docker exec -i ${mongodb_id} mongo app -u app -p app"

$mongo --eval 'db.upload.update({}, {$unset: {provider:1}} , {multi: true});'
